import unittest

import test_consts
import config
import bot_dummy

import questions.questions_entry as questions_entry
import form_complain


place_name = test_consts.test_place1
first_one_one_answer_five_after_court = "Во время содержания в {} было примерно 5 дней, " \
                                        "когда меня не вывели на прогулку. Исходя из самого названия главы 13 " \
                                        "Правил № 313, прогулки должны быть ежедневными.".format(place_name)

first_one_two_answer_six_three_after_court = "Во время содержания в {} меня вывели на прогулку примерно 6 раз. " \
                                             "Из всех прогулок продолжительность 3 " \
                                             "была менее одного часа.".format(place_name)


after_court_rules = "Согласно пункту 1 правила 23 Правил Нельсона Манделы каждый имеет ежедневно право по " \
                    "крайней мере на час подходящих физических упражнений во дворе, если это позволяет погода."


class TestWalk(unittest.TestCase):
    def test_walk1(self):
        is_after_court = True
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.WALK.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answers = [
            test_consts.BotCall(2, chat_id),  # 11.3.1.1.	Были ли дни, когда вас не выводили
            test_consts.BotCall(1, chat_id),  # 11.3.1.2.	Когда вас выводили на прогулку, она
        ]

        bot_dummy.answer_questions(bot, answers)
        personal_info = bot.bot_info.personal_info
        true_complains_list = []

        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        # print (complains_list)
        self.assertEqual(complains_list, true_complains_list)

    def test_walk2(self):
        is_after_court = True
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.WALK.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answers = [
            test_consts.BotCall(3, chat_id),  # 11.3.1.1.	Были ли дни, когда вас не выводили
        ]

        bot_dummy.answer_questions(bot, answers)

        personal_info = bot.bot_info.personal_info
        true_complains_list = [[
            "Во время содержания в {} меня ни разу не вывели на прогулку. Исходя из самого названия главы "
            "13 Правил № 313, прогулки должны быть ежедневными.".format(place_name),
        ]]

        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        self.assertEqual(complains_list, true_complains_list)
        self.assertEqual(type(bot.bot_info.get_state(chat_id)), form_complain.StateFormComplain)

    def test_walk3(self):
        is_after_court = True
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.WALK.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answers = [test_consts.BotCall(1, chat_id),  # 1
                   test_consts.BotMessage(5, chat_id),  # 1.11
                   test_consts.BotCall(2, chat_id),  # 1.2
                   test_consts.BotMessage(6, chat_id),  # 1.211
                   test_consts.BotMessage(3, chat_id),  # 1.212
                   ]

        bot_dummy.answer_questions(bot, answers)

        personal_info = bot.bot_info.personal_info
        true_complains_list = [[first_one_one_answer_five_after_court,
                               first_one_two_answer_six_three_after_court,
                               after_court_rules
                               ]]

        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        self.assertEqual(complains_list, true_complains_list)

    def test_walk4(self):
        is_after_court = False
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.WALK.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answers = [
            test_consts.BotCall(2, chat_id),  # 11.3.1.1.	Были ли дни, когда вас не выводили
            test_consts.BotCall(1, chat_id),  # 11.3.1.2.	Когда вас выводили на прогулку, она
        ]

        bot_dummy.answer_questions(bot, answers)
        personal_info = bot.bot_info.personal_info
        true_complains_list = []

        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        # print (complains_list)
        self.assertEqual(complains_list, true_complains_list)

    def test_walk5(self):
        is_after_court = False
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.WALK.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answers = [
            test_consts.BotCall(3, chat_id),  # 11.3.1.1.	Были ли дни, когда вас не выводили
        ]

        bot_dummy.answer_questions(bot, answers)

        personal_info = bot.bot_info.personal_info
        true_complains_list = [[
            "Во время содержания в {} меня ни разу не вывели на прогулку. "
            "Это нарушает пункт 101 Правил № 996.".format(place_name),
        ]]

        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        self.assertEqual(complains_list, true_complains_list)
        self.assertEqual(type(bot.bot_info.get_state(chat_id)), form_complain.StateFormComplain)

    def test_walk6(self):
        is_after_court = False
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.WALK.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answers = [test_consts.BotCall(1, chat_id),  # 1
                   test_consts.BotMessage(3, chat_id),  # 1.11
                   test_consts.BotCall(2, chat_id),  # 1.2
                   test_consts.BotMessage(1, chat_id),  # 1.211
                   ]

        bot_dummy.answer_questions(bot, answers)

        personal_info = bot.bot_info.personal_info
        true_complains_list = [["Во время содержания в {} было примерно 3 дня, "
                               "когда меня не вывели на прогулку. Это нарушает пункт 101 "
                               "Правил № 996.".format(place_name),
                               "Во время содержания в {} меня вывели на прогулку один раз. Эта прогулка длилась "
                               "менее двух часов, что нарушает пункт 101 Правил № 996. ".format(place_name),
                            ]]

        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        # print(complains_list)
        self.assertEqual(complains_list, true_complains_list)

    def test_walk7(self):
        is_after_court = False
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.WALK.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answers = [test_consts.BotCall(1, chat_id),  # 1
                   test_consts.BotMessage(3, chat_id),  # 1.11
                   test_consts.BotCall(2, chat_id),  # 1.2
                   test_consts.BotMessage(5, chat_id),  # 1.211
                   test_consts.BotMessage(2, chat_id),  # 1.211
                   ]

        bot_dummy.answer_questions(bot, answers)

        personal_info = bot.bot_info.personal_info
        true_complains_list = [["Во время содержания в {} было примерно 3 дня, "
                               "когда меня не вывели на прогулку. Это нарушает пункт 101 "
                               "Правил № 996.".format(place_name),
                               "Во время содержания в {} меня вывели на прогулку примерно "
                               "5 раз. Из всех прогулок продолжительность"
                               " 2 была менее двух часов, "
                               "что нарушает пункт 101 Правил № 996.".format(place_name),
                            ]]

        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        # print(complains_list)
        self.assertEqual(complains_list, true_complains_list)

    #до и после суда
    def test_walk8(self):
        is_after_court = 2
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.WALK.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answers = [
            test_consts.BotCall(2, chat_id),  # 11.3.1.1.	Были ли дни, когда вас не выводили
            test_consts.BotCall(1, chat_id),  # 11.3.1.2.	Когда вас выводили на прогулку, она
        ]

        bot_dummy.answer_questions(bot, answers)
        personal_info = bot.bot_info.personal_info
        true_complains_list = []

        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        self.assertEqual(complains_list, true_complains_list)

    def test_walk9(self):
        is_after_court = 2
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.WALK.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answers = [
            test_consts.BotCall(3, chat_id),  # 11.3.1.1.	Были ли дни, когда вас не выводили
        ]

        bot_dummy.answer_questions(bot, answers)

        personal_info = bot.bot_info.personal_info
        true_complains_list = [[
            "Во время содержания в {} меня ни разу не вывели на прогулку. Исходя из самого названия главы "
            "13 Правил № 313 и пункта 101 Правил № 996, прогулки должны быть ежедневными.".format(place_name),
        ]]

        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        self.assertEqual(complains_list, true_complains_list)
        self.assertEqual(type(bot.bot_info.get_state(chat_id)), form_complain.StateFormComplain)


    def test_walk10(self):
        is_after_court = 2
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.WALK.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answers = [test_consts.BotCall(1, chat_id),  # 1
                   test_consts.BotMessage(3, chat_id),  # 1.11
                   test_consts.BotCall(2, chat_id),  # 1.2
                   test_consts.BotMessage(1, chat_id),  # 1.211

                   test_consts.BotCall(2, chat_id),  # 1.3
                   test_consts.BotMessage(1, chat_id),  # 1.311
                   ]

        bot_dummy.answer_questions(bot, answers)

        personal_info = bot.bot_info.personal_info
        true_complains_list = [["Во время содержания в {} было примерно 3 дня, "
                               "когда меня не вывели на прогулку. Исходя из самого названия главы 13 Правил № 313"
                               " и пункта 101 Правил № 996, прогулки должны быть ежедневными.".format(place_name),
                               "Во время содержания в {} до суда меня вывели на прогулку один раз. "
                               "Эта прогулка длилась менее двух часов, "
                               "что нарушает пункт 101 Правил № 996. ".format(place_name),
                               "Во время содержания в {} после суда меня вывели на прогулку один раз. "
                               "Эта прогулка длилась менее часа.".format(place_name),
                               ]]

        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        self.assertEqual(complains_list, true_complains_list)

    def test_walk11(self):
        is_after_court = 2
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.WALK.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answers = [test_consts.BotCall(1, chat_id),  # 1
                   test_consts.BotMessage(6, chat_id),  # 1.11
                   test_consts.BotCall(2, chat_id),  # 1.2
                   test_consts.BotMessage(2, chat_id),  # 1.211
                   test_consts.BotMessage(1, chat_id),  # 1.211

                   test_consts.BotCall(2, chat_id),  # 1.3
                   test_consts.BotMessage(4, chat_id),  # 1.311
                   test_consts.BotMessage(3, chat_id),  # 1.311
                   ]

        bot_dummy.answer_questions(bot, answers)

        personal_info = bot.bot_info.personal_info
        true_complains_list = [["Во время содержания в {} было примерно 6 дней, "
                               "когда меня не вывели на прогулку. Исходя из самого названия главы 13 Правил № "
                               "313 и пункта 101 Правил № 996, прогулки должны быть ежедневными.".format(place_name),
                               "Во время содержания в {} до суда меня вывели на прогулку примерно "
                               "2 раза. Из всех прогулок продолжительность"
                               " 1 была менее двух часов, "
                               "что нарушает пункт 101 Правил № 996.".format(place_name),
                               "Во время содержания в {} после суда меня вывели на прогулку примерно"
                               " 4 раза. Из всех прогулок продолжительность 3 была менее "
                               "одного часа.".format(place_name),
                            ]]

        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        self.assertEqual(complains_list, true_complains_list)
