import unittest


import test_consts
import bot_dummy
import config

import questions.questions_entry as questions_entry
import questions.question_groups.abstract_group as abstract_group
import states.ask_question as ask_question


class TestBed(unittest.TestCase):

    def test_bed1(self):
        is_after_court = True
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.BED.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answers = [
            test_consts.BotCall(10, chat_id),  # 1
            test_consts.BotCall("NextQ", chat_id),  # 1
            test_consts.BotCall(2, chat_id),  # 1
        ]
        bot_dummy.answer_questions(bot, answers)
        personal_info = bot.bot_info.personal_info
        true_complains_list = [["Выданные постельные принадлежности и белье были грязными. Из-за этого "
                               "пользоваться ими было негигиенично и небезопасно для здоровья.",
                               ]]
        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        self.assertEqual(complains_list, true_complains_list)

    def test_bed2(self):
        is_after_court = 2
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.BED.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answers = [
            test_consts.BotCall(3, chat_id),  # 1.8.1.3.	Наволочка
            test_consts.BotCall(4, chat_id),  # 1.8.1.4.	Подушка ватная
            test_consts.BotCall("NextQ", chat_id),  # 1
            test_consts.BotMessage(2, chat_id),  # 1.8.1.4.1.	Сколько ночей вы спали
            test_consts.BotCall(1, chat_id),  # 1.8.1.4.2.	Отбирали ли у вас днем наматрасник?

            test_consts.BotMessage(2, chat_id),  # 1.8.1.5.1.	Сколько ночей вы спали
            test_consts.BotCall(1, chat_id),  # 1.8.2.	Постельные принадлежности и белье были
        ]

        bot_dummy.answer_questions(bot, answers)

        true_complains_list = [[
            "Во время нахождения в {} меня не обеспечили постельными принадлежностями и постельным "
            "бельем по Нормам обеспечения (Приложение 3 к Правилам № 996 и Приложение 3 к "
            "Правилам № 313).".format(test_consts.test_place1),
            "2 ночи мне пришлось спать без тюфячной наволочки, поскольку её не выдавали. Кроме того, днем её "
            "у меня отбирали. Из-за этого днем у меня не было возможности удобно и гигиенично сидеть на кровати.",
            "2 ночи мне пришлось спать без подушки, поскольку её не выдавали."
        ]]

        questions = bot.bot_info.personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        self.assertEqual(complains_list, true_complains_list)


