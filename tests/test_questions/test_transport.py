import unittest


import test_consts
import bot_dummy
import config

import questions.questions_entry as questions_entry
import questions.question_groups.abstract_group as abstract_group
import states.ask_question as ask_question


class TestTransport(unittest.TestCase):

    def test_transport1(self):
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.TRANSPORT.value]
        selected_place_type = config.ComplainPlaceType.SPlaceTransport
        bot = bot_dummy.get_question_bot(selected_groups, None, selected_place_type)

        answers = [
            test_consts.BotCall(1, chat_id),  # 15.	Было ли вам тесно во время транспортировки?
            test_consts.BotMessage(2, chat_id),  # 15.1.1.	Какая примерно была длина камеры, в которой вас перевозили
            test_consts.BotMessage(2, chat_id),  # 15.1.2.	Какая примерно была ширина камеры,
            test_consts.BotMessage(4, chat_id),  # 15.1.3.	Сколько людей было с вами в этой камере
            test_consts.BotCall(1, chat_id),  # 15.1.4.	Сковывали ли вас наручниками с другими
            test_consts.BotMessage(3, chat_id),  # 15.1.4.1.1.	Сколько человек было в связке с вами
            test_consts.BotMessage("1 час 15 минут", chat_id),  # 15.1.4.1.2.	Примерно как долго вы были скованы
            test_consts.BotCall(1, chat_id),  # 16.	Было ли вам холодно во время нахождения
            test_consts.BotMessage("1 час 45 минут", chat_id), # 16.1.1.Примерно как долго вас держали в машине
            test_consts.BotCall(2, chat_id),  # 17.	Хватало ли вам свежего воздуха при транспортировке?
        ]

        bot_dummy.answer_questions(bot, answers)

        personal_info = bot.bot_info.personal_info
        true_complains_list = [[
            "Во время транспортировки мне было очень тесно. Площадь отдела автомобиля, в котором меня перевозили,"
            " составляла примерно 4 кв.м. При этом там находилось 4 человека, включая меня. "
            "Т.е. на одного человека приходилось 1.0 кв.м. Кроме того, я и другие задержанные"
            " были все время скованы в одну цепь. В связке со мной было 3 человека. Мы были скованы примерно"
            " 1 час 15 минут. Из-за этого у меня отсутствовало личное пространство и возможность двигать руками"
            " по своему усмотрению.",
            "При транспортировке мне было холодно. В холодной машине меня держали примерно 1 час 45 минут. При этом "
            "сотрудники, осуществлявшие конвоирование, не могли не осознавать неадекватный температурный режим и то, "
            "что подобное обращение является жестоким и бесчеловечным.",
            "Мне не хватало свежего воздуха. Это вызывало значительный дискомфорт. У меня не было возможности "
            "элементарно вдохнуть воздух полной грудью. Такие условия транспортировки являются негуманными.",
        ]]

        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        self.assertEqual(complains_list, true_complains_list)

    def test_transport2(self):
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.TRANSPORT.value]
        selected_place_type = config.ComplainPlaceType.SPlaceTransport
        bot = bot_dummy.get_question_bot(selected_groups, None, selected_place_type)

        answers = [
            test_consts.BotCall(2, chat_id),  # 15.	Было ли вам тесно во время транспортировки?
            test_consts.BotCall(1, chat_id),  # 15.2.1.	Сковывали ли вас наручниками с другими задержанными
            test_consts.BotMessage(3, chat_id),  # 15.2.1.1.1.	Сколько человек было в связке с вами?
            test_consts.BotMessage("1 час 15 минут", chat_id),  # 15.2.1.1.2.	Примерно как долго вы были скованы
            test_consts.BotCall(2, chat_id),  # 16.	Было ли вам холодно во время нахождения
            test_consts.BotCall(1, chat_id),  # 16.2.1.	Было ли вам жарко во время нахождения в машине?
            test_consts.BotMessage("1 час 45 минут", chat_id),  # 16.2.1.1.1.Примерно как долго вас держали в жаре
            test_consts.BotCall(1, chat_id),  # 17.	Хватало ли вам свежего воздуха при транспортировке?
        ]

        bot_dummy.answer_questions(bot, answers)

        personal_info = bot.bot_info.personal_info
        true_complains_list = [[
            "Во время транспортировки меня сковали в одну цепь с другими задержанными. В связке со мной было"
            " 3 человека. Мы были скованы примерно 1 час 15 минут. Из-за этого у меня отсутствовало "
            "личное пространство и возможность двигать руками по своему усмотрению.",
            "При транспортировке мне было жарко. В жаркой душной машине меня держали примерно 1 час 45 минут. "
            "При этом сотрудники, осуществлявшие конвоирование, не могли не осознавать неадекватный температурный "
            "режим и то, что подобное обращение является жестоким и бесчеловечным.",
        ]]
        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        self.assertEqual(complains_list, true_complains_list)
