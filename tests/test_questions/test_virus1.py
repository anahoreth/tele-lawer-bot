import unittest

import test_consts
import config
import bot_dummy

import questions.questions_entry as questions_entry


class TestVirus(unittest.TestCase):

    def test_virus1(self):
        is_after_court = True
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.VIRUS.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answer_numbers = [1, 1, 1, 1]
        answers = [test_consts.BotCall(number, chat_id) for number in answer_numbers]
        bot_dummy.answer_questions(bot, answers)

        personal_info = bot.bot_info.personal_info
        true_complains_list = []

        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        self.assertEqual(complains_list, true_complains_list)

    def test_virus2(self):
        is_after_court = True
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.VIRUS.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answer_numbers = [1, 1, 1, 2]
        answers = [test_consts.BotCall(number, chat_id) for number in answer_numbers]
        bot_dummy.answer_questions(bot, answers)

        personal_info = bot.bot_info.personal_info
        true_complains_list = [[self.before_text,
                                self.fourth_two_answer_after_court,
                                self.virus_after_complains_text[0],
                                self.virus_after_complains_text[1]]]

        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        self.assertEqual(complains_list, true_complains_list)

    place_name = test_consts.test_place1
    before_text = "Во время содержания в {} меня подвергали опасности заразиться COVID-19. " \
                  "Отсутствовала профилактика коронавируса.".format(place_name)

    fourth_two_answer_after_court = "Сотрудники {} не всегда использовали средства индивидуальной защиты. " \
                                    "При этом люди могут бессимптомно переносить и распространять данный вирус. " \
                                    "Я не могу знать наверняка, " \
                                    "что меня не заразили сотрудники {}.".format(place_name, place_name)

    virus_after_complains_text = \
        ["Согласно данным Всемирной организации здравоохранения, несмотря на активное принятие мер по сдерживанию "
         "распространения COVID-19, кумулятивное число заболеваний и летальных исходов не снижает темпа роста. Кроме "
         "того, согласно последним научным исследованиям COVID-19 нередко протекает в лёгкой или средней форме, "
         "но в некоторых случаях вызывает сильные воспалительные процессы (цитокиновый шторм), которые могут привести "
         "к смертельной пневмонии и острому респираторному дистресс-синдрому. COVID-19 также вызывает иные "
         "осложнения, в числе которых острая сердечная недостаточность, острая почечная недостаточность, "
         "кардиомиопатии, септический шок и другие.",
         "Перечисленные выше обстоятельства нарушают мое конституционное право на охрану здоровья, а также "
         "требования части 4 статьи 2.4 ПИКоАП, в соответствии с которыми содержание лица, задержанного за "
         "административное правонарушение или административно арестованного, должно осуществляться в условиях, "
         "исключающих угрозу для его жизни и здоровья."]
