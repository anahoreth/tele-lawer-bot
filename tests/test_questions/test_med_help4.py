import unittest

import test_consts
import config
import bot_dummy
import questions.questions_entry as questions_entry


class TestMedHelp(unittest.TestCase):

    def test_med_help1(self):
        is_after_court = True
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.MED.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answers = [
            test_consts.BotCall("NextQ", chat_id),  # 11.4.1. Этот раздел может включать большое количество нарушений
            test_consts.BotCall(1, chat_id),  # 11.4.2.	Были ли у вас телесные повреждения во время заключения?
            test_consts.BotCall(1, chat_id),  # 11.4.2.1.1.	Проводился ли медицинский осмотр телесных повреждений?
            test_consts.BotCall(2, chat_id),  # 11.4.2.1.1.1.1.	Осмотр провели в течение суток
            test_consts.BotMessage("через два дня", chat_id),  # 11.4.2.1.1.1.1.2.1.	Через какое время
            test_consts.BotCall(2, chat_id),  # 11.4.2.1.1.1.2.	Осмотр проводился сотрудником одного с вами пола?
            test_consts.BotCall(2, chat_id),  # 11.4.2.1.1.1.3.	Результаты осмотра сообщили вам?

            test_consts.BotCall(1, chat_id),  # 11.4.3.	Были ли вам необходимы лекарства?
            test_consts.BotCall(2, chat_id),  # 11.4.3.1.1.	Был ли у вас доступ к нужным лекарствам
            test_consts.BotCall("NextQ", chat_id),  # 11.4.3.1.1.2.1.	“Я зафиксировала ваш ответ
            test_consts.BotCall(1, chat_id),  # 11.4.4.	Болели ли вы во время заключения?
            test_consts.BotCall(1, chat_id),  # 11.4.4.1.1.	Вы начали болеть до заключения или после?
            test_consts.BotCall(2, chat_id),  # 11.4.4.1.1.1.1.	Обеспечивалось ли вам необходимое лечение?
            test_consts.BotCall("NextQ", chat_id),  # 11.4.4.1.1.1.1.2.1.	“Я зафиксировала ваш
            test_consts.BotCall(3, chat_id),  # 11.4.5.	Вызывали ли скорую медицинскую помощь при необходимости?
            test_consts.BotCall("NextQ", chat_id),  # 11.4.5.3.1.	“Я зафиксировала
        ]

        bot_dummy.answer_questions(bot, answers)
        personal_info = bot.bot_info.personal_info
        true_complains_list = ["Во время нахождения в {} у меня были телесные повреждения.".format(
            test_consts.test_place1),
            "В нарушение части первой пункта 95 Правил № 313 освидетельствование было проведено только через два дня.",
            "Освидетельствование проводилось сотрудником не одного со мной пола, что нарушает "
            "часть вторую пункта 95 Правил № 313.",
            "Результаты медицинского освидетельствования телесных повреждений, которые у меня были, не были "
            "зафиксированы в журнале регистрации случаев оказания первичной медицинской помощи. Это нарушает часть "
            "три пункта 95 Правил № 313.",
            "Согласно статье 45 Конституции гражданам Республики Беларусь гарантируется право на охрану здоровья. "
            "Кроме того, правило 24 Правил Нельсона Манделы устанавливает необходимость обеспечивать те же "
            "стандарты медико-санитарного обслуживания, которые существуют в обществе, а также бесплатный доступ "
            "к необходимым медико-санитарным услугам без дискриминации. Медицинское обслуживание следует "
            "организовывать таким образом, чтобы обеспечить непрерывность лечения и ухода.",
            "Тем не менее, мне не был предоставлен доступ к необходимым мне лекарствам.",
            "При поступлении в {} у меня имелись проблемы со здоровьем, однако мне не было обеспечено необходимое "
            "лечение.".format(test_consts.test_place1),
            "Во время содержания в {} у меня обострились проблемы со здоровьем, и появилась потребность в оказании"
            " скорой медицинской помощи. Тем не менее, в нарушение пункта 90 Правил № 313, "
            "скорую помощь мне не вызвали.".format(test_consts.test_place1)
        ]

        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()[0]
        self.assertEqual(complains_list, true_complains_list)
