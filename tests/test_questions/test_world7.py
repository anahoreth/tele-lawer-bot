import unittest

import test_consts
import config
import bot_dummy

import questions.questions_entry as questions_entry

import form_complain


class TestWorld(unittest.TestCase):
    place_name = test_consts.test_place1

    def test_world1(self):
        is_after_court = True
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.WORLD.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answer_numbers = [2, 2, 1, 2, 1]
        answers = [test_consts.BotCall(number, chat_id) for number in answer_numbers]
        bot_dummy.answer_questions(bot, answers)

        four = "Во время нахождения в {} мне необоснованно отказали в телефонных разговорах "\
                               "по таксофону. Согласно пунктам 65, 69 Правил № 313 начальник места отбывания "\
                               "административного ареста может разрешить административно арестованным телефонные"\
                               " разговоры. Административно арестованный, которому разрешен телефонный разговор,"\
                               " для оплаты телефонного разговора может использовать принадлежащую ему телефонную"\
                               " карточку. У меня была телефонная карточка, следовательно, объективные основания"\
                               " для отказа мне в телефонном разговоре отсутствовали.".format(self.place_name)
        personal_info = bot.bot_info.personal_info
        true_complains_list = [["Мне достоверно известно, что администрация {} не вручила мне некоторые " \
                                   "письма. Это нарушает пункт 62 Правил № 313, согласно которому вручение писем " \
                                   "и телеграмм, поступающих на имя административно арестованного, производится " \
                                   "не позднее чем в трехдневный срок со дня поступления в место отбывания " \
                                   "административного ареста письма, телеграммы, посылки, бандероли, мелкого " \
                                   "пакета, за исключением праздничных и выходных дней.".format(self.place_name),
                               "Администрация {} отправила не все мои письма. Это нарушает пункт 62 Правил "\
                               "№ 313, согласно которому отправление писем, телеграмм, посылок, бандеролей или "\
                               "мелких пакетов производится не позднее чем в трехдневный срок со дня поступления "\
                               "в место отбывания административного ареста письма, телеграммы, посылки, бандероли,"\
                               " мелкого пакета, за исключением праздничных и выходных дней.".format(self.place_name),
                               four
                               ]]

        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        # print (complains_list)
        self.assertEqual(complains_list, true_complains_list)
        self.assertEqual(type(bot.bot_info.get_state(chat_id)), form_complain.StateFormComplain)


    def test_world2(self):
        is_after_court = False
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.WORLD.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answer_numbers = [2, 2, 1]
        answers = [test_consts.BotCall(number, chat_id) for number in answer_numbers]
        bot_dummy.answer_questions(bot, answers)

        personal_info = bot.bot_info.personal_info
        true_complains_list = [["Мне достоверно известно, что администрация {} не вручила мне некоторые письма. " \
                                    "Это нарушает " \
                                    "правило 58 Правил Нельсона Манделы, согласно которому администрация должна давать возможность " \
                                    "общаться через регулярные промежутки времени и под должным надзором с семьями или друзьями " \
                                    "посредством письменной переписки и с использованием, если есть такая возможность, " \
                                    "телекоммуникационных, электронных, цифровых и иных средств, " \
                                    "а также в ходе свиданий. ".format(self.place_name),
                               "Администрация {} отправила не все мои письма. Это нарушает правило 58 Правил "
                               "Нельсона Манделы, согласно которому администрация должна давать возможность общаться"
                               " через регулярные промежутки времени и под должным надзором с семьями или друзьями"
                               " посредством письменной переписки и с использованием, если есть такая возможность,"
                               " телекоммуникационных, электронных, цифровых и иных средств, "
                               "а также в ходе свиданий. ".format(self.place_name),

                               ]]

        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        self.assertEqual(complains_list, true_complains_list)
        self.assertEqual(type(bot.bot_info.get_state(chat_id)), form_complain.StateFormComplain)

    def test_world3(self):
        is_after_court = 2
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.WORLD.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answer_numbers = [2, 2, 1, 4]
        answers = [test_consts.BotCall(number, chat_id) for number in answer_numbers]
        bot_dummy.answer_questions(bot, answers)

        one = "Мне достоверно известно, что администрация {} не вручила мне некоторые письма. Это нарушает " \
              "пункт 62 Правил № 313, согласно которому вручение писем и телеграмм, поступающих на имя админи" \
              "стративно арестованного, производится не позднее чем в трехдневный срок со дня поступления в место " \
              "отбывания административного ареста письма, телеграммы, посылки, бандероли, мелкого пакета, за искл" \
              "ючением праздничных и выходных дней. Кроме того, это нарушает правило 58 Правил Нельсона Манделы, сог" \
              "ласно которому администрация должна давать возможность общаться через регулярные промежутки времени " \
              "и под должным надзором с семьями или друзьями посредством письменной переписки и с использованием, " \
              "если есть такая возможность, телекоммуникационных, электронных, цифровых и иных средств, а также в ход" \
              "е свиданий. ".format(self.place_name)

        two = "Администрация {} отправила не все мои письма. Это нарушает пункт 62 Правил № 313, согласно" \
              " которому отправление писем, телеграмм, посылок, бандеролей или мелких пакетов производится не" \
              " позднее чем в трехдневный срок со дня поступления в место отбывания административного ареста письма," \
              " телеграммы, посылки, бандероли, мелкого пакета, за исключением праздничных и выходных дней. Кроме " \
              "того, это нарушает правило 58 Правил Нельсона Манделы, согласно которому администрация должна давать" \
              " возможность общаться через регулярные промежутки времени и под должным надзором с семьями или друзь" \
              "ями посредством письменной переписки и с использованием, если есть такая возможность, телекоммуникаци" \
              "онных, электронных, цифровых и иных средств, а также в ходе свиданий. ".format(self.place_name)

        four = "Во время отбывания административного ареста в {} меня фактически лишили права на телефонные " \
               "звонки по таксофону. В соответствии с пунктом 66 Правил № 313 на территории места отбывания " \
               "административного ареста для ведения телефонных разговоров устанавливаются таксофоны. Вместе с" \
               " тем, в {} таксофон отсутствовал. Таким образом, мое право на телефонные звонки, " \
               "предусмотренное пунктом 65 Правил № 313, было нарушено.".format(self.place_name, self.place_name)
        personal_info = bot.bot_info.personal_info

        true_complains_list = [[one, two, four]]

        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()

        self.assertEqual(complains_list, true_complains_list)
        self.assertEqual(type(bot.bot_info.get_state(chat_id)), form_complain.StateFormComplain)


