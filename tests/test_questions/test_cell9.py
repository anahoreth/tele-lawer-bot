import unittest


import test_consts
import config
import bot_dummy
import questions.questions_entry as questions_entry


class TestCell(unittest.TestCase):
    place_name = test_consts.test_place1
    two_days = "2 дня"
    one_cell_two_days = "Условия в первой камере, в которой меня держали в {}, были " \
                        "бесчеловечными, унижали мое человеческое достоинство и представляли угрозу, как минимум, " \
                        "моему здоровью. В этой камере меня держали {}.".format(place_name, two_days)

    first_group = "Камера была переполнена. Площадь камеры составляла примерно 2.0 кв.м., " \
                  "при этом в ней было размещено 3 человека. Таким образом, на одного " \
                  "человека приходилось менее 4 квадратных метров, что нарушает часть 1 статьи 18.7 ПИКоАП."

    # одна камера из двух, одна категория
    def test_cell1(self):
        is_after_court = True
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.CELL.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answers = [
            test_consts.BotMessage(2, chat_id),  # 11.9.1.
            test_consts.BotCall(1, chat_id),  # 11.9.2
            test_consts.BotCall("NextQ", chat_id),  # 11.9.2
            test_consts.BotMessage(2, chat_id),  # 11.9.2.1.1.	Примерно сколько дней вы
            test_consts.BotCall(1, chat_id),  # 12.1 Тесноту в камере
            test_consts.BotCall("NextQ", chat_id),
            test_consts.BotMessage(1, chat_id),  # 12.1.2.1.	Какая примерно была длина камеры (в метрах)?
            test_consts.BotMessage(2, chat_id),  # 12.1.2.2.	Какая примерно была ширина камеры (в метрах)?
            test_consts.BotMessage(3, chat_id),  # 12.1.2.3.	Сколько задержанных было в камере
        ]

        bot_dummy.answer_questions(bot, answers)
        personal_info = bot.bot_info.personal_info
        true_complains_list = [[
            "Во время нахождения в {} меня держали в 2 камерах.".format(test_consts.test_place1),
            self.one_cell_two_days,
            self.first_group,
            ]]

        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        self.assertEqual(complains_list, true_complains_list)

    # одна камера, три категории
    def test_cell2(self):
        is_after_court = True
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.CELL.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answers = [
            test_consts.BotMessage(2, chat_id),  # 11.9.1.
            test_consts.BotCall(1, chat_id),  # 11.9.2
            test_consts.BotCall("NextQ", chat_id),  # 11.9.2
            test_consts.BotMessage(2, chat_id),  # 11.9.2.1.1.	Примерно сколько дней вы
            test_consts.BotCall(2, chat_id),  # 12.2.	Загрязненность камеры и угрозу заражения болезнями
            test_consts.BotCall(3, chat_id),  # 12.3.	Температуру в камере
            test_consts.BotCall(6, chat_id),  # 12.6.	Оборудование туалета
            test_consts.BotCall("NextQ", chat_id),
            test_consts.BotCall(2, chat_id),  # 12.2.1.2.	В камере было грязно
            test_consts.BotCall(1, chat_id),  # 12.2.2.1.	Да, насекомые
            test_consts.BotCall(2, chat_id),  # 12.2.3.	нет: Содержали ли вас вместе с людьми с заразными болезнями
            test_consts.BotCall(2, chat_id),  # 12.3.1.	В камере было холодно? нет
            test_consts.BotCall(1, chat_id),  # 12.3.1.2	В камере было жарко? да

            test_consts.BotCall(2, chat_id),  # 12.6.1.	Была ли камера оборудована туалетом?
        ]
        bot_dummy.answer_questions(bot, answers)
        personal_info = bot.bot_info.personal_info
        true_complains_list = [[
            "Во время нахождения в {} меня держали в 2 камерах.".format(test_consts.test_place1),
            self.one_cell_two_days,
            "В камере было грязно, что не соответствует правилу 17 Правил Нельсона Манделы, согласно которому все "
            "помещения, которыми административно арестованные пользуются регулярно, должны всегда содержаться в "
            "должном порядке и самой строгой чистоте. Наличие грязи способствует распространению заболеваний, "
            "появлению насекомых, грызунов.",

            "В камере были насекомые. Согласно части первой пункта 106 Правил № 313 в целях предотвращения "
            "возникновения, распространения"
            " инфекционных заболеваний, чесотки, педикулеза, а также их локализации и ликвидации в установленном "
            "законодательством порядке проводятся дезинфекционные, дезинсекционные и дератизационные мероприятия. "
            "Очевидно, что в данном случае эти мероприятия не были проведены надлежащим образом.",
            "В камере было жарко. Такое неадекватное отопление нарушает правило 13 Правил Нельсона Манделы, "
            "согласно которому при оборудовании камеры должное внимание следует на климатические условия, в том "
            "числе на отопление.",
            "Камера не была оборудована туалетом. Это нарушает пункт 37 Правил № 313."
        ]]
        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        self.assertEqual(complains_list, true_complains_list)

    # две камеры, по одной категории
    def test_cell3(self):
        is_after_court = True
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.CELL.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answers = [
            test_consts.BotMessage(2, chat_id),  # 11.9.1.
            test_consts.BotCall(1, chat_id),  # 11.9.2
            test_consts.BotCall(2, chat_id),  # 11.9.2
            test_consts.BotCall("NextQ", chat_id),  # 11.9.2

            # первая камера
            test_consts.BotMessage(1, chat_id),  # 11.9.2.1.1.	Примерно сколько дней вы
            test_consts.BotCall(5, chat_id),  # 12.5.	Освещение в камере
            test_consts.BotCall("NextQ", chat_id),  #
            test_consts.BotCall(2, chat_id),  # 12.5.1.	Естественного освещения было достаточно,
            test_consts.BotCall(2, chat_id),  # 12.5.2.	Искусственного освещения было достаточно
            test_consts.BotCall(1, chat_id),  # 12.5.3.	Мешал ли свет в камере спать ночью?

            #вторая камера
            test_consts.BotMessage(5, chat_id),  # 11.9.2.1.1.	Примерно сколько дней вы
            test_consts.BotCall(4, chat_id),  # 12.4.	Отсутствие свежего воздуха
            test_consts.BotCall("NextQ", chat_id),  #
            test_consts.BotCall(2, chat_id),  # 12.4.1.	Было ли в камере достаточно свежего воздуха?
            test_consts.BotCall(1, chat_id),  # 12.4.1.2.1.	Был ли неприятный запах в камере?
        ]

        bot_dummy.answer_questions(bot, answers)
        personal_info = bot.bot_info.personal_info
        true_complains_list = [[
            "Во время нахождения в {} меня держали в 2 камерах.".format(self.place_name),
            "Условия в первой камере, в которой меня держали в {}, были бесчеловечными, унижали мое человеческое "
            "достоинство и представляли угрозу, как минимум, моему здоровью. В этой камере меня держали "
            "1 день.".format(self.place_name),
            "В камере не хватало естественного света. Из-за этого у меня не было возможности комфортно читать и "
            "писать при дневном свете. Это нарушает пункт «a» правила 14 Правил Нельсона Манделы, согласно которому "
            "окна в камерах должны иметь достаточные размеры для того, чтобы там можно было читать и писать "
            "при дневном свете.",
            "Искусственное освещение в камере было слишком тусклым. Из-за этого у меня не было возможности читать "
            "и писать комфортно, без опасности для зрения. Это нарушает пункт «b» правила 14 Правил Нельсона Манделы, "
            "согласно которому искусственное освещение в камерах должно быть достаточным для того, чтобы там можно "
            "было читать или писать без опасности для зрения.",
            "По ночам в камере горел яркий свет. Это мешало мне спать и, соответственно, "
            "не позволяло восстанавливать силы.",
            "Условия во второй камере, в которой меня держали в {}, были бесчеловечными, унижали мое человеческое "
            "достоинство и представляли угрозу, как минимум, моему здоровью. В этой камере меня держали "
            "5 дней.".format(self.place_name),
            "В камере не хватало свежего воздуха, было душно, стоял неприятный запах. Это значит, что через окна не "
            "поступало достаточно свежего воздуха, а вентиляционная система не справлялась со своими функциями. "
            "Такие условия содержания нарушают пункт «а» правила 14 Правил Нельсона Манделы.",
        ]]
        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        self.assertEqual(complains_list, true_complains_list)

    # две камеры, по несколько категорий
    def test_cell4(self):
        is_after_court = True
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.CELL.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answers = [
            test_consts.BotMessage(3, chat_id),  # 11.9.1.
            test_consts.BotCall(1, chat_id),  # 11.9.2
            test_consts.BotCall(3, chat_id),  # 11.9.2
            test_consts.BotCall("NextQ", chat_id),  # 11.9.2

            # первая камера
            test_consts.BotMessage(10, chat_id),  # 11.9.2.1.1.	Примерно сколько дней вы
            test_consts.BotCall(4, chat_id),  # 12.4.	Отсутствие свежего воздуха
            test_consts.BotCall(5, chat_id),  # 12.5.	Освещение в камере
            test_consts.BotCall("NextQ", chat_id),  #
            test_consts.BotCall(2, chat_id),  # 12.4.1.	Было ли в камере достаточно свежего воздуха?
            test_consts.BotCall(2, chat_id),  # 12.4.1.2.1.	Был ли неприятный запах в камере?

            test_consts.BotCall(2, chat_id),  # 12.5.1.	Естественного освещения было достаточно,
            test_consts.BotCall(2, chat_id),  # 12.5.2.	Искусственного освещения было достаточно

            test_consts.BotCall(1, chat_id),  # 12.5.3.	Мешал ли свет в камере спать ночью?

            # третья камера
            test_consts.BotMessage(20, chat_id),  # 11.9.2.1.1.	Примерно сколько дней вы
            test_consts.BotCall(4, chat_id),  # 12.4.	Отсутствие свежего воздуха
            test_consts.BotCall(6, chat_id),  # 12.6.	Оборудование туалета
            test_consts.BotCall("NextQ", chat_id),  #
            test_consts.BotCall(2, chat_id),  # 12.4.1.	Было ли в камере достаточно свежего воздуха?
            test_consts.BotCall(1, chat_id),  # 12.4.1.2.1.	Был ли неприятный запах в камере?
            test_consts.BotCall("NextQ", chat_id),  #

            test_consts.BotCall(1, chat_id),  # 12.6.1.	Была ли камера оборудована туалетом?
            test_consts.BotCall(2, chat_id),  # 12.6.1.1.1.	Он работал?
            test_consts.BotCall(1, chat_id),  # 12.6.2.	Могли ли другие сокамерники видеть вас во время посещения
            test_consts.BotCall(1, chat_id),  # 12.6.3.	Ваша кровать располагалась слишком близко к туалету?
            test_consts.BotMessage(20, chat_id),  # 12.6.3.1.1.	Сколько примерно сантиметров было до туалета?
        ]

        bot_dummy.answer_questions(bot, answers)
        personal_info = bot.bot_info.personal_info
        true_complains_list = [[
            "Во время нахождения в {} меня держали в 3 камерах.".format(self.place_name),
            "Условия в первой камере, в которой меня держали в {}, были бесчеловечными, унижали мое человеческое "
            "достоинство и представляли угрозу, как минимум, моему здоровью. В этой камере меня держали "
            "10 дней.".format(self.place_name),
            "В камере не хватало свежего воздуха, и было душно. В камере не хватало свежего воздуха, было душно, "
            "стоял неприятный запах. Это значит, что через окна не поступало достаточно свежего воздуха, а "
            "вентиляционная система не справлялась со своими функциями. Такие условия содержания нарушают пункт «а» "
            "правила 14 Правил Нельсона Манделы.",
            "В камере не хватало естественного света. Из-за этого у меня не было возможности комфортно читать и "
            "писать при дневном свете. Это нарушает пункт «a» правила 14 Правил Нельсона Манделы, согласно которому "
            "окна в камерах должны иметь достаточные размеры для того, чтобы там можно было читать и писать "
            "при дневном свете.",
            "Искусственное освещение в камере было слишком тусклым. Из-за этого у меня не было возможности читать "
            "и писать комфортно, без опасности для зрения. Это нарушает пункт «b» правила 14 Правил Нельсона Манделы, "
            "согласно которому искусственное освещение в камерах должно быть достаточным для того, чтобы там можно "
            "было читать или писать без опасности для зрения.",
            "По ночам в камере горел яркий свет. Это мешало мне спать и, соответственно, "
            "не позволяло восстанавливать силы.",
            "Условия в третьей камере, в которой меня держали в {}, были бесчеловечными, унижали мое человеческое "
            "достоинство и представляли угрозу, как минимум, моему здоровью. В этой камере меня держали "
            "20 дней.".format(self.place_name),
            "В камере не хватало свежего воздуха, было душно, стоял неприятный запах. Это значит, что через окна не "
            "поступало достаточно свежего воздуха, а вентиляционная система не справлялась со своими функциями. "
            "Такие условия содержания нарушают пункт «а» правила 14 Правил Нельсона Манделы.",
            "Туалет в камере был в нерабочем состоянии. Следовательно, им невозможно было пользоваться. "
            "Отсутствие возможности свободно пользоваться туалетом является бесчеловечным обращением.",
            "В камере не было обеспечено уединение при использовании туалета. Из-за этого каждое использование"
            " туалета провоцировало ощущение дискомфорта. Такое обращение нарушает правило 15 Правил Нельсона "
            "Манделы, согласно которому у арестованных должна быть возможность удовлетворять свои естественные "
            "потребности в условия пристойности.",
            "Моя кровать находилась слишком близко к туалету: в 20 см. Такие условия были антисанитарными "
            "и порождали постоянное ощущение брезгливости.",
        ]]
        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        self.assertEqual(complains_list[0], true_complains_list[0])
