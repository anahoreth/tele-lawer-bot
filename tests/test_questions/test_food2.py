import unittest


import test_consts
import config
import bot_dummy

import questions.questions_entry as questions_entry


class TestFood(unittest.TestCase):

    place_name = test_consts.test_place1

    def test_food1(self):
        is_after_court = True
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.FOOD.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answer_numbers = [1]
        answers = [test_consts.BotCall(number, chat_id) for number in answer_numbers]
        bot_dummy.answer_questions(bot, answers)

        personal_info = bot.bot_info.personal_info
        true_complains_list = []

        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        self.assertEqual(complains_list, true_complains_list)

    def test_food2(self):
        is_after_court = True
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.FOOD.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answers = [
            test_consts.BotCall(2, chat_id),  # 11.2.1.	Кормили ли вас в день поступления?
            test_consts.BotCall(3, chat_id),  # 11.2.2.	Устраивало ли вас качество пищи?

            test_consts.BotCall(2, chat_id),  # 11.2.8.	Был ли у вас доступ к питьевой воде?
            test_consts.BotCall(3, chat_id),  # 11.2.9.	Привлекали ли вас к раздаче пищи или мытью посуды?
        ]
        bot_dummy.answer_questions(bot, answers)

        personal_info = bot.bot_info.personal_info
        true_complains_list = [[
            "Во время моего содержания в {} были нарушены мои права, касающиеся питания. В день "
            "поступления мне не предоставили пищу. Голод угнетал меня и отбирал силы.".format(self.place_name),
            "Кроме того, на протяжении всего времени содержания в {} нарушались и другие мои права, "
            "касающиеся питания.".format(self.place_name),
            "Правила Нельсона Манделы содержат положения, которые касаются питания. В частности, согласно "
            "пункту 1 правила 22 мне должны были предоставить пищу, которая должна была быть достаточно "
            "питательной для поддержания здоровья и сил, имеющей достаточно хорошее качество, хорошо "
            "приготовленной и поданной. Указанные выше условия нарушают данное Правило.",
            'У меня не всегда был доступ к питьевой воде. Это нарушает пункт 37 Правил № 313. Жажда доставляла'
            ' значительный дискомфорт, мысли часто занимала невозможность попить. Кроме того, достаточное количество'
            ' питьевой воды является необходимым для полноценного функционирования организма. Недостаток воды может'
            ' негативно сказываться на здоровье. Согласно пункту 2 правила 22 Правил Нельсона Манделы должен быть'
            ' обеспечен доступ к питьевой воде, когда возникает такая потребность.',
            "Меня привлекали к раздаче пищи и мытью столовой посуды, что прямо нарушает пункт 107 Правил № 313. "
            "Кроме того, это является принудительным трудом, что запрещено статьей 41 Конституции."
        ]]

        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        self.assertEqual(complains_list, true_complains_list)



