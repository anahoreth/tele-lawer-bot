import unittest


import test_consts
import bot_dummy
import config

import questions.questions_entry as questions_entry
import questions.question_groups.abstract_group as abstract_group
import states.ask_question as ask_question
import form_complain


class TestComplain(unittest.TestCase):

    def test_complain1(self):
        is_after_court = 2
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.CANNOT.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answers = [
            test_consts.BotCall(1, chat_id),  # 1.6.1.	Разъяснялись ли вам права, обязанности и режим содержания?
            test_consts.BotCall(1, chat_id),  # 1.6.2.	Была ли у вас возможность подать жалобу
            test_consts.BotCall(1, chat_id),  # 1.6.3.	Принимали ли сотрудники ваши жало
            test_consts.BotCall(1, chat_id),  # 1.6.4.	Выдавали ли по вашей просьбе бумагу и письменные
            test_consts.BotCall(1, chat_id),  # 1.6.4.	Выдавали ли по вашей просьбе бумагу и письменные
            test_consts.BotCall(1, chat_id),  # 1.6.5.	Отправляли ли ваши жалобы
        ]

        bot_dummy.answer_questions(bot, answers)

        personal_info = bot.bot_info.personal_info
        true_complains_list = []

        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        self.assertEqual(complains_list, true_complains_list)

    def test_complain2(self):
        is_after_court = 2
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.CANNOT.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answers = [
            test_consts.BotCall(2, chat_id),  # 1.6.1.	Разъяснялись ли вам права, обязанности и режим содержания?
            test_consts.BotCall(3, chat_id),  # 1.6.2.	Была ли у вас возможность подать жалобу

            test_consts.BotCall(1, chat_id),  # 1.6.2.3.1.	Что вам помешало подать жалобу?
            test_consts.BotCall(3, chat_id),  # 1.6.2.3.1.	Что вам помешало подать жалобу?
            test_consts.BotCall("NextQ", chat_id),  # 1.6.2.	Была ли у вас возможность подать жалобу

            test_consts.BotCall(1, chat_id),  # 1.6.3.	Принимали ли сотрудники ваши жало
            test_consts.BotCall(2, chat_id),  # 1.6.4.	Выдавали ли по вашей просьбе бумагу и письменные
            test_consts.BotCall(2, chat_id),  # 1.6.5.	Отправляли ли ваши жалобы в место назначения
        ]

        bot_dummy.answer_questions(bot, answers)

        personal_info = bot.bot_info.personal_info
        true_text_before_complains = "Согласно части первой статьи 60 Конституции каждому гарантируется защита" \
                                     " его прав и свобод компетентным, независимым и беспристрастным судом в " \
                                     "определенные законом сроки. Однако мое право на защиту суда было нарушено. " \
                                     "Так, вследствие действий сотрудников {} у меня не получилось подать жалобу" \
                                     " на постановление об административном нарушении.".format(test_consts.test_place1)
        complain_four = "У меня не было бумаги и письменных принадлежностей для подготовки заявлений и жалоб. " \
                        "Это нарушает пункт 52 Правил № 996 и пункт 43 Правил № 313, согласно которым для " \
                        "написания ходатайств, предложений, заявлений и жалоб задержанным лицам и административно " \
                        "арестованным по "\
            "просьбе выдаются письменные принадлежности и бумага. Однако мне было отказано в выдаче"\
            " данных предметов. Из-за этого у меня не получилось жаловаться в письменном виде."

        complain_five = "Во время содержания в {} администрация не отправила адресату мои жалобы. Исходя из пункта "\
            "72 Правил № 313, жалобы должны быть направлены начальником {} соответствующему адресату."\
            " Согласно пункту 79 Правил № 313 о направлении ходатайства, предложения, заявления или"\
            " жалобы объявляется административно арестованному, подавшему их. Кроме того, это нарушает "\
            "пункт 3 правила 56 Правил Нельсона Манделы, согласно которому каждый должен иметь "\
            "возможность обращаться к центральным органам тюремного управления и судебным или иным "\
            "компетентным органам, включая те, что уполномочены пересматривать дело или принимать "\
            "меры по исправлению положения, с заявлениями или жалобами по поводу обращения с ним или "\
            "с ней, которые не должны подвергаться цензуре.".format(test_consts.test_place1, test_consts.test_place1)
        true_complains_list = [[
            "При приеме в {} как во время задержания, так и при отбывании ареста мне не разъяснили права "
            "и обязанности, а также режим содержания. Это нарушает пункт 9 Правил № 996 и пункт 6 Правил № 313, "
            "согласно которым сотрудники {} обязаны ознакомить с правами и обязанностями, режимом содержания, "
            "порядком подачи ходатайств, предложений, заявлений и жалоб.".format(test_consts.test_place1,
                                                                                 test_consts.test_place1),
            true_text_before_complains,
            "Для подготовки жалобы мне была необходима помощь адвоката. Исходя из статьи 62 Конституции, каждый "
            "имеет право на юридическую помощь. Противодействие оказанию правовой помощи в Республике Беларусь "
            "запрещается. Тем не менее, ко мне незаконно не пустили адвоката. Из-за этого у меня не получилось "
            "подготовить качественную жалобу.",
            "Администрация отказалась отправить мою жалобу в суд. Согласно пункту 74 Правил № 313 жалобы на "
            "постановления по делу об административном правонарушении направляются в порядке, предусмотренном "
            "статьей 12.2 ПИКоАП. Таким образом, администрация обязана была направить мою жалобу в соответствующий "
            "суд. Тем не менее, жалоба не была направлена.",
            complain_four,
            complain_five,
        ]]
        self.assertEqual(type(bot.bot_info.get_state(chat_id)), form_complain.StateFormComplain)
        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()

        self.assertEqual(complains_list, true_complains_list)

    def test_complain3(self):
        is_after_court = 2
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.CANNOT.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answers = [
            test_consts.BotCall(1, chat_id),  # 1.6.1.	Разъяснялись ли вам права, обязанности и режим содержания?
            test_consts.BotCall(1, chat_id),  # 1.6.2.	Была ли у вас возможность подать жалобу
            test_consts.BotCall(1, chat_id),  # 1.6.3.	Принимали ли сотрудники ваши жало
            test_consts.BotCall(1, chat_id),  # 1.6.4.	Выдавали ли по вашей просьбе бумагу и письменные
            test_consts.BotCall(1, chat_id),  # 1.6.4.	Выдавали ли по вашей просьбе бумагу и письменные
            test_consts.BotCall(1, chat_id),  # 1.6.5.	Отправляли ли ваши жалобы
        ]

        bot_dummy.answer_questions(bot, answers)

        personal_info = bot.bot_info.personal_info
        true_complains_list = []

        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        self.assertEqual(complains_list, true_complains_list)

    def test_complain4(self):
        is_after_court = 2
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.CANNOT.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answers = [
            test_consts.BotCall(2, chat_id),  # 1.6.1.	Разъяснялись ли вам права, обязанности и режим содержания?
            test_consts.BotCall(3, chat_id),  # 1.6.2.	Была ли у вас возможность подать жалобу

            test_consts.BotCall(1, chat_id),  # 1.6.2.3.1.	Что вам помешало подать жалобу?
            test_consts.BotCall(3, chat_id),  # 1.6.2.3.1.	Что вам помешало подать жалобу?
            test_consts.BotCall("NextQ", chat_id),  # 1.6.2.	Была ли у вас возможность подать жалобу

            test_consts.BotCall(1, chat_id),  # 1.6.3.	Принимали ли сотрудники ваши жало
            test_consts.BotCall(1, chat_id),  # 1.6.4.	Выдавали ли по вашей просьбе бумагу и письменные
            test_consts.BotCall(1, chat_id),  # 1.6.5.	Отправляли ли ваши жалобы
        ]

        bot_dummy.answer_questions(bot, answers)

        personal_info = bot.bot_info.personal_info

        true_complains_list = [[
            "При приеме в {} как во время задержания, так и при отбывании ареста мне не разъяснили права и"
            " обязанности, а также режим содержания. Это нарушает пункт 9 Правил № 996 и пункт 6 Правил № 313,"
            " согласно которым сотрудники {} обязаны ознакомить с правами и обязанностями, режимом содержания, "
            "порядком подачи ходатайств, предложений, заявлений и жалоб.".format(test_consts.test_place1,
                                                                                 test_consts.test_place1),
            "Согласно части первой статьи 60 Конституции каждому гарантируется защита"
            " его прав и свобод компетентным, независимым и беспристрастным судом в "
            "определенные законом сроки. Однако мое право на защиту суда было нарушено. "
            "Так, вследствие действий сотрудников {} у меня не получилось подать жалобу"
            " на постановление об административном нарушении.".format(test_consts.test_place1),
            "Для подготовки жалобы мне была необходима помощь адвоката. Исходя из статьи 62 Конституции, каждый "
            "имеет право на юридическую помощь. Противодействие оказанию правовой помощи в Республике Беларусь "
            "запрещается. Тем не менее, ко мне незаконно не пустили адвоката. Из-за этого у меня не получилось "
            "подготовить качественную жалобу.",
            "Администрация отказалась отправить мою жалобу в суд. Согласно пункту 74 Правил № 313 жалобы на "
            "постановления по делу об административном правонарушении направляются в порядке, предусмотренном "
            "статьей 12.2 ПИКоАП. Таким образом, администрация обязана была направить мою жалобу в соответствующий "
            "суд. Тем не менее, жалоба не была направлена."
        ]]

        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        self.assertEqual(complains_list, true_complains_list)

