import unittest

import test_consts
import config
import bot_dummy
import questions.questions_entry as questions_entry


class TestShower(unittest.TestCase):

    def test_shower1(self):
        is_after_court = False
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.SHOWER.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answers = [
            test_consts.BotCall(1, chat_id),  # 11.5.3.	Выдавались ли вам на каждые
            test_consts.BotCall(3, chat_id),  # 11.5.3.	Выдавались ли вам на каждые
            test_consts.BotCall("NextQ", chat_id),  #
        ]

        bot_dummy.answer_questions(bot, answers)
        personal_info = bot.bot_info.personal_info
        true_complains_list = [["Во время содержания в {} в нарушение Норм обеспечения задержанных лиц "
                               "средствами личной гигиены (Приложение 4 к Правилам № 996) мне не выдавалось"
                               " необходимого количества хозяйственного мыла, "
                               "туалетного мыла.".format(test_consts.test_place1)
                               ]]
        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        self.assertEqual(complains_list, true_complains_list)
