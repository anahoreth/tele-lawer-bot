import unittest


import test_consts
import config
import bot_dummy
import questions.questions_entry as questions_entry


class TestCellBoth(unittest.TestCase):

    # одна камера, одна категория
    def test_cell1(self):
        is_after_court = 2
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.CELL.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answers = [
            test_consts.BotMessage(1, chat_id),  # 1.9.1. В скольких камерах, включая карцер, вы находились?
            test_consts.BotMessage(3, chat_id),  # 11.9.1.
            test_consts.BotCall(3, chat_id),  # 1.9.1.2.1.2.	Когда вы были в этой камере?

            test_consts.BotCall(2, chat_id),  # 2.2.	Загрязненность камеры и угрозу заражения болезнями
            test_consts.BotCall("NextQ", chat_id),

            test_consts.BotCall(2, chat_id),  # 2.2.1.	Была ли в камере грязь или плесень?
            test_consts.BotCall(1, chat_id),  # 2.2.2.	Были ли в камере насекомые или грызуны?
            test_consts.BotCall(2, chat_id),  # 2.2.3.	Содержали ли вас вместе с людьми
        ]

        bot_dummy.answer_questions(bot, answers)
        true_complains_list = [[
            "Во время нахождения в {} меня держали в одной камере. Условия в этой камере были бесчеловечными,"
            " унижали мое человеческое достоинство и представляли угрозу, как минимум, моему здоровью. "
            "В этой камере меня держали 3 дня.".format(test_consts.test_place1),
                                "В камере было грязно, что нарушает пункт 99 Правил № 996. Это также не соответствует"
                                " правилу 17 Правил Нельсона Манделы, согласно которому все помещения, "
                                "которыми задержанные пользуются регулярно, должны всегда содержаться в должном "
                                "порядке и самой строгой чистоте. Наличие грязи способствует распространению "
                                "заболеваний, появлению насекомых, грызунов.",
                               "В камере были насекомые. Согласно части первой пункта 100 Правил № 996, части "
                               "первой пункта 106 Правил № 313 в целях предотвращения возникновения, "
                               "распространения инфекционных заболеваний, чесотки, педикулеза, а также их "
                               "локализации и ликвидации в установленном законодательством порядке проводятся"
                               " дезинфекционные, дезинсекционные и дератизационные мероприятия. Очевидно, "
                               "что в данном случае эти мероприятия не были проведены надлежащим образом.",
                               ]]

        questions = bot.bot_info.personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        self.assertEqual(complains_list, true_complains_list)

    # одна камера до суда, одна категория
    def test_cell2(self):
        is_after_court = 2
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.CELL.value]
        bot = bot_dummy.get_question_bot(selected_groups, is_after_court)

        answers = [
            test_consts.BotMessage(1, chat_id),  # 1.9.1. В скольких камерах, включая карцер, вы находились?
            test_consts.BotMessage(3, chat_id),  # 11.9.1.
            test_consts.BotCall(2, chat_id),  # 1.9.1.2.1.2.	Когда вы были в этой камере?

            test_consts.BotCall(2, chat_id),  # 2.2.	Загрязненность камеры и угрозу заражения болезнями
            test_consts.BotCall("NextQ", chat_id),

            test_consts.BotCall(2, chat_id),  # 2.2.1.	Была ли в камере грязь или плесень?
            test_consts.BotCall(1, chat_id),  # 2.2.2.	Были ли в камере насекомые или грызуны?
            test_consts.BotCall(2, chat_id),  # 2.2.3.	Содержали ли вас вместе с людьми
        ]

        bot_dummy.answer_questions(bot, answers)
        true_complains_list = [[
            "Во время нахождения в {} меня держали в одной камере. Условия в этой камере были бесчеловечными,"
            " унижали мое человеческое достоинство и представляли угрозу, как минимум, моему здоровью. "
            "В этой камере меня держали 3 дня.".format(test_consts.test_place1),
            "В камере было грязно, что не соответствует правилу 17 Правил Нельсона Манделы, согласно которому "
            "все помещения, которыми административно арестованные пользуются регулярно, должны всегда содержаться"
            " в должном порядке и самой строгой чистоте. Наличие грязи способствует распространению заболеваний,"
            " появлению насекомых, грызунов.",
            "В камере были насекомые. Согласно части первой пункта 106 Правил № 313 в целях предотвращения "
            "возникновения, распространения инфекционных заболеваний, чесотки, педикулеза, а также их локализации"
            " и ликвидации в установленном законодательством порядке проводятся дезинфекционные, дезинсекционные "
            "и дератизационные мероприятия. Очевидно, что в данном случае эти мероприятия не были проведены "
            "надлежащим образом."
        ]]

        questions = bot.bot_info.personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[
            0]
        complains_list = questions.process()
        self.assertEqual(complains_list, true_complains_list)


