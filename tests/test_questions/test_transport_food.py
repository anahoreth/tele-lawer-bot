import unittest


import test_consts
import bot_dummy
import config

import questions.questions_entry as questions_entry
import questions.question_groups.abstract_group as abstract_group
import states.ask_question as ask_question

import choose_places_to_complain

class TestTransportFood(unittest.TestCase):

    def test_transport1(self):
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.TRANSPORT_FOOD.value]
        selected_place_type = config.ComplainPlaceType.SPlaceTransportFood
        bot = bot_dummy.get_question_bot(selected_groups, None, selected_place_type)

        answers = [
            test_consts.BotCall(2, chat_id),  # 13.	Кормили ли вас завтраком?
            test_consts.BotCall(2, chat_id),  # 13.2.1.	Кормили ли вас обедом?
            test_consts.BotCall(2, chat_id),  # 13.1.1.2.1.	Кормили ли вас ужином?
            test_consts.BotCall(4, chat_id),  # 14.	Позволили ли вам есть свою еду?
        ]
        bot_dummy.answer_questions(bot, answers)
        personal_info = bot.bot_info.personal_info

        true_complains_list = [[
            "10.10.2020, когда меня возили из ИВС Бешенковичского РОВД в"
            " ЦИП Новополоцкого ГОВД, со мной "
            "обращались бесчеловечным образом.",
            "В этот день меня ни разу не покормили: ни завтраком, ни обедом, ни ужином. На основании пункта 1 "
            "правила 22 Правил Нельсона Манделы мне должна была быть обеспечена пища, достаточно питательная для "
            "поддержания здоровья и сил, имеющая достаточно хорошее качество, хорошо приготовленная и поданная. "
            "Тем не менее, этого сделано не было.",
            "В этот день мне не разрешили есть свою еду. Сотрудники, осуществлявшие конвоирование, не реагировали "
            "на мои жалобы на голод и отклонили мои просьбы выдать мне мою еду. Считаю подобные действия незаконным"
            " запретом распоряжаться собственным имуществом, а также осознанным жестоким обращением.",
        ]]

        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        self.assertEqual(complains_list, true_complains_list)

    def test_transport2(self):
        chat_id = test_consts.test_chat_id
        selected_groups = [questions_entry.QuestionGroupsName.TRANSPORT_FOOD.value]
        selected_place_type = config.ComplainPlaceType.SPlaceTransportFood

        selected_places = [choose_places_to_complain.ComplainPlaceInfo(selected_place_type,
                                                                       test_consts.test_place1,
                                                                       next_name=test_consts.test_court_place1,
                                                                       transfer_date=test_consts.test_date0)]
        bot = bot_dummy.get_question_bot_places(selected_places, selected_groups)

        answers = [
            test_consts.BotCall(1, chat_id),  # 13.	Кормили ли вас завтраком?
            test_consts.BotCall(1, chat_id),  # 13.2.1.	Кормили ли вас обедом?
            test_consts.BotCall(2, chat_id),  # 13.1.1.2.1.	Кормили ли вас ужином?
            test_consts.BotCall(4, chat_id),  # 14.	Позволили ли вам есть свою еду?

        ]
        bot_dummy.answer_questions(bot, answers)
        personal_info = bot.bot_info.personal_info

        true_complains_list = [[
            "10.10.2020, когда меня возили из ИВС Бешенковичского РОВД в"
            " суд Бешенковичского района, со мной "
            "обращались бесчеловечным образом.",
            "В этот день меня не покормили ужином. На основании пункта 1 правила 22 Правил Нельсона Манделы мне "
            "должна была быть обеспечена пища, достаточно питательная для поддержания здоровья и сил, имеющая "
            "достаточно хорошее качество, хорошо приготовленная и поданная. Тем не менее, этого сделано не было.",
            "В этот день мне не разрешили есть свою еду. Сотрудники, осуществлявшие конвоирование, не реагировали "
            "на мои жалобы на голод и отклонили мои просьбы выдать мне мою еду. Считаю подобные действия незаконным"
            " запретом распоряжаться собственным имуществом, а также осознанным жестоким обращением.",
            "Таким образом, в день рассмотрения моего дела в суде из-за отсутствия нормального питания, меня доставили"
            " в суд в состоянии голода и стресса. В день рассмотрения административного дела в суде такое обращение"
            " является особенно непозволительным, поскольку препятствует возможности полноценно защищать себя в суде,"
            " ведь это требует спокойствия, рассудительности, возможности вспомнить все факты и надлежаще изложить "
            "их суду."
        ]]

        questions = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        complains_list = questions.process()
        self.assertEqual(complains_list, true_complains_list)
