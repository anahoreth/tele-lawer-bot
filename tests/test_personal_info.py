import unittest

import config
import create_complain
import dbworker
import keyboard
import messages
import names
import test_consts

import questions.questions_entry as questions_entry
import questions.question_groups.abstract_group as abstract_group


class TestPlace(unittest.TestCase):

    def testPlaceCity(self):
        pl = dbworker.Place()
        city_name_th = "city"
        pl.place_set_parameter(config.PlaceKeys.KeyPlaceCity, city_name_th)
        city_name = pl.place_get_parameter(config.PlaceKeys.KeyPlaceCity)
        self.assertEqual(city_name, city_name_th)


class TestPoliceOffice(unittest.TestCase):

    def testPolice(self):
        for place_list in names.PLACE_DICT.values():
            # print(place_list)
            for place_name in place_list:
                police_office_name = names.get_police_office_name(place_name)
                assert police_office_name, "{}".format(place_name)

