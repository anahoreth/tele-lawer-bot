
test_name = "RR"
test_chat_id = 2
test_start_message = '/start'
test_address = "address"
test_email = "email"

test_year0 = 2020
test_month0 = 10
test_day0 = 10
test_date0 = (test_year0, test_month0, test_day0)
test_date_input0 = "DAY;2020;10;10"

test_year2 = 2022
test_month2 = 12
test_day2 = 12
test_date2 = (test_year2, test_month2, test_day2)
test_date_input2 = "DAY;2022;12;12"

test_date3 = (2024, 2, 3)
test_date_input3 = "DAY;2024;2;3"

test_date4 = (2024, 3, 4)
test_date_input4 = "DAY;2024;3;4"

test_date5 = (2024, 4, 5)
test_date_input5 = "DAY;2024;4;5"


test_release_date = (2023, 1, 2)
test_release_date_input = "DAY;2023;1;2"


test_city1 = 'Витебская область'
test_place1 = "ИВС Бешенковичского РОВД"

test_city2 = 'Витебская область'
test_place2 = "ЦИП Новополоцкого ГОВД"

test_city3 = 'Витебская область'
test_place3 = "ИВС Браславского РОВД"


test_court_city1 = "В Витебской области"
test_court_place1 = "Суд Бешенковичского района"
test_court_city2 = "В Витебской области"
test_court_place2 = "Суд Браславского района"


class Chat(object):
    def __init__(self, chat_id):
        self.id = chat_id


class BotMessage(object):

    def __init__(self, message, chat_id):
        self.text = str(message)
        self.message_id = 3
        self.chat = Chat(chat_id)


class BotCall(object):
    def __init__(self, message, chat_id):
        self.message = BotMessage(message, chat_id)
        self.data = message
