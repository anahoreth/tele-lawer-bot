
import unittest

import bot_dummy
import config
from test_consts import *

import get_city
import form_complain
import create_complain
import show_places_list
from messages import *
import test_bot
import test_bot_before_court


class TestFull(unittest.TestCase):

    @test_bot.is_demo_decorator
    def test2(self):
        # три места после суда
        bot = bot_dummy.DummyBot()
        message = BotMessage('/start', test_chat_id)  # start
        bot.command_handler(message)

        answers = [
            BotMessage(test_email, test_chat_id),  # пользователь вводит эл. почту
            BotMessage(test_city1, test_chat_id),  # 5. пользователь вводит область, где его задержали
            BotMessage(test_place1, test_chat_id),  # 5.2 пользователь вводит место, где его задержали
            BotCall(test_date_input0, test_chat_id),  # 6. дату, когда задержали
            BotCall("Да, меня поместили отбывать арест", test_chat_id),  # 7.	Вас поместили туда по решению суда?
            BotMessage(test_court_city1, test_chat_id),  # 7.1.1.	Какой суд осудил вас?
            BotMessage(test_court_place1, test_chat_id),  # 7.1.1.	Какой суд осудил вас?
            BotCall(test_date_input2, test_chat_id),  # 7.1.2.	Когда этот суд осудил вас?

            BotCall("Вывозили в ЦИП или ИВС", test_chat_id),  # 7.1.3. Во время ареста вас вывозили из ...
            BotMessage(test_city2, test_chat_id),  # 7.1.3.1.1.1.	Куда вас поместили?
            BotMessage(test_place2, test_chat_id),  # 7.1.3.1.1.1.	Куда вас поместили?
            BotCall("DAY;2024;2;3", test_chat_id),  # 7.1.3.1.1.2.	Когда вас туда поместили?

            BotCall("Вывозили в ЦИП или ИВС", test_chat_id),  # 7.1.3. Во время ареста вас вывозили из ...
            BotMessage(test_city3, test_chat_id),  # 7.1.3.1.1.1.	Куда вас поместили?
            BotMessage(test_place3, test_chat_id),  # 7.1.3.1.1.1.	Куда вас поместили?
            BotCall("DAY;2024;3;4", test_chat_id),  # 7.1.3.1.1.2.	Когда вас туда поместили?

            BotCall("Не вывозили", test_chat_id),  # 7.1.3. Во время ареста вас вывозили из ...
            BotCall("Нет", test_chat_id),  # 7.1.4.	Сразу после этого ареста вас снова задержали?
            BotCall(test_release_date_input, test_chat_id),  # 7.1.4.2.1.	Когда вас освободили?

            BotCall("Да", test_chat_id),  # 9.	Проверка:  Верно?
            BotCall(1, test_chat_id),  # 10.	Я хочу пожаловаться на:
            BotCall(2, test_chat_id),  # 10.	Я хочу пожаловаться на:
            BotCall(0, test_chat_id),  # 10.	Я хочу пожаловаться на:

            BotCall(1, test_chat_id),  # 11.	Выберите, на что вы хотите пожаловаться
            BotCall(0, test_chat_id),  # 11.	Выберите, на что вы хотите пожаловаться

            BotCall(1, test_chat_id),  # 11.	Выберите, на что вы хотите пожаловаться
            BotCall(1, test_chat_id),  # 11.	Выберите, на что вы хотите пожаловаться
            BotCall(1, test_chat_id),  # 11.	Выберите, на что вы хотите пожаловаться
            BotCall(2, test_chat_id),  # 11.	Выберите, на что вы хотите пожаловаться


            BotCall(2, test_chat_id),  # 11.	Выберите, на что вы хотите пожаловаться
            BotCall(0, test_chat_id),  # 11.	Выберите, на что вы хотите пожаловаться

            BotCall(2, test_chat_id),  # 11.2.1.	Кормили ли вас в день поступления?
            BotCall(3, test_chat_id),  # 11.2.2.	Устраивало ли вас качество пищи?
            BotCall(2, test_chat_id),  # 11.2.8.	Был ли у вас доступ к питьевой воде?
            BotCall(3, test_chat_id),  # 11.2.9.	Привлекали ли вас к раздаче пищи или мытью посуды?

        ]

        bot_dummy.answer_questions(bot, answers)
        personal_info = bot.bot_info.personal_info
        places = personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyPlacesList)

        self.assertEqual(3, len(places))

        self.assertEqual(
            test_release_date, personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyReleaseDate))

        first_paragraph = create_complain.get_first_paragraph_text(personal_info, test_chat_id)
        first_paragraph_true = "12.12.2022 суд Бешенковичского " \
                               "района вынес в отношении меня административное взыскание в виде административного" \
                               " ареста. 10.10.2020 меня поместили в {}. 03.02.2024 " \
                               "меня вывезли из {} в {}. 04.03.2024 меня вывезли из {} в {}. " \
                               "02.01.2023 меня освободили. ".format(test_place1, test_place1, test_place2,
                                                                     test_place2, test_place3)
        self.assertEqual(first_paragraph, first_paragraph_true)

        questions = personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[0]
        # print (questions)
        complains_list = questions.process()
        true_complains_list = ["Во время содержания в {} меня подвергали опасности заразиться COVID-19. "
                               "Отсутствовала профилактика коронавируса.".format(test_place1),
                               "Сотрудники {} не всегда использовали средства индивидуальной защиты. "
                               "При этом люди могут бессимптомно переносить и распространять данный вирус. "
                               "Я не могу знать наверняка, "
                               "что меня не заразили сотрудники {}.".format(test_place1, test_place1),
                               "Согласно данным Всемирной организации здравоохранения, несмотря на активное принятие "
                               "мер по сдерживанию распространения COVID-19, кумулятивное число заболеваний и "
                               "летальных исходов не снижает темпа роста. Кроме того, согласно последним научным "
                               "исследованиям COVID-19 нередко протекает в лёгкой или средней форме, но в некоторых "
                               "случаях вызывает сильные воспалительные процессы (цитокиновый шторм), которые могут "
                               "привести к смертельной пневмонии и острому респираторному дистресс-синдрому. COVID-19 "
                               "также вызывает иные осложнения, в числе которых острая сердечная недостаточность, "
                               "острая почечная недостаточность, кардиомиопатии, септический шок и другие.",
                               "Перечисленные выше обстоятельства нарушают мое конституционное право на охрану "
                               "здоровья, а также "
                               "требования части 4 статьи 2.4 ПИКоАП, в соответствии с которыми содержание лица, "
                               "задержанного за "
                               "административное правонарушение или административно арестованного, должно "
                               "осуществляться в условиях, "
                               "исключающих угрозу для его жизни и здоровья."]

        self.assertEqual(complains_list[0], true_complains_list)

        questions = personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyComplainQuestions)[1]
        complains_list = questions.process()
        true_complains_list = [
            "Во время моего содержания в {} были нарушены мои права, касающиеся питания. В день "
            "поступления мне не предоставили пищу. Голод угнетал меня и отбирал силы.".format(test_place2),
            "Кроме того, на протяжении всего времени содержания в {} нарушались и другие мои права, "
            "касающиеся питания.".format(test_place2),
            "Правила Нельсона Манделы содержат положения, которые касаются питания. В частности, согласно "
            "пункту 1 правила 22 мне должны были предоставить пищу, которая должна была быть достаточно "
            "питательной для поддержания здоровья и сил, имеющей достаточно хорошее качество, хорошо "
            "приготовленной и поданной. Указанные выше условия нарушают данное Правило.",
            'У меня не всегда был доступ к питьевой воде. Это нарушает пункт 37 Правил № 313. Жажда доставляла'
            ' значительный дискомфорт, мысли часто занимала невозможность попить. Кроме того, достаточное количество'
            ' питьевой воды является необходимым для полноценного функционирования организма. Недостаток воды может'
            ' негативно сказываться на здоровье. Согласно пункту 2 правила 22 Правил Нельсона Манделы должен быть'
            ' обеспечен доступ к питьевой воде, когда возникает такая потребность.',
            "Меня привлекали к раздаче пищи и мытью столовой посуды, что прямо нарушает пункт 107 Правил № 313. "
            "Кроме того, это является принудительным трудом, что запрещено статьей 41 Конституции."
        ]

        self.assertEqual(complains_list[0], true_complains_list)
        self.assertEqual(type(bot.bot_info.get_state(test_chat_id)), form_complain.StateFormComplain)
