import unittest

from test_consts import *

import bot_dummy
import config

import create_complain
import get_city
import show_places_list

import test_bot


class TestBotManyArrests(unittest.TestCase):

    @test_bot.is_demo_decorator
    def test1(self):
        # одно место, сразу после ареста второе место, и освободили
        bot = bot_dummy.DummyBot()
        message = BotMessage('/start', test_chat_id)  # start
        bot.command_handler(message)

        answers = [
            BotMessage(test_email, test_chat_id),  # пользователь вводит эл. почту

            BotMessage(test_city1, test_chat_id),  # 5. пользователь вводит область, где его задержали
            BotMessage(test_place1, test_chat_id),  # 5.2 пользователь вводит место, где его задержали
            BotCall("DAY;2020;10;10", test_chat_id),  # 6. дату, когда задержали
            BotCall("Да, меня поместили отбывать арест", test_chat_id),  # 7.	Вас поместили туда по решению суда?
            BotMessage(test_court_city1, test_chat_id),  # 7.1.1.	Какой суд осудил вас?
            BotMessage(test_court_place1, test_chat_id),  # 7.1.1.	Какой суд осудил вас?
            BotCall("DAY;2022;12;12", test_chat_id),  # 7.1.2.	Когда этот суд осудил вас?
            BotCall("Не вывозили", test_chat_id),  # 7.1.3. Во время ареста вас вывозили из ...
            BotCall("Да", test_chat_id),  # 7.1.4.	Сразу после этого ареста вас снова задержали?
            BotCall("DAY;2024;2;3", test_chat_id),   # 7.1.4.1.1. Когда закончился ваш пред арест?

            BotMessage(test_city2, test_chat_id),  # 5. пользователь вводит область, где его задержали
            BotMessage(test_place2, test_chat_id),  # 5.2 пользователь вводит место, где его задержали
            BotCall("DAY;2024;3;4", test_chat_id),  # 6. дату, когда задержали
            BotCall("Да, меня поместили отбывать арест", test_chat_id),  # 7.	Вас поместили туда по решению суда?
            BotMessage(test_court_city2, test_chat_id),  # 7.1.1.	Какой суд осудил вас?
            BotMessage(test_court_place2, test_chat_id),  # 7.1.1.	Какой суд осудил вас?
            BotCall("DAY;2024;4;5", test_chat_id),  # 7.1.2.	Когда этот суд осудил вас?
            BotCall("Не вывозили", test_chat_id),  # 7.1.3. Во время ареста вас вывозили из ...
            BotCall("Нет", test_chat_id),  # 7.1.4.	Сразу после этого ареста вас снова задержали?

            BotCall("DAY;2023;1;2", test_chat_id),  # 7.1.4.2.1.	Когда вас освободили?
        ]

        bot_dummy.answer_questions(bot, answers)

        personal_info = bot.bot_info.personal_info
        places = personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyPlacesList)

        self.assertEqual(2, len(places))

        place0_dict = {
            config.PlaceKeys.KeyPlaceCity: test_city1,
            config.PlaceKeys.KeyPlaceName: test_place1,
            config.PlaceKeys.KeyPlaceIsCourt: False,
            config.PlaceKeys.KeyPlaceIsFirst: True,
            config.PlaceKeys.KeyPlaceIsLast: True,
            config.PlaceKeys.KeyPlaceIsAfterCourt: True,
        }

        place1_dict = {
            config.PlaceKeys.KeyPlaceCity: test_city2,
            config.PlaceKeys.KeyPlaceName: test_place2,
            config.PlaceKeys.KeyPlaceIsCourt: False,
            config.PlaceKeys.KeyPlaceIsFirst: True,
            config.PlaceKeys.KeyPlaceIsLast: True,
            config.PlaceKeys.KeyPlaceIsAfterCourt: True,
        }

        test_bot.verify_place(places[0], place0_dict, self)
        test_bot.verify_place(places[1], place1_dict, self)

        self.assertEqual(
            test_release_date, personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyReleaseDate))

        self.assertEqual(type(bot.bot_info.get_state(test_chat_id)), show_places_list.StatePlaceShowList)

        first_paragraph = create_complain.get_first_paragraph_text(personal_info, test_chat_id)
        first_paragraph_true = "12.12.2022 суд Бешенковичского " \
                               "района вынес в отношении меня административное взыскание в виде административного" \
                               " ареста. 10.10.2020 меня поместили в {}. " \
                               "03.02.2024 мой первый административный арест закончился. " \
                               "05.04.2024 {} вынес в отношении меня административное взыскание в виде " \
                               "административного ареста. " \
                               "04.03.2024 меня поместили в {}. " \
                               "02.01.2023 меня освободили. ".format(test_place1,
                                                                     "суд Браславского района", test_place2)

        self.assertEqual(first_paragraph, first_paragraph_true)
