import unittest

import bot_dummy
import config
from test_consts import *

import get_city
import create_complain
import show_places_list
from messages import *


def is_demo_decorator(func):
    def wrapper(*args, **kwargs):
        is_real_bot = config.IS_REAL_BOT
        config.IS_REAL_BOT = False
        is_demo = config.IS_DEMO
        config.IS_DEMO = True

        res = func(*args, **kwargs)
        config.IS_DEMO = is_demo
        config.IS_REAL_BOT = is_real_bot
        return res
    return wrapper


def not_demo_decorator(func):
    def wrapper(*args, **kwargs):
        is_real_bot = config.IS_REAL_BOT
        config.IS_REAL_BOT = False

        is_demo = config.IS_DEMO
        config.IS_DEMO = False
        res = func(*args, **kwargs)
        config.IS_DEMO = is_demo
        config.IS_REAL_BOT = is_real_bot
        return res
    return wrapper


def verify_place(place, dict, object):
    for key, value in dict.items():
        # print(key, value)
        object.assertEqual(value, place.place_get_parameter(key), key)


class TestBot(unittest.TestCase):

    @not_demo_decorator
    def testUntilStartDate(self):
        # start
        bot = bot_dummy.DummyBot()
        message = BotMessage('/start', test_chat_id)
        bot.command_handler(message)

        # пользователь вводит имя
        last_message = bot.get_last_message(test_chat_id)
        self.assertEqual(last_message, "Введите фамилию, имя и отчество\n(в именительном падеже через пробел)")

        message = BotMessage(test_name, test_chat_id)
        bot.message_handler(message)

        # 3. пользователь вводит адрес проживания
        last_message = bot.get_last_message(test_chat_id)
        self.assertEqual(last_message, "Введите Ваш адрес проживания")
        message = BotMessage(test_address, test_chat_id)
        bot.message_handler(message)

        # 4. пользователь вводит адрес эл. почты
        last_message = bot.get_last_message(test_chat_id)
        self.assertEqual(last_message, "Введите электронную почту для получения ответа на жалобу")
        message = BotMessage(test_email, test_chat_id)
        bot.message_handler(message)

        # 5. пользователь вводит область, где его задержали
        last_message = bot.get_last_message(test_chat_id)

        self.assertEqual(last_message, get_city.MESSAGE_GET_CITY_FIRST)
        message = BotMessage(test_city1, test_chat_id)
        bot.message_handler(message)

        # 5.2 пользователь вводит место, где его задержали
        last_message = bot.get_last_message(test_chat_id)
        self.assertEqual(last_message, MESSAGE_GET_PLACE)
        message = BotMessage(test_place1, test_chat_id)
        bot.message_handler(message)

    @is_demo_decorator
    def testWhenIsDemo(self):
        # start
        bot = bot_dummy.DummyBot()
        message = BotMessage('/start', test_chat_id)
        bot.command_handler(message)

        # пользователь вводит эл. почту
        last_message = bot.get_last_message(test_chat_id)
        self.assertEqual(last_message, "Введите электронную почту для получения ответа на жалобу")
        message = BotMessage(test_email, test_chat_id)
        bot.message_handler(message)

        # 5. пользователь вводит область, где его задержали
        last_message = bot.get_last_message(test_chat_id)
        self.assertEqual(last_message, get_city.MESSAGE_GET_CITY_FIRST)
        message = BotMessage(test_city1, test_chat_id)
        bot.message_handler(message)

        # 5.2 пользователь вводит место, где его задержали
        last_message = bot.get_last_message(test_chat_id)
        self.assertEqual(last_message, MESSAGE_GET_PLACE)
        message = BotMessage(test_place1, test_chat_id)
        bot.message_handler(message)

        # 6 пользователь вводит дату задержания
        last_message = bot.get_last_message(test_chat_id)
        self.assertEqual(last_message, MESSAGE_GET_START_DATE_FIRST)

        personal_info = bot.bot_info.personal_info
        my_email = personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyEmail)
        self.assertEqual(test_email, my_email)

        my_places = personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyPlacesList)
        self.assertEqual(1, len(my_places))
