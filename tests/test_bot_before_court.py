
import unittest

import bot_dummy
import config
from test_consts import *
import states.show_places_list as show_places_list
import create_complain
import test_bot
import states.ask_question as ask_question
import states.choose_questions_category as choose_questions_category


def continue_to_choose_places(bot, personal_info, chat_id, first_paragraph_true,
                              true_message_places_complain, chosen_places_numbers, is_first_place_prison, object):
    to_bot = BotCall("Да", chat_id)  # 9.1 Верно?
    bot.query_handler(to_bot)

    message_places_complain = bot.get_last_message(test_chat_id)
    # print(true_message_places_complain)
    # print(message_places_complain)
    object.assertEqual(message_places_complain, true_message_places_complain)

    first_paragraph = create_complain.get_first_paragraph_text(personal_info, test_chat_id)
    # print(first_paragraph_true)
    # print(first_paragraph)
    object.assertEqual(first_paragraph, first_paragraph_true)

    for n in chosen_places_numbers:
        to_bot = BotCall(n, test_chat_id)  # номер места
        bot.query_handler(to_bot)
    to_bot = BotCall(0, test_chat_id)  # ГОТОВО
    bot.query_handler(to_bot)
    state = bot.bot_info.get_state(test_chat_id)
    if is_first_place_prison:
        object.assertEqual(type(state), choose_questions_category.StateGetCategories)
    else:
        object.assertEqual(type(state), ask_question.StateComplainQuestions)


class TestBotBeforeCourt(unittest.TestCase):

    @test_bot.is_demo_decorator
    def test1(self):
        # одно место до cуда, судили, и освободили
        bot = bot_dummy.DummyBot()
        message = BotMessage('/start', test_chat_id)  # start
        bot.command_handler(message)

        answers = [
            BotMessage(test_email, test_chat_id),  # пользователь вводит эл. почту
            BotMessage(test_city1, test_chat_id),  # 5. пользователь вводит область, где его задержали
            BotMessage(test_place1, test_chat_id),  # 5.2 пользователь вводит место, где его задержали
            BotCall(test_date_input0, test_chat_id),  # 6. дату, когда задержали
            BotCall("Нет. Меня задержали до суда", test_chat_id),  # 7.	Вас поместили туда по решению суда?
            BotCall("Нет", test_chat_id),  # 7.2.1.	Вас перевозили до суда из [прошлый МПС] в другой ИВС или ЦИП?

            BotCall("Нет. Cуд был по видеосвязи", test_chat_id),  # 7.2.2.	Вас возили в суд из [прошлый МПС]?
            BotCall("Да", test_chat_id),  # 7.2.2.2.1.	После суда вас освободили?

            BotMessage(test_court_city1, test_chat_id),  # 7.2.2.2.1.1.1.	Какой суд вас освободил?
            BotMessage(test_court_place1, test_chat_id),  # 7.2.2.2.1.1.1.	Какой суд вас освободил?
            BotCall(test_release_date_input, test_chat_id),  # 7.1.4.2.1.	Когда вас освободили?
        ]
        bot_dummy.answer_questions(bot, answers)
        personal_info = bot.bot_info.personal_info
        places = personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyPlacesList)
        self.assertEqual(1, len(places))
        dict_place = {
            config.PlaceKeys.KeyPlaceCity: test_city1,
            config.PlaceKeys.KeyPlaceName: test_place1,
            config.PlaceKeys.KeyPlaceIsCourt: False,
            config.PlaceKeys.KeyPlaceIsAfterCourt: False,
            config.PlaceKeys.KeyPlaceCourtCity: test_court_city1,
            config.PlaceKeys.KeyPlaceCourtPlace: test_court_place1,
        }
        test_bot.verify_place(places[0], dict_place, self)
        self.assertEqual(
            test_release_date, personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyReleaseDate))

        true_message_places_complain = "Я хочу пожаловаться на:\n\n" \
                                       "1. Условия содержания в ИВС Бешенковичского РОВД\n\n"\
                                       "(здесь и далее выбранное будет выделяться *жирным*)\n"

        first_paragraph_true = "10.10.2020 меня поместили до суда в ИВС " \
                               "Бешенковичского РОВД. 02.01.2023 суд Бешенковичского района по " \
                               "видеосвязи рассмотрел административное дело в отношении меня. В результате " \
                               "рассмотрения дела меня освободили. "

        choosen_places_numbers = [1]
        is_first_place_prison = True
        continue_to_choose_places(bot, personal_info, test_chat_id, first_paragraph_true,
                                  true_message_places_complain, choosen_places_numbers, is_first_place_prison, self)

    @test_bot.is_demo_decorator
    def test2(self):
        # два места до cуда, судили, и освободили
        bot = bot_dummy.DummyBot()
        message = BotMessage('/start', test_chat_id)  # start
        bot.command_handler(message)

        answers = [
            BotMessage(test_email, test_chat_id),  # пользователь вводит эл. почту
            BotMessage(test_city1, test_chat_id),  # 5. пользователь вводит область, где его задержали
            BotMessage(test_place1, test_chat_id),  # 5.2 пользователь вводит место, где его задержали
            BotCall(test_date_input0, test_chat_id),  # 6. дату, когда задержали
            BotCall("Нет. Меня задержали до суда", test_chat_id),  # 7.	Вас поместили туда по решению суда?
            BotCall("Да", test_chat_id),  # 7.2.1.	Вас перевозили до суда из [прошлый МПС] в другой ИВС или ЦИП?
            BotMessage(test_city2, test_chat_id),  # 7.2.1.1.1.	Куда вас поместили?
            BotMessage(test_place2, test_chat_id),  # 7.2.1.1.1.	Куда вас поместили?
            BotCall("DAY;2021;1;1", test_chat_id),  # 7.2.1.1.2.	Когда вас туда поместили?
            BotCall("Нет", test_chat_id),  # 7.2.1.	Вас перевозили до суда из [прошлый МПС] в другой ИВС или ЦИП?
            BotCall("Нет. Cуд был по видеосвязи", test_chat_id),  # 7.2.2.	Вас возили в суд из [прошлый МПС]?
            BotCall("Да", test_chat_id),  # 7.2.2.2.1.	После суда вас освободили?
            BotMessage(test_court_city1, test_chat_id),  # 7.2.2.2.1.1.1.	Какой суд вас освободил?
            BotMessage(test_court_place1, test_chat_id),  # 7.2.2.2.1.1.1.	Какой суд вас освободил?
            BotCall(test_release_date_input, test_chat_id),  # 7.1.4.2.1.	Когда вас освободили?
        ]

        bot_dummy.answer_questions(bot, answers)
        personal_info = bot.bot_info.personal_info
        places = personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyPlacesList)
        self.assertEqual(len(places), 2)

        dict_place0 = {
            config.PlaceKeys.KeyPlaceCity: test_city1,
            config.PlaceKeys.KeyPlaceStartDate: test_date0,
            config.PlaceKeys.KeyPlaceName: test_place1,
            config.PlaceKeys.KeyPlaceIsCourt: False,
            config.PlaceKeys.KeyPlaceIsFirst: True,
            config.PlaceKeys.KeyPlaceIsLast: False,
            config.PlaceKeys.KeyPlaceIsAfterCourt: False,
        }
        dict_place1 = {
            config.PlaceKeys.KeyPlaceStartDate: (2021, 1, 1),
            config.PlaceKeys.KeyPlaceName: test_place2,
            config.PlaceKeys.KeyPlaceIsCourt: False,
            config.PlaceKeys.KeyPlaceIsFirst: False,
            config.PlaceKeys.KeyPlaceIsLast: True,
            config.PlaceKeys.KeyPlaceIsAfterCourt: False,
            config.PlaceKeys.KeyPlaceCourtCity: test_court_city1,
            config.PlaceKeys.KeyPlaceCourtPlace: test_court_place1,
            config.PlaceKeys.KeyPlaceIsReleasedInCourt: True,
        }

        test_bot.verify_place(places[0], dict_place0, self)
        test_bot.verify_place(places[1], dict_place1, self)

        self.assertEqual(
            test_release_date, personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyReleaseDate))

        true_message_places_complain = "Я хочу пожаловаться на:\n\n" \
                                       "1. Условия содержания в {}\n".format(test_place1) + \
                                       "2. Условия содержания в {}\n".format(test_place2) + \
                                       "3. Отсутствие питания в день поездки из {} в {} " \
                                       "01.01.2021\n".format(test_place1, test_place2) + \
                                       "4. Условия содержания во время поездки из {} " \
                                       "в {} 01.01.2021\n".format(test_place1, test_place2) + \
                                       "\n(здесь и далее выбранное будет выделяться *жирным*)\n"

        first_paragraph_true = "10.10.2020 меня поместили до суда в {}. " \
                               "01.01.2021 меня вывезли из {} в {}. " \
                               "02.01.2023 {} по " \
                               "видеосвязи рассмотрел административное дело в отношении меня. " \
                               "В результате рассмотрения дела меня освободили. ".format(test_place1,
                                                                                         test_place1, test_place2,
                                                                                         "суд Бешенковичского района")
        choosen_places_numbers = [3, 4]
        is_first_place_prison = False

        continue_to_choose_places(bot, personal_info, test_chat_id, first_paragraph_true,
                                  true_message_places_complain, choosen_places_numbers, is_first_place_prison, self)

    @test_bot.is_demo_decorator
    def test3(self):
        # место до cуда, судили в суде, и освободили
        bot = bot_dummy.DummyBot()
        message = BotMessage('/start', test_chat_id)  # start
        bot.command_handler(message)

        answers = [
            BotMessage(test_email, test_chat_id),  # пользователь вводит эл. почт
            BotMessage(test_city1, test_chat_id),  # 5. пользователь вводит область, где его задержали
            BotMessage(test_place1, test_chat_id),  # 5.2 пользователь вводит место, где его задержали
            BotCall(test_date_input0, test_chat_id),  # 6. дату, когда задержали
            BotCall("Нет. Меня задержали до суда", test_chat_id),  # 7.	Вас поместили туда по решению суда?
            BotCall("Нет", test_chat_id),  # 7.2.1.	Вас перевозили до суда из [прошлый МПС] в другой ИВС или ЦИП?
            BotCall("Нет. Cуд был по видеосвязи", test_chat_id),  # 7.2.2.	Вас возили в суд из [прошлый МПС]?
            BotCall("Нет", test_chat_id),  # 7.2.2.2.1.	После суда вас освободили?
            BotMessage(test_court_city1, test_chat_id),  # 7.2.2.2.1.1.1.	Какой суд осудил вас на сутки?
            BotMessage(test_court_place1, test_chat_id),  # 7.2.2.2.1.2.1.	Какой суд осудил вас на сутки?
            BotCall("DAY;2022;12;12", test_chat_id),  # 7.2.2.2.1.2.2.	Когда вас осудили на сутки?
            BotMessage("Витебская область", test_chat_id),  # 8.3.2.1.1.1.	В каком ЦИП или ИВС вы начали отбывать арест?
            BotMessage("ЦИП Новополоцкого ГОВД", test_chat_id),  # 8.3.2.1.1.1. # В каком вы начали отбывать арест?
            BotCall("Не вывозили", test_chat_id),  # 7.1.3. Во время ареста вас вывозили из ...
            BotCall("Нет", test_chat_id),  # 7.1.4.	Сразу после этого ареста вас снова задержали?
            BotCall("DAY;2023;1;2", test_chat_id),  # 7.1.4.2.1.	Когда вас освободили?
        ]
        bot_dummy.answer_questions(bot, answers)
        personal_info = bot.bot_info.personal_info
        places = personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyPlacesList)
        self.assertEqual(len(places), 2)
        dict_place0 = {
            config.PlaceKeys.KeyPlaceCity: test_city1,
            config.PlaceKeys.KeyPlaceStartDate: test_date0,
            config.PlaceKeys.KeyPlaceName: test_place1,
            config.PlaceKeys.KeyPlaceIsCourt: False,
            config.PlaceKeys.KeyPlaceIsFirst: True,
            config.PlaceKeys.KeyPlaceIsLast: False,
            config.PlaceKeys.KeyPlaceIsReleasedInCourt: False,
            config.PlaceKeys.KeyPlaceCourtDate: (2022, 12, 12),
            config.PlaceKeys.KeyPlaceCourtPlace: test_court_place1,
            config.PlaceKeys.KeyPlaceIsAfterCourt: False,
        }

        dict_place1 = {
            config.PlaceKeys.KeyPlaceStartDate: (2022, 12, 12),
            config.PlaceKeys.KeyPlaceName: test_place2,
            config.PlaceKeys.KeyPlaceIsCourt: False,
            config.PlaceKeys.KeyPlaceIsFirst: False,
            config.PlaceKeys.KeyPlaceIsLast: True,
            config.PlaceKeys.KeyPlaceIsAfterCourt: True,
        }

        test_bot.verify_place(places[0], dict_place0, self)
        test_bot.verify_place(places[1], dict_place1, self)

        self.assertEqual(
            (2023, 1, 2), personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyReleaseDate))

        true_message_places_complain = "Я хочу пожаловаться на:\n\n" \
                                       "1. Условия содержания в {}\n".format(test_place1) + \
                                       "2. Условия содержания в {}\n".format(test_place2) + \
                                       "3. Отсутствие питания в день поездки из {} в {} 12.12.2022\n".format(
                                           test_place1, test_place2) + \
                                       "4. Условия содержания во время поездки из {} в {} 12.12.2022\n".format(
                                           test_place1, test_place2) + \
                                       "\n(здесь и далее выбранное будет выделяться *жирным*)\n"

        first_paragraph_true = "10.10.2020 меня поместили до суда в ИВС Бешенковичского РОВД. " \
                               "12.12.2022 суд Бешенковичского района по " \
                               "видеосвязи рассмотрел административное дело в отношении меня. Суд вынес в отношении" \
                               " меня административное взыскание в виде административного ареста. " \
                               "После суда меня поместили в ЦИП Новополоцкого ГОВД для отбытия административного" \
                               " ареста. 02.01.2023 меня освободили. "

        choosen_places_numbers = [1, 3]
        is_first_place_prison = True

        continue_to_choose_places(bot, personal_info, test_chat_id, first_paragraph_true,
                                  true_message_places_complain, choosen_places_numbers, is_first_place_prison, self)

    @test_bot.is_demo_decorator
    def test4(self):
        # возили в cуд и освободили
        bot = bot_dummy.DummyBot()
        message = BotMessage('/start', test_chat_id)  # start
        bot.command_handler(message)

        answers = [
            BotMessage(test_email, test_chat_id),  # пользователь вводит эл. почт
            BotMessage(test_city1, test_chat_id),  # 5. пользователь вводит область, где его задержали
            BotMessage(test_place1, test_chat_id),  # 5.2 пользователь вводит место, где его задержали
            BotCall("DAY;2024;2;3", test_chat_id),  # 6. дату, когда задержали

            BotCall("Нет. Меня задержали до суда", test_chat_id),  # 7.	Вас поместили туда по решению суда?
            BotCall("Нет", test_chat_id),  # 7.2.1.	Вас перевозили до суда из [прошлый МПС] в другой ИВС или ЦИП?

            BotCall("Да", test_chat_id),  # 7.2.2.	Вас возили в суд из [прошлый МПС]?
            BotCall("DAY;2022;12;12", test_chat_id),  # 8.1.	Когда вас отвезли в суд?
            BotMessage(test_court_city1, test_chat_id),  # 8.2.	В какой суд вас отвезли?
            BotMessage(test_court_place1, test_chat_id),  # 8.2.В какой суд вас отвезли?
            BotCall("Да", test_chat_id),  # 8.3.	В суде вас освободили?
        ]

        bot_dummy.answer_questions(bot, answers)
        personal_info = bot.bot_info.personal_info
        places = personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyPlacesList)
        self.assertEqual(len(places), 2)

        place_dict0 = {
            config.PlaceKeys.KeyPlaceName: test_place1,
            config.PlaceKeys.KeyPlaceStartDate: (2024, 2, 3),
            config.PlaceKeys.KeyPlaceIsCourt: False,
            config.PlaceKeys.KeyPlaceIsAfterCourt: False,
            config.PlaceKeys.KeyPlaceIsFirst: True,
            config.PlaceKeys.KeyPlaceIsLast: False,
        }

        place_dict1 = {
            config.PlaceKeys.KeyPlaceName: test_court_place1,
            config.PlaceKeys.KeyPlaceStartDate: (2022, 12, 12),
            config.PlaceKeys.KeyPlaceIsCourt: True,
            config.PlaceKeys.KeyPlaceIsAfterCourt: False,
            config.PlaceKeys.KeyPlaceIsFirst: False,
            config.PlaceKeys.KeyPlaceIsLast: True,
        }

        test_bot.verify_place(places[0], place_dict0, self)
        test_bot.verify_place(places[1], place_dict1, self)

        self.assertEqual(
            (2022, 12, 12), personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyReleaseDate))

        true_message_places_complain = "Я хочу пожаловаться на:\n\n" \
                                       "1. Условия содержания в {}\n".format(test_place1) + \
                                       "2. Отсутствие питания в день поездки из {} в {} 12.12.2022\n".format(
                                           test_place1, "суд Бешенковичского района") + \
                                       "3. Условия содержания во время поездки из {} в {} 12.12.2022\n".format(
                                           test_place1, "суд Бешенковичского района") + \
                                       "\n(здесь и далее выбранное будет выделяться *жирным*)\n"

        first_paragraph_true = "03.02.2024 меня поместили до суда в {}. " \
                               "12.12.2022 меня вывезли из {} в {}. "\
                               "В результате рассмотрения административного дела в " \
                               "суде меня освободили. ".format(test_place1, test_place1, "суд Бешенковичского района")

        choosen_places_numbers = [2, 3]

        is_first_place_prison = False

        continue_to_choose_places(bot, personal_info, test_chat_id, first_paragraph_true,
                                  true_message_places_complain, choosen_places_numbers, is_first_place_prison, self)

    @test_bot.is_demo_decorator
    def test5(self):
        # до суда, потом перевезли в суд, потом еще один арест, и потом освободили
        bot = bot_dummy.DummyBot()
        message = BotMessage('/start', test_chat_id)  # start
        bot.command_handler(message)

        answers = [
            BotMessage(test_email, test_chat_id),  # пользователь вводит эл. почт
            BotMessage(test_city1, test_chat_id),  # 5. пользователь вводит область, где его задержали
            BotMessage(test_place1, test_chat_id),  # 5.2 пользователь вводит место, где его задержали
            BotCall("DAY;2020;10;10", test_chat_id),  # 6. дату, когда задержали

            BotCall("Нет. Меня задержали до суда", test_chat_id),  # 7.	Вас поместили туда по решению суда?
            BotCall("Нет", test_chat_id),  # 7.2.1.	Вас перевозили до суда из [прошлый МПС] в другой ИВС или ЦИП?

            BotCall("Да", test_chat_id),  # 7.2.2.	Вас возили в суд из [прошлый МПС]?
            BotCall("DAY;2022;12;12", test_chat_id),  # 8.1.	Когда вас отвезли в суд?
            BotMessage(test_court_city1, test_chat_id),  # 8.2.	В какой суд вас отвезли?
            BotMessage(test_court_place1, test_chat_id),  # 8.2.В какой суд вас отвезли?

            BotCall("Нет", test_chat_id),  # 8.3.	В суде вас освободили?
            BotCall("Да", test_chat_id),  # 8.3.2.1.	Суд  дал вам сутки?
            BotMessage("Витебская область", test_chat_id),  # 8.3.2.1.1.1.	В каком ЦИП или ИВС вы начали отбывать арест?
            BotMessage("ЦИП Новополоцкого ГОВД", test_chat_id),  # 8.3.2.1.1.1. # В каком вы начали отбывать арест?
            BotCall("Не вывозили", test_chat_id),  # 7.1.3. Во время ареста вас вывозили из ...
            BotCall("Нет", test_chat_id),  # 7.1.4.	Сразу после этого ареста вас снова задержали?
            BotCall("DAY;2023;1;2", test_chat_id),  # 7.1.4.2.1.	Когда вас освободили?
        ]

        bot_dummy.answer_questions(bot, answers)
        personal_info = bot.bot_info.personal_info
        places = personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyPlacesList)
        self.assertEqual(len(places), 3)

        place0_dict = {
            config.PlaceKeys.KeyPlaceName: test_place1,
            config.PlaceKeys.KeyPlaceStartDate: (2020, 10, 10),
            config.PlaceKeys.KeyPlaceIsFirst: True,
            config.PlaceKeys.KeyPlaceIsLast: False,
            config.PlaceKeys.KeyPlaceIsAfterCourt: False,
        }

        place1_dict = {
            config.PlaceKeys.KeyPlaceIsFirst: False,
            config.PlaceKeys.KeyPlaceStartDate: (2022, 12, 12),
            config.PlaceKeys.KeyPlaceName: test_court_place1,
            config.PlaceKeys.KeyPlaceIsLast: False,
            config.PlaceKeys.KeyPlaceIsCourt: True,
            config.PlaceKeys.KeyPlaceIsAfterCourt: False,
        }

        place2_dict = {
            config.PlaceKeys.KeyPlaceIsFirst: False,
            config.PlaceKeys.KeyPlaceName: "ЦИП Новополоцкого ГОВД",
            config.PlaceKeys.KeyPlaceStartDate: (2022, 12, 12),
            config.PlaceKeys.KeyPlaceIsLast: True,
            config.PlaceKeys.KeyPlaceIsCourt: False,
            config.PlaceKeys.KeyPlaceIsAfterCourt: True,
        }

        test_bot.verify_place(places[0], place0_dict, self)
        test_bot.verify_place(places[1], place1_dict, self)
        test_bot.verify_place(places[2], place2_dict, self)

        self.assertEqual(
            (2023, 1, 2), personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyReleaseDate))

        true_message_places_complain = "Я хочу пожаловаться на:\n\n" \
                                       "1. Условия содержания в {}\n".format(test_place1) + \
                                       "2. Условия содержания в {}\n".format("ЦИП Новополоцкого ГОВД") + \
                                       "3. Отсутствие питания в день поездки " \
                                       "из {} в {} и из {} в {} 12.12.2022\n".format(test_place1,
                                                                                     "суд Бешенковичского района",
                                                                                     "суда Бешенковичского района",
                                                                                     test_place2) + \
                                       "4. Условия содержания во время поездки из {} в {} 12.12.2022\n".format(
                                           test_place1, "суд Бешенковичского района") + \
                                       "5. Условия содержания во время поездки из {} в {} 12.12.2022\n".format(
                                           "суда Бешенковичского района", test_place2) + \
                                       "\n(здесь и далее выбранное будет выделяться *жирным*)\n"

        first_paragraph_true = "10.10.2020 меня поместили до суда в {}. " \
                               "12.12.2022 меня вывезли из {} в {}. " \
                               "Суд вынес в отношении меня административное взыскание в виде административного " \
                               "ареста. После суда меня поместили в ЦИП Новополоцкого ГОВД для отбытия " \
                               "административного ареста. " \
                               "02.01.2023 меня освободили. ".format(test_place1, test_place1,
                                                                     "суд Бешенковичского района")
        chosen_places_numbers = [4, 5]
        is_first_place_prison = False

        continue_to_choose_places(bot, personal_info, test_chat_id, first_paragraph_true,
                                  true_message_places_complain, chosen_places_numbers, is_first_place_prison, self)

    @test_bot.is_demo_decorator
    def test6(self):

        bot = bot_dummy.DummyBot()
        message = BotMessage('/start', test_chat_id)  # start
        bot.command_handler(message)

        answers = [
            BotMessage(test_email, test_chat_id),  # пользователь вводит эл. почт
            BotMessage(test_city1, test_chat_id),  # 5. пользователь вводит область, где его задержали
            BotMessage(test_place1, test_chat_id),  # 5.2 пользователь вводит место, где его задержали
            BotCall("DAY;2020;10;10", test_chat_id),  # 6. дату, когда задержали

            BotCall("Нет. Меня задержали до суда", test_chat_id),  # 7.	Вас поместили туда по решению суда?
            BotCall("Да", test_chat_id),  # 7.2.1.	Вас перевозили до суда из [прошлый МПС] в другой ИВС или ЦИП?

            BotMessage(test_city2, test_chat_id),  # 7.2.1.1.1.	Куда вас поместили?
            BotMessage(test_place2, test_chat_id),  # 7.2.1.1.1.	Куда вас поместили?
            BotCall("DAY;2021;1;1", test_chat_id),  # 7.2.1.1.2.	Когда вас туда поместили?

            BotCall("Нет", test_chat_id),  # 7.2.1.	Вас перевозили до суда из [прошлый МПС] в другой ИВС или ЦИП?

            BotCall("Да", test_chat_id),  # 7.2.2.	Вас возили в суд из [прошлый МПС]?
            BotCall("DAY;2022;12;12", test_chat_id),  # 8.1.	Когда вас отвезли в суд?
            BotMessage(test_court_city1, test_chat_id),  # 8.2.	В какой суд вас отвезли?
            BotMessage(test_court_place1, test_chat_id),  # 8.2.В какой суд вас отвезли?

            BotCall("Нет", test_chat_id),  # 8.3.	В суде вас освободили?
            BotCall("Да", test_chat_id),  # 8.3.2.1.	Суд  дал вам сутки?
            BotMessage("Витебская область", test_chat_id),  # 8.3.2.1.1.1.	В каком ЦИП или ИВС вы начали отбывать арест?
            BotMessage("ЦИП Новополоцкого ГОВД", test_chat_id),  # 8.3.2.1.1.1. # В каком вы начали отбывать арест?

            BotCall("Вывозили в ЦИП или ИВС", test_chat_id),  # 7.1.3. Во время ареста вас вывозили из ...
            BotMessage(test_city3, test_chat_id),  # 7.1.3.1.1.1.	Куда вас поместили?
            BotMessage(test_place3, test_chat_id),  # 7.1.3.1.1.1.	Куда вас поместили?
            BotCall("DAY;2024;3;4", test_chat_id),  # 7.1.3.1.1.2.	Когда вас туда поместили?

            BotCall("Не вывозили", test_chat_id),  # 7.1.3. Во время ареста вас вывозили из ...
            BotCall("Нет", test_chat_id),  # 7.1.4.	Сразу после этого ареста вас снова задержали?
            BotCall(test_release_date_input, test_chat_id),  # 7.1.4.2.1.	Когда вас освободили?
        ]

        bot_dummy.answer_questions(bot, answers)
        personal_info = bot.bot_info.personal_info
        places = personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyPlacesList)
        self.assertEqual(len(places), 5)

        dict_place0 = {
            config.PlaceKeys.KeyPlaceStartDate: (2020, 10, 10),
            config.PlaceKeys.KeyPlaceName: test_place1,
            config.PlaceKeys.KeyPlaceIsCourt: False,
            config.PlaceKeys.KeyPlaceIsFirst: True,
            config.PlaceKeys.KeyPlaceIsLast: False,
            config.PlaceKeys.KeyPlaceIsAfterCourt: False,
        }
        dict_place1 = {
            config.PlaceKeys.KeyPlaceStartDate: (2021, 1, 1),
            config.PlaceKeys.KeyPlaceName: test_place2,
            config.PlaceKeys.KeyPlaceIsCourt: False,
            config.PlaceKeys.KeyPlaceIsFirst: False,
            config.PlaceKeys.KeyPlaceIsLast: False,
            config.PlaceKeys.KeyPlaceIsAfterCourt: False,
        }
        dict_place2 = {
            config.PlaceKeys.KeyPlaceStartDate: (2022, 12, 12),
            config.PlaceKeys.KeyPlaceName: test_court_place1,
            config.PlaceKeys.KeyPlaceIsCourt: True,
            config.PlaceKeys.KeyPlaceIsFirst: False,
            config.PlaceKeys.KeyPlaceIsLast: False,
            config.PlaceKeys.KeyPlaceIsAfterCourt: False,
        }
        dict_place3 = {
            config.PlaceKeys.KeyPlaceStartDate: (2022, 12, 12),
            config.PlaceKeys.KeyPlaceName: "ЦИП Новополоцкого ГОВД",
            config.PlaceKeys.KeyPlaceIsCourt: False,
            config.PlaceKeys.KeyPlaceIsFirst: False,
            config.PlaceKeys.KeyPlaceIsLast: False,
            config.PlaceKeys.KeyPlaceIsAfterCourt: True,
        }
        dict_place4 = {
            config.PlaceKeys.KeyPlaceStartDate: (2024, 3, 4),
            config.PlaceKeys.KeyPlaceName: test_place3,
            config.PlaceKeys.KeyPlaceIsCourt: False,
            config.PlaceKeys.KeyPlaceIsFirst: False,
            config.PlaceKeys.KeyPlaceIsLast: True,
            config.PlaceKeys.KeyPlaceIsAfterCourt: True,
        }

        test_bot.verify_place(places[0], dict_place0, self)
        test_bot.verify_place(places[1], dict_place1, self)
        test_bot.verify_place(places[2], dict_place2, self)
        test_bot.verify_place(places[3], dict_place3, self)
        test_bot.verify_place(places[4], dict_place4, self)

        self.assertEqual(
            (2023, 1, 2), personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyReleaseDate))

        true_message_places_complain = "Я хочу пожаловаться на:\n\n" \
                                       "1. Условия содержания в {}\n".format(test_place1) + \
                                       "2. Условия содержания в {}\n".format(test_place2) + \
                                       "3. Условия содержания в {}\n".format(test_place3) + \
                                       "4. Отсутствие питания в день поездки из {} в {} 01.01.2021\n".format(
                                           test_place1, test_place2) + \
                                       "5. Отсутствие питания в день поездки из {} в {} и обратно 12.12.2022\n".format(
                                           test_place2, "суд Бешенковичского района") + \
                                       "6. Отсутствие питания в день поездки из {} в {} 04.03.2024\n".format(
                                           test_place2, test_place3) + \
                                       "7. Условия содержания во время поездки из {} в {} 01.01.2021\n".format(
                                           test_place1, test_place2) + \
                                       "8. Условия содержания во время поездки из {} в {} 12.12.2022\n".format(
                                           test_place2, "суд Бешенковичского района") + \
                                       "9. Условия содержания во время поездки из {} в {} 12.12.2022\n".format(
                                           "суда Бешенковичского района", test_place2) + \
                                       "10. Условия содержания во время поездки из {} в {} 04.03.2024\n".format(
                                           test_place2, test_place3) + \
                                       "\n(здесь и далее выбранное будет выделяться *жирным*)\n"

        first_paragraph_true = "10.10.2020 меня поместили до суда в {}. " \
                               "01.01.2021 меня вывезли из {} в {}. " \
                               "12.12.2022 меня вывезли из {} в {}. " \
                               "Суд вынес в отношении меня административное взыскание в виде административного " \
                               "ареста. После суда меня поместили в ЦИП Новополоцкого ГОВД для отбытия " \
                               "административного ареста. " \
                               "04.03.2024 меня вывезли из ЦИП Новополоцкого ГОВД в {}. " \
                               "02.01.2023 меня освободили. ".format(test_place1, test_place1, test_place2,
                                                                     test_place2, "суд Бешенковичского района",
                                                                     test_place3)
        choosen_places_numbers = [1, 3, 10]
        is_first_place_prison = True

        continue_to_choose_places(bot, personal_info, test_chat_id, first_paragraph_true,
                                  true_message_places_complain, choosen_places_numbers, is_first_place_prison, self)

    @test_bot.is_demo_decorator
    def test7(self):
        # МПС1 - test_court_place1 - МПС 1
        bot = bot_dummy.DummyBot()
        message = BotMessage('/start', test_chat_id)  # start
        bot.command_handler(message)

        previous = 'Вернули в ИВС Бешенковичского РОВ'
        answers = [
            BotMessage(test_email, test_chat_id),  # пользователь вводит эл. почт
            BotMessage(test_city1, test_chat_id),  # 5. пользователь вводит область, где его задержали
            BotMessage(test_place1, test_chat_id),  # 5.2 пользователь вводит место, где его задержали
            BotCall("DAY;2020;10;10", test_chat_id),  # 6. дату, когда задержали
            BotCall("Нет. Меня задержали до суда", test_chat_id),  # 7.	Вас поместили туда по решению суда?

            BotCall("Нет", test_chat_id),  # 7.2.1.	Вас перевозили до суда из [прошлый МПС] в другой ИВС или ЦИП?
            BotCall("Да", test_chat_id),  # 7.2.2.	Вас возили в суд из [прошлый МПС]?
            BotCall("DAY;2022;12;12", test_chat_id),  # 8.1.	Когда вас отвезли в суд?
            BotMessage(test_court_city1, test_chat_id),  # 8.2.	В какой суд вас отвезли?
            BotMessage(test_court_place1, test_chat_id),  # 8.2.В какой суд вас отвезли?

            BotCall("Нет", test_chat_id),  # 8.3.	В суде вас освободили?
            BotCall("Нет", test_chat_id),  # 8.3.2.1.	Суд  дал вам сутки?

            BotMessage(previous, test_chat_id),  # 8.3.2.1.2.1.	Куда вас поместили после суда?
            BotCall("Нет", test_chat_id),  # 7.2.1.	Вас перевозили до суда из [прошлый МПС] в другой ИВС или ЦИП?
            BotCall("Нет. Cуд был по видеосвязи", test_chat_id),  # 7.2.2.	Вас возили в суд из [прошлый МПС]?
            BotCall("Да", test_chat_id),  # 7.2.2.2.1.	После суда вас освободили?

            BotMessage(test_court_city1, test_chat_id),  # 7.2.2.2.1.1.1.	Какой суд вас освободил?
            BotMessage(test_court_place2, test_chat_id),  # 7.2.2.2.1.1.1.	Какой суд вас освободил?
            BotCall(test_release_date_input, test_chat_id),  # 7.1.4.2.1.	Когда вас освободили?
        ]

        bot_dummy.answer_questions(bot, answers)
        personal_info = bot.bot_info.personal_info
        places = personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyPlacesList)
        self.assertEqual(len(places), 3)

        dict_place0 = {
            config.PlaceKeys.KeyPlaceCity: test_city1,
            config.PlaceKeys.KeyPlaceStartDate: (2020, 10, 10),
            config.PlaceKeys.KeyPlaceName: test_place1,
            config.PlaceKeys.KeyPlaceIsCourt: False,
            config.PlaceKeys.KeyPlaceIsFirst: True,
            config.PlaceKeys.KeyPlaceIsLast: False,
            config.PlaceKeys.KeyPlaceIsAfterCourt: False,
        }
        dict_place1 = {
            config.PlaceKeys.KeyPlaceStartDate: (2022, 12, 12),
            config.PlaceKeys.KeyPlaceName: test_court_place1,
            config.PlaceKeys.KeyPlaceIsCourt: True,
            config.PlaceKeys.KeyPlaceIsFirst: False,
            config.PlaceKeys.KeyPlaceIsLast: False,
            config.PlaceKeys.KeyPlaceIsAfterCourt: False,
        }
        dict_place2 = {
            config.PlaceKeys.KeyPlaceStartDate: (2022, 12, 12),
            config.PlaceKeys.KeyPlaceName: test_place1,
            config.PlaceKeys.KeyPlaceIsCourt: False,
            config.PlaceKeys.KeyPlaceIsFirst: False,
            config.PlaceKeys.KeyPlaceIsLast: True,
            config.PlaceKeys.KeyPlaceIsAfterCourt: False,
            config.PlaceKeys.KeyPlaceIsReleasedInCourt: True,
            config.PlaceKeys.KeyPlaceCourtPlace: test_court_place2,
        }

        test_bot.verify_place(places[0], dict_place0, self)
        test_bot.verify_place(places[1], dict_place1, self)
        test_bot.verify_place(places[2], dict_place2, self)

        self.assertEqual(
            test_release_date, personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyReleaseDate))

        true_message_places_complain = "Я хочу пожаловаться на:\n\n" \
                                       "1. Условия содержания в {}\n".format(test_place1) + \
                                       "2. Отсутствие питания в день поездки из {} в {} и обратно " \
                                       "12.12.2022\n".format(test_place1, "суд Бешенковичского района") + \
                                       "3. Условия содержания во время поездки из {} " \
                                       "в {} 12.12.2022\n".format(test_place1, "суд Бешенковичского района") + \
                                       "4. Условия содержания во время поездки из {} " \
                                       "в {} 12.12.2022\n".format("суда Бешенковичского района", test_place1) + \
                                       "\n(здесь и далее выбранное будет выделяться *жирным*)\n"

        first_paragraph_true = "10.10.2020 меня поместили до суда в {}. " \
                               "12.12.2022 меня вывезли из {} в {}. " \
                               "Суд не рассмотрел административное дело в отношении меня. " \
                               "В результате меня отвезли в {}. " \
                               "02.01.2023 {} по видеосвязи рассмотрел административное дело в отношении меня. " \
                               "В результате рассмотрения дела меня освободили. ".format(test_place1,
                                                                                         test_place1,
                                                                                         "суд Бешенковичского района",
                                                                                         test_place1,
                                                                                         "суд Браславского района"
                                                                                         )
        choosen_places_numbers = [1, 2]
        is_first_place_prison = True

        continue_to_choose_places(bot, personal_info, test_chat_id, first_paragraph_true,
                                  true_message_places_complain, choosen_places_numbers, is_first_place_prison, self)

    @test_bot.is_demo_decorator
    def test8(self):
        # test_place1 - test_court_place1 - test_place2 -
        bot = bot_dummy.DummyBot()
        message = BotMessage('/start', test_chat_id)  # start
        bot.command_handler(message)

        answers = [
            BotMessage(test_email, test_chat_id),  # пользователь вводит эл. почт
            BotMessage(test_city1, test_chat_id),  # 5. пользователь вводит область, где его задержали
            BotMessage(test_place1, test_chat_id),  # 5.2 пользователь вводит место, где его задержали
            BotCall("DAY;2020;10;10", test_chat_id),  # 6. дату, когда задержали
            BotCall("Нет. Меня задержали до суда", test_chat_id),  # 7.	Вас поместили туда по решению суда?

            BotCall("Нет", test_chat_id),  # 7.2.1.	Вас перевозили до суда из [прошлый МПС] в другой ИВС или ЦИП?
            BotCall("Да", test_chat_id),  # 7.2.2.	Вас возили в суд из [прошлый МПС]?
            BotCall("DAY;2022;12;12", test_chat_id),  # 8.1.	Когда вас отвезли в суд?
            BotMessage(test_court_city1, test_chat_id),  # 8.2.	В какой суд вас отвезли?
            BotMessage(test_court_place1, test_chat_id),  # 8.2.В какой суд вас отвезли?

            BotCall("Нет", test_chat_id),  # 8.3.	В суде вас освободили?
            BotCall("Нет", test_chat_id),  # 8.3.2.1.	Суд  дал вам сутки?

            BotMessage("В другое место", test_chat_id),  # 8.3.2.1.2.1.	Куда вас поместили после суда?
            BotMessage(test_city1, test_chat_id),  # 8.3.2.1.2.1.2.1.	Куда вас поместили?
            BotMessage(test_place2, test_chat_id),  # 8.3.2.1.2.1.2.1.	Куда вас поместили?

            BotCall("Нет", test_chat_id),  # 7.2.1.	Вас перевозили до суда из [прошлый МПС] в другой ИВС или ЦИП?
            BotCall("Нет. Cуд был по видеосвязи", test_chat_id),  # 7.2.2.	Вас возили в суд из [прошлый МПС]?
            BotCall("Да", test_chat_id),  # 7.2.2.2.1.	После суда вас освободили?

            BotMessage(test_court_city1, test_chat_id),  # 7.2.2.2.1.1.1.	Какой суд вас освободил?
            BotMessage(test_court_place2, test_chat_id),  # 7.2.2.2.1.1.1.	Какой суд вас освободил?
            BotCall(test_release_date_input, test_chat_id),  # 7.1.4.2.1.	Когда вас освободили?
        ]

        bot_dummy.answer_questions(bot, answers)
        personal_info = bot.bot_info.personal_info
        places = personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyPlacesList)
        self.assertEqual(len(places), 3)

        dict_place0 = {
            config.PlaceKeys.KeyPlaceCity: test_city1,
            config.PlaceKeys.KeyPlaceStartDate: (2020, 10, 10),
            config.PlaceKeys.KeyPlaceName: test_place1,
            config.PlaceKeys.KeyPlaceIsCourt: False,
            config.PlaceKeys.KeyPlaceIsFirst: True,
            config.PlaceKeys.KeyPlaceIsLast: False,
            config.PlaceKeys.KeyPlaceIsAfterCourt: False,
        }
        dict_place1 = {
            config.PlaceKeys.KeyPlaceStartDate: (2022, 12, 12),
            config.PlaceKeys.KeyPlaceName: test_court_place1,
            config.PlaceKeys.KeyPlaceIsCourt: True,
            config.PlaceKeys.KeyPlaceIsFirst: False,
            config.PlaceKeys.KeyPlaceIsLast: False,
            config.PlaceKeys.KeyPlaceIsAfterCourt: False,
        }
        dict_place2 = {
            config.PlaceKeys.KeyPlaceStartDate: (2022, 12, 12),
            config.PlaceKeys.KeyPlaceName: test_place2,
            config.PlaceKeys.KeyPlaceIsCourt: False,
            config.PlaceKeys.KeyPlaceIsFirst: False,
            config.PlaceKeys.KeyPlaceIsLast: True,
            config.PlaceKeys.KeyPlaceIsAfterCourt: False,
            config.PlaceKeys.KeyPlaceIsReleasedInCourt: True,
            config.PlaceKeys.KeyPlaceCourtPlace: test_court_place2,
        }

        test_bot.verify_place(places[0], dict_place0, self)
        test_bot.verify_place(places[1], dict_place1, self)
        test_bot.verify_place(places[2], dict_place2, self)

        self.assertEqual(
            test_release_date, personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyReleaseDate))

        true_message_places_complain = "Я хочу пожаловаться на:\n\n" \
                                       "1. Условия содержания в {}\n".format(test_place1) + \
                                       "2. Условия содержания в {}\n".format(test_place2) + \
                                       "3. Отсутствие питания в день поездки из {} в {} и " \
                                       "из {} в {} " \
                                       "12.12.2022\n".format(test_place1, "суд Бешенковичского района",
                                                             "суда Бешенковичского района", test_place2) + \
                                       "4. Условия содержания во время поездки из {} " \
                                       "в {} 12.12.2022\n".format(test_place1, "суд Бешенковичского района") + \
                                       "5. Условия содержания во время поездки из {} " \
                                       "в {} 12.12.2022\n".format("суда Бешенковичского района", test_place2) + \
                                       "\n(здесь и далее выбранное будет выделяться *жирным*)\n"

        first_paragraph_true = "10.10.2020 меня поместили до суда в {}. " \
                               "12.12.2022 меня вывезли из {} в {}. " \
                               "Суд не рассмотрел административное дело в отношении меня. " \
                               "В результате меня отвезли в {}. " \
                               "02.01.2023 {} по видеосвязи рассмотрел административное дело в отношении меня. " \
                               "В результате рассмотрения дела меня освободили. ".format(test_place1,
                                                                                         test_place1,
                                                                                         "суд Бешенковичского района",
                                                                                         test_place2,
                                                                                         "суд Браславского района"
                                                                                         )
        choosen_places_numbers = [2, 5]
        is_first_place_prison = True

        continue_to_choose_places(bot, personal_info, test_chat_id, first_paragraph_true,
                                  true_message_places_complain, choosen_places_numbers, is_first_place_prison, self)
