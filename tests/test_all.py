import unittest

test_all_without_doc = False
if test_all_without_doc:
    from test_bot import TestBot
    from test_bot_after_court import TestBotAfterCourt
    from test_bot_before_court import TestBotBeforeCourt
    from test_bot_many_arrests import TestBotManyArrests
    from test_doc import TestDocFirstParagraph, TestDocSecondParagraph
    from test_personal_info import TestPlace, TestPoliceOffice

    from test_bed8 import TestBed
    from test_cant_complain6 import TestComplain
    from test_cell9 import TestCell
    from test_cell9_both import TestCellBoth
    from test_food2 import TestFood
    from test_med_help4 import TestMedHelp
    from test_shower5 import TestShower
    from test_transport import TestTransport
    from test_transport_food import TestTransportFood
    from test_virus1 import TestVirus
    from test_walk3 import TestWalk
    from test_world7 import TestWorld

    from test_full_after_court import TestFull


test_doc = False
if test_doc:
    from test_doc import TestDoc


from test_bot_after_court import TestBotAfterCourt

if __name__ == '__main__':
    unittest.main()
