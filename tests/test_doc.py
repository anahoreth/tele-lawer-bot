import unittest
import dbworker
import config
import keyboard
import create_complain
import test_consts
import questions.questions_entry as questions_entry
import questions.question_groups.abstract_group as abstract_group
import form_complain


class TestDoc(unittest.TestCase):
    def testDoc(self):
        chat_id = test_consts.test_chat_id
        personal_info = dbworker.PersonalInfo()
        personal_info.init_chat(chat_id)
        personal_info.add_place(chat_id)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceName, test_consts.test_place1)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceStartDate, test_consts.test_date0)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsCourt, False)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsLast, False)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsFirst, True)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsAfterCourt, True)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceCourtPlace, test_consts.test_court_place1)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceCourtDate, test_consts.test_date2)
        personal_info.set_parameter(chat_id, config.PersonalInfoKeys.KeyReleaseDate, test_consts.test_release_date)
        personal_info.set_parameter(chat_id, config.PersonalInfoKeys.KeyDate, test_consts.test_date3)

        place_name = test_consts.test_place1
        place_type = config.ComplainPlaceType.SPlacePrison
        is_after_court = True
        selected_places = [(place_type, place_name, is_after_court)]
        personal_info.set_parameter(chat_id, config.PersonalInfoKeys.KeyComplainSelectedPlaceNames, selected_places)

        selected_cat = []
        asking_type = abstract_group.AskingType.AfterCourt
        questions_list = [questions_entry.QuestionsDB(personal_info, chat_id,
                                                      place_name, place_type, selected_cat, asking_type)]

        personal_info.set_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions, questions_list)
        selected_places_group = form_complain.get_selected_places_groups(selected_places)
        # print(selected_places_group[0])
        doc = create_complain.make_complain(personal_info, chat_id, selected_places_group[0])
        complaint_filename = str(chat_id) + form_complain.DOCUMENT_FILENAME
        doc.save(complaint_filename)


class TestDocFirstParagraph(unittest.TestCase):

    def testDoc(self):
        # одно место после суда
        chat_id = test_consts.test_chat_id
        personal_info = dbworker.PersonalInfo()
        personal_info.init_chat(chat_id)
        personal_info.add_place(chat_id)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceName, test_consts.test_place1)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceStartDate, test_consts.test_date0)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsCourt, False)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsLast, True)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsFirst, True)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsAfterCourt, True)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceCourtPlace, test_consts.test_court_place1)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceCourtDate, test_consts.test_date2)
        personal_info.set_parameter(chat_id, config.PersonalInfoKeys.KeyReleaseDate, test_consts.test_release_date)

        first_paragraph = create_complain.get_first_paragraph_text(chat_id=chat_id, personal_info=personal_info)
        first_paragraph_true = "12.12.2022 суд Бешенковичского района вынес в отношении меня административное " \
                               "взыскание в виде административного ареста. 10.10.2020 меня поместили в ИВС " \
                               "Бешенковичского РОВД. 02.01.2023 меня освободили. "
        self.assertEqual(first_paragraph, first_paragraph_true)

    def testDoc2(self):
        # два МПС после суда
        chat_id = test_consts.test_chat_id
        personal_info = dbworker.PersonalInfo()
        personal_info.init_chat(chat_id)
        personal_info.add_place(chat_id)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceName, test_consts.test_place1)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceStartDate, test_consts.test_date0)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsCourt, False)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsLast, False)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsFirst, True)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsAfterCourt, True)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceCourtPlace, test_consts.test_court_place1)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceCourtDate, test_consts.test_date2)

        personal_info.add_place(chat_id)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceName, test_consts.test_place2)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceStartDate, test_consts.test_date3)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsCourt, False)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsLast, True)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsFirst, False)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsAfterCourt, True)

        personal_info.set_parameter(chat_id, config.PersonalInfoKeys.KeyReleaseDate, test_consts.test_release_date)

        first_paragraph = create_complain.get_first_paragraph_text(chat_id=chat_id, personal_info=personal_info)
        first_paragraph_true = "12.12.2022 суд Бешенковичского района вынес в отношении меня административное " \
                               "взыскание в виде административного ареста. 10.10.2020 меня поместили в ИВС " \
                               "Бешенковичского РОВД. 03.02.2024 меня вывезли из ИВС " \
                               "Бешенковичского РОВД в ЦИП Новополоцкого ГОВД. " \
                               "02.01.2023 меня освободили. "
        self.assertEqual(first_paragraph, first_paragraph_true)

    def testDoc3(self):
        # МПС, суд, после суда освободили
        chat_id = test_consts.test_chat_id
        personal_info = dbworker.PersonalInfo()
        personal_info.init_chat(chat_id)
        personal_info.add_place(chat_id)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceName, test_consts.test_place1)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceStartDate, test_consts.test_date0)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsCourt, False)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsLast, False)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsFirst, True)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsAfterCourt, True)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceCourtPlace, test_consts.test_court_place1)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceCourtDate, test_consts.test_date2)

        personal_info.add_place(chat_id)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceName, test_consts.test_court_place2)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceStartDate, test_consts.test_date3)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsCourt, True)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsLast, True)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsFirst, False)

        personal_info.set_parameter(chat_id, config.PersonalInfoKeys.KeyReleaseDate, test_consts.test_release_date)

        first_paragraph = create_complain.get_first_paragraph_text(chat_id=chat_id, personal_info=personal_info)
        first_paragraph_true = "12.12.2022 суд Бешенковичского района вынес в отношении меня административное " \
                               "взыскание в виде административного ареста. 10.10.2020 меня поместили в ИВС " \
                               "Бешенковичского РОВД. 03.02.2024 меня вывезли из ИВС " \
                               "Бешенковичского РОВД в суд Браславского района. " \
                               "После суда меня освободили. "
        self.assertEqual(first_paragraph, first_paragraph_true)

    def testDoc4(self):
        # МПС, суд, после суда в другой ИВС и потом освободили
        chat_id = test_consts.test_chat_id
        personal_info = dbworker.PersonalInfo()
        personal_info.init_chat(chat_id)
        personal_info.add_place(chat_id)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceName, test_consts.test_place1)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceStartDate, test_consts.test_date0)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsCourt, False)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsLast, False)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsFirst, True)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsAfterCourt, True)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceCourtPlace, test_consts.test_court_place1)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceCourtDate, test_consts.test_date2)

        personal_info.add_place(chat_id)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceName, test_consts.test_court_place2)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceStartDate, test_consts.test_date3)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsCourt, True)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsLast, False)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsFirst, False)

        personal_info.add_place(chat_id)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceName, test_consts.test_place2)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsCourt, False)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsLast, True)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsFirst, False)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsAfterCourt, True)
        personal_info.set_parameter(chat_id, config.PersonalInfoKeys.KeyReleaseDate, test_consts.test_release_date)

        first_paragraph = create_complain.get_first_paragraph_text(chat_id=chat_id, personal_info=personal_info)
        first_paragraph_true = "12.12.2022 суд Бешенковичского района вынес в отношении меня административное " \
                               "взыскание в виде административного ареста. 10.10.2020 меня поместили в ИВС " \
                               "Бешенковичского РОВД. 03.02.2024 меня вывезли из ИВС " \
                               "Бешенковичского РОВД в суд Браславского района. " \
                               "После суда меня отвезли в ЦИП Новополоцкого ГОВД. 02.01.2023 меня освободили. "
        self.assertEqual(first_paragraph, first_paragraph_true)

    def testDoc5(self):
        # МПС, суд, после суда в тот же ИВС и потом освободили
        chat_id = test_consts.test_chat_id
        personal_info = dbworker.PersonalInfo()
        personal_info.init_chat(chat_id)
        personal_info.add_place(chat_id)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceName, test_consts.test_place1)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceStartDate, test_consts.test_date0)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsCourt, False)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsLast, False)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsFirst, True)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsAfterCourt, True)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceCourtPlace, test_consts.test_court_place1)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceCourtDate, test_consts.test_date2)

        personal_info.add_place(chat_id)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceName, test_consts.test_court_place2)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceStartDate, test_consts.test_date3)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsCourt, True)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsLast, False)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsFirst, False)

        personal_info.add_place(chat_id)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceName, test_consts.test_place1)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsCourt, False)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsLast, True)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsFirst, False)
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsAfterCourt, True)
        personal_info.set_parameter(chat_id, config.PersonalInfoKeys.KeyReleaseDate, test_consts.test_release_date)

        first_paragraph = create_complain.get_first_paragraph_text(chat_id=chat_id, personal_info=personal_info)
        first_paragraph_true = "12.12.2022 суд Бешенковичского района вынес в отношении меня административное " \
                               "взыскание в виде административного ареста. 10.10.2020 меня поместили в ИВС " \
                               "Бешенковичского РОВД. 03.02.2024 меня вывезли из ИВС " \
                               "Бешенковичского РОВД в суд Браславского района. " \
                               "После суда меня вернули в ИВС Бешенковичского РОВД. 02.01.2023 меня освободили. "
        self.assertEqual(first_paragraph, first_paragraph_true)


import choose_places_to_complain


class TestDocSecondParagraph(unittest.TestCase):

    def test1(self):
        place1 = choose_places_to_complain.ComplainPlaceInfo(config.ComplainPlaceType.SPlacePrison,
                                                             "ИВС Бешенковичского РОВД", is_after_court=True)
        place2 = choose_places_to_complain.ComplainPlaceInfo(config.ComplainPlaceType.SPlacePrison,
                                                             "ЦИП Новополоцкого ГОВД", is_after_court=True)

        transport1 = choose_places_to_complain.ComplainPlaceInfo(
            config.ComplainPlaceType.SPlaceTransport, "ИВС Бешенковичского РОВД",
            next_name="ЦИП Новополоцкого ГОВД", transfer_date=(2023, 1, 2))

        transport_court1 = choose_places_to_complain.ComplainPlaceInfo(
            config.ComplainPlaceType.SPlaceTransport, "ИВС Бешенковичского РОВД",
            next_name="Суд Бешенковичского района", transfer_date=(2023, 1, 2))

        transport_court_food1 = choose_places_to_complain.ComplainPlaceInfo(
            config.ComplainPlaceType.SPlaceTransportFood, "ИВС Бешенковичского РОВД",
            next_name="Суд Новополоцкого района", transfer_date=(2023, 2, 3))

        selected_places = [place1, place2, transport1, transport_court1, transport_court_food1]
        selected_places_groups = form_complain.get_selected_places_groups(selected_places)
        selected_places_groups_true = [[(place1, 0), (transport1, 2), (transport_court1, 3),
                                        (transport_court_food1, 4)],
                                       [(place2, 1)]]

        self.assertEqual(selected_places_groups, selected_places_groups_true)

        second_paragraph_true_zero = "Я считаю условия содержания во время моего нахождения в " \
                                     "ИВС Бешенковичского РОВД, во время поездки из ИВС Бешенковичского РОВД в " \
                                     "ЦИП Новополоцкого ГОВД 02.01.2023, " \
                                     "и во время поездки из ИВС Бешенковичского РОВД " \
                                     "в суд Бешенковичского района 02.01.2023, а также в день поездки из " \
                                     "ИВС Бешенковичского РОВД в суд Новополоцкого района 03.02.2023 " \
                                     "бесчеловечными, унижающими человеческое достоинство и представлявшими " \
                                     "угрозу, как минимум, моему здоровью."

        second_paragraph = create_complain.get_second_paragraph_text(selected_places_groups[0])
        self.assertEqual(second_paragraph, second_paragraph_true_zero)

        second_paragraph_true_one = "Я считаю условия содержания во время моего нахождения в " \
                                     "ЦИП Новополоцкого ГОВД бесчеловечными, унижающими человеческое " \
                                    "достоинство и представлявшими угрозу, как минимум, моему здоровью."
        second_paragraph = create_complain.get_second_paragraph_text(selected_places_groups[1])
        self.assertEqual(second_paragraph, second_paragraph_true_one)

    def test2(self):
        place1 = choose_places_to_complain.ComplainPlaceInfo(config.ComplainPlaceType.SPlacePrison,
                                                             "ИВС Бешенковичского РОВД", is_after_court=True)

        selected_places = [place1]
        selected_places_groups = form_complain.get_selected_places_groups(selected_places)
        selected_places_groups_true = [[(place1, 0)]]
        self.assertEqual(selected_places_groups, selected_places_groups_true)

        second_paragraph_true_zero = "Я считаю условия содержания во время моего нахождения в " \
                                     "ИВС Бешенковичского РОВД "\
                                     "бесчеловечными, унижающими человеческое достоинство и представлявшими " \
                                     "угрозу, как минимум, моему здоровью."
        second_paragraph = create_complain.get_second_paragraph_text(selected_places_groups[0])
        self.assertEqual(second_paragraph, second_paragraph_true_zero)

