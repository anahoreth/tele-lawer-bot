

import unittest
import copy
import bot_dummy
import config
from test_consts import *

import get_city
import create_complain
import show_places_list
from messages import *
import test_bot
import test_bot_before_court

# TODO добавить проверки параметров


class TestBotAfterCourt(unittest.TestCase):

    @test_bot.is_demo_decorator
    def test1(self):
        bot = bot_dummy.DummyBot()
        message = BotMessage('/start', test_chat_id)  # start
        bot.command_handler(message)

        answers = [
            BotMessage(test_email, test_chat_id),  # пользователь вводит эл. почту
            BotMessage("Витебская область", test_chat_id),  # 5. пользователь вводит область, где его задержали
            BotMessage("ИВС Бешенковичского РОВД", test_chat_id),  # 5.2 пользователь вводит место, где его задержали
            BotCall("DAY;2020;10;10", test_chat_id),  # 6. дату, когда задержали
            BotCall("Да, меня поместили отбывать арест", test_chat_id),  # 7.	Вас поместили туда по решению суда?
            BotMessage("В Витебской области", test_chat_id),  # 7.1.1.	Какой суд осудил вас?
            BotMessage("Суд Бешенковичского района", test_chat_id),  # 7.1.1.	Какой суд осудил вас?
            BotCall("DAY;2022;12;12", test_chat_id),  # 7.1.2.	Когда этот суд осудил вас?
            BotCall("Не вывозили", test_chat_id),  # 7.1.3. Во время ареста вас вывозили из ...
            BotCall("Нет", test_chat_id),  # 7.1.4.	Сразу после этого ареста вас снова задержали?
            BotCall("DAY;2023;1;2", test_chat_id),  # 7.1.4.2.1.	Когда вас освободили?
        ]

        bot_dummy.answer_questions(bot, answers)
        personal_info = bot.bot_info.personal_info

        my_date = personal_info.get_place_parameter(test_chat_id, config.PlaceKeys.KeyPlaceStartDate)
        self.assertEqual(test_date0, my_date)
        is_court = personal_info.get_place_parameter(test_chat_id, config.PlaceKeys.KeyPlaceIsAfterCourt)
        self.assertEqual(True, is_court)
        court_name = personal_info.get_place_parameter(test_chat_id, config.PlaceKeys.KeyPlaceCourtCity)
        self.assertEqual(test_court_city1, court_name)
        court_place = personal_info.get_place_parameter(test_chat_id, config.PlaceKeys.KeyPlaceCourtPlace)
        self.assertEqual(test_court_place1, court_place)
        court_date = personal_info.get_place_parameter(test_chat_id, config.PlaceKeys.KeyPlaceCourtDate)
        self.assertEqual(test_date2, court_date)
        release_date = personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyReleaseDate)
        self.assertEqual(test_release_date, release_date)
        state = bot.bot_info.get_state(test_chat_id)
        self.assertEqual(type(state), show_places_list.StatePlaceShowList)

        first_paragraph_true = "12.12.2022 суд Бешенковичского " \
                               "района вынес в отношении меня административное взыскание в виде административного" \
                               " ареста. 10.10.2020 меня поместили в ИВС Бешенковичского РОВД. 02.01.2023" \
                               " меня освободили. "

        true_message_places_complain = "Я хочу пожаловаться на:\n\n" \
                                       "1. Условия содержания в {}\n".format("ИВС Бешенковичского РОВД") + \
                                       "\n(здесь и далее выбранное будет выделяться *жирным*)\n"

        chosen_places_numbers = [1]
        is_first_place_prison = True
        test_bot_before_court.continue_to_choose_places(bot, personal_info, test_chat_id, first_paragraph_true,
                                                        true_message_places_complain, chosen_places_numbers,
                                                        is_first_place_prison, self)

    @test_bot.is_demo_decorator
    def test2(self):
        # три места после суда
        bot = bot_dummy.DummyBot()
        message = BotMessage('/start', test_chat_id)  # start
        bot.command_handler(message)

        answers = [
            BotMessage(test_email, test_chat_id),  # пользователь вводит эл. почту
            BotMessage(test_city1, test_chat_id),  # 5. пользователь вводит область, где его задержали
            BotMessage(test_place1, test_chat_id),  # 5.2 пользователь вводит место, где его задержали
            BotCall(test_date_input0, test_chat_id),  # 6. дату, когда задержали
            BotCall("Да, меня поместили отбывать арест", test_chat_id),  # 7.	Вас поместили туда по решению суда?
            BotMessage(test_court_city1, test_chat_id),  # 7.1.1.	Какой суд осудил вас?
            BotMessage(test_court_place1, test_chat_id),  # 7.1.1.	Какой суд осудил вас?
            BotCall(test_date_input2, test_chat_id),  # 7.1.2.	Когда этот суд осудил вас?

            BotCall("Вывозили в ЦИП или ИВС", test_chat_id),  # 7.1.3. Во время ареста вас вывозили из ...
            BotMessage(test_city2, test_chat_id),  # 7.1.3.1.1.1.	Куда вас поместили?
            BotMessage(test_place2, test_chat_id),  # 7.1.3.1.1.1.	Куда вас поместили?
            BotCall("DAY;2024;2;3", test_chat_id),  # 7.1.3.1.1.2.	Когда вас туда поместили?

            BotCall("Вывозили в ЦИП или ИВС", test_chat_id),  # 7.1.3. Во время ареста вас вывозили из ...
            BotMessage(test_city3, test_chat_id),  # 7.1.3.1.1.1.	Куда вас поместили?
            BotMessage(test_place3, test_chat_id),  # 7.1.3.1.1.1.	Куда вас поместили?
            BotCall("DAY;2024;3;4", test_chat_id),  # 7.1.3.1.1.2.	Когда вас туда поместили?

            BotCall("Не вывозили", test_chat_id),  # 7.1.3. Во время ареста вас вывозили из ...
            BotCall("Нет", test_chat_id),  # 7.1.4.	Сразу после этого ареста вас снова задержали?
            BotCall(test_release_date_input, test_chat_id),  # 7.1.4.2.1.	Когда вас освободили?
        ]
        # TODO добавить проверки параметров
        bot_dummy.answer_questions(bot, answers)
        personal_info = bot.bot_info.personal_info
        places = personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyPlacesList)

        self.assertEqual(3, len(places))

        self.assertEqual(
            test_release_date, personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyReleaseDate))

        first_paragraph_true = "12.12.2022 суд Бешенковичского " \
                               "района вынес в отношении меня административное взыскание в виде административного" \
                               " ареста. 10.10.2020 меня поместили в {}. 03.02.2024 " \
                               "меня вывезли из {} в {}. 04.03.2024 меня вывезли из {} в {}. " \
                               "02.01.2023 меня освободили. ".format(test_place1, test_place1, test_place2,
                                                                     test_place2, test_place3)

        true_message_places_complain = "Я хочу пожаловаться на:\n\n" \
                                       "1. Условия содержания в {}\n".format(test_place1) + \
                                       "2. Условия содержания в {}\n".format(test_place2) + \
                                       "3. Условия содержания в {}\n".format(test_place3) + \
                                       "4. Отсутствие питания в день поездки " \
                                       "из {} в {} 03.02.2024\n".format(test_place1, test_place2) + \
                                       "5. Отсутствие питания в день поездки " \
                                       "из {} в {} 04.03.2024\n".format(test_place2, test_place3) + \
                                       "6. Условия содержания во время поездки " + \
                                       "из {} в {} 03.02.2024\n".format(test_place1, test_place2) + \
                                       "7. Условия содержания во время поездки " + \
                                       "из {} в {} 04.03.2024\n".format(test_place2, test_place3) + \
                                       "\n(здесь и далее выбранное будет выделяться *жирным*)\n"

        chosen_places_numbers = [2, 3]
        is_first_place_prison = True
        test_bot_before_court.continue_to_choose_places(copy.deepcopy(bot), personal_info, test_chat_id,
                                                        first_paragraph_true, true_message_places_complain,
                                                        chosen_places_numbers, is_first_place_prison, self)

        chosen_places_numbers = [7]
        is_first_place_prison = False
        test_bot_before_court.continue_to_choose_places(bot, personal_info, test_chat_id, first_paragraph_true,
                                                        true_message_places_complain, chosen_places_numbers,
                                                        is_first_place_prison, self)

    @test_bot.is_demo_decorator
    def test3(self):
        # одно место, суд и освободили
        bot = bot_dummy.DummyBot()
        message = BotMessage('/start', test_chat_id)  # start
        bot.command_handler(message)

        answers = [
            BotMessage(test_email, test_chat_id),  # пользователь вводит эл. почту
            BotMessage(test_city1, test_chat_id),  # 5. пользователь вводит область, где его задержали
            BotMessage(test_place1, test_chat_id),  # 5.2 пользователь вводит место, где его задержали
            BotCall(test_date_input0, test_chat_id),  # 6. дату, когда задержали
            BotCall("Да, меня поместили отбывать арест", test_chat_id),  # 7.	Вас поместили туда по решению суда?
            BotMessage(test_court_city1, test_chat_id),  # 7.1.1.	Какой суд осудил вас?
            BotMessage(test_court_place1, test_chat_id),  # 7.1.1.	Какой суд осудил вас?
            BotCall(test_date_input2, test_chat_id),  # 7.1.2.	Когда этот суд осудил вас?

            BotCall("Вывозили в суд", test_chat_id),  # 7.1.3. Во время ареста вас вывозили из ...
            BotMessage(test_court_city2, test_chat_id),  # 7.1.3.2.1.	В какой суд вас вывозили?
            BotMessage(test_court_place2, test_chat_id),  # 7.1.3.2.1.	В какой суд вас вывозили?
            BotCall("DAY;2024;2;3", test_chat_id),  # 7.1.3.2.2.	Когда вас вывозили в суд?
            BotCall("Освободили", test_chat_id),  # 7.1.3.2.3.	Что с вами произошло после суда?
        ]

        bot_dummy.answer_questions(bot, answers)
        personal_info = bot.bot_info.personal_info
        places = personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyPlacesList)

        # TODO добавить проверки параметров
        self.assertEqual(2, len(places))

        self.assertEqual(
            test_court_city2, places[1].place_get_parameter(config.PlaceKeys.KeyPlaceCity))
        self.assertEqual(
            test_court_place2, places[1].place_get_parameter(config.PlaceKeys.KeyPlaceName))
        self.assertEqual(
            True, places[1].place_get_parameter(config.PlaceKeys.KeyPlaceIsCourt))

        self.assertEqual(
            test_date3, personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyReleaseDate))

        self.assertEqual(type(bot.bot_info.get_state(test_chat_id)), show_places_list.StatePlaceShowList)

        first_paragraph_true = "12.12.2022 суд Бешенковичского " \
                               "района вынес в отношении меня административное взыскание в виде административного" \
                               " ареста. 10.10.2020 меня поместили в {}. " \
                               "03.02.2024 меня вывезли из {} в суд Браславского района. " \
                               "После суда меня освободили. ".format(test_place1, test_place1)

        true_message_places_complain = "Я хочу пожаловаться на:\n\n" \
                                       "1. Условия содержания в {}\n".format(test_place1) + \
                                       "2. Отсутствие питания в день поездки " \
                                       "из {} в {} 03.02.2024\n".format(test_place1, "суд Браславского района") + \
                                       "3. Условия содержания во время поездки " + \
                                       "из {} в {} 03.02.2024\n".format(test_place1, "суд Браславского района") + \
                                       "\n(здесь и далее выбранное будет выделяться *жирным*)\n"

        chosen_places_numbers = [1, 2]
        is_first_place_prison = True
        test_bot_before_court.continue_to_choose_places(copy.deepcopy(bot), personal_info, test_chat_id,
                                                        first_paragraph_true, true_message_places_complain,
                                                        chosen_places_numbers, is_first_place_prison, self)

    @test_bot.is_demo_decorator
    def test4(self):
        # одно место, суд и вернули
        bot = bot_dummy.DummyBot()
        message = BotMessage('/start', test_chat_id)  # start
        bot.command_handler(message)

        answer_return = "Вернули в {}".format(test_place1)[:33]

        answers = [
            BotMessage(test_email, test_chat_id),  # пользователь вводит эл. почту
            BotMessage(test_city1, test_chat_id),  # 5. пользователь вводит область, где его задержали
            BotMessage(test_place1, test_chat_id),  # 5.2 пользователь вводит место, где его задержали
            BotCall(test_date_input0, test_chat_id),  # 6. дату, когда задержали
            BotCall("Да, меня поместили отбывать арест", test_chat_id),  # 7.	Вас поместили туда по решению суда?
            BotMessage(test_court_city1, test_chat_id),  # 7.1.1.	Какой суд осудил вас?
            BotMessage(test_court_place1, test_chat_id),  # 7.1.1.	Какой суд осудил вас?
            BotCall(test_date_input2, test_chat_id),  # 7.1.2.	Когда этот суд осудил вас?

            BotCall("Вывозили в суд", test_chat_id),  # 7.1.3. Во время ареста вас вывозили из ...
            BotMessage(test_court_city2, test_chat_id),  # 7.1.3.2.1.	В какой суд вас вывозили?
            BotMessage(test_court_place2, test_chat_id),  # 7.1.3.2.1.	В какой суд вас вывозили?
            BotCall(test_date_input3, test_chat_id),  # 7.1.3.2.2.	Когда вас вывозили в суд?

            BotCall(answer_return, test_chat_id),  # 7.1.3.2.3.	Что с вами произошло после суда?

            BotCall("Не вывозили", test_chat_id),  # 7.1.3. Во время ареста вас вывозили из ...
            BotCall("Нет", test_chat_id),  # 7.1.4.	Сразу после этого ареста вас снова задержали?
            BotCall(test_release_date_input, test_chat_id),  # 7.1.4.2.1.	Когда вас освободили?
        ]

        bot_dummy.answer_questions(bot, answers)
        personal_info = bot.bot_info.personal_info
        places = personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyPlacesList)
        self.assertEqual(3, len(places))

        self.assertEqual(
            test_court_city2, places[1].place_get_parameter(config.PlaceKeys.KeyPlaceCity))
        self.assertEqual(
            test_court_place2, places[1].place_get_parameter(config.PlaceKeys.KeyPlaceName))
        self.assertEqual(
            True, places[1].place_get_parameter(config.PlaceKeys.KeyPlaceIsCourt))

        self.assertEqual(
            test_city1, places[0].place_get_parameter(config.PlaceKeys.KeyPlaceCity))
        self.assertEqual(
            test_place1, places[0].place_get_parameter(config.PlaceKeys.KeyPlaceName))
        self.assertEqual(
            False, places[0].place_get_parameter(config.PlaceKeys.KeyPlaceIsCourt))

        self.assertEqual(
            test_city1, places[2].place_get_parameter(config.PlaceKeys.KeyPlaceCity))
        self.assertEqual(
            test_place1, places[2].place_get_parameter(config.PlaceKeys.KeyPlaceName))
        self.assertEqual(
            False, places[2].place_get_parameter(config.PlaceKeys.KeyPlaceIsCourt))

        self.assertEqual(
            False, places[2].place_get_parameter(config.PlaceKeys.KeyPlaceIsFirst))

        self.assertEqual(
            test_release_date, personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyReleaseDate))

        self.assertEqual(type(bot.bot_info.get_state(test_chat_id)), show_places_list.StatePlaceShowList)

        first_paragraph_true = "12.12.2022 суд Бешенковичского " \
                               "района вынес в отношении меня административное взыскание в виде административного" \
                               " ареста. 10.10.2020 меня поместили в {}. " \
                               "03.02.2024 меня вывезли из {} в суд Браславского района. " \
                               "После суда меня вернули в {}. " \
                               "02.01.2023 меня освободили. ".format(test_place1, test_place1, test_place1)

        true_message_places_complain = "Я хочу пожаловаться на:\n\n" \
                                       "1. Условия содержания в {}\n".format(test_place1) + \
                                       "2. Отсутствие питания в день поездки " \
                                       "из {} в {} и обратно 03.02.2024\n".format(
                                           test_place1, "суд Браславского района") + \
                                       "3. Условия содержания во время поездки из {} в {} 03.02.2024\n".format(
                                           test_place1, "суд Браславского района") + \
                                       "4. Условия содержания во время поездки из {} в {} 03.02.2024\n".format(
                                           "суда Браславского района", test_place1) + \
                                       "\n(здесь и далее выбранное будет выделяться *жирным*)\n"

        chosen_places_numbers = [4]
        is_first_place_prison = False
        test_bot_before_court.continue_to_choose_places(copy.deepcopy(bot), personal_info, test_chat_id,
                                                        first_paragraph_true, true_message_places_complain,
                                                        chosen_places_numbers, is_first_place_prison, self)

    @test_bot.is_demo_decorator
    def test5(self):
        # одно место, суд, второе место, и освободили
        bot = bot_dummy.DummyBot()
        message = BotMessage('/start', test_chat_id)  # start
        bot.command_handler(message)

        answers = [
            BotMessage(test_email, test_chat_id),  # пользователь вводит эл. почту
            BotMessage(test_city1, test_chat_id),  # 5. пользователь вводит область, где его задержали
            BotMessage(test_place1, test_chat_id),  # 5.2 пользователь вводит место, где его задержали
            BotCall(test_date_input0, test_chat_id),  # 6. дату, когда задержали
            BotCall("Да, меня поместили отбывать арест", test_chat_id),  # 7.	Вас поместили туда по решению суда?
            BotMessage(test_court_city1, test_chat_id),  # 7.1.1.	Какой суд осудил вас?
            BotMessage(test_court_place1, test_chat_id),  # 7.1.1.	Какой суд осудил вас?
            BotCall(test_date_input2, test_chat_id),  # 7.1.2.	Когда этот суд осудил вас?

            BotCall("Вывозили в суд", test_chat_id),  # 7.1.3. Во время ареста вас вывозили из ...
            BotMessage(test_court_city2, test_chat_id),  # 7.1.3.2.1.	В какой суд вас вывозили?
            BotMessage(test_court_place2, test_chat_id),  # 7.1.3.2.1.	В какой суд вас вывозили?
            BotCall(test_date_input3, test_chat_id),  # 7.1.3.2.2.	Когда вас вывозили в суд?
            BotCall("Поместили в другой ЦИП или ИВС", test_chat_id),  # 7.1.3.2.3.	Что с вами произошло после суда?

            BotMessage(test_city2, test_chat_id),  # 7.1.3.2.3.2.1.	Куда вас поместили?
            BotMessage(test_place2, test_chat_id),  # 7.1.3.2.3.2.1.Куда вас поместили?


            BotCall("Не вывозили", test_chat_id),  # 7.1.3. Во время ареста вас вывозили из ...
            BotCall("Нет", test_chat_id),  # 7.1.4.	Сразу после этого ареста вас снова задержали?
            BotCall(test_release_date_input, test_chat_id),  # 7.1.4.2.1.	Когда вас освободили?
        ]

        bot_dummy.answer_questions(bot, answers)
        personal_info = bot.bot_info.personal_info
        places = personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyPlacesList)
        self.assertEqual(3, len(places))

        self.assertEqual(
            test_court_city2, places[1].place_get_parameter(config.PlaceKeys.KeyPlaceCity))
        self.assertEqual(
            test_court_place2, places[1].place_get_parameter(config.PlaceKeys.KeyPlaceName))
        self.assertEqual(
            True, places[1].place_get_parameter(config.PlaceKeys.KeyPlaceIsCourt))

        self.assertEqual(
            test_city1, places[0].place_get_parameter(config.PlaceKeys.KeyPlaceCity))
        self.assertEqual(
            test_place1, places[0].place_get_parameter(config.PlaceKeys.KeyPlaceName))
        self.assertEqual(
            False, places[0].place_get_parameter(config.PlaceKeys.KeyPlaceIsCourt))

        self.assertEqual(
            test_city2, places[2].place_get_parameter(config.PlaceKeys.KeyPlaceCity))
        self.assertEqual(
            test_place2, places[2].place_get_parameter(config.PlaceKeys.KeyPlaceName))
        self.assertEqual(
            False, places[2].place_get_parameter(config.PlaceKeys.KeyPlaceIsCourt))

        self.assertEqual(
            False, places[2].place_get_parameter(config.PlaceKeys.KeyPlaceIsFirst))

        self.assertEqual(
            test_release_date, personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyReleaseDate))

        self.assertEqual(type(bot.bot_info.get_state(test_chat_id)), show_places_list.StatePlaceShowList)

        first_paragraph_true = "12.12.2022 суд Бешенковичского " \
                               "района вынес в отношении меня административное взыскание в виде административного" \
                               " ареста. 10.10.2020 меня поместили в {}. " \
                               "03.02.2024 меня вывезли из {} в суд Браславского района. " \
                               "После суда меня отвезли в {}. " \
                               "02.01.2023 меня освободили. ".format(test_place1, test_place1, test_place2)

        true_message_places_complain = "Я хочу пожаловаться на:\n\n" \
                                       "1. Условия содержания в {}\n".format(test_place1) + \
                                       "2. Условия содержания в {}\n".format(test_place2) + \
                                       "3. Отсутствие питания в день поездки " \
                                       "из {} в {} и из {} в {} 03.02.2024\n".format(
                                           test_place1, "суд Браславского района",
                                           "суда Браславского района", test_place2) + \
                                       "4. Условия содержания во время поездки из {} в {} 03.02.2024\n".format(
                                           test_place1, "суд Браславского района") + \
                                       "5. Условия содержания во время поездки из {} в {} 03.02.2024\n".format(
                                           "суда Браславского района", test_place2) + \
                                       "\n(здесь и далее выбранное будет выделяться *жирным*)\n"

        chosen_places_numbers = [5]
        is_first_place_prison = False
        test_bot_before_court.continue_to_choose_places(copy.deepcopy(bot), personal_info, test_chat_id,
                                                        first_paragraph_true, true_message_places_complain,
                                                        chosen_places_numbers, is_first_place_prison, self)

    @test_bot.is_demo_decorator
    def test6(self):
        # Одно место, освободили. Второй арест, освободили
        bot = bot_dummy.DummyBot()
        message = BotMessage('/start', test_chat_id)  # start
        bot.command_handler(message)

        answers = [
            BotMessage(test_email, test_chat_id),  # пользователь вводит эл. почту
            BotMessage(test_city1, test_chat_id),  # 5. пользователь вводит область, где его задержали
            BotMessage(test_place1, test_chat_id),  # 5.2 пользователь вводит место, где его задержали
            BotCall(test_date_input0, test_chat_id),  # 6. дату, когда задержали
            BotCall("Да, меня поместили отбывать арест", test_chat_id),  # 7.	Вас поместили туда по решению суда?
            BotMessage(test_court_city1, test_chat_id),  # 7.1.1.	Какой суд осудил вас?
            BotMessage(test_court_place1, test_chat_id),  # 7.1.1.	Какой суд осудил вас?
            BotCall(test_date_input2, test_chat_id),  # 7.1.2.	Когда этот суд осудил вас?
            BotCall("Не вывозили", test_chat_id),  # 7.1.3. Во время ареста вас вывозили из ...
            BotCall("Да", test_chat_id),  # 7.1.4.	Сразу после этого ареста вас снова задержали?
            BotCall("DAY;2023;1;2", test_chat_id),  # 7.1.4.1.1.	Когда закончился ваш предыдущий арест?

            BotMessage(test_city1, test_chat_id),  # 5. пользователь вводит область, где его задержали
            BotMessage(test_place2, test_chat_id),  # 5.2 пользователь вводит место, где его задержали
            BotCall("DAY;2022;2;2", test_chat_id),  # 6. дату, когда задержали
            BotCall("Да, меня поместили отбывать арест", test_chat_id),  # 7.	Вас поместили туда по решению суда?
            BotMessage(test_court_city1, test_chat_id),  # 7.1.1.	Какой суд осудил вас?
            BotMessage(test_court_place2, test_chat_id),  # 7.1.1.	Какой суд осудил вас?
            BotCall("DAY;2024;2;3", test_chat_id),  # 7.1.2.	Когда этот суд осудил вас?
            BotCall("Не вывозили", test_chat_id),  # 7.1.3. Во время ареста вас вывозили из ...
            BotCall("Нет", test_chat_id),  # 7.1.4.	Сразу после этого ареста вас снова задержали?
            BotCall("DAY;2022;4;5", test_chat_id),  # 7.1.4.1.1. 7.1.4.2.1.	Когда вас освободили?
        ]

        bot_dummy.answer_questions(bot, answers)
        personal_info = bot.bot_info.personal_info
        places = personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyPlacesList)
        self.assertEqual(2, len(places))

        self.assertEqual(
            (2022, 4, 5), personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyReleaseDate))

        first_paragraph_true = "12.12.2022 суд Бешенковичского " \
                               "района вынес в отношении меня административное взыскание в виде административного" \
                               " ареста. 10.10.2020 меня поместили в {}. " \
                               "03.02.2024 меня вывезли из {} в суд Браславского района. " \
                               "После суда меня отвезли в {}. " \
                               "02.01.2023 меня освободили. ".format(test_place1, test_place1, test_place2)

        true_message_places_complain = "Я хочу пожаловаться на:\n\n" \
                                       "1. Условия содержания в {}\n".format(test_place1) + \
                                       "2. Условия содержания в {}\n".format(test_place2) + \
                                       "3. Отсутствие питания в день поездки " \
                                       "из {} в {} и из {} в {} 03.02.2024\n".format(
                                           test_place1, "суд Браславского района",
                                           "суда Браславского района", test_place2) + \
                                       "4. Условия содержания во время поездки из {} в {} 03.02.2024\n".format(
                                           test_place1, "суд Браславского района") + \
                                       "5. Условия содержания во время поездки из {} в {} 03.02.2024\n".format(
                                           "суда Браславского района", test_place2) + \
                                       "\n(здесь и далее выбранное будет выделяться *жирным*)\n"


        first_paragraph_true = "12.12.2022 суд Бешенковичского " \
                               "района вынес в отношении меня административное взыскание в виде административного" \
                               " ареста. 10.10.2020 меня поместили в {}. 02.01.2023 мой первый административный " \
                               "арест закончился. " \
                               "03.02.2024 суд Браславского района вынес в отношении меня административное взыскание " \
                               "в виде административного ареста. 02.02.2022 меня поместили в {}. " \
                               "05.04.2022 меня освободили. ".format(test_place1, test_place2)

        true_message_places_complain = "Я хочу пожаловаться на:\n\n" \
                                       "1. Условия содержания в {}\n".format(test_place1) + \
                                       "2. Условия содержания в {}\n".format(test_place2) + \
                                       "\n(здесь и далее выбранное будет выделяться *жирным*)\n"

        chosen_places_numbers = [1, 2]
        is_first_place_prison = True
        test_bot_before_court.continue_to_choose_places(copy.deepcopy(bot), personal_info, test_chat_id,
                                                        first_paragraph_true, true_message_places_complain,
                                                        chosen_places_numbers, is_first_place_prison, self)

    @test_bot.is_demo_decorator
    def test7(self):
        # Одно место, другое, освободили. Второй арест, суд, освободили. Третий арест, освободили
        bot = bot_dummy.DummyBot()
        message = BotMessage('/start', test_chat_id)  # start
        bot.command_handler(message)

        answers = [
            BotMessage(test_email, test_chat_id),  # пользователь вводит эл. почту
            BotMessage(test_city1, test_chat_id),  # 5. пользователь вводит область, где его задержали
            BotMessage(test_place1, test_chat_id),  # 5.2 пользователь вводит место, где его задержали
            BotCall("DAY;2021;1;1", test_chat_id),  # 6. дату, когда задержали
            BotCall("Да, меня поместили отбывать арест", test_chat_id),  # 7.	Вас поместили туда по решению суда?
            BotMessage(test_court_city1, test_chat_id),  # 7.1.1.	Какой суд осудил вас?
            BotMessage(test_court_place1, test_chat_id),  # 7.1.1.	Какой суд осудил вас?
            BotCall(test_date_input2, test_chat_id),  # 7.1.2.	Когда этот суд осудил вас?
            BotCall("Вывозили в суд", test_chat_id),  # 7.1.3. Во время ареста вас вывозили из ...
            BotMessage(test_court_city2, test_chat_id),  # 7.1.3.2.1.	В какой суд вас вывозили?
            BotMessage(test_court_place2, test_chat_id),  # 7.1.3.2.1.	В какой суд вас вывозили?
            BotCall(test_date_input3, test_chat_id),  # 7.1.3.2.2.	Когда вас вывозили в суд?
            BotCall("Поместили в другой ЦИП или ИВС", test_chat_id),  # 7.1.3.2.3.	Что с вами произошло после суда?
            BotMessage(test_city2, test_chat_id),  # 7.1.3.2.3.2.1.	Куда вас поместили?
            BotMessage(test_place2, test_chat_id),  # 7.1.3.2.3.2.1.Куда вас поместили?
            BotCall("Не вывозили", test_chat_id),  # 7.1.3. Во время ареста вас вывозили из ...
            BotCall("Да", test_chat_id),  # 7.1.4.	Сразу после этого ареста вас снова задержали?
            BotCall("DAY;2022;2;2", test_chat_id),  # 7.1.4.1.1.	Когда закончился ваш предыдущий арест?

            BotMessage(test_city1, test_chat_id),  # 5. пользователь вводит область, где его задержали
            BotMessage(test_place2, test_chat_id),  # 5.2 пользователь вводит место, где его задержали
            BotCall("DAY;2022;2;2", test_chat_id),  # 6. дату, когда задержали
            BotCall("Да, меня поместили отбывать арест", test_chat_id),  # 7.	Вас поместили туда по решению суда?
            BotMessage(test_court_city1, test_chat_id),  # 7.1.1.	Какой суд осудил вас?
            BotMessage(test_court_place2, test_chat_id),  # 7.1.1.	Какой суд осудил вас?
            BotCall("DAY;2024;2;3", test_chat_id),  # 7.1.2.	Когда этот суд осудил вас?
            BotCall("Вывозили в ЦИП или ИВС", test_chat_id),  # 7.1.3. Во время ареста вас вывозили из ...
            BotMessage(test_city3, test_chat_id),  # 7.1.3.1.1.1.	Куда вас поместили?
            BotMessage(test_place3, test_chat_id),  # 7.1.3.1.1.1.	Куда вас поместили?
            BotCall("DAY;2024;3;4", test_chat_id),  # 7.1.3.1.1.2.	Когда вас туда поместили?
            BotCall("Не вывозили", test_chat_id),  # 7.1.3. Во время ареста вас вывозили из ...
            BotCall("Да", test_chat_id),  # 7.1.4.	Сразу после этого ареста вас снова задержали?
            BotCall("DAY;2024;4;4", test_chat_id),  # 7.1.4.1.1.	Когда закончился ваш предыдущий арест?

            BotMessage(test_city1, test_chat_id),  # 5. пользователь вводит область, где его задержали
            BotMessage(test_place1, test_chat_id),  # 5.2 пользователь вводит место, где его задержали
            BotCall("DAY;2023;3;3", test_chat_id),  # 6. дату, когда задержали
            BotCall("Да, меня поместили отбывать арест", test_chat_id),  # 7.	Вас поместили туда по решению суда?
            BotMessage(test_court_city1, test_chat_id),  # 7.1.1.	Какой суд осудил вас?
            BotMessage(test_court_place1, test_chat_id),  # 7.1.1.	Какой суд осудил вас?
            BotCall("DAY;2024;2;3", test_chat_id),  # 7.1.2.	Когда этот суд осудил вас?
            BotCall("Не вывозили", test_chat_id),  # 7.1.3. Во время ареста вас вывозили из ...
            BotCall("Нет", test_chat_id),  # 7.1.4.	Сразу после этого ареста вас снова задержали?
            BotCall("DAY;2025;5;5", test_chat_id),  # 7.1.4.1.1. 7.1.4.2.1.	Когда вас освободили?
        ]

        bot_dummy.answer_questions(bot, answers)
        personal_info = bot.bot_info.personal_info
        places = personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyPlacesList)
        self.assertEqual(7, len(places))
        self.assertEqual(
            (2025, 5, 5), personal_info.get_parameter(test_chat_id, config.PersonalInfoKeys.KeyReleaseDate))

        first_paragraph_true = "12.12.2022 суд Бешенковичского " \
                               "района вынес в отношении меня административное взыскание в виде административного" \
                               " ареста. 01.01.2021 меня поместили в {}. " \
                               "03.02.2024 меня вывезли из {} в суд Браславского района. " \
                               "04.03.2024 меня вывезли из {} в {}.02.02.2022 мой первый административный " \
                               "арест закончился. " \
                               "" \
                               "03.02.2024 суд Браславского района вынес в отношении меня административное взыскание " \
                               "в виде административного ареста. 02.02.2022 меня поместили в {}. " \
                               "05.04.2022 меня освободили. ".format(test_place1, test_place1,
                                                                     test_place2)


        first_paragraph_true = "12.12.2022 суд Бешенковичского " \
                               "района вынес в отношении меня административное взыскание в виде административного" \
                               " ареста. 10.10.2020 меня поместили в {}. 02.01.2023 мой первый административный " \
                               "арест закончился. " \
                               "03.02.2024 суд Браславского района вынес в отношении меня административное взыскание " \
                               "в виде административного ареста. 02.02.2022 меня поместили в {}. " \
                               "05.04.2022 меня освободили. ".format(test_place1, test_place2)

        first_paragraph_true = "12.12.2022 суд Бешенковичского " \
                               "района вынес в отношении меня административное взыскание в виде административного" \
                               " ареста. 10.10.2020 меня поместили в {}. 03.02.2024 " \
                               "меня вывезли из {} в {}. 04.03.2024 меня вывезли из {} в {}. " \
                               "02.01.2023 меня освободили. ".format(test_place1, test_place1, test_place2,
                                                                     test_place2, test_place3)
        true_message_places_complain = "Я хочу пожаловаться на:\n\n" \
                                       "1. Условия содержания в {}\n".format(test_place1) + \
                                       "2. Условия содержания в {}\n".format(test_place2) + \
                                       "3. Условия содержания в {}\n".format(test_place3) + \
                                       "4. Отсутствие питания в день поездки " \
                                       "из {} в {} 03.02.2024\n".format(test_place1, test_place2) + \
                                       "5. Отсутствие питания в день поездки " \
                                       "из {} в {} 04.03.2024\n".format(test_place2, test_place3) + \
                                       "6. Условия содержания во время поездки " + \
                                       "из {} в {} 03.02.2024\n".format(test_place1, test_place2) + \
                                       "7. Условия содержания во время поездки " + \
                                       "из {} в {} 04.03.2024\n".format(test_place2, test_place3) + \
                                       "\n(здесь и далее выбранное будет выделяться *жирным*)\n"

        true_message_places_complain = "Я хочу пожаловаться на:\n\n" \
                                       "1. Условия содержания в {}\n".format(test_place1) + \
                                       "2. Условия содержания в {}\n".format(test_place2) + \
                                       "\n(здесь и далее выбранное будет выделяться *жирным*)\n"

        chosen_places_numbers = [1, 2]
        is_first_place_prison = True
        test_bot_before_court.continue_to_choose_places(copy.deepcopy(bot), personal_info, test_chat_id,
                                                        first_paragraph_true, true_message_places_complain,
                                                        chosen_places_numbers, is_first_place_prison, self)
