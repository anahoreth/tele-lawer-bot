

import bot.bot_info as bot_info
import test_consts
import config
import states.ask_question as ask_question
import choose_places_to_complain


class DummyBot(object):
    def __init__(self):
        self.bot_info = bot_info.BotInfo(self)
        self.sent_messages = {}

    def command_handler(self, message):
        self.bot_info.handle_command(message)

    def message_handler(self, message):
        self.bot_info.handle_message(message)

    def query_handler(self, call):
        self.bot_info.handle_query(call)

    def send_message(self, chat_id, text, reply_markup=None, parse_mode="Markdown"):
        if self.sent_messages.get(chat_id) is None:
            self.sent_messages[chat_id] = [(text, reply_markup)]
        else:
            self.sent_messages[chat_id].append((text, reply_markup))
        return test_consts.BotMessage(text, chat_id)

    def delete_message(self, chat_id, message_id):
        return None

    def get_last_message(self, chat_id):
        if self.sent_messages.get(chat_id) is None:
            return None
        messages_count = len(self.sent_messages[chat_id])
        return self.sent_messages[chat_id][messages_count - 1][0]

    def edit_message_text(self, chat_id, text, message_id, reply_markup, parse_mode):
        return self.send_message(chat_id, text, reply_markup, parse_mode)


def get_question_bot(selected_groups, is_after_court, selected_place_type=config.ComplainPlaceType.SPlacePrison):
    chat_id = test_consts.test_chat_id
    bot = DummyBot()
    bot_info = bot.bot_info
    personal_info = bot_info.personal_info
    personal_info.init_chat(chat_id)

    if selected_place_type == config.ComplainPlaceType.SPlacePrison:
        selected_places = [choose_places_to_complain.ComplainPlaceInfo(selected_place_type,
                                                                       test_consts.test_place1,
                                                                       is_after_court=is_after_court)]
    else:  # selected_place_type == config.ComplainPlaceType.SPlaceTransport:
        selected_places = [choose_places_to_complain.ComplainPlaceInfo(selected_place_type,
                                                                       test_consts.test_place1,
                                                                       next_name=test_consts.test_place2,
                                                                       transfer_date=test_consts.test_date0)]

    personal_info.set_parameter(chat_id, config.PersonalInfoKeys.KeyComplainSelectedPlaceNames, selected_places)
    personal_info.set_parameter(chat_id, config.PersonalInfoKeys.KeyComplainSelectedPlaceCurrentNumber, 0)
    personal_info.set_parameter(chat_id, config.PersonalInfoKeys.KeyQuestionsSelectedCategories, selected_groups)

    new_state = ask_question.StateComplainQuestions(bot.bot_info, chat_id)
    bot_info.set_state(chat_id, new_state)
    return bot


def get_question_bot_places(selected_places, selected_groups):
    chat_id = test_consts.test_chat_id
    bot = DummyBot()
    bot_info = bot.bot_info
    personal_info = bot_info.personal_info
    personal_info.init_chat(chat_id)

    personal_info.set_parameter(chat_id, config.PersonalInfoKeys.KeyComplainSelectedPlaceNames, selected_places)
    personal_info.set_parameter(chat_id, config.PersonalInfoKeys.KeyComplainSelectedPlaceCurrentNumber, 0)
    personal_info.set_parameter(chat_id, config.PersonalInfoKeys.KeyQuestionsSelectedCategories, selected_groups)

    new_state = ask_question.StateComplainQuestions(bot.bot_info, chat_id)
    bot_info.set_state(chat_id, new_state)
    return bot


def answer_questions(bot, answers):
    for answer in answers:
        last_message = bot.get_last_message(test_consts.test_chat_id)

        if type(answer) == test_consts.BotMessage:
            # print("mes:", last_message, answer.text)
            bot.message_handler(answer)
        elif type(answer) == test_consts.BotCall:
            # print("call:",last_message, answer.message.text)
            bot.query_handler(answer)
