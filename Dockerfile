# Dockerfile
FROM python
RUN pip install pyTelegramBotAPI python-docx flask gunicorn xlrd openpyxl pandas
ADD bot /bot
ADD run_server.py /
ARG bot_token
ARG application_name
ARG admin_id
RUN echo "TOKEN_MASTER = '$bot_token'\nAPP_NAME = '$application_name' \nADMIN_ID_MASTER = $admin_id" >> tokens.py
CMD ["gunicorn", "run_server:server"]