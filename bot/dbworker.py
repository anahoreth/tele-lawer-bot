import config


class Place(object):
    def __init__(self):
        self.place_info = {}

    def place_set_parameter(self, key, value):
        self.place_info[key] = value

    def place_get_parameter(self, key):
        value = self.place_info.get(key)
        return value

    def to_string(self):
        # TODO проверить все это вообще
        start_date = self.place_info.get(config.PlaceKeys.KeyPlaceStartDate)
        assert start_date
        string_date = "{:02d}.{:02d}.{:04d}".format(start_date[2], start_date[1], start_date[0])

        is_court = self.place_info.get(config.PlaceKeys.KeyPlaceIsCourt)
        if is_court:
            name = self.place_info.get(config.PlaceKeys.KeyPlaceName)
            message = "отвезли в {} {}, ".format(name, string_date)
        else:
            is_first = self.place_info.get(config.PlaceKeys.KeyPlaceIsFirst)
            name = self.place_info[config.PlaceKeys.KeyPlaceName]
            if is_first:
                message = "задержали {}, поместили в {}, ".format(string_date, name)
            else:
                message = "{} поместили в {}, ".format(string_date, name)

        assert message
        return message


class PersonalInfo(object):
    def __init__(self):
        self.my_db = {}

    def init_chat(self, chat_id):
        self.my_db[chat_id] = {}
        self.set_parameter(chat_id, config.PersonalInfoKeys.KeyComplainSelectedPlaceNumbers, set())
        self.set_parameter(chat_id, config.PersonalInfoKeys.KeyComplainSelectedPlaceNames, [])
        self.set_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions, [])
        self.set_parameter(chat_id, config.PersonalInfoKeys.KeyPlacesList, [])
        self.set_parameter(chat_id, config.PersonalInfoKeys.KeyPlacesList, [])
        self.set_parameter(chat_id, config.PersonalInfoKeys.KeyIsArrestCourtDecision, True)

    def set_state(self, chat_id, state):
        self.my_db[chat_id][config.PersonalInfoKeys.KeyState] = state

    def get_current_state(self, chat_id):
        not_inited = self.my_db.get(chat_id) is None
        if not_inited:
            self.init_chat(chat_id)
            return None
        not_state = (self.my_db[chat_id]).get(config.PersonalInfoKeys.KeyState) is None
        if not_state:
            self.init_chat(chat_id)
            return None
        state = self.my_db[chat_id][config.PersonalInfoKeys.KeyState]
        return state

    def set_parameter(self, chat_id, parameter_name, value):
        self.my_db[chat_id][parameter_name] = value

    def get_parameter(self, chat_id, parameter_name):
        chat_db = self.my_db.get(chat_id)
        assert chat_db, "WRONG chat_id"
        parameter = chat_db.get(parameter_name)
        if parameter is None:
            print("WRONG parameter:", parameter_name)
        return parameter

    def set_place_parameter(self, chat_id, place_parameter_name, value):
        if place_parameter_name not in config.PlaceKeys:
            print("error!!! not in set place", place_parameter_name)
            return

        places = self.my_db[chat_id][config.PersonalInfoKeys.KeyPlacesList]
        current_place_number = len(places) - 1
        current_place = places[current_place_number]
        current_place.place_set_parameter(place_parameter_name, value)

    def get_place_parameter(self, chat_id, place_parameter_name):
        if place_parameter_name not in config.PlaceKeys:
            print("error!!! not in get place", place_parameter_name)
            return None
        try:
            is_after_court = self.get_parameter(chat_id, config.PersonalInfoKeys.KeyIsArrestCourtDecision)
            places_key = config.PersonalInfoKeys.KeyPlacesList \
                if is_after_court else config.PersonalInfoKeys.KeyPlacesList

            places_count = len(self.my_db[chat_id][places_key])
            current_place_number = places_count - 1
            current_place = self.my_db[chat_id][places_key][current_place_number]
            return current_place.place_get_parameter(place_parameter_name)
        except:
            print("error!!! get exception", place_parameter_name)
            return None

    def add_place(self, chat_id):
        (self.my_db[chat_id][config.PersonalInfoKeys.KeyPlacesList]).append(Place())

    def get_previous_place(self, chat_id):
        places = self.get_parameter(chat_id, config.PersonalInfoKeys.KeyPlacesList)
        if len(places) < 2:
            return None
        previous_place_number = len(places) - 2
        return places[previous_place_number]

    def is_previous_place_court(self, chat_id):
        length = len(self.my_db[chat_id][config.PersonalInfoKeys.KeyPlacesList])
        if length < 2:
            return False
        prev_place = self.get_previous_place(chat_id)
        return prev_place.place_get_parameter(config.PlaceKeys.KeyPlaceIsCourt)

    def is_returning_after_court_to_the_same_place(self, chat_id):
        is_transported_from_court = self.is_previous_place_court(chat_id)
        if not is_transported_from_court:
            return False

        places = self.get_parameter(chat_id, config.PersonalInfoKeys.KeyPlacesList)
        after_court_place_number = len(places) - 1
        after_court_place_name = places[after_court_place_number].place_get_parameter(config.PlaceKeys.KeyPlaceName)
        before_court_place_number = len(places) - 3
        before_court_place_name = places[before_court_place_number].place_get_parameter(config.PlaceKeys.KeyPlaceName)
        return after_court_place_name == before_court_place_name
