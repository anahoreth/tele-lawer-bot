from states.abstract import BotState

from keyboard import get_yes_no_keyboard, is_yes


class StateFatalError(BotState):
    def __init__(self, bot_info, chat_id):
        super().__init__(bot_info, chat_id)
        self.sent_start_message()

    def sent_start_message(self) -> None:
        chat_id = self._chat_id
        personal_info = self.bot_info.personal_info
        message = "Лол, надо всё переделывать. Нажмите /start"  # personal_info.get_info_about_places(chat_id)
        keyboard = None  # get_yes_no_keyboard()
        self._bot_info.bot_send_message(chat_id, message, keyboard)

    def handle_message(self, message) -> None:
        pass