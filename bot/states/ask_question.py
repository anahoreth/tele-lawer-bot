

from config import PersonalInfoKeys, IS_LOCAL, ComplainPlaceType

import config
from keyboard import get_question_keyboard


import questions.question_groups.abstract_group as abstract_group
import questions.question_groups.questions_types as questions_types
import questions.questions_entry as questions_entry


import enum

if config.IS_LOCAL:
    import abstract
    import choose_questions_category
    import form_complain
else:
    import states.abstract as abstract
    import states.choose_questions_category as choose_questions_category
    import states.form_complain as form_complain


class StateComplainQuestions(abstract.BotState):
    def __init__(self, bot_info, chat_id):
        super().__init__(bot_info, chat_id)
        personal_info = self.bot_info.personal_info
        self.selected_places = personal_info.get_parameter(chat_id, PersonalInfoKeys.KeyComplainSelectedPlaceNames)

        self.questions_number = personal_info.get_parameter(chat_id,
                                                            PersonalInfoKeys.KeyComplainSelectedPlaceCurrentNumber)

        self.selected_place = self.selected_places[self.questions_number]

        self.message_id = 0  # TODO разобраться с этим костылем
        self.sent_start_message()

    def sent_start_message(self):
        chat_id = self._chat_id
        personal_info = self.bot_info.personal_info

        selected_groups = None
        if self.selected_place.place_type == config.ComplainPlaceType.SPlacePrison:
            selected_groups = \
                [s for s in personal_info.get_parameter(self.chat_id, PersonalInfoKeys.KeyQuestionsSelectedCategories)]
        elif self.selected_place.place_type == config.ComplainPlaceType.SPlaceTransport:
            selected_groups = [questions_entry.QuestionGroupsName.TRANSPORT.value]
        elif self.selected_place.place_type in [config.ComplainPlaceType.SPlaceTransportFood,
                                                config.ComplainPlaceType.SPlaceTransportFoodCourt]:
            selected_groups = [questions_entry.QuestionGroupsName.TRANSPORT_FOOD.value]

        if selected_groups:
            new_questions = questions_entry.QuestionsDB(personal_info, chat_id, self.selected_place, selected_groups)
            questions = personal_info.get_parameter(chat_id, PersonalInfoKeys.KeyComplainQuestions)
            questions.append(new_questions)
            personal_info.set_parameter(chat_id, PersonalInfoKeys.KeyComplainQuestions, questions)
            # print (questions[self.questions_number])
            message, keyboard = get_message_and_keyboard(questions[self.questions_number])
            new_message = self._bot_info.bot_send_message(chat_id, message, keyboard, parse_mode="Markdown")
            self.message_id = new_message.message_id

    def get_ask_next_place_state(self):
        self.questions_number += 1
        self.bot_info.personal_info.set_parameter(self.chat_id,
                                                  PersonalInfoKeys.KeyComplainSelectedPlaceCurrentNumber,
                                                  self.questions_number)
        selected_places_count = len(self.selected_places)
        # если по каким-то местам еще не спрашивали, то переходим к следующему
        if self.questions_number < selected_places_count:
            next_place_type = self.selected_places[self.questions_number].place_type
            # если это МПС, то надо выбрать сначала категории
            if next_place_type == ComplainPlaceType.SPlacePrison:
                return choose_questions_category.StateGetCategories(self.bot_info, self.chat_id)
            # если транспортировка или еда при транспортировке, то сразу задаем вопросы
            return StateComplainQuestions(self.bot_info, self.chat_id)
        # если уже по всем местам опросили, формируем жалобу
        return form_complain.StateFormComplain(self.bot_info, self.chat_id)

    def handle_message(self, message):
        answer = message.text
        personal_info = self.bot_info.personal_info
        questions = personal_info.get_parameter(self.chat_id, PersonalInfoKeys.KeyComplainQuestions)
        current_questions = questions[self.questions_number]
        question = current_questions.get_current_question()
        question_type = question.get_type()
        if question_type.value == 1:  # questions_types.QuestionAnswerType.ANSWER_MESSAGE
            user_m_id = message.message_id
            self.bot_info.bot.delete_message(self.chat_id, user_m_id)
            question.set_choice(answer)
            if not current_questions.step_forward():
                next_state = self.get_ask_next_place_state()
                self.set_next_state(next_state)
                return

            self.edit_message(self.message_id)

    def handle_query(self, call):
        chat_id = self._chat_id
        personal_info = self.bot_info.personal_info
        questions = personal_info.get_parameter(chat_id, PersonalInfoKeys.KeyComplainQuestions)
        current_questions = questions[self.questions_number]
        command = call.data
        if command == "Pass":
            return
        elif command == "NextTopic":
            if not current_questions.topic_forward():
                next_state = self.get_ask_next_place_state()
                # assert next_state
                self.set_next_state(next_state)
                return
        elif command == "Ready":
            next_state = form_complain.StateFormComplain(self.bot_info, chat_id)
            self.set_next_state(next_state)
            return
        elif command == "PrevQ":
            if not current_questions.step_backward():
                message_id = call.message.message_id
                self.message_id = message_id
                return
        elif command == "NextQ":
            if not current_questions.step_forward():
                next_state = self.get_ask_next_place_state()
                # assert next_state
                self.set_next_state(next_state)
                return
        else:
            question = current_questions.get_current_question()
            question_type = question.get_type()

            if question_type.value == 2:  # ANSWER_QUERY_ONLY
                number = int(call.data)
                question.set_choice(number - 1)
                if not current_questions.step_forward():
                    next_state = self.get_ask_next_place_state()
                    assert next_state
                    self.set_next_state(next_state)
                    return
            else:  # ANSWER_QUERY_MANY
                choice = question.get_choice()
                number = int(call.data) - 1
                if number in choice:
                    choice.remove(number)
                else:
                    choice.add(number)
                question.set_choice(choice)

        message_id = call.message.message_id
        self.message_id = message_id
        self.edit_message(message_id)

    def edit_message(self, message_id):
        chat_id = self.chat_id
        personal_info = self.bot_info.personal_info
        questions = personal_info.get_parameter(chat_id, PersonalInfoKeys.KeyComplainQuestions)
        message, keyboard = get_message_and_keyboard(questions[self.questions_number])
        self.bot_info.bot.edit_message_text(chat_id=chat_id, text=message, message_id=message_id,
                                            reply_markup=keyboard, parse_mode="Markdown")


def get_message_and_keyboard(questions):
    question = questions.get_current_question()
    assert question
    message = "{}\n{}".format(questions.get_title, question.get_message())
    keyboard = question.get_keyboard()
    return message, keyboard
