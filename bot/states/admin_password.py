
import config

if config.IS_LOCAL:
    import abstract
    import admin_mode
else:
    import states.abstract as abstract
    import states.admin_mode as admin_mode


ADMIN_PASSWORD = "123"


class GetAdminPasswordState(abstract.BotState):
    def __init__(self, bot_info, chat_id):
        super().__init__(bot_info, chat_id)
        self.sent_start_message()

    def sent_start_message(self):
        message = "Вы перешли в режим администирования. " \
                             "Введите пароль (пароль по умолчанию - {}".format(ADMIN_PASSWORD)
        keyboard = None
        self._bot_info.bot_send_message(self.chat_id, message, keyboard)

    def handle_message(self, message) -> None:
        password = message.text
        chat_id = self._chat_id
        if password == ADMIN_PASSWORD:
            next_state = admin_mode.AdminModeState(self._bot_info, chat_id)
            self.set_next_state(next_state)
        else:
            message = "Неверный пароль администратора. Попробуйте еще раз набрать /admin или наберите /start" \
                               " для выхода из режима администрирования"
            keyboard = None
            self._bot_info.bot_send_message(chat_id, message, keyboard)
