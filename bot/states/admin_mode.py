

import config
import keyboard as kb
import os
import enum

if config.IS_LOCAL:
    import abstract
else:
    import states.abstract as abstract


def get_admin_keyboard():
    buttons_texts = ["Посмотреть статистику"]
    callback_data = ['Statistics']
    return kb.get_inline_keyboard_from_list(buttons_texts, callback_data)


class AdminModeState(abstract.BotState):
    def __init__(self, bot_info, chat_id):
        super().__init__(bot_info, chat_id)
        self.sent_start_message()

    def sent_start_message(self) -> None:
        message = "Выберите операцию"
        keyboard = get_admin_keyboard()
        self._bot_info.bot_send_message(self.chat_id, message, keyboard)

    def handle_query(self, call) -> None:
        action = call.data

        if action == "Statistics":
            stat_filename = self._bot_info.admin_table.get_filename()
            sent_result = self.send_document(stat_filename)

    def send_document(self, file_name):
        chat_id = self._chat_id
        bot = self.bot_info.bot
        if os.path.exists(file_name):
            doc = open(file_name, 'rb')
            bot.send_document(chat_id, doc)
            return True
        return False
