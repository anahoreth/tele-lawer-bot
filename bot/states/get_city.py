

import config
from keyboard import get_city_keyboard
from enum import Enum

if config.IS_LOCAL:
    import abstract
    import get_place as get_place
else:
    import states.abstract as abstract
    import states.get_place as get_place


# 5.1 места, где задержали
MESSAGE_GET_CITY_FIRST = 'В какой ИВС или ЦИП вас поместили сразу после задержания?'
MESSAGE_GET_COURT_REGION = 'В какой суд Вас вывозили (выберите регион)?'
MESSAGE_GET_CITY_ANOTHER_PLACE = 'Куда Вас поместили (выберите регион)'
MESSAGE_GET_COURT_BEFORE_ARREST_REGION = 'Какой суд осудил Вас (выберите регион)?'


class TypeOfGetCity(Enum):
    CITY_FIRST = "city_first"  # 5.1 - сразу после задержания
    COURT_WHICH_CONDEMNED = "court"  # 7.1.1 - суд, который осудил
    CITY_TRANSPORTED_AFTER_ARREST = "city_transported_after_arrest"  # 7.1.3.1 Вывозили в ЦИП или ИВС
    COURT_TRANSPORTED_AFTER_ARREST = "court_transported_after_arrest"  # 7.1.3.2.	Вывозили в суд
    CITY_TRANSPORTED_AFTER_COURT_AFTER_ARREST = "city_t_ac_aa"  # 7.1.3.2.3.2.1. Куда вас поместили?
    CITY_FIRST_AFTER_COURT_CONDEMNED = "c_f_a_c_c"  # 8.3.2.1.1.1 В каком ЦИП или ИВС вы начали отбывать арест?
    COURT_AFTER_ARREST_CITY = "c_a_a_c"  # 8.2 В какой суд вас отвезли?
    NEW_PLACE_NO_ARREST_AFTER_COURT_CITY = "n_p_n_a_c_c"  # 8.3.2.1.2.1.2.1 Куда вас поместили?


class StatePlaceGetCity(abstract.BotState):
    def __init__(self, bot_info, chat_id, type_of_get_city):
        super().__init__(bot_info, chat_id)
        self.type_of_get_city = type_of_get_city
        self.start_processing()
        self.sent_start_message()

    def start_processing(self):
        personal_info = self.bot_info.personal_info
        if self.type_of_get_city == TypeOfGetCity.CITY_FIRST:
            personal_info.add_place(self.chat_id)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsCourt, False)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsFirst, True)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsLast, False)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsAfterCourt, None)

        elif self.type_of_get_city == TypeOfGetCity.CITY_TRANSPORTED_AFTER_ARREST:
            personal_info.add_place(self.chat_id)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsCourt, False)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsLast, False)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsFirst, False)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsAfterCourt, True)
        elif self.type_of_get_city == TypeOfGetCity.COURT_TRANSPORTED_AFTER_ARREST:
            personal_info.add_place(self.chat_id)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsCourt, True)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsLast, False)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsFirst, False)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsAfterCourt, True)
        elif self.type_of_get_city == TypeOfGetCity.CITY_TRANSPORTED_AFTER_COURT_AFTER_ARREST:
            # это случай, когда после суда перемещают в другое место, поэтому дата начала - это дата суда
            # но это может быть и случай, когда суд был по видеосвязи
            is_released_in_court = personal_info.get_place_parameter(
                self.chat_id, config.PlaceKeys.KeyPlaceIsReleasedInCourt)
            # был суд по видеосвязи
            if (is_released_in_court is not None) and (not is_released_in_court):
                court_date = personal_info.get_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceCourtDate)
            else:
                court_date = personal_info.get_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceStartDate)

            personal_info.add_place(self.chat_id)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsCourt, False)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsLast, False)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsFirst, False)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsAfterCourt, True)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceStartDate, court_date)
        elif self.type_of_get_city == TypeOfGetCity.CITY_FIRST_AFTER_COURT_CONDEMNED:
            # это случай, когда суда перемещают первое место ареста, поэтому дата начала - это дата суда

            # но это может быть и случай, когда суд был по видеосвязи
            is_released_in_court = personal_info.get_place_parameter(
                self.chat_id, config.PlaceKeys.KeyPlaceIsReleasedInCourt)
            # был суд по видеосвязи
            if (is_released_in_court is not None) and (not is_released_in_court):
                court_date = personal_info.get_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceCourtDate)
            else:
                court_date = personal_info.get_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceStartDate)

            personal_info.add_place(self.chat_id)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsCourt, False)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsLast, False)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsFirst, False)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsAfterCourt, True)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceStartDate, court_date)
        elif self.type_of_get_city == TypeOfGetCity.COURT_AFTER_ARREST_CITY:
            # это случай, когда дата уже введена, и место уже добавлено, поэтому ничего не надо делать
            pass
        elif self.type_of_get_city == TypeOfGetCity.NEW_PLACE_NO_ARREST_AFTER_COURT_CITY:
            # это случай, когда после суда перевели в ИВС, поэтому дата начала - это дата суда
            court_date = personal_info.get_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceStartDate)
            personal_info.add_place(self.chat_id)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsCourt, False)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsLast, False)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsFirst, False)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsAfterCourt, False)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceStartDate, court_date)

    def sent_start_message(self):
        # TODO суд
        is_court = None
        if self.type_of_get_city == TypeOfGetCity.CITY_FIRST:
            is_court = False
        elif self.type_of_get_city == TypeOfGetCity.CITY_TRANSPORTED_AFTER_ARREST:
            is_court = False
        elif self.type_of_get_city == TypeOfGetCity.COURT_TRANSPORTED_AFTER_ARREST:
            is_court = True
        elif self.type_of_get_city == TypeOfGetCity.COURT_WHICH_CONDEMNED:
            is_court = True
        elif self.type_of_get_city == TypeOfGetCity.CITY_TRANSPORTED_AFTER_COURT_AFTER_ARREST:
            is_court = False
        elif self.type_of_get_city == TypeOfGetCity.CITY_FIRST_AFTER_COURT_CONDEMNED:
            is_court = False
        elif self.type_of_get_city == TypeOfGetCity.COURT_AFTER_ARREST_CITY:
            is_court = True
        elif self.type_of_get_city == TypeOfGetCity.NEW_PLACE_NO_ARREST_AFTER_COURT_CITY:
            is_court = False

        assert is_court is not None
        keyboard = get_city_keyboard(is_court)
        message = self.get_message_get_city()
        self._bot_info.bot_send_message(self.chat_id, message, keyboard)

    # TODO обработка того, что город неправильно введен
    def handle_message(self, message) -> None:
        city = message.text
        personal_info = self.bot_info.personal_info

        next_state = None
        if self.type_of_get_city == TypeOfGetCity.CITY_FIRST:
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceCity, city)
            type_get_place = get_place.TypeOfGetPlace.USUAL_PLACE
            next_state = get_place.StatePlaceGetPlace(self.bot_info, self.chat_id, type_get_place)
        elif self.type_of_get_city == TypeOfGetCity.COURT_WHICH_CONDEMNED:
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceCourtCity, city)
            next_state = get_place.StatePlaceGetPlace(self.bot_info,
                                                      self.chat_id, get_place.TypeOfGetPlace.COURT_WHICH_CONDEMNED)

        elif self.type_of_get_city == TypeOfGetCity.CITY_TRANSPORTED_AFTER_ARREST:
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceCity, city)
            next_state = get_place.StatePlaceGetPlace(self.bot_info,
                                                      self.chat_id, get_place.TypeOfGetPlace.AFTER_COURT_TRANSPORTED_PLACE)

        elif self.type_of_get_city == TypeOfGetCity.COURT_TRANSPORTED_AFTER_ARREST:
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceCity, city)
            next_state = get_place.StatePlaceGetPlace(self.bot_info,
                                                      self.chat_id, get_place.TypeOfGetPlace.AFTER_COURT_TRANSPORTED_COURT)

        elif self.type_of_get_city == TypeOfGetCity.CITY_TRANSPORTED_AFTER_COURT_AFTER_ARREST:
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceCity, city)
            type_ = get_place.TypeOfGetPlace.AFTER_ARREST_PLACE_TRANSPORTED_AFTER_COURT
            next_state = get_place.StatePlaceGetPlace(
                self.bot_info, self.chat_id, type_)
        elif self.type_of_get_city == TypeOfGetCity.CITY_FIRST_AFTER_COURT_CONDEMNED:
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceCity, city)
            type_ = get_place.TypeOfGetPlace.PLACE_FIRST_AFTER_COURT_CONDEMNED
            next_state = get_place.StatePlaceGetPlace(self.bot_info, self.chat_id, type_)
        elif self.type_of_get_city == TypeOfGetCity.COURT_AFTER_ARREST_CITY:
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceCity, city)
            type_ = get_place.TypeOfGetPlace.COURT_AFTER_ARREST
            next_state = get_place.StatePlaceGetPlace(self.bot_info, self.chat_id, type_)
        elif self.type_of_get_city == TypeOfGetCity.NEW_PLACE_NO_ARREST_AFTER_COURT_CITY:
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceCity, city)
            type_ = get_place.TypeOfGetPlace.NEW_PLACE_NO_ARREST_AFTER_COURT
            next_state = get_place.StatePlaceGetPlace(self.bot_info, self.chat_id, type_)
        assert next_state
        self.set_next_state(next_state)

    def get_message_get_city(self):
        message = ""
        if self.type_of_get_city == TypeOfGetCity.CITY_FIRST:
            message = MESSAGE_GET_CITY_FIRST
        elif self.type_of_get_city == TypeOfGetCity.COURT_WHICH_CONDEMNED:
            message = MESSAGE_GET_COURT_BEFORE_ARREST_REGION
        elif self.type_of_get_city == TypeOfGetCity.CITY_TRANSPORTED_AFTER_ARREST:
            message = MESSAGE_GET_CITY_ANOTHER_PLACE
        elif self.type_of_get_city == TypeOfGetCity.COURT_TRANSPORTED_AFTER_ARREST:
            message = MESSAGE_GET_COURT_REGION
        elif self.type_of_get_city == TypeOfGetCity.CITY_TRANSPORTED_AFTER_COURT_AFTER_ARREST:
            message = MESSAGE_GET_CITY_ANOTHER_PLACE
        elif self.type_of_get_city == TypeOfGetCity.CITY_FIRST_AFTER_COURT_CONDEMNED:
            message = "В каком ЦИП или ИВС вы начали отбывать арест (выберите регион)?"
        elif self.type_of_get_city == TypeOfGetCity.COURT_AFTER_ARREST_CITY:
            message = "В какой суд вас отвезли? (выберите регион)?"
        elif self.type_of_get_city == TypeOfGetCity.NEW_PLACE_NO_ARREST_AFTER_COURT_CITY:
            message = "Куда вас поместили? (выберите регион)?"
        assert message
        return message


class TypeOfBeforeCourtGetCity(Enum):
    COURT_WHICH_RELEASE = "c_w_r"  # 7.2.2.2.1.1.1.	Какой суд вас освободил?
    PLACE_TRANSPORTED_BEFORE_COURT = "p_t_b_c"  # 7.2.1.1.1. Куда вас поместили?
    COURT_WHICH_CONDEMNED = "c_w_c"  # 7.2.1.2.1. Какой суд осудил вас на сутки?


def get_message(type_):
    if type_ == TypeOfBeforeCourtGetCity.COURT_WHICH_RELEASE:
        return "Какой суд вас освободил (выберите регион)?"
    if type_ == TypeOfBeforeCourtGetCity.PLACE_TRANSPORTED_BEFORE_COURT:
        return "Куда Вас поместили (выберите регион)?"  # 7.2.1.1.1
    if type_ == TypeOfBeforeCourtGetCity.COURT_WHICH_CONDEMNED:
        return "Какой суд осудил вас на сутки (выберите регион)?"  # 7.2.1.2.1


class StateGetCityBeforeCourt(abstract.BotState):
    def __init__(self, bot_info, chat_id, type_of_get_city):
        super().__init__(bot_info, chat_id)
        self.type_of_get_city = type_of_get_city
        self.start_processing()
        self.sent_start_message()

    def start_processing(self):
        personal_info = self.bot_info.personal_info
        if self.type_of_get_city == TypeOfBeforeCourtGetCity.COURT_WHICH_RELEASE:
            # здесь была видеосвязь, в суд не водили, поэтому новое место не добавляется
            pass
        elif self.type_of_get_city == TypeOfBeforeCourtGetCity.PLACE_TRANSPORTED_BEFORE_COURT:
            personal_info.add_place(self.chat_id)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsCourt, False)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsFirst, False)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsLast, False)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsAfterCourt, False)
        elif self.type_of_get_city == TypeOfBeforeCourtGetCity.COURT_WHICH_CONDEMNED:
            # здесь была видеосвязь, в суд не водили, поэтому новое место не добавляется
            pass

    def sent_start_message(self):
        is_court = None
        if self.type_of_get_city == TypeOfBeforeCourtGetCity.COURT_WHICH_RELEASE:
            is_court = True
        if self.type_of_get_city == TypeOfBeforeCourtGetCity.PLACE_TRANSPORTED_BEFORE_COURT:
            is_court = False
        if self.type_of_get_city == TypeOfBeforeCourtGetCity.COURT_WHICH_CONDEMNED:
            is_court = True
        assert is_court is not None

        keyboard = get_city_keyboard(is_court)
        message = get_message(self.type_of_get_city)
        self._bot_info.bot_send_message(self.chat_id, message, keyboard)

    def handle_message(self, message) -> None:
        city = message.text
        personal_info = self.bot_info.personal_info

        next_state = None
        if self.type_of_get_city == TypeOfBeforeCourtGetCity.COURT_WHICH_RELEASE:
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceCourtCity, city)
            type_get_place = get_place.TypeOfGetPlaceBeforeCourt.COURT_WHICH_RELEASE
            next_state = get_place.StatePlaceBeforeGetPlace(self.bot_info, self.chat_id, type_get_place)
        elif self.type_of_get_city == TypeOfBeforeCourtGetCity.PLACE_TRANSPORTED_BEFORE_COURT:
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceCity, city)
            type_get_place = get_place.TypeOfGetPlaceBeforeCourt.PLACE_TRANSPORTED_BEFORE_COURT
            next_state = get_place.StatePlaceBeforeGetPlace(self.bot_info, self.chat_id, type_get_place)
        elif self.type_of_get_city == TypeOfBeforeCourtGetCity.COURT_WHICH_CONDEMNED:
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceCourtCity, city)
            type_get_place = get_place.TypeOfGetPlaceBeforeCourt.COURT_WHICH_CONDEMNED
            next_state = get_place.StatePlaceBeforeGetPlace(self.bot_info, self.chat_id, type_get_place)
        assert next_state
        self.set_next_state(next_state)



