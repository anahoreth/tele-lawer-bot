



from telegramcalendar import create_calendar, separate_callback_data, \
    CalendarActions, get_new_calendar

import datetime
from messages import MESSAGE_GET_START_DATE_COURT, MESSAGE_GET_START_DATE_FIRST, \
    MESSAGE_GET_START_DATE_ANOTHER_PLACE

from config import PlaceKeys

import states.abstract as abstract
import states.is_release_after_court as is_release_after_court
import states.get_city as get_city
import states.is_transported as is_transported


class StatePlaceGetReleaseDate(abstract.BotState):
    def __init__(self, bot_info, chat_id):
        super().__init__(bot_info, chat_id)
        now = datetime.datetime.now()
        self.year = now.year
        self.month = now.month
        self.sent_start_message()

    def sent_start_message(self) -> None:
        chat_id = self._chat_id
        message = self.get_message_get_date()
        keyboard = create_calendar(self.year, self.month)
        self.bot_info.bot_send_message(chat_id, message, keyboard)

    def handle_query(self, call) -> None:
        chat_id = call.message.chat.id
        (action, year, month, day) = separate_callback_data(call.data)
        year, month, day = int(year), int(month), int(day)

        if action == CalendarActions.C_DAY.value:
            key = PlaceKeys.KeyPlaceStartDate
            personal_info = self.bot_info.personal_info
            personal_info.set_place_parameter(chat_id, key, (year, month, day))
            # print((year, month, day))
            is_court = personal_info.get_place_parameter(chat_id, PlaceKeys.KeyPlaceIsCourt)
            if is_court:
                next_state = is_release_after_court.StatePlaceIsReleaseAfterCourt(self.bot_info, chat_id)
            else:
                is_first_place = personal_info.get_place_parameter(chat_id, PlaceKeys.KeyPlaceIsFirst)
                if is_first_place:
                    next_state = get_city.StatePlaceGetCity(self.bot_info, chat_id)
                else:
                    next_state = is_transported.StatePlaceIsTransported(self.bot_info, chat_id)

            self.set_next_state(next_state)
        elif action in [CalendarActions.C_PREV_MONTH.value, CalendarActions.C_NEXT_MONTH.value]:
            try:
                keyboard = get_new_calendar(action, year, month)
                message_id = call.message.message_id
                self.edit_message(message_id, keyboard)
            except:
                pass
        else:
            pass

   # def handle_command(self, command_name) -> None:
   #     pass

    # дополнительные функции

    def edit_message(self, message_id, keyboard) -> None:
        chat_id = self.chat_id
        message = self.get_message_get_date()
        self.bot_info.bot.edit_message_text(chat_id=chat_id, text=message, message_id=message_id,
                                            reply_markup=keyboard)

