
import keyboard as kb
import config

if config.IS_LOCAL:
    import abstract as abstract
    import get_city as get_city
    import is_transported as is_transported
else:
    import states.abstract as abstract
    import states.get_city as get_city
    import states.is_transported as is_transported


class StatePlaceWhereWentAfterCourt(abstract.BotState):
    def __init__(self, bot_info, chat_id):
        super().__init__(bot_info, chat_id)
        self.previous_place_name, self.previous_place_city = self.get_previous_name_and_city()
        self.court_date = self.get_court_date()
        self.sent_start_message()

    def get_previous_name_and_city(self):
        places = self.bot_info.personal_info.get_parameter(self.chat_id, config.PersonalInfoKeys.KeyPlacesList)
        assert len(places) >= 2
        previous_place_number = len(places) - 2
        previous_place_name = places[previous_place_number].place_get_parameter(config.PlaceKeys.KeyPlaceName)
        previous_place_city = places[previous_place_number].place_get_parameter(config.PlaceKeys.KeyPlaceCity)
        return previous_place_name, previous_place_city

    def sent_start_message(self):
        keyboard = kb.get_after_court_keyboard(self.previous_place_name)
        message = "Куда вас поместили после суда?"
        self._bot_info.bot_send_message(self.chat_id, message, keyboard)

    def get_court_date(self):
        places = self.bot_info.personal_info.get_parameter(self.chat_id, config.PersonalInfoKeys.KeyPlacesList)
        assert len(places) >= 2
        previous_court_number = len(places) - 1
        court_date = places[previous_court_number].place_get_parameter(config.PlaceKeys.KeyPlaceStartDate)
        return court_date

    def handle_message(self, message):
        choice = message.text
        is_the_same_place = not (choice == kb.AfterCourtButtons.Another.value)
        personal_info = self.bot_info.personal_info
        if is_the_same_place:
            personal_info.add_place(self.chat_id)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceName, self.previous_place_name)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceCity, self.previous_place_city)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceStartDate, self.court_date)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsFirst, False)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsCourt, False)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsAfterCourt, False)
            next_state = is_transported.StateIsTransportedFromPlaceToAnotherPlaceBeforeCourt(self.bot_info, self.chat_id)
        else:
            type_ = get_city.TypeOfGetCity.NEW_PLACE_NO_ARREST_AFTER_COURT_CITY
            next_state = get_city.StatePlaceGetCity(self.bot_info, self.chat_id, type_)

        self.set_next_state(next_state)
