from config import PersonalInfoKeys, IS_LOCAL

from keyboard import get_categories_keyboard

from messages import MESSAGE_CHOOSE_COMPLAIN_CATEGORIES, MESSAGE_NO_CATEGORY_CHOOSEN

if IS_LOCAL:
    import abstract
    import ask_question
else:
    import states.abstract as abstract
    import states.ask_question as ask_question
SELECTION_COMPLETE = 0


class StateGetCategories(abstract.BotState):
    def __init__(self, bot_info, chat_id):
        super().__init__(bot_info, chat_id)
        self.bot_info.personal_info.set_parameter(self.chat_id,
                                                  PersonalInfoKeys.KeyQuestionsSelectedCategories, [])
        personal_info = self.bot_info.personal_info
        self.current_number = personal_info.get_parameter(chat_id,
                                                          PersonalInfoKeys.KeyComplainSelectedPlaceCurrentNumber)
        places = personal_info.get_parameter(
            chat_id, PersonalInfoKeys.KeyComplainSelectedPlaceNames)

        self.place_name = places[self.current_number].current_name

        self.sent_start_message()

    def get_start_message(self):

        message = "_" + self.place_name + "\n\n" + MESSAGE_CHOOSE_COMPLAIN_CATEGORIES + "\n_"

        selected = self.bot_info.personal_info.get_parameter(self.chat_id,
                                                             PersonalInfoKeys.KeyQuestionsSelectedCategories)

        for cat_number in range(1, 1 + len(categories)):
            new_cat = "\n{}. {}".format(cat_number, categories[cat_number - 1])

            if cat_number in selected:
                new_cat = "*{}*".format(new_cat)
            message += new_cat

        return message

    def sent_start_message(self) -> None:
        chat_id = self._chat_id
        message = self.get_start_message()
        keyboard = get_categories_keyboard(len(categories))
        self._bot_info.bot_send_message(chat_id, message, keyboard, parse_mode="Markdown")

    def handle_message(self, message) -> None:
        pass

    def handle_query(self, call):
        number = int(call.data)
        personal_info = self.bot_info.personal_info
        if number == SELECTION_COMPLETE:
            selected_cat = personal_info.get_parameter(self.chat_id, PersonalInfoKeys.KeyQuestionsSelectedCategories)
            if len(selected_cat) > 0:
                selected_cat.sort()
                next_state = ask_question.StateComplainQuestions(self.bot_info, self.chat_id)
            else:
                message = MESSAGE_NO_CATEGORY_CHOOSEN
                keyboard = None
                self.bot_info.bot_send_message(self.chat_id, message, keyboard)
                next_state = StateGetCategories(self.bot_info, self.chat_id)
            self.set_next_state(next_state)
        else:
            personal_info = self.bot_info.personal_info
            selected_categories = \
                personal_info.get_parameter(self.chat_id, PersonalInfoKeys.KeyQuestionsSelectedCategories)
            if number in selected_categories:
                selected_categories.remove(number)
            else:
                selected_categories.append(number)

            personal_info.set_parameter(self.chat_id,
                                        PersonalInfoKeys.KeyQuestionsSelectedCategories, selected_categories)
            message_id = call.message.message_id
            self.edit_message(message_id)

    def edit_message(self, message_id):
        chat_id = self.chat_id
        message = self.get_start_message()
        keyboard = get_categories_keyboard(len(categories))
        self.bot_info.bot.edit_message_text(chat_id=chat_id, text=message, message_id=message_id,
                                            reply_markup=keyboard, parse_mode="Markdown")


categories = [
    'Отсутствие профилактики коронавируса',  # 1
    'Плохое питание или доступ к воде',  # 2
    "Отсутствие прогулок или их порядок",  # 3
    "Отсутствие необходимой медицинской помощи и осмотра в случае телесных повреждений",  # 4
    "Отсутствие доступа к душу или средствам личной гигиены",  # 5
    "Неразъяснение прав или отсутствие возможности жаловаться",  # 6
    "Отсутствие контактов с внешним миром и доступа к адвокату",  # 7
    "Постельные принадлежности",  # 8
    "Состояние и обстановка камеры и угроза заражения болезнями",  # 9
]
