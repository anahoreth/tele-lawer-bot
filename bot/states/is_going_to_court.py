
'''
from messages import MESSAGE_IS_GOING_TO_COURT
import config

import states.abstract as abstract

if not config.IS_LOCAL:
    import states.get_city as get_city
    import states.get_date as get_date
else:
    import get_city as get_city
    import get_date as get_date


class StatePlaceIsGoingToCourt(abstract.YesNoQuestionState):
    def __init__(self, bot_info, chat_id):
        super().__init__(bot_info, chat_id, MESSAGE_IS_GOING_TO_COURT)

    def answer_action(self, is_going_to_court):
        personal_info = self.bot_info.personal_info
        personal_info.add_place(self.chat_id)
        personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsFirst, False)
        personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsCourt, is_going_to_court)

        if is_going_to_court:
            next_state = get_date.StatePlaceGetDate(self.bot_info, self.chat_id, get_date.TypeOfGetDate.Court)
        else:

            next_state = get_city.StatePlaceGetCity(self.bot_info, self.chat_id)
        self.set_next_state(next_state)
'''
