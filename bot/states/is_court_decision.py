import config

from enum import Enum

if config.IS_LOCAL:
    import states.abstract as abstract
    import is_transported as is_transported
    import get_city
else:
    import states.abstract as abstract
    import states.is_transported as is_transported
    import states.get_city as get_city


class ButtonNames(Enum):
    AfterCourt = "Да, меня поместили отбывать арест"
    BeforeCourt = "Нет. Меня задержали до суда"


keyboard_answers_list = [[a.value] for a in ButtonNames]


MESSAGE_IS_COURT_DECISION = "Вас поместили туда по решению суда?"


class StatePlaceIsCourtDecision(abstract.QuestionWithAnswersState):
    def __init__(self, bot_info, chat_id):

        super().__init__(bot_info, chat_id, MESSAGE_IS_COURT_DECISION, keyboard_answers_list)

    def answer_action(self, answer):
        # print ("a StatePlaceIsCourtDecision")
        personal_info = self.bot_info.personal_info
        is_after_court = answer == ButtonNames.AfterCourt.value
        personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsAfterCourt, is_after_court)

        next_state = None
        if answer == ButtonNames.AfterCourt.value:
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsAfterCourt, True)
            next_state = get_city.StatePlaceGetCity(self.bot_info, self.chat_id,
                                                    get_city.TypeOfGetCity.COURT_WHICH_CONDEMNED)
        elif answer == ButtonNames.BeforeCourt.value:
            # print ("answer == ButtonNames.BeforeCourt.value")
            next_state = is_transported.StateIsTransportedFromPlaceToAnotherPlaceBeforeCourt(self.bot_info, self.chat_id)

        if next_state:
            self.set_next_state(next_state)
