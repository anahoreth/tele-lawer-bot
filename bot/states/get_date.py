

import telegramcalendar

import datetime
from messages import MESSAGE_GET_START_DATE_FIRST, MESSAGE_GET_START_DATE_ANOTHER_PLACE, \
    MESSAGE_GET_RELEASE_DATE, MESSAGE_GET_COURT_CONDEMNED_DATE

from config import PlaceKeys, PersonalInfoKeys, IS_LOCAL
from enum import Enum


if IS_LOCAL:
    import abstract
    import get_city
    import is_transported
    import show_places_list
    import is_court_decision
    import get_transport_after_court
else:
    import states.abstract as abstract
    import states.get_city as get_city
    import states.is_transported as is_transported
    import states.show_places_list as show_places_list
    import states.is_court_decision as is_court_decision
    import states.get_transport_after_court as get_transport_after_court


class TypeOfGetDate(Enum):
    Start = "start"  # дата начала в новом месте (п. 6, )
    COURT_DATE = "court_date"  # 7.1.2 дата суда, после которого поместили в ИВС или ЦИП
    AFTER_COURT_PLACE_TRANSPORTED = "aa_start"  # 7.1.3.1.1.2 дата начала в месте, в которое перевели
    AFTER_COURT_COURT_TRANSPORTED_DATE = "c_t_a_a"  # 7.1.3.2.2.	Когда вас вывозили в суд?
    AFTER_COURT_ARREST_END = "a_c_a_e"  # 7.1.4.1.1.	Когда закончился ваш предыдущий арест?
    BEFORE_COURT_PLACE_TRANSPORT_DATE = "b_c_p_t_d"  # 7.2.1.1.2	Когда вас туда поместили?
    BEFORE_COURT_COURT_WHICH_CONDEMNED_DATE = "b_c_c_w_c"  # Когда вас осудили на сутки?
    COURT_AFTER_ARREST_DATE = "c_a_a_d"  # 8.1 Когда вас отвезли в суд? Если в суд возили несколько ра
    Release = "release"  # освобождение 7.1.4.2.1.1


MESSAGE_GET_COURT_TRANSPORTED_AFTER_ARREST_DATE = "Когда вас вывозили в суд?"



class StatePlaceGetDate(abstract.BotState):
    def __init__(self, bot_info, chat_id, new_type):
        super().__init__(bot_info, chat_id)
        now = datetime.datetime.now()
        self.year = now.year
        self.month = now.month
        self.type = new_type

        self.sent_start_message()

    def sent_start_message(self) -> None:
        chat_id = self._chat_id
        message = self.get_message_get_date()
        keyboard = telegramcalendar.create_calendar(self.year, self.month)
        self.bot_info.bot_send_message(chat_id, message, keyboard)

    def handle_query(self, call) -> None:
        chat_id = call.message.chat.id
        action, year, month, day = telegramcalendar.separate_callback_data(call.data)
        year, month, day = int(year), int(month), int(day)

        if action == telegramcalendar.CalendarActions.C_DAY.value:
            personal_info = self.bot_info.personal_info

            next_state = None
            if self.type == TypeOfGetDate.Start:
                key = PlaceKeys.KeyPlaceStartDate
                personal_info.set_place_parameter(chat_id, key, (year, month, day))
                next_state = is_court_decision.StatePlaceIsCourtDecision(self.bot_info, chat_id)
            elif self.type == TypeOfGetDate.COURT_DATE:
                key = PlaceKeys.KeyPlaceCourtDate
                personal_info.set_place_parameter(chat_id, key, (year, month, day))
                type_of_transport = is_transported.TypeOfTransportation.AfterCourt
                next_state = is_transported.StatePlaceIsTransported(self.bot_info, chat_id, type_of_transport)
            elif self.type == TypeOfGetDate.AFTER_COURT_PLACE_TRANSPORTED:
                key = PlaceKeys.KeyPlaceStartDate
                personal_info.set_place_parameter(chat_id, key, (year, month, day))
                type_of_transport = is_transported.TypeOfTransportation.AfterCourt
                next_state = is_transported.StatePlaceIsTransported(self.bot_info, chat_id, type_of_transport)
            elif self.type == TypeOfGetDate.AFTER_COURT_COURT_TRANSPORTED_DATE:
                key = PlaceKeys.KeyPlaceStartDate
                personal_info.set_place_parameter(chat_id, key, (year, month, day))
                next_state = get_transport_after_court.StatePlaceGetTransportAfterCourt(self.bot_info, chat_id)
            elif self.type == TypeOfGetDate.AFTER_COURT_ARREST_END:
                # TODO вроде как не надо хранить эту дату, так как всё равно она потом изменится
                key = PlaceKeys.KeyPlaceArrestEndDate
                personal_info.set_place_parameter(chat_id, key, (year, month, day))

                typ = get_city.TypeOfGetCity.CITY_FIRST
                next_state = get_city.StatePlaceGetCity(self.bot_info, chat_id, typ)
            elif self.type == TypeOfGetDate.BEFORE_COURT_PLACE_TRANSPORT_DATE:
                key = PlaceKeys.KeyPlaceStartDate
                personal_info.set_place_parameter(chat_id, key, (year, month, day))
                next_state = is_transported.StateIsTransportedFromPlaceToAnotherPlaceBeforeCourt(self.bot_info, chat_id)
            elif self.type == TypeOfGetDate.BEFORE_COURT_COURT_WHICH_CONDEMNED_DATE:
                key = PlaceKeys.KeyPlaceCourtDate
                personal_info.set_place_parameter(chat_id, key, (year, month, day))
                type_ = get_city.TypeOfGetCity.CITY_FIRST_AFTER_COURT_CONDEMNED
                next_state = get_city.StatePlaceGetCity(self.bot_info, chat_id, type_)
            elif self.type == TypeOfGetDate.COURT_AFTER_ARREST_DATE:

                personal_info.add_place(self.chat_id)
                personal_info.set_place_parameter(self.chat_id, PlaceKeys.KeyPlaceIsCourt, True)
                personal_info.set_place_parameter(self.chat_id, PlaceKeys.KeyPlaceIsFirst, False)
                personal_info.set_place_parameter(self.chat_id, PlaceKeys.KeyPlaceIsFirst, False)
                personal_info.set_place_parameter(self.chat_id, PlaceKeys.KeyPlaceIsAfterCourt, False)
                personal_info.set_place_parameter(self.chat_id, PlaceKeys.KeyPlaceStartDate, (year, month, day))

                type_ = get_city.TypeOfGetCity.COURT_AFTER_ARREST_CITY
                next_state = get_city.StatePlaceGetCity(self.bot_info, chat_id, type_)

            elif self.type == TypeOfGetDate.Release:
                key = PersonalInfoKeys.KeyReleaseDate
                personal_info.set_parameter(chat_id, key, (year, month, day))
                personal_info.set_place_parameter(chat_id,  PlaceKeys.KeyPlaceIsLast, True)
                next_state = show_places_list.StatePlaceShowList(self.bot_info, chat_id)

            assert next_state
            self.set_next_state(next_state)

        elif action in [telegramcalendar.CalendarActions.C_PREV_MONTH.value,
                        telegramcalendar.CalendarActions.C_NEXT_MONTH.value]:
            try:
                keyboard = telegramcalendar.get_new_calendar(action, year, month)
                message_id = call.message.message_id
                message = self.get_message_get_date()
                self.bot_info.bot_edit_message(self.chat_id, message, message_id, keyboard)
            except:
                pass
        else:
            pass

    def get_message_get_date(self):
        message = ""
        if self.type == TypeOfGetDate.Start:
            personal_info = self.bot_info.personal_info
            chat_id = self.chat_id
            is_first = personal_info.get_place_parameter(chat_id, PlaceKeys.KeyPlaceIsFirst)
            if is_first:
                message = MESSAGE_GET_START_DATE_FIRST
            else:
                # TODO
                pass
        if self.type == TypeOfGetDate.COURT_DATE:
            return MESSAGE_GET_COURT_CONDEMNED_DATE
        if self.type == TypeOfGetDate.AFTER_COURT_PLACE_TRANSPORTED:
            return MESSAGE_GET_START_DATE_ANOTHER_PLACE
        if self.type == TypeOfGetDate.Release:
            return MESSAGE_GET_RELEASE_DATE
        if self.type == TypeOfGetDate.AFTER_COURT_COURT_TRANSPORTED_DATE:
            return MESSAGE_GET_COURT_TRANSPORTED_AFTER_ARREST_DATE
        if self.type == TypeOfGetDate.AFTER_COURT_ARREST_END:
            return "Когда закончился ваш предыдущий арест?"
        if self.type == TypeOfGetDate.BEFORE_COURT_COURT_WHICH_CONDEMNED_DATE:
            return "Когда вас осудили на сутки?"
        if self.type == TypeOfGetDate.COURT_AFTER_ARREST_DATE:
            return "Когда вас отвезли в суд? \nЕсли в суд возили несколько раз, укажите сначала дату первой поездки " \
                   "и далее отвечайте про первую поездку"

        if self.type == TypeOfGetDate.BEFORE_COURT_PLACE_TRANSPORT_DATE:
            return "Когда вас туда поместили?"
        return message

