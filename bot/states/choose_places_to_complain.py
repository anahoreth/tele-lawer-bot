
from config import PersonalInfoKeys, PlaceKeys, ComplainPlaceType
import keyboard as kb

from messages import months_names

import states.abstract as abstract
import states.choose_questions_category as choose_questions_category
import states.ask_question as ask_question

SELECTION_COMPLETE = 0
MESSAGE_NO_PLACE_CHOOSEN = "Вы не выбрали ни одного пункта. Попробуйте, пожалуйста, еще раз"


# TODO убрать костыль
before_and_after_court = 2
before_and_after_court_message = "до и после суда"


def get_places_list_for_complain(personal_info, chat_id):
    places = personal_info.get_parameter(chat_id, PersonalInfoKeys.KeyPlacesList)

    if not places:
        return set(), []

    places_unique = []
    transfers = []
    transfers_food = []

    for place in places:
        is_court = place.place_get_parameter(PlaceKeys.KeyPlaceIsCourt)
        if is_court:
            continue
        current_name = place.place_get_parameter(PlaceKeys.KeyPlaceName)
        is_after_court = place.place_get_parameter(PlaceKeys.KeyPlaceIsAfterCourt)
        place_type = ComplainPlaceType.SPlacePrison
        is_found = False
        for i in range(len(places_unique)):
            existed_current_name = places_unique[i].current_name
            existed_is_after_court = places_unique[i].is_after_court
            if current_name != existed_current_name:
                continue
            if is_after_court == existed_is_after_court:
                is_found = True
                break
            places_unique.remove(places_unique[i])
            new_element = ComplainPlaceInfo(place_type, current_name, is_after_court=before_and_after_court)
            places_unique.append(new_element)
            is_found = True
            break

        if not is_found:
            element = ComplainPlaceInfo(place_type, current_name, is_after_court=is_after_court)
            places_unique.append(element)
    # print(places_unique)

    # TODO рассмотреть ситуацию суд-мпс1-суд-мпс2
    for i in range(0, len(places) - 1):
        # когда несколько арестов, запретить переход от последнего места ареста к первому месту следующего ареста
        is_current_place_last = places[i].place_get_parameter(PlaceKeys.KeyPlaceIsLast)
        if is_current_place_last:
            continue

        current_name = places[i].place_get_parameter(PlaceKeys.KeyPlaceName)
        next_name = places[i + 1].place_get_parameter(PlaceKeys.KeyPlaceName)
        transfer_date = places[i + 1].place_get_parameter(PlaceKeys.KeyPlaceStartDate)
        # добавляем сначала просто транспортировку
        place_type = ComplainPlaceType.SPlaceTransport
        new_element = ComplainPlaceInfo(place_type, current_name, transfer_date=transfer_date,
                                        next_name=next_name)
        transfers.append(new_element)

        # теперь разбираемся с едой
        # если место - суд, то оно учитывалось в схеме МПС-суд-МПС
        is_place_court = places[i].place_get_parameter(PlaceKeys.KeyPlaceIsCourt)
        if is_place_court:
            continue

        is_next_court = places[i + 1].place_get_parameter(PlaceKeys.KeyPlaceIsCourt)
        if is_next_court:
            next_next_name = None
            if i < len(places) - 2:
                next_next_name = places[i + 2].place_get_parameter(PlaceKeys.KeyPlaceName)
            place_type = ComplainPlaceType.SPlaceTransportFoodCourt
            # МПС - суд или МПС - суд - МПС
            new_element = ComplainPlaceInfo(place_type, current_name, transfer_date=transfer_date,
                                            next_name=next_name, next_next_name=next_next_name)
            transfers_food.append(new_element)

        elif current_name != next_name:
            # МПС - МПС
            place_type = ComplainPlaceType.SPlaceTransportFood
            new_element = ComplainPlaceInfo(place_type, current_name, transfer_date=transfer_date,
                                            next_name=next_name)
            transfers_food.append(new_element)

    return places_unique + transfers_food + transfers


def fix_court_message(m):
    if m.find("Суд") != -1 or m.find("суд") != -1:
        m = m.replace("из Суд ", "из суда ")
        m = m.replace("в Суд ", "в суд ")
        m = m.replace("из Минский городской суд", "из Минского городского суда")
        m = m.replace("из Брестский областной суд", "из Брестского областного суда")
        m = m.replace("из Гомельский областной суд", "из Гомельского областного суда")
        m = m.replace("из Гродненский областной суд", "из Гродненского областного суда")
        m = m.replace("из Минский областной суд", "из Минского областного суда")
        m = m.replace("из Могилевский областной суд", "из Могилевского областного суда")
    return m


def get_message_place(place):
    place_type = place.place_type
    if place_type == ComplainPlaceType.SPlacePrison:
        current_name = place.current_name
        return "Условия содержания в {}".format(current_name)
    if place_type == ComplainPlaceType.SPlaceTransport:
        current_name = place.current_name
        next_name = place.next_name
        transfer_date = place.transfer_date
        new_message = "Условия содержания во время поездки из {} в {} {:02d}.{:02d}.{:04d}".format(
            current_name, next_name, transfer_date[2], transfer_date[1], transfer_date[0])
        new_message = fix_court_message(new_message)
        return new_message
    if place_type == ComplainPlaceType.SPlaceTransportFood:
        current_name = place.current_name
        next_name = place.next_name
        transfer_date = place.transfer_date
        new_message = "Отсутствие питания в день поездки из {} в {} {:02d}.{:02d}.{:04d}".format(
            current_name, next_name, transfer_date[2], transfer_date[1], transfer_date[0])
        new_message = fix_court_message(new_message)
        return new_message
    if place_type == ComplainPlaceType.SPlaceTransportFoodCourt:
        current_name = place.current_name
        next_name = place.next_name
        next_next_name = place.next_next_name
        transfer_date = place.transfer_date
        if not next_next_name:
            new_message = "Отсутствие питания в день поездки из {} в {} {:02d}.{:02d}.{:04d}".format(
                current_name, next_name, transfer_date[2], transfer_date[1], transfer_date[0])
            new_message = fix_court_message(new_message)
            return new_message
        if current_name == next_next_name:
            new_message = "Отсутствие питания в день поездки из {} в {} и обратно {:02d}.{:02d}.{:04d}".format(
                current_name, next_name, transfer_date[2], transfer_date[1], transfer_date[0])
            new_message = fix_court_message(new_message)
            return new_message

        new_message = "Отсутствие питания в день поездки из {} в {} и из {} в {} {:02d}.{:02d}.{:04d}".format(
            current_name, next_name, next_name, next_next_name,
            transfer_date[2], transfer_date[1], transfer_date[0])
        new_message = fix_court_message(new_message)
        return new_message


class StateComplainChoosePlacesToComplain(abstract.BotState):
    def __init__(self, bot_info, chat_id):
        super().__init__(bot_info, chat_id)
        self.places = get_places_list_for_complain(self.bot_info.personal_info, self.chat_id)
        self.sent_start_message()

    def sent_start_message(self):
        message = self.get_message()
        keyboard = self.get_keyboard()
        self._bot_info.bot_send_message(self.chat_id, message, keyboard)

    def handle_message(self, message):
        pass

    def edit_message(self, message_id):
        chat_id = self.chat_id
        message = self.get_message()
        keyboard = self.get_keyboard()

        self.bot_info.bot.edit_message_text(chat_id=chat_id, text=message, message_id=message_id,
                                            reply_markup=keyboard, parse_mode="Markdown")

    def get_keyboard(self):
        return kb.get_keyboard_digits(len(self.places))



    def get_message(self):

        chat_id = self.chat_id
        personal_info = self.bot_info.personal_info
        selected = personal_info.get_parameter(chat_id, PersonalInfoKeys.KeyComplainSelectedPlaceNumbers)

        message = "Я хочу пожаловаться на:\n\n"
        counter = 1
        for place in self.places:
            new_message = "{}. {}\n".format(counter, get_message_place(place))
            if not new_message:
                continue
            if counter in selected:
                new_message = "*{}*".format(new_message)
            message += new_message
            counter += 1

        message_will_be_bold = "\n(здесь и далее выбранное будет выделяться *жирным*)\n"
        message += message_will_be_bold
        return message

    def handle_query(self, call):
        try:
            number = int(call.data)
        except:
            return
        personal_info = self.bot_info.personal_info

        if number == SELECTION_COMPLETE:
            selected_places_numbers = \
                personal_info.get_parameter(self.chat_id, PersonalInfoKeys.KeyComplainSelectedPlaceNumbers)

            if not selected_places_numbers:
                self.bot_info.bot_send_message(self.chat_id, MESSAGE_NO_PLACE_CHOOSEN, None)
                next_state = StateComplainChoosePlacesToComplain(self.bot_info, self.chat_id)
                self.set_next_state(next_state)
                return

            selected_places_names = [self.places[i-1] for i in selected_places_numbers]

            # print("выбранные места: ", selected_places_names)

            personal_info.set_parameter(self.chat_id, PersonalInfoKeys.KeyComplainSelectedPlaceNames,
                                        selected_places_names)

            new_place_number = 0
            personal_info.set_parameter(self.chat_id, PersonalInfoKeys.KeyComplainSelectedPlaceCurrentNumber,
                                        new_place_number)
            # если ИВС, то надо выбрать категорию, иначе сразу задаем вопросы
            first_place_type = selected_places_names[new_place_number].place_type
            if first_place_type == ComplainPlaceType.SPlacePrison:
                next_state = choose_questions_category.StateGetCategories(self.bot_info, self.chat_id)
            else:
                next_state = ask_question.StateComplainQuestions(self.bot_info, self.chat_id)
            self.set_next_state(next_state)
        else:
            personal_info = self.bot_info.personal_info

            selected_place_numbers = \
                personal_info.get_parameter(self.chat_id, PersonalInfoKeys.KeyComplainSelectedPlaceNumbers)

            if number in selected_place_numbers:
                selected_place_numbers.remove(number)
            else:
                selected_place_numbers.add(number)

            personal_info.set_parameter(self.chat_id, PersonalInfoKeys.KeyComplainSelectedPlaceNumbers,
                                        selected_place_numbers)
            message_id = call.message.message_id
            self.edit_message(message_id)


class ComplainPlaceInfo(object):
    def __init__(self, place_type, current_name, is_after_court=None,
                 transfer_date=None, next_name=None, next_next_name=None):
        self.place_type = place_type
        self.current_name = current_name
        self.is_after_court = is_after_court
        self.transfer_date = transfer_date
        self.next_name = next_name
        self.next_next_name = next_next_name



"""


    def set_selected(self):
        chat_id = self.chat_id
        personal_info = self.bot_info.personal_info

        places_unique, court_dates, transfers = \
            get_places_list_for_complain(personal_info, chat_id)
        selected_places = []
        selected = personal_info.get_parameter(chat_id, PersonalInfoKeys.KeyComplainSelectedPlaceNumbers)
        counter = 1
        for place in places_unique:
            if counter in selected:
                message = "Жалобы на условия содержания в " + place[0] + " " + place[1]
                selected_places.append((message, ComplainPlaceType.SPlacePrison, place[2]))
            counter += 1

        for transfer in transfers:
            first, second = transfer
            if counter in selected:
                new_message = "Отсутствие питания в день поездки из " + \
                              first[0] + " " + first[1] + " в " + second[0] + second[1] + "\n"
                selected_places.append((new_message, ComplainPlaceType.SPlaceTransportFood))
            counter += 1

        for transfer in transfers:
            first, second = transfer
            if counter in selected:
                new_message = "Условия содержания во время поездки из " + first[0] + " " + first[1] \
                              + " в " + second[0] + second[1] + "\n"
                selected_places.append((new_message, ComplainPlaceType.SPlaceTransport))
            counter += 1
        personal_info.set_parameter(chat_id, PersonalInfoKeys.KeyComplainSelectedPlaceNames, selected_places)

        return len(selected_places)
        
        
for court_date in court_dates:
            if counter in selected:
                year, month, day = court_date
                month = months_names[month - 1]

                new_message = "Жалобы на то, что было во время поездки в суд и/или из суда и/или " \
                              "во время нахождения в суде " \
                              + str(day) + " " + month + " " + str(year) + "\n"
                selected_places.append((new_message, ComplainPlaceType.SPlaceCourt))
            counter += 1
 
for court_date in court_dates:
    year, month, day = court_date
    month = months_names[month - 1]
    new_message = str(counter) + \
                  ". Во время поездки в суд и/или из суда и/или во время нахождения в суде " \
                  + str(day) + " " + month + " " + str(year) + ".\n"
    if counter in selected:
        new_message = "*" + new_message + "*"
    message += new_message + '\n'
    counter += 1"""