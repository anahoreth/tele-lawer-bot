from config import PlaceKeys, IS_LOCAL, IS_REAL_BOT
from keyboard import get_place_keyboard

import names

import states.abstract as abstract
import states.get_date as get_date
import states.where_went_after_court as where_went_after_court
import states.is_transported as is_transported
import states.is_release_after_court as is_release_after_court

from enum import Enum


class TypeOfGetPlace(Enum):
    USUAL_PLACE = 'place'
    COURT_WHICH_CONDEMNED = "court"  # 7.1.1 - суд, который осудил
    AFTER_COURT_TRANSPORTED_PLACE = "aa_place"  # 7.1.3.1.1.1 - место, куда посадили
    AFTER_COURT_TRANSPORTED_COURT = "aa_court"  # 7.1.3.2.1.	В какой суд вас вывозили?
    AFTER_ARREST_PLACE_TRANSPORTED_AFTER_COURT = "aa_p_t_a_c"  # 7.1.3.2.3.2.1.	Куда вас поместили?
    PLACE_FIRST_AFTER_COURT_CONDEMNED = "p_f_c_c"  # 8.3.2.1.1 В каком ЦИП или ИВС вы начали отбывать арест?
    COURT_AFTER_ARREST = "c_a_a"  # 8.2 В какой суд вас отвезли?
    NEW_PLACE_NO_ARREST_AFTER_COURT = "n_p_n_a_a_c"  # 8.3.2.1.2.1.2.1 Куда вас поместили?


MESSAGE_GET_CITY_ANOTHER_PLACE = 'Куда Вас поместили (выберите город)'
MESSAGE_GET_COURT = 'Какой суд осудил Вас (выберите суд)?'
MESSAGE_GET_COURT_BEFORE_COURT = 'Какой суд вас освободил?'
MESSAGE_GET_PLACE = 'Укажите конкретный ЦИП или ИВС'


class StatePlaceGetPlace(abstract.BotState):
    def __init__(self, bot_info, chat_id, type_):
        super().__init__(bot_info, chat_id)
        self.type = type_
        self.is_court = self.type in [TypeOfGetPlace.COURT_WHICH_CONDEMNED,
                                      TypeOfGetPlace.AFTER_COURT_TRANSPORTED_COURT,
                                      TypeOfGetPlace.COURT_AFTER_ARREST]
        self.sent_start_message()

    def sent_start_message(self) -> None:
        chat_id = self._chat_id
        personal_info = self.bot_info.personal_info
        keyboard = None
        message = ""
        if self.type == TypeOfGetPlace.USUAL_PLACE:
            city = personal_info.get_place_parameter(chat_id, PlaceKeys.KeyPlaceCity)
            keyboard = get_place_keyboard(city, False)
            message = MESSAGE_GET_PLACE
        elif self.type == TypeOfGetPlace.COURT_WHICH_CONDEMNED:
            city = personal_info.get_place_parameter(chat_id, PlaceKeys.KeyPlaceCourtCity)
            keyboard = get_place_keyboard(city, True)
            message = MESSAGE_GET_COURT
        elif self.type == TypeOfGetPlace.AFTER_COURT_TRANSPORTED_PLACE:
            city = personal_info.get_place_parameter(chat_id, PlaceKeys.KeyPlaceCity)
            keyboard = get_place_keyboard(city, False)
            message = MESSAGE_GET_PLACE
        elif self.type == TypeOfGetPlace.AFTER_COURT_TRANSPORTED_COURT:
            city = personal_info.get_place_parameter(chat_id, PlaceKeys.KeyPlaceCity)
            keyboard = get_place_keyboard(city, True)
            message = MESSAGE_GET_COURT
        elif self.type == TypeOfGetPlace.AFTER_ARREST_PLACE_TRANSPORTED_AFTER_COURT:
            city = personal_info.get_place_parameter(chat_id, PlaceKeys.KeyPlaceCity)
            keyboard = get_place_keyboard(city, False)
            message = MESSAGE_GET_PLACE
        elif self.type == TypeOfGetPlace.PLACE_FIRST_AFTER_COURT_CONDEMNED:
            city = personal_info.get_place_parameter(chat_id, PlaceKeys.KeyPlaceCity)
            keyboard = get_place_keyboard(city, self.is_court)
            message = "В каком ЦИП или ИВС вы начали отбывать арест?"
        elif self.type == TypeOfGetPlace.COURT_AFTER_ARREST:
            city = personal_info.get_place_parameter(chat_id, PlaceKeys.KeyPlaceCity)
            keyboard = get_place_keyboard(city, self.is_court)
            message = "В какой суд вас отвезли?"
        elif self.type == TypeOfGetPlace.NEW_PLACE_NO_ARREST_AFTER_COURT:
            city = personal_info.get_place_parameter(chat_id, PlaceKeys.KeyPlaceCity)
            keyboard = get_place_keyboard(city, self.is_court)
            message = "Куда вас поместили?"

        if IS_REAL_BOT:
            assert keyboard is not None
        assert message
        self._bot_info.bot_send_message(chat_id, message, keyboard)

    def handle_message(self, message) -> None:
        place = message.text
        personal_info = self.bot_info.personal_info
        # TODO что-то делать вместо assert в этом случае
        assert self.is_correct_place(personal_info, self.chat_id, place)

        next_state = None
        if self.type == TypeOfGetPlace.USUAL_PLACE:
            personal_info.set_place_parameter(self.chat_id, PlaceKeys.KeyPlaceName, place)
            next_state = get_date.StatePlaceGetDate(self._bot_info, self.chat_id, get_date.TypeOfGetDate.Start)
        elif self.type == TypeOfGetPlace.COURT_WHICH_CONDEMNED:
            personal_info.set_place_parameter(self.chat_id, PlaceKeys.KeyPlaceCourtPlace, place)
            next_state = get_date.StatePlaceGetDate(self._bot_info, self.chat_id, get_date.TypeOfGetDate.COURT_DATE)
        elif self.type == TypeOfGetPlace.AFTER_COURT_TRANSPORTED_PLACE:
            personal_info.set_place_parameter(self.chat_id, PlaceKeys.KeyPlaceName, place)
            next_state = get_date.StatePlaceGetDate(self._bot_info, self.chat_id,
                                                    get_date.TypeOfGetDate.AFTER_COURT_PLACE_TRANSPORTED)
        elif self.type == TypeOfGetPlace.AFTER_COURT_TRANSPORTED_COURT:
            personal_info.set_place_parameter(self.chat_id, PlaceKeys.KeyPlaceName, place)
            next_state = get_date.StatePlaceGetDate(self._bot_info, self.chat_id,
                                                    get_date.TypeOfGetDate.AFTER_COURT_COURT_TRANSPORTED_DATE)
        elif self.type == TypeOfGetPlace.AFTER_ARREST_PLACE_TRANSPORTED_AFTER_COURT:
            personal_info.set_place_parameter(self.chat_id, PlaceKeys.KeyPlaceName, place)
            type_of_transport = is_transported.TypeOfTransportation.AfterCourt
            next_state = is_transported.StatePlaceIsTransported(self._bot_info, self.chat_id, type_of_transport)
        elif self.type == TypeOfGetPlace.PLACE_FIRST_AFTER_COURT_CONDEMNED:
            personal_info.set_place_parameter(self.chat_id, PlaceKeys.KeyPlaceName, place)
            type_ = is_transported.TypeOfTransportation.AfterCourt
            next_state = is_transported.StatePlaceIsTransported(self._bot_info, self.chat_id, type_)
        elif self.type == TypeOfGetPlace.COURT_AFTER_ARREST:
            personal_info.set_place_parameter(self.chat_id, PlaceKeys.KeyPlaceName, place)
            next_state = is_release_after_court.StatePlaceIsReleaseInCourt(self.bot_info, self.chat_id)
        elif self.type == TypeOfGetPlace.NEW_PLACE_NO_ARREST_AFTER_COURT:
            personal_info.set_place_parameter(self.chat_id, PlaceKeys.KeyPlaceName, place)
            next_state = is_transported.StateIsTransportedFromPlaceToAnotherPlaceBeforeCourt(self.bot_info, self.chat_id)
        # assert next_state
        self.set_next_state(next_state)

    def is_correct_place(self, personal_info, chat_id, place):
        city = ""
        if self.type == TypeOfGetPlace.USUAL_PLACE:
            city = personal_info.get_place_parameter(chat_id, PlaceKeys.KeyPlaceCity)
        elif self.type == TypeOfGetPlace.AFTER_COURT_TRANSPORTED_PLACE:
            city = personal_info.get_place_parameter(chat_id, PlaceKeys.KeyPlaceCity)
        elif self.type == TypeOfGetPlace.COURT_WHICH_CONDEMNED:
            city = personal_info.get_place_parameter(chat_id, PlaceKeys.KeyPlaceCourtCity)
        elif self.type == TypeOfGetPlace.AFTER_COURT_TRANSPORTED_COURT:
            city = personal_info.get_place_parameter(chat_id, PlaceKeys.KeyPlaceCity)
        elif self.type == TypeOfGetPlace.AFTER_ARREST_PLACE_TRANSPORTED_AFTER_COURT:
            city = personal_info.get_place_parameter(chat_id, PlaceKeys.KeyPlaceCity)
        elif self.type == TypeOfGetPlace.PLACE_FIRST_AFTER_COURT_CONDEMNED:
            city = personal_info.get_place_parameter(chat_id, PlaceKeys.KeyPlaceCity)
        elif self.type == TypeOfGetPlace.COURT_AFTER_ARREST:
            city = personal_info.get_place_parameter(chat_id, PlaceKeys.KeyPlaceCity)
        elif self.type == TypeOfGetPlace.NEW_PLACE_NO_ARREST_AFTER_COURT:
            city = personal_info.get_place_parameter(chat_id, PlaceKeys.KeyPlaceCity)
        assert city
        # print (city, self.is_court)
        return names.is_correct_place(place, city, self.is_court)


class TypeOfGetPlaceBeforeCourt(Enum):
    COURT_WHICH_RELEASE = 'c_w_r'  # 7.2.2.2.1.1.1.	Какой суд вас освободил?
    PLACE_TRANSPORTED_BEFORE_COURT = 'p_t_b_c'  # 7.2.1.1.1.	Куда вас поместили?
    COURT_WHICH_CONDEMNED = 'c_w_c'  # 7.2.2.2.1.2.1.	Какой суд осудил вас на сутки?


class StatePlaceBeforeGetPlace(abstract.BotState):
    def __init__(self, bot_info, chat_id, type_):
        super().__init__(bot_info, chat_id)
        self.type = type_
        self.is_court = self.type in [TypeOfGetPlaceBeforeCourt.COURT_WHICH_RELEASE,
                                      TypeOfGetPlaceBeforeCourt.COURT_WHICH_CONDEMNED]

        self.sent_start_message()

    def sent_start_message(self) -> None:
        chat_id = self._chat_id
        personal_info = self.bot_info.personal_info

        keyboard = None
        message = ""
        if self.type == TypeOfGetPlaceBeforeCourt.COURT_WHICH_RELEASE:
            city = personal_info.get_place_parameter(chat_id, PlaceKeys.KeyPlaceCourtCity)
            keyboard = get_place_keyboard(city, self.is_court)
            message = MESSAGE_GET_COURT_BEFORE_COURT
        elif self.type == TypeOfGetPlaceBeforeCourt.PLACE_TRANSPORTED_BEFORE_COURT:
            city = personal_info.get_place_parameter(chat_id, PlaceKeys.KeyPlaceCity)
            keyboard = get_place_keyboard(city, self.is_court)
            message = "Куда вас поместили?"
        elif self.type == TypeOfGetPlaceBeforeCourt.COURT_WHICH_CONDEMNED:
            city = personal_info.get_place_parameter(chat_id, PlaceKeys.KeyPlaceCourtCity)
            # print(city, self.is_court)
            keyboard = get_place_keyboard(city, self.is_court)
            message = "Какой суд осудил вас на сутки?"

        if IS_REAL_BOT:
            assert keyboard is not None
        self._bot_info.bot_send_message(chat_id, message, keyboard)

    def handle_message(self, message) -> None:
        place = message.text
        personal_info = self.bot_info.personal_info
        # TODO что-то делать в этом случае
        assert self.is_correct_place(personal_info, self.chat_id, place)

        if self.type == TypeOfGetPlaceBeforeCourt.COURT_WHICH_RELEASE:
            personal_info.set_place_parameter(self.chat_id, PlaceKeys.KeyPlaceCourtPlace, place)
            next_state = get_date.StatePlaceGetDate(self._bot_info, self.chat_id, get_date.TypeOfGetDate.Release)
        elif self.type == TypeOfGetPlaceBeforeCourt.PLACE_TRANSPORTED_BEFORE_COURT:
            personal_info.set_place_parameter(self.chat_id, PlaceKeys.KeyPlaceName, place)
            type_ = get_date.TypeOfGetDate.BEFORE_COURT_PLACE_TRANSPORT_DATE
            next_state = get_date.StatePlaceGetDate(self._bot_info, self.chat_id, type_)
        elif self.type == TypeOfGetPlaceBeforeCourt.COURT_WHICH_CONDEMNED:
            personal_info.set_place_parameter(self.chat_id, PlaceKeys.KeyPlaceCourtPlace, place)
            type_ = get_date.TypeOfGetDate.BEFORE_COURT_COURT_WHICH_CONDEMNED_DATE
            next_state = get_date.StatePlaceGetDate(self._bot_info, self.chat_id, type_)
        else:
            next_state = None
        assert next_state
        self.set_next_state(next_state)

    # дополнительные
    def is_correct_place(self, personal_info, chat_id, place):
        city = ""
        if self.type == TypeOfGetPlaceBeforeCourt.COURT_WHICH_RELEASE:
            city = personal_info.get_place_parameter(chat_id, PlaceKeys.KeyPlaceCourtCity)
        elif self.type == TypeOfGetPlaceBeforeCourt.PLACE_TRANSPORTED_BEFORE_COURT:
            city = personal_info.get_place_parameter(chat_id, PlaceKeys.KeyPlaceCity)
        elif self.type == TypeOfGetPlaceBeforeCourt.COURT_WHICH_CONDEMNED:
            city = personal_info.get_place_parameter(chat_id, PlaceKeys.KeyPlaceCourtCity)
        assert city
        return names.is_correct_place(place, city, self.is_court)
