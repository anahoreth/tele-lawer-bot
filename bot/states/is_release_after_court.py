
import config

import keyboard

if config.IS_LOCAL:
    import abstract
    import get_city
    import where_went_after_court
    import show_places_list
else:
    import states.abstract as abstract
    import states.get_city as get_city
    import states.show_places_list as show_places_list
    import states.where_went_after_court as where_went_after_court



class StatePlaceIsReleaseAfterCourt(abstract.YesNoQuestionState):
    def __init__(self, bot_info, chat_id):
        super().__init__(bot_info, chat_id, "После суда Вас освободили?")

    def answer_action(self, answer):
        is_release_after_court = (answer == keyboard.YES_STRING)
        chat_id = self._chat_id
        personal_info = self.bot_info.personal_info
        personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsLast, is_release_after_court)

        if is_release_after_court:
            personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsReleasedInCourt, True)
            typ = get_city.TypeOfBeforeCourtGetCity.COURT_WHICH_RELEASE
            next_state = get_city.StateGetCityBeforeCourt(self.bot_info, chat_id, typ)
        else:
            personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsReleasedInCourt, False)
            typ = get_city.TypeOfBeforeCourtGetCity.COURT_WHICH_CONDEMNED
            next_state = get_city.StateGetCityBeforeCourt(self.bot_info, chat_id, typ)
        self.set_next_state(next_state)


# 8.3 В суде вас освободили?
MESSAGE_IS_RELEASE_IN_COURT = "В суде вас освободили?"


class StatePlaceIsReleaseInCourt(abstract.YesNoQuestionState):
    def __init__(self, bot_info, chat_id):
        super().__init__(bot_info, chat_id, MESSAGE_IS_RELEASE_IN_COURT)

    def answer_action(self, answer):
        is_release_in_court = (answer == keyboard.YES_STRING)
        personal_info = self.bot_info.personal_info
        if is_release_in_court:
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsLast, True)
            court_date = personal_info.get_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceStartDate)
            personal_info.set_parameter(self.chat_id, config.PersonalInfoKeys.KeyReleaseDate, court_date)
            next_state = show_places_list.StatePlaceShowList(self.bot_info, self.chat_id)
        else:
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsLast, False)
            next_state = StatePlaceIsArrestedAfterCourt(self.bot_info, self.chat_id)
        # assert next_state
        self.set_next_state(next_state)


# 8.3.2.1 Суд [дата суда из 8.1.1 в формате дд.мм.гггг] дал вам сутки?

class StatePlaceIsArrestedAfterCourt(abstract.YesNoQuestionState):

    def __init__(self, bot_info, chat_id):
        court_date = bot_info.personal_info.get_place_parameter(chat_id, config.PlaceKeys.KeyPlaceStartDate)
        start_message = "Суд {:02d}.{:02d}.{:04d} дал вам сутки?".format(court_date[2], court_date[1], court_date[0])
        super().__init__(bot_info, chat_id, start_message)

    def answer_action(self, answer):
        is_arrested_in_court = (answer == keyboard.YES_STRING)
        if is_arrested_in_court:
            self.bot_info.personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsLast, False)
            type_ = get_city.TypeOfGetCity.CITY_FIRST_AFTER_COURT_CONDEMNED
            next_state = get_city.StatePlaceGetCity(self.bot_info, self.chat_id, type_)
        else:
            next_state = where_went_after_court.StatePlaceWhereWentAfterCourt(self.bot_info, self.chat_id)
        self.set_next_state(next_state)
