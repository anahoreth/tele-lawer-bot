
from config import PersonalInfoKeys, PlaceKeys, IS_LOCAL


import states.abstract as abstract
import states.get_city as get_city



# получение имени пользователя
class GetNameState(abstract.BotState):
    def __init__(self, bot_info, chat_id):
        super().__init__(bot_info, chat_id)
        self.sent_start_message()

    def sent_start_message(self) -> None:
        chat_id = self._chat_id
        message = "Введите фамилию, имя и отчество\n(в именительном падеже через пробел)"
        keyboard = None
        self._bot_info.bot_send_message(chat_id, message, keyboard)

    def handle_message(self, message) -> None:
        name = message.text
        chat_id = self._chat_id
        personal_info = self.bot_info.personal_info
        personal_info.set_parameter(chat_id, PersonalInfoKeys.KeyPersonName, name)
        next_state = GetAddressState(self._bot_info, chat_id)
        self.set_next_state(next_state)


# получение адреса пользователя
class GetAddressState(abstract.BotState):
    def __init__(self, bot_info, chat_id):
        super().__init__(bot_info, chat_id)
        self.sent_start_message()

    def sent_start_message(self) -> None:
        chat_id = self._chat_id
        message = "Введите Ваш адрес проживания"
        keyboard = None
        self._bot_info.bot_send_message(chat_id, message, keyboard)

    def handle_message(self, message) -> None:
        name = message.text
        chat_id = self._chat_id
        personal_info = self.bot_info.personal_info
        personal_info.set_parameter(chat_id, PersonalInfoKeys.KeyAddress, name)
        next_state = GetEmailState(self._bot_info, chat_id)
        self.set_next_state(next_state)


# получение эл. почты
class GetEmailState(abstract.BotState):
    def __init__(self, bot_info, chat_id):
        super().__init__(bot_info, chat_id)
        self.sent_start_message()

    def sent_start_message(self) -> None:
        chat_id = self._chat_id
        message = "Введите электронную почту для получения ответа на жалобу"
        keyboard = None
        self._bot_info.bot_send_message(chat_id, message, keyboard)

    def handle_message(self, message) -> None:
        email = message.text
        chat_id = self._chat_id
        personal_info = self.bot_info.personal_info
        personal_info.set_parameter(chat_id, PersonalInfoKeys.KeyEmail, email)
        next_state = get_city.StatePlaceGetCity(
            self._bot_info, chat_id, get_city.TypeOfGetCity.CITY_FIRST)

        self.set_next_state(next_state)
