
import enum
import config
from keyboard import YesNoButtons, YesNoButtonsList
import messages


if config.IS_LOCAL:
    import abstract
    import get_city
    import get_date
    import is_one_arrest
    import is_release_after_court
else:
    import states.abstract as abstract
    import states.get_city as get_city
    import states.get_date as get_date
    import states.is_one_arrest as is_one_arrest
    import states.is_release_after_court as is_release_after_court


class TypeOfTransportation(enum.Enum):
    BeforeCourt = "before_court"
    AfterCourt = "after_court"


class ButtonNamesAfterCourt(enum.Enum):
    TransferToAnotherPlace = "Вывозили в ЦИП или ИВС"
    TransferToCourt = "Вывозили в суд"
    NoTransfer = "Не вывозили"


keyboard_answers_list_after_court = [[a.value] for a in ButtonNamesAfterCourt]


'''def get_message(bot_info, chat_id, type_of_transport):
    personal_info = bot_info.personal_info
    place_name = personal_info.get_place_parameter(chat_id, PlaceKeys.KeyPlaceName)

    previous_place = personal_info.get_previous_place(chat_id)
    if previous_place is not None:
        if personal_info.is_returning_after_court_to_the_same_place(chat_id):
            message = "После возвращения в " + place_name + " вас еще куда-либо вывозили?"
            return message

    message = "Вас вывозили из " + place_name + " в другое место?"
    if type_of_transport == TypeOfTransportation.AfterCourt:
        message = "Во время отбытия этого ареста " + message
    else:
        message = "До суда " + message
    return message
'''




class StatePlaceIsTransported(abstract.QuestionWithAnswersState):

    @staticmethod
    def my_get_message(bot_info, chat_id, type_of_transport):
        personal_info = bot_info.personal_info
        place_name = personal_info.get_place_parameter(chat_id, config.PlaceKeys.KeyPlaceName)
        place_start_date = personal_info.get_place_parameter(chat_id, config.PlaceKeys.KeyPlaceStartDate)
        place_start_date_string = messages.get_string_date(place_start_date)

        previous_place = personal_info.get_previous_place(chat_id)
        if previous_place is not None:
            if personal_info.is_returning_after_court_to_the_same_place(chat_id):
                message = "После возвращения в " + place_name + " " + place_start_date_string + \
                          " г.  вас еще куда-либо вывозили?"
                return message

        message = "Вас вывозили из " + place_name + " (после " + place_start_date_string + \
                  "г.)  в другое место?"
        if type_of_transport == TypeOfTransportation.AfterCourt:
            message = "Во время отбытия этого ареста " + message
        else:
            message = "До суда " + message
        return message



    def __init__(self, bot_info, chat_id, type_of_transport):
        message = self.my_get_message(bot_info, chat_id, type_of_transport)

        super().__init__(bot_info, chat_id, message, keyboard_answers_list_after_court)


    def answer_action(self, answer):
        if answer == ButtonNamesAfterCourt.TransferToAnotherPlace.value:
            next_state = get_city.StatePlaceGetCity(
                self.bot_info, self.chat_id, get_city.TypeOfGetCity.CITY_TRANSPORTED_AFTER_ARREST)

        elif answer == ButtonNamesAfterCourt.TransferToCourt.value:
            next_state = get_city.StatePlaceGetCity(
                self.bot_info, self.chat_id, get_city.TypeOfGetCity.COURT_TRANSPORTED_AFTER_ARREST)

        elif answer == ButtonNamesAfterCourt.NoTransfer.value:
            next_state = is_one_arrest.StatePlaceIsOneArrest(self.bot_info, self.chat_id)
        else:
            next_state = None
        # assert next_state is not None
        self.set_next_state(next_state)


# TODO добавить подчеркивание *до суда*


# 7.2.1
class StateIsTransportedFromPlaceToAnotherPlaceBeforeCourt(abstract.QuestionWithAnswersState):
    def __init__(self, bot_info, chat_id):
        previous_place_name = bot_info.personal_info.get_place_parameter(chat_id, config.PlaceKeys.KeyPlaceName)
        if not previous_place_name:
            previous_place_name = "этого МПС"
        message = "Вас перевозили *до суда* из {} в другой ИВС или ЦИП?\n\n" \
                  '(здесь и далее "до суда" значит до момента, когда суд принял в отношении вас какое-либо ' \
                  'решение. Например, об отсутствии правонарушения, об аресте или штрафе и т.д.)' \
                  ''.format(previous_place_name)

        super().__init__(bot_info, chat_id,
                         message, YesNoButtonsList)

    def answer_action(self, answer):
        # print ("b StateIsTransportedFromPlaceToAnotherPlaceBeforeCourt")
        next_state = None
        if answer == YesNoButtons.Yes.value:
            # print ("answer == YesNoButtons.Yes.value")
            type_ = get_city.TypeOfBeforeCourtGetCity.PLACE_TRANSPORTED_BEFORE_COURT
            next_state = get_city.StateGetCityBeforeCourt(self.bot_info, self.chat_id, type_)
        elif answer == YesNoButtons.No.value:
            # print ("answer == YesNoButtons.No.value")
            next_state = StateIsTransportedFromPlaceToCourtBeforeCourt(self.bot_info, self.chat_id)
        assert next_state
        self.set_next_state(next_state)


# 7.2.2

class StateIsTransportedFromPlaceToCourtBeforeCourt(abstract.QuestionWithAnswersState):

    class ButtonNamesCourt(enum.Enum):
        Yes = "Да"
        NoVideo = "Нет. Cуд был по видеосвязи"
        NoHere = "Нет. Cуд был в ИВС / ЦИП"

    keyboard_answers_list_before_court = [[a.value] for a in ButtonNamesCourt]

    def __init__(self, bot_info, chat_id):
        previous_place_name = bot_info.personal_info.get_place_parameter(chat_id, config.PlaceKeys.KeyPlaceName)
        if not previous_place_name:
            previous_place_name = "этого МПС"
        message = "Вас возили в суд из {}?".format(previous_place_name)
        super().__init__(bot_info, chat_id, message, self.keyboard_answers_list_before_court)

    def answer_action(self, answer):
        next_state = None
        if answer == self.ButtonNamesCourt.Yes.value:
            type_ = get_date.TypeOfGetDate.COURT_AFTER_ARREST_DATE
            next_state = get_date.StatePlaceGetDate(self.bot_info, self.chat_id, type_)
        elif answer == self.ButtonNamesCourt.NoVideo.value:
            self.bot_info.personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsVideoCourt, True)
            next_state = is_release_after_court.StatePlaceIsReleaseAfterCourt(self.bot_info, self.chat_id)
        elif answer == self.ButtonNamesCourt.NoHere.value:
            self.bot_info.personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsVideoCourt, False)
            next_state = is_release_after_court.StatePlaceIsReleaseAfterCourt(self.bot_info, self.chat_id)
        self.set_next_state(next_state)
