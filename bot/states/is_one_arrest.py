
import config
import keyboard

if config.IS_LOCAL:
    import abstract
    import get_date
else:
    import states.abstract as abstract
    import states.get_date as get_date


MESSAGE_IS_HAS_ANOTHER_ARRESTS = "Сразу после этого ареста Вас снова задержали?"


class StatePlaceIsOneArrest(abstract.YesNoQuestionState):
    def __init__(self, bot_info, chat_id):
        super().__init__(bot_info, chat_id, MESSAGE_IS_HAS_ANOTHER_ARRESTS)

    def answer_action(self, answer):
        is_has_another_arrest = (answer == keyboard.YES_STRING)
        self.bot_info.personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsLast, True)

        # TODO добавить, что делать в этом случае
        if is_has_another_arrest:
            next_state = get_date.StatePlaceGetDate(
                self.bot_info, self.chat_id, get_date.TypeOfGetDate.AFTER_COURT_ARREST_END)
        else:
            next_state = get_date.StatePlaceGetDate(self.bot_info, self.chat_id, get_date.TypeOfGetDate.Release)

        # assert next_state is not None
        self.set_next_state(next_state)
