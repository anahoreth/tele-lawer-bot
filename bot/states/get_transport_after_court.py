
import config
if config.IS_LOCAL:
    import abstract
    import show_places_list as show_places_list
    import get_city
    import is_transported
else:
    import states.abstract as abstract
    import states.show_places_list as show_places_list
    import states.get_city as get_city
    import states.is_transported as is_transported

from enum import Enum


class ButtonNames(Enum):
    # TransferToTheSamePlace = "Вернули в (прошлый МПС)"
    TransferToAnotherPlace = "Поместили в другой ЦИП или ИВС"
    Released = "Освободили"


MESSAGE_GET_TRANSPORT_AFTER_COURT = "Что с вами произошло после суда?"


class StatePlaceGetTransportAfterCourt(abstract.QuestionWithAnswersState):

    def get_keyboard_answers_list(self, bot_info, chat_id):
        # TODO какую-то проверку previous_name
        places = bot_info.personal_info.get_parameter(
            chat_id, config.PersonalInfoKeys.KeyPlacesList)
        previous_name = places[len(places) - 2].place_get_parameter(config.PlaceKeys.KeyPlaceName)
        # обрубаем, чтобы влезло в название кнопки
        previous_name = "Вернули в {}".format(previous_name)
        previous_name = previous_name[:33]
        return [[previous_name]] + [[a.value] for a in ButtonNames]

    def __init__(self, bot_info, chat_id):
        self.keyboard_answers_list = self.get_keyboard_answers_list(bot_info, chat_id)
        message = MESSAGE_GET_TRANSPORT_AFTER_COURT
        super().__init__(bot_info, chat_id, message, self.keyboard_answers_list)

    def answer_action(self, answer):
        personal_info = self.bot_info.personal_info
        if answer == self.keyboard_answers_list[0][0]:
            places = personal_info.get_parameter(self.chat_id, config.PersonalInfoKeys.KeyPlacesList)
            places_count = len(places)
            assert places_count > 1
            previous_place = places[places_count - 2]
            previous_place_city = previous_place.place_get_parameter(config.PlaceKeys.KeyPlaceCity)
            previous_place_name = previous_place.place_get_parameter(config.PlaceKeys.KeyPlaceName)

            court_date = (places[places_count - 1]).place_get_parameter(config.PlaceKeys.KeyPlaceStartDate)

            personal_info.add_place(self.chat_id)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsCourt, False)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsFirst, False)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsAfterCourt, True)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceCity, previous_place_city)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceName, previous_place_name)
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceStartDate, court_date)

            type_of_transport = is_transported.TypeOfTransportation.AfterCourt
            next_state = is_transported.StatePlaceIsTransported(
                self.bot_info, self.chat_id, type_of_transport)

        elif answer == ButtonNames.TransferToAnotherPlace.value:

            next_state = get_city.StatePlaceGetCity(
                self.bot_info, self.chat_id, get_city.TypeOfGetCity.CITY_TRANSPORTED_AFTER_COURT_AFTER_ARREST)

        elif answer == ButtonNames.Released.value:
            # значит дата освобождения = дата суда
            personal_info.set_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceIsLast, True)
            court_date = personal_info.get_place_parameter(self.chat_id, config.PlaceKeys.KeyPlaceStartDate)
            personal_info.set_parameter(self.chat_id, config.PersonalInfoKeys.KeyReleaseDate, court_date)

            next_state = show_places_list.StatePlaceShowList(self.bot_info, self.chat_id)
        else:
            next_state = None
        # assert next_state is not None
        self.set_next_state(next_state)
