import config
import create_complain
import os
import datetime
import names

if config.IS_LOCAL:
    import abstract
else:
    import states.abstract as abstract

# отправка уже готового документа
DOCUMENT_ERROR = 'При подготовке документа произошла ошибка. Для того, чтобы составить новый документ, ' \
                 'наберите /start или любой другой текст'
DOCUMENT_DELETE_ERROR = 'При удалении файла с сервера произошла ошибка.'
DOCUMENT_FILENAME = 'complaint.docx'


def get_selected_places_groups(places):

    groups = []

    for places_number in range(len(places)):
        place = places[places_number]
        is_transport = place.place_type in [config.ComplainPlaceType.SPlaceTransport,
                                            config.ComplainPlaceType.SPlaceTransportFood]
        is_current_court = create_complain.is_court(place.current_name)
        if is_transport and is_current_court:
            needed_place_name = place.next_name
        else:
            needed_place_name = place.current_name

        is_found = False
        for i in range(len(groups)):
            if is_found:
                break
            # смотрим уже в созданных группах
            for place_in_group, place_in_group_number in groups[i]:
                place_in_group_type = place_in_group.place_type
                if place_in_group_type == config.ComplainPlaceType.SPlacePrison:
                    if needed_place_name == place_in_group.current_name:
                        groups[i].append((place, places_number))
                        is_found = True
                        break
                elif place_in_group_type in [config.ComplainPlaceType.SPlaceTransport,
                                             config.ComplainPlaceType.SPlaceTransportFood]:
                    place_in_group_name = place_in_group.current_name
                    # update: в любом случае добавляем к первой
                    if create_complain.is_court(place_in_group_name):  # транфер из суда в МПС, добавляем ко второму
                        place_in_group_needed_name = place_in_group.next_name
                    else:  # иначе добавляем к тому месту, откуда вывозили
                        place_in_group_needed_name = place_in_group.current_name
                    if needed_place_name == place_in_group_needed_name:
                        groups[i].append((place, places_number))
                        is_found = True
                        break
                # TODO спросить, куда направлять жалобу, если не совпадают места до и после суда
                elif place_in_group_type == config.ComplainPlaceType.SPlaceTransportFoodCourt:
                    # update: в любом случае добавляем к первой
                    if needed_place_name == place_in_group.current_name:
                        is_found = True
                        break
        if not is_found:
            groups.append([(place, places_number)])
    # print("selected_places_groups:{}".format(selected_places_groups))
    return groups


class StateFormComplain(abstract.BotState):
    def __init__(self, bot_info, chat_id):
        super().__init__(bot_info, chat_id)
        if config.IS_REAL_BOT:
            self.sent_start_message()

    def add_admin_info(self, time, selected_place_name):
        self.bot_info.admin_table.add_row(time, selected_place_name)
        bot_id = int(config.TOKEN[:3])
        bot_type = "тестовый" if bot_id != 134 else "основной"
        text = "Новая жалоба!\nДата жалобы: {:02d}.{:02d}.{:04d}\nМПС: {}\nТип бота: {}" \
               "".format(time[2], time[1], time[0], selected_place_name, bot_type)

        self.bot_info.bot.send_message(chat_id=config.ADMIN_ID, text=text)

    def write_to_admin(self, selected_places):
        date_today = datetime.datetime.now()
        time = (date_today.year, date_today.month, date_today.day)
        for selected_places_number in range(len(selected_places)):
            selected_place = selected_places[selected_places_number]
            selected_place_type = selected_place.place_type
            if selected_place_type.value == config.ComplainPlaceType.SPlacePrison.value:
                selected_place_name = selected_place.current_name
                self.add_admin_info(time, selected_place_name)

            elif selected_place_type.value in [config.ComplainPlaceType.SPlaceTransport.value,
                                               config.ComplainPlaceType.SPlaceTransportFood.value]:
                selected_place_name_in = selected_place.current_name
                selected_place_name_out = selected_place.next_name
                if not create_complain.is_court(selected_place_name_in):
                    self.add_admin_info(time, selected_place_name_in)
                if not create_complain.is_court(selected_place_name_out):
                    self.add_admin_info(time, selected_place_name_out)

            elif selected_place_type.value in [config.ComplainPlaceType.SPlaceTransportFoodCourt.value]:
                selected_place_name_in = selected_place.current_name
                selected_place_name_out = selected_place.next_next_name
                if not create_complain.is_court(selected_place_name_in):
                    self.add_admin_info(time, selected_place_name_in)
                if selected_place_name_out and (not create_complain.is_court(selected_place_name_out)):
                    self.add_admin_info(time, selected_place_name_out)

    def sent_start_message(self):
        personal_info = self.bot_info.personal_info
        date_today = datetime.datetime.now()
        time = (date_today.year, date_today.month, date_today.day)
        personal_info.set_parameter(self.chat_id, config.PersonalInfoKeys.KeyDate, time)
        selected_places = personal_info.get_parameter(
            self.chat_id, config.PersonalInfoKeys.KeyComplainSelectedPlaceNames)
        selected_places_groups = get_selected_places_groups(selected_places)
        self.write_to_admin(selected_places)
        # assert len(selected_places_groups) > 0

        sent_result = 0
        for selected_places_group in selected_places_groups:
            complaint = create_complain.make_complain(personal_info, self.chat_id, selected_places_group)
            complaint_filename = str(self.chat_id) + DOCUMENT_FILENAME
            complaint.save(complaint_filename)

            bot = self.bot_info.bot
            sent_result = self.send_document(complaint_filename)
            if not config.IS_LOCAL:
                delete_result = self.safety_delete(complaint_filename)
                if not delete_result:
                    bot.send_message(self.chat_id, DOCUMENT_DELETE_ERROR)

        if sent_result:
            self.bot_info.bot_send_message(self.chat_id, self.get_prelast_message(selected_places_groups), None)
            self.bot_info.bot_send_message(self.chat_id, MESSAGE_LAST_MESSAGE, None)
        else:
            self.bot_info.bot_send_message(self.chat_id, DOCUMENT_ERROR, None)

    def handle_message(self, message):
        pass

    def get_prelast_message(self, selected_places_groups):
        if len(selected_places_groups) > 1:
            return "_Жалобы можно отправить по электронной почте или заказным письмом. " \
                   "Электронные почты и адреса прокуратур указаны в шапках жалоб. " \
                   "Если отправляете жалобу почтой, не забудьте ее подписать._"

        # берем первое место из единственной группы
        place, place_number = selected_places_groups[0][0]
        is_transport = place.place_type in [config.ComplainPlaceType.SPlaceTransport,
                                            config.ComplainPlaceType.SPlaceTransportFood]
        is_current_court = create_complain.is_court(place.current_name)
        if is_transport and is_current_court:
            needed_place_name = place.next_name
        else:
            needed_place_name = place.current_name
        police_office_name = names.get_police_office_name(needed_place_name)

        if not police_office_name:
            police_office_name = ("(название прокуратуры из шапки)",
                                  "(e-mail прокуратуры из шапки)", "(адрес прокуратуры из шапки)")

        police_office = police_office_name[0].replace("куратура", "куратуру")

        return "_Жалобу можно отправить в {} по электронной почте {} или заказным письмом по адресу {}. " \
               "Если отправляете жалобу почтой, не забудьте ее подписать._" \
               "".format(police_office, police_office_name[2], police_office_name[1])

    def send_document(self, file_name):
        chat_id = self._chat_id
        bot = self.bot_info.bot
        if os.path.exists(file_name):
            doc = open(file_name, 'rb')
            bot.send_document(chat_id, doc)
            return True
        else:
            return False

    def safety_delete(self, filename):
        if os.path.exists(filename):

            try:
                #  size = os.path.getsize(filename)
                # zero_bytes = bytes(size)
                # with open(filename, "wb") as file:
                #   pickle.dump(zero_bytes, file)

                os.remove(filename)
                return True
            except IOError:
                return False
        return False


MESSAGE_LAST_MESSAGE = "_Напоминаю, что текст жалобы можно самостоятельно изменить или попросить" \
                       ' "Правовую инициативу" дописать то, что не было учтено. После получения ответа ' \
                       'прокуратуры вам также помогут с его обжалованием. \n\nКстати, "Правовая инициатива" ' \
                       "не только оказывает юридическую помощь жертвам нарушений прав человека, но и " \
                       "организует квизы и квесты, снимает интерактивные фильмы, социальные опросы и эксперименты," \
                       " записывает подкасты и многое другое. Следите за каналом_ " + "@legal\_initiative. " + \
                       "_\nИ делитесь информацией обо мне с теми, кому нужна моя помощь!_"
