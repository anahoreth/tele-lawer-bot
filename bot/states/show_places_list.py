
import config
import enum
import keyboard as kb

if config.IS_LOCAL:
    import abstract as abstract
    import choose_places_to_complain
    import get_city
else:
    import states.abstract as abstract
    import states.choose_places_to_complain as choose_places_to_complain
    import states.get_city as get_city


MESSAGE_RETURN_TO_PLACE_CHOOSE = "Сожалею, что так получилось. Давайте попробуем еще раз"


class Buttons(enum.Enum):
    Yes = 'Да'
    No = 'Нет. Начать сначала'


ButtonsList = [[Buttons.Yes.value, Buttons.No.value]]


class StatePlaceShowList(abstract.BotState):
    def __init__(self, bot_info, chat_id):
        super().__init__(bot_info, chat_id)
        self.start_message_id = self.sent_start_message()

    def sent_start_message(self):
        chat_id = self._chat_id
        message = self.get_info_about_places(chat_id)
        keyboard = kb.get_answers_keyboard(ButtonsList)
        return self._bot_info.bot_send_message(chat_id, message, keyboard).message_id

    def handle_query(self, call) -> None:
        chat_id = call.message.chat.id
        personal_info = self.bot_info.personal_info
        next_state = None

        if call.data == Buttons.Yes.value:
            personal_info.set_parameter(chat_id, config.PersonalInfoKeys.KeyComplainSelectedPlaceNumbers, set())
            next_state = choose_places_to_complain.StateComplainChoosePlacesToComplain(self.bot_info, chat_id)
        elif call.data == Buttons.No.value:
            message = MESSAGE_RETURN_TO_PLACE_CHOOSE
            keyboard = None
            self.bot_info.bot_send_message(chat_id, message, keyboard)

            personal_info.set_parameter(chat_id, config.PersonalInfoKeys.KeyPlacesList, [])

            next_state = get_city.StatePlaceGetCity(
                self._bot_info, chat_id, get_city.TypeOfGetCity.CITY_FIRST)
        assert next_state
        self.set_next_state(next_state)

    def get_info_about_places(self, chat_id):
        personal_info = self.bot_info.personal_info
        places = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyPlacesList)
        message = "*Проверка*\n\nВас "

        for place in places:
            message += place.to_string()

        release_date = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyReleaseDate)
        message += "освободили {:02d}.{:02d}.{:04d}. \n\nВерно?".format(release_date[2], release_date[1], release_date[0])

        return message
