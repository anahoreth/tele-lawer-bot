
import pandas as pd
import os.path
import messages
import config

admin_statistics_filename = "admin_table.xlsx"


class AdminTable(object):
    def __init__(self):
        self.filename = admin_statistics_filename
        if not os.path.isfile(self.filename):
            df1 = pd.DataFrame()
            df1.to_excel(self.filename)

    def get_filename(self):
        return self.filename

    def add_row(self, time, place):

        a = pd.read_excel(self.filename, index_col=0, dtype={"Дата жалобы": str, "МПС": str})
        time = messages.get_string_date(time)
        a = a.append({"Дата жалобы": time, "МПС": place},  ignore_index=True)
        a.to_excel(self.filename)
