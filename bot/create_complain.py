import pickle
import os
import config
import messages
import names

if config.IS_LOCAL:
    import choose_places_to_complain
else:
    import states.choose_places_to_complain as choose_places_to_complain


if config.IS_REAL_BOT:
    from docx import Document
    from docx.shared import Pt, Cm, Inches
    from docx.enum.text import WD_ALIGN_PARAGRAPH, WD_LINE_SPACING
    from docx.oxml import OxmlElement
    from docx.oxml.ns import qn


def delete_paragraph(paragraph):
    p = paragraph._element
    p.getparent().remove(p)
    p._p = p._element = None


# по идее это с п. 7.1.3
def after_court(places, start_index, release_date):
    paragraph_text = ""
    places_count = len(places)
    for i in range(start_index, places_count):
        place_is_court = places[i].place_get_parameter(config.PlaceKeys.KeyPlaceIsCourt)
        if place_is_court:
            transfer_date = places[i].place_get_parameter(config.PlaceKeys.KeyPlaceStartDate)
            prev_place_name = places[i - 1].place_get_parameter(config.PlaceKeys.KeyPlaceName)
            new_place_name = places[i].place_get_parameter(config.PlaceKeys.KeyPlaceName)
            paragraph_text += "{:02d}.{:02d}.{:04d} меня вывезли из {} в {}. ".format(
                transfer_date[2], transfer_date[1], transfer_date[0], prev_place_name, new_place_name)

            paragraph_text = choose_places_to_complain.fix_court_message(paragraph_text)

            is_last = places[i].place_get_parameter(config.PlaceKeys.KeyPlaceIsLast)
            if is_last:
                # assert i == places_count - 1
                paragraph_text += "После суда меня освободили. "
                return paragraph_text
            else:
                # assert i < places_count - 1
                prev_place_name = places[i - 1].place_get_parameter(config.PlaceKeys.KeyPlaceName)
                next_place_name = places[i + 1].place_get_parameter(config.PlaceKeys.KeyPlaceName)
                if prev_place_name == next_place_name:
                    paragraph_text += "После суда меня вернули в {}. ".format(next_place_name)
                else:
                    paragraph_text += "После суда меня отвезли в {}. ".format(next_place_name)
        else:
            is_prev_place_court = places[i - 1].place_get_parameter(config.PlaceKeys.KeyPlaceIsCourt)
            if not is_prev_place_court:
                transfer_date = places[i].place_get_parameter(config.PlaceKeys.KeyPlaceStartDate)
                new_place_name = places[i].place_get_parameter(config.PlaceKeys.KeyPlaceName)
                prev_place_name = places[i - 1].place_get_parameter(config.PlaceKeys.KeyPlaceName)
                paragraph_text += "{:02d}.{:02d}.{:04d} меня вывезли из {} в {}. ".format(
                    transfer_date[2], transfer_date[1], transfer_date[0], prev_place_name, new_place_name)

                paragraph_text = choose_places_to_complain.fix_court_message(paragraph_text)

    # paragraph_text += "{:02d}.{:02d}.{:04d} меня освободили. ".format(
    #    release_date[2], release_date[1], release_date[0])
    return paragraph_text


def before_court(places, start_index, end_index):
    paragraph_text = ""

    places_count = end_index
    for i in range(start_index, places_count - 1):
        place_is_court = places[i].place_get_parameter(config.PlaceKeys.KeyPlaceIsCourt)
        if place_is_court:
            assert i > 0, "Так как это суд, до него обязательно должно быть МПС"
            new_place_name = places[i + 1].place_get_parameter(config.PlaceKeys.KeyPlaceName)
            prev_place_name = places[i - 1].place_get_parameter(config.PlaceKeys.KeyPlaceName)
            # TODO подумать над тем, надо ли вообще здесь эта проверка
            if new_place_name == prev_place_name:
                paragraph_text += "Суд не рассмотрел административное дело в отношении меня. " \
                                  "В результате меня отвезли в {}. ".format(prev_place_name)
            else:
                paragraph_text += "Суд не рассмотрел административное дело в отношении меня. " \
                                  "В результате меня отвезли в {}. ".format(new_place_name)
        else:
            transfer_date = places[i + 1].place_get_parameter(config.PlaceKeys.KeyPlaceStartDate)
            new_place_name = places[i + 1].place_get_parameter(config.PlaceKeys.KeyPlaceName)
            prev_place_name = places[i].place_get_parameter(config.PlaceKeys.KeyPlaceName)
            paragraph_text += "{:02d}.{:02d}.{:04d} меня вывезли из {} в {}. ".format(
                transfer_date[2], transfer_date[1], transfer_date[0], prev_place_name, new_place_name)

            paragraph_text = choose_places_to_complain.fix_court_message(paragraph_text)
    return paragraph_text


def get_release_phrase(release_date):
    return "{:02d}.{:02d}.{:04d} меня освободили. ".format(release_date[2], release_date[1], release_date[0])


def get_arrest_end_phrase(arrest_number, arrest_end_date):
    arrest_number_names = [
        'первый',
        'второй',
        'третий',
        'четвертый',
        'пятый',
        'шестой',
        'седьмой',
        'восьмой',
        'девятый',
        'десятый',
    ]
    return "{:02d}.{:02d}.{:04d} мой {} административный арест закончился. ".format(
        arrest_end_date[2], arrest_end_date[1], arrest_end_date[0], arrest_number_names[arrest_number])


def get_first_paragraph_text(personal_info, chat_id):

    places = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyPlacesList)

    last_index_list = []
    places_count = len(places)
    for i in range(places_count):
        if places[i].place_get_parameter(config.PlaceKeys.KeyPlaceIsLast):
            last_index_list.append(i)

    last_index_list_count = len(last_index_list)
    first_paragraph = ""
    for i in range(last_index_list_count):
        first_index = 0 if i == 0 else last_index_list[i - 1] + 1
        second_index = last_index_list[i]
        sub_places = places[first_index:second_index + 1]
        release_date = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyReleaseDate)
        first_paragraph_unit = get_first_paragraph_text_unit(sub_places, release_date)
        # print(first_paragraph_unit)
        first_paragraph += first_paragraph_unit
        if i < last_index_list_count - 1:
            arrest_end_date = places[last_index_list[i]].place_get_parameter(config.PlaceKeys.KeyPlaceArrestEndDate)
            first_paragraph += get_arrest_end_phrase(i, arrest_end_date)
        else:
            is_court_ = places[last_index_list[i]].place_get_parameter(
                config.PlaceKeys.KeyPlaceIsCourt)
            is_released_in_court = places[last_index_list[i]].place_get_parameter(
                config.PlaceKeys.KeyPlaceIsReleasedInCourt)
            if is_court_ or is_released_in_court:
                pass
            else:
                first_paragraph += get_release_phrase(release_date)

    return first_paragraph


def get_first_paragraph_text_unit(places, release_date):

    # assert places and len(places) > 0

    places_count = len(places)

    first_after_court_place_index = places_count
    for i in range(places_count):
        is_after_court = places[i].place_get_parameter(config.PlaceKeys.KeyPlaceIsAfterCourt)
        if is_after_court:
            first_after_court_place_index = i
            break
    # только после суда
    if first_after_court_place_index == 0:
        paragraph_text = ""
        court_date = places[0].place_get_parameter(config.PlaceKeys.KeyPlaceCourtDate)
        court_name = places[0].place_get_parameter(config.PlaceKeys.KeyPlaceCourtPlace)
        paragraph_text += "{:02d}.{:02d}.{:04d} {} вынес в отношении меня административное " \
                          "взыскание в виде административного " \
                          "ареста. ".format(court_date[2], court_date[1], court_date[0], court_name)
        start_date = places[0].place_get_parameter(config.PlaceKeys.KeyPlaceStartDate)
        place_name = places[0].place_get_parameter(config.PlaceKeys.KeyPlaceName)
        paragraph_text += "{:02d}.{:02d}.{:04d} меня поместили в {}. ".format(
            start_date[2], start_date[1], start_date[0], place_name)

        paragraph_text += after_court(places, 1, release_date)
        paragraph_text = paragraph_text.replace('Суд', "суд")
        paragraph_text = paragraph_text.replace('. суд', ". Суд")
        return paragraph_text

    # только до суда
    if first_after_court_place_index == places_count:
        paragraph_text = ""
        start_date = places[0].place_get_parameter(config.PlaceKeys.KeyPlaceStartDate)
        place_name = places[0].place_get_parameter(config.PlaceKeys.KeyPlaceName)
        paragraph_text += "{:02d}.{:02d}.{:04d} меня поместили до суда в {}. ".format(
            start_date[2], start_date[1], start_date[0], place_name)

        paragraph_text += before_court(places, 0, len(places))

        court_date = release_date
        is_last_court = places[places_count - 1].place_get_parameter(config.PlaceKeys.KeyPlaceIsCourt)
        if is_last_court:
            paragraph_text += "В результате рассмотрения административного дела в суде меня освободили. "
        else:
            court_name = places[places_count - 1].place_get_parameter(config.PlaceKeys.KeyPlaceCourtPlace)

            is_video_court = places[places_count - 1].place_get_parameter(
                config.PlaceKeys.KeyPlaceIsVideoCourt)
            if is_video_court:
                court_type_name = "по видеосвязи"
            else:
                court_type_name = "в выездном заседании"
            paragraph_text += "{:02d}.{:02d}.{:04d} {} {} рассмотрел административное " \
                              "дело в отношении меня. В результате рассмотрения дела меня освободили. " \
                              "".format(court_date[2], court_date[1], court_date[0], court_name, court_type_name)
        paragraph_text = paragraph_text.replace('Суд', "суд")
        paragraph_text = paragraph_text.replace('. суд', ". Суд")
        return paragraph_text

    # и до, и после суда
    before_arrest_after_court_is_court = \
        places[first_after_court_place_index - 1].place_get_parameter(config.PlaceKeys.KeyPlaceIsCourt)

    if not before_arrest_after_court_is_court:

        start_date = places[0].place_get_parameter(config.PlaceKeys.KeyPlaceStartDate)
        place_name = places[0].place_get_parameter(config.PlaceKeys.KeyPlaceName)
        before = "{:02d}.{:02d}.{:04d} меня поместили до суда в {}. ".format(
            start_date[2], start_date[1], start_date[0], place_name)

        before += before_court(places, 0, first_after_court_place_index)

        court_date = places[first_after_court_place_index - 1].place_get_parameter(
            config.PlaceKeys.KeyPlaceCourtDate)
        court_name = places[first_after_court_place_index - 1].place_get_parameter(
            config.PlaceKeys.KeyPlaceCourtPlace)
        is_video_court = places[first_after_court_place_index - 1].place_get_parameter(
            config.PlaceKeys.KeyPlaceIsVideoCourt)
        if is_video_court:
            court_type_name = "по видеосвязи"
        else:
            court_type_name = "в выездном заседании"
        mid = "{:02d}.{:02d}.{:04d} {} {} рассмотрел административное дело в отношении меня. " \
              "Суд вынес в отношении меня административное взыскание в виде административного ареста. " \
              "".format(court_date[2], court_date[1], court_date[0], court_name, court_type_name)

        place_after_court_name = places[first_after_court_place_index ].place_get_parameter(
            config.PlaceKeys.KeyPlaceName)
        mid += "После суда меня поместили в {} для отбытия административного ареста. ".format(place_after_court_name)
        after = after_court(places, first_after_court_place_index + 1, release_date)
        paragraph_text = before + mid + after
        paragraph_text = paragraph_text.replace('Суд', "суд")
        paragraph_text = paragraph_text.replace('. суд', ". Суд")
        return paragraph_text

    # if before_arrest_after_court_is_court:
    start_date = places[0].place_get_parameter(config.PlaceKeys.KeyPlaceStartDate)
    place_name = places[0].place_get_parameter(config.PlaceKeys.KeyPlaceName)
    before = "{:02d}.{:02d}.{:04d} меня поместили до суда в {}. ".format(
        start_date[2], start_date[1], start_date[0], place_name)

    before += before_court(places, 0, first_after_court_place_index)

    mid = "Суд вынес в отношении меня административное взыскание в виде административного ареста. "

    place_after_court_name = places[first_after_court_place_index ].place_get_parameter(
        config.PlaceKeys.KeyPlaceName)
    mid += "После суда меня поместили в {} для отбытия административного ареста. ".format(place_after_court_name)
    after = after_court(places, first_after_court_place_index + 1, release_date)

    paragraph_text = before + mid + after
    paragraph_text = paragraph_text.replace('Суд', "суд")
    paragraph_text = paragraph_text.replace('. суд', ". Суд")
    return paragraph_text


def get_second_paragraph_text(selected_places_group):

    def get_prison_text(places_group):
        prison_list = []
        for place, place_number in places_group:
            place_type = place.place_type
            if place_type == config.ComplainPlaceType.SPlacePrison:
                prison_list.append(place.current_name)
        prisons_count = len(prison_list)
        if prisons_count == 0:
            return ""
        if prisons_count == 1:
            return "во время моего нахождения в {}".format(prison_list[0])
        text = "во время моего нахождения в "
        for i in range(prisons_count - 1):
            text += "{}, ".format(prison_list[i])
        text += "и {}".format(prisons_count - 1)
        return text

    def get_transport_text(places_group):
        transport_list = []
        for place, place_number in places_group:
            place_type = place.place_type
            if place_type == config.ComplainPlaceType.SPlaceTransport:

                transport_list.append((place.current_name, place.next_name, place.transfer_date))
        transport_count = len(transport_list)
        if transport_count == 0:
            return ""
        if transport_count == 1:
            text = "во время поездки из {} в {} {:02d}.{:02d}.{:04d}".format(
                transport_list[0][0], transport_list[0][1], transport_list[0][2][2],
                transport_list[0][2][1], transport_list[0][2][0])
            return choose_places_to_complain.fix_court_message(text)

        text = ""
        for i in range(transport_count - 1):
            place1, place2, transfer_date = transport_list[i]
            text += "во время поездки из {} в {} {:02d}.{:02d}.{:04d}, ".format(
                place1, place2, transfer_date[2], transfer_date[1], transfer_date[0])
            text = choose_places_to_complain.fix_court_message(text)

        place1, place2, transfer_date = transport_list[transport_count - 1]
        text += "и во время поездки из {} в {} {:02d}.{:02d}.{:04d}".format(
            place1, place2, transfer_date[2], transfer_date[1], transfer_date[0])
        text = choose_places_to_complain.fix_court_message(text)
        return text

    def get_transport_food_text(places_group):
        transport_food_list = []
        for place, place_number in places_group:
            place_type = place.place_type
            if place_type == config.ComplainPlaceType.SPlaceTransportFood:
                transport_food_list.append((place.current_name, place.next_name, place.transfer_date))
        transport_food_count = len(transport_food_list)
        if transport_food_count == 0:
            return ""
        if transport_food_count == 1:
            place1, place2, transfer_date = transport_food_list[0]
            text = "в день поездки из {} в {} {:02d}.{:02d}.{:04d}".format(
                place1, place2, transfer_date[2], transfer_date[1], transfer_date[0])
            return choose_places_to_complain.fix_court_message(text)

        text = ""
        for i in range(transport_food_count - 1):
            place1, place2, transfer_date = transport_food_list[i]
            text += "в день поездки из {} в {} {:02d}.{:02d}.{:04d}, ".format(
                place1, place2, transfer_date[2], transfer_date[1], transfer_date[0])
            text = choose_places_to_complain.fix_court_message(text)

        place1, place2, transfer_date = transport_food_list[transport_food_count - 1]
        text += "в день поездки из {} в {} {:02d}.{:02d}.{:04d}".format(
            place1, place2, transfer_date[2], transfer_date[1], transfer_date[0])
        return choose_places_to_complain.fix_court_message(text)

    prison_text = get_prison_text(selected_places_group)
    tranport_text = get_transport_text(selected_places_group)
    tranport_food_text = get_transport_food_text(selected_places_group)

    lst = [prison_text, tranport_text, tranport_food_text]

    for _ in range(3):
        if lst.__contains__(""):
            lst.remove("")

    places_text = ""
    if len(lst) == 0:
        return places_text
    if len(lst) == 1:
        places_text = lst[0]
    if len(lst) == 2:
        places_text = "{} и {}".format(lst[0], lst[1])
    if len(lst) == 3:
        places_text = "{}, {}, а также {}".format(lst[0], lst[1], lst[2])

    return "Я считаю условия содержания {} бесчеловечными, унижающими человеческое достоинство и " \
           "представлявшими угрозу, как минимум, моему здоровью.".format(places_text)


class MyDocument:

    def __init__(self):
        template_file_name = "/bot/template.docx"
        if config.IS_LOCAL:
            template_file_name = "template.docx"
        if config.IS_REAL_BOT:
            template_file_name = "../bot/template.docx"

        self.document = Document(template_file_name)
        style = self.document.styles['Normal']
        paragraph_format = style.paragraph_format
        paragraph_format.space_after = Pt(0)

        paragraph_format = self.document.styles['List Number'].paragraph_format
        paragraph_format.first_line_indent = Cm(1.25)

    def add_police_office(self, selected_place_name):
        police_office_name = names.get_police_office_name(selected_place_name)
        # print(police_office_name)
        if not police_office_name:
            return

        find_count = 0
        for paragraph in self.document.paragraphs:
            if find_count == 3:
                break
            if paragraph.text.find("[определение прокуратур") != -1:
                paragraph.text = police_office_name[0]
                find_count += 1
            if paragraph.text.find("[адрес прокуратуры]") != -1:
                paragraph.text = police_office_name[1]
                find_count += 1
            if paragraph.text.find("[email прокуратуры]") != -1:
                paragraph.text = police_office_name[2]
                find_count += 1

    def add_person_info(self, person_info, chat_id):
        person_name = person_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyPersonName)
        address = person_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyAddress)
        email = person_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyEmail)

        find_count = 0
        for paragraph in self.document.paragraphs:
            if find_count == 3:
                break
            if person_name and paragraph.text.find("Заявитель:") != -1:
                paragraph.text = "Заявитель: {}".format(person_name)
                find_count += 1
            if address and paragraph.text.find("Адрес:") != -1:
                paragraph.text = "Адрес: {}".format(address)
                find_count += 1
            if email and paragraph.text.find("Электронная почта:") != -1:
                paragraph.text = "Электронная почта: {}".format(email)
                find_count += 1

    def add_first_paragraph(self, person_info, chat_id):
        p = self.document.add_paragraph('')
        p.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
        first_paragraph_text = get_first_paragraph_text(person_info, chat_id)
        for paragraph in self.document.paragraphs:
            if paragraph.text.find("Абзац 1") != -1:
                paragraph.text = first_paragraph_text
                return

    def add_second_paragraph(self, selected_places_group):
        p = self.document.add_paragraph('')
        p.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
        second_paragraph_text = get_second_paragraph_text(selected_places_group)
        for paragraph in self.document.paragraphs:
            if paragraph.text.find("Абзац 2") != -1:
                paragraph.text = second_paragraph_text
                return

    def add_sixth_paragraph(self, selected_places_group):
        is_after_court = None
        for place in selected_places_group:
            if place[0] == config.ComplainPlaceType.SPlacePrison:
                is_after_court = place[2]
                break

        if is_after_court is None:
            paragraph_text = ""
        elif is_after_court == 2:  # до и после суда
            paragraph_text = "Условия содержания до судебного заседания регулируются Правилами содержания " \
                             "физического лица, в отношении которого применено административное задержание, " \
                             "утвержденными Постановлением Совета Министров Республики Беларусь от 21.11.2013 № " \
                             "996 (далее – Правила № 996). Условия содержания при отбывании административного ареста " \
                             "регулируются Правилами внутреннего распорядка мест отбывания административного ареста, " \
                             "утвержденными Постановлением Министерства внутренних дел Республики Беларусь от " \
                             "20.10.2015 № 313 (далее – Правила № 313). "
        elif is_after_court:
            paragraph_text = "Условия содержания при отбывании административного ареста регулируются Правилами " \
                             "внутреннего распорядка мест отбывания административного ареста, утвержденными " \
                             "Постановлением Министерства внутренних дел Республики Беларусь от 20.10.2015 " \
                             "№ 313 (далее – Правила № 313)."
        else:  # is_after_court == false
            paragraph_text = "Условия содержания до судебного заседания регулируются Правилами содержания " \
                             "физического лица, в отношении которого применено административное задержание, " \
                             "утвержденными Постановлением Совета Министров Республики Беларусь от 21.11.2013 " \
                             "№ 996 (далее – Правила № 996)."

        # в случае когда МПС (когда была не транспортировка)
        if paragraph_text:
            self.document.add_paragraph(paragraph_text)

    def add_seventh_paragraph(self, personal_info, chat_id, selected_places_group):
        text_array = []

        questions_list = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)
        is_only_transport = True

        for place, place_number in selected_places_group:
            title = choose_places_to_complain.get_message_place(place)
            # TODO этот ассерт падал
            # assert len(questions_list) > place_number
            questions = questions_list[place_number]
            if questions:
                complains_list = questions.process()
                text_array.append((title, complains_list))

        text = "Я считаю, что обращение, которому меня подвергли, противоречит указанным правовым нормам и " \
               "международным стандартам по нижеследующим причинам."

        p = self.document.add_paragraph(text)
        p.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY

        # если место
        # удаление пустых абзацев руками
        for i in range(len(self.document.paragraphs)):
            if self.document.paragraphs[i].text == text:
                if is_only_transport:
                    delete_paragraph(self.document.paragraphs[i - 1])
                    delete_paragraph(self.document.paragraphs[i - 2])
                else:
                    delete_paragraph(self.document.paragraphs[i - 2])
                    delete_paragraph(self.document.paragraphs[i - 3])
                break

        if len(text_array) == 1:
            complains_list = text_array[0][1]
            for group_complain in complains_list:
                for i in range(len(group_complain)):
                    p = self.document.add_paragraph(group_complain[i])
                    p.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
                    if i == 0:
                        p.style = 'List Number'
            return

        for text_unit in text_array:
            title, complains_list = text_unit
            p = self.document.add_paragraph("")
            p.add_run(title).bold = True
            for group_complain in complains_list:
                for i in range(len(group_complain)):
                    p = self.document.add_paragraph(group_complain[i])
                    p.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
                    if i == 0:
                        p.style = 'List Number'

    def add_before_last_paragraph(self, selected_places_group):
        is_after_court = None
        for place in selected_places_group:
            if place[0] == config.ComplainPlaceType.SPlacePrison:
                is_after_court = place[2]
                break

        # Транспортировка или еда
        if is_after_court is None:
            paragraph_text = "Таким образом, в совокупности условия моего содержания нарушают статью 2.4 ПИКоАП," \
                             " часть 3 статьи 25 Конституции Республики Беларусь, статью 7 и часть 1 статьи " \
                             "10 Международного пакта о гражданских и политических правах."
        elif is_after_court == 2:
            paragraph_text = "Таким образом, в совокупности условия моего содержания нарушают Правила № 996, " \
                             "Правила № 313, статью 2.4 ПИКоАП, часть 3 статьи 25 Конституции Республики Беларусь," \
                             " статью 7 и часть 1 статьи 10 Международного пакта о гражданских и политических правах."
        elif is_after_court:
            paragraph_text = "Таким образом, в совокупности условия моего содержания нарушают Правила № 313, " \
                             "статью 2.4 ПИКоАП, часть 3 статьи 25 Конституции Республики " \
                             "Беларусь, статью 7 и часть 1 статьи 10 Международного пакта о гражданских и" \
                             " политических правах."
        else:  # asking_type == abstract_group.AskingType.BeforeCourt:
            paragraph_text = "Таким образом, в совокупности условия моего содержания нарушают Правила № 996, " \
                             "статью 2.4 Процессуально-исполнительного кодекса Республики Беларусь об " \
                             "административных правонарушениях, часть 3 статьи 25 Конституции Республики " \
                             "Беларусь, статью 7 и часть 1 статьи 10 Международного пакта о гражданских и" \
                             " политических правах."

        if paragraph_text:
            p = self.document.add_paragraph(paragraph_text)
            p.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY

    def add_last_paragraph(self):

        # TODO сделать нормальное форматирование
        paragraph_text = "В соответствии со статьей 4 Закона Республики Беларусь от 08.05.2007 № 220-З " \
                         "«О прокуратуре Республики Беларусь» задачами прокуратуры являются обеспечение " \
                         "верховенства права, законности и правопорядка, защита прав и законных интересов граждан."

        p = self.document.add_paragraph(paragraph_text)
        p.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY

        p = self.document.add_paragraph("Исходя из вышеизложенного,")
        p.alignment = WD_ALIGN_PARAGRAPH.LEFT

        p = self.document.add_paragraph('')
        p.alignment = WD_ALIGN_PARAGRAPH.CENTER
        p.add_run("ПРОШУ:").bold = True

    def add_requires(self, personal_info, chat_id, selected_places_group):
        prison_name = ""
        for place in selected_places_group:
            if place[0] == config.ComplainPlaceType.SPlacePrison:
                prison_name = place[1]
                break

        complains_list = []
        if prison_name:
            complains_list.append("Провести прокурорскую проверку по указанным фактам с обязательным посещением "
                                  "{} на предмет соблюдения прав человека и действующего "
                                  "законодательства.".format(prison_name))
        else:
            complains_list.append("Провести прокурорскую проверку по указанным фактам на предмет соблюдения "
                                  "прав человека и действующего законодательства.")

        if prison_name:
            complains_list.append("Признать условия моего содержания в {} бесчеловечными и унижающими "
                                  "человеческое достоинство.".format(prison_name))
        else:
            complains_list.append("Признать условия моего содержания бесчеловечными и унижающими "
                                  "человеческое достоинство.")

        address = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyAddress)
        if not address:
            address = "ВАШ АДРЕС"
        email = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyEmail)
        if not address:
            address = "АДРЕС ВАШЕЙ ЭЛЕКТРОННОЙ ПОЧТЫ"
        complains_list.append("Направить ответ на адрес электронной почты {} "
                              "либо письмом на адрес {}.".format(email, address))

        for i in range(len(complains_list)):
            p = self.document.add_paragraph(complains_list[i], style='List Number 2')
            p.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY

    def add_sign(self, date):
        self.document.add_paragraph()
        self.document.add_paragraph(messages.get_string_date(date))

    def get_document(self):
        return self.document


def make_complain(personal_info, chat_id, selected_places_group):
    my_document = MyDocument()

    selected_place_name = get_place_name(selected_places_group)
    my_document.add_police_office(selected_place_name)
    my_document.add_person_info(personal_info, chat_id)
    my_document.add_first_paragraph(personal_info, chat_id)
    my_document.add_second_paragraph(selected_places_group)
    my_document.add_sixth_paragraph(selected_places_group)
    my_document.add_seventh_paragraph(personal_info, chat_id, selected_places_group)
    my_document.add_before_last_paragraph(selected_places_group)
    my_document.add_last_paragraph()
    my_document.add_requires(personal_info, chat_id, selected_places_group)
    date = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyDate)
    my_document.add_sign(date)
    return my_document.get_document()


def is_court(place_name):
    return place_name.find("суд") != -1 or place_name.find("Суд") != -1


def get_place_name(selected_places_group):
    prison_name = ""
    for place, place_number in selected_places_group:
        if place.place_type.value == config.ComplainPlaceType.SPlacePrison.value:
            prison_name = place.current_name
            break
        else:
            current_place = place.current_name
            #  суд - МПС: выбираем МПС
            if is_court(current_place):
                prison_name = place.next_name
                break
            # иначе выбираем первое место
            else:
                prison_name = place.current_name
                break
    return prison_name


def get_asking_type(personal_info, chat_id, selected_places_number):
    questions_list = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)
    # TODO посмотреть здесь
    if len(questions_list) > selected_places_number:
        questions = questions_list[selected_places_number]
        if questions:
            return questions.asking_type
    return None


def safety_delete(filename):
    if os.path.exists(filename):
        size = os.path.getsize(filename)
        try:
            zero_bytes = bytes(size)
            with open(filename, "wb") as file:
                pickle.dump(zero_bytes, file)

            os.remove(filename)
            return True
        except IOError:
            return False
    return False


'''
    def add_seventh_paragraph_old(self, personal_info, chat_id, selected_places_number):

        p = self.document.add_paragraph('')
        p.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY

        questions_list = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainQuestions)

        assert len(questions_list) > selected_places_number

        questions = questions_list[selected_places_number]
        assert questions

        text_before_complains_all, complains_list_all = questions.process()

        # TODO добавить нумерацию
        text = ""
        text += text_before_complains_all
        if text != "":
            p.add_run(text)
            p = self.document.add_paragraph('')
            p.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY

        text = "Я считаю, что обращение, которому меня подвергли, противоречит указанным правовым нормам и " \
               "международным стандартам по нижеследующим причинам."
        p.add_run(text)

        p = self.document.add_paragraph('')
        p.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY

        selected_places = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyComplainSelectedPlaceNames)
        selected_place_name = selected_places[selected_places_number][0]

        text = "Нарушения в {}".format(selected_place_name)
        p.add_run(text)
        p = self.document.add_paragraph('')
        p.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY

        for i in range(len(complains_list_all)):
            text = str(i + 1) + ". " + complains_list_all[i]
            p.add_run(text)
            p = self.document.add_paragraph('')
            p.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
    def add_title(self):
        p = self.document.add_paragraph('')
        p.alignment = WD_ALIGN_PARAGRAPH.CENTER
        p.add_run(complaint_name + '\n').bold = True


complaint_name = "Жалоба на условия содержания, нарушающие статью 7 " \
                 "и пункт 1 статьи 10 Международного пакта о гражданских " \
                 "и политических правах и часть третью статьи 25 Конституции Республики Беларусь"

footer_text = \
    [" Указ Президиума Верховного Совета Белорусской ССР «О ратификации Международного пакта об экономических, "
     "социальных и культурных правах и Международного пакта о гражданских и политических правах» "
     "от 5 октября 1973 года.\n",
     " Международный пакт о гражданских и политических правах, статья 7.\n",
     " Международный пакт о гражданских и политических правах, статья 10 (1).\n",
     " Приняты Резолюцией Генеральной Ассамблеи ООН 70/175 17 декабря 2015 года.\n"]


    def add_third_paragraph(self, person_info, chat_id):
        p = self.document.add_paragraph('')
        p.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
        p.add_run(complain_third_paragraph_text)

    def add_fourth_paragraph(self, person_info, chat_id):
        p = self.document.add_paragraph('')
        p.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
        p.add_run(complain_fourth_paragraph_text)

    def add_fifth_paragraph(self, person_info, chat_id):
        p = self.document.add_paragraph('')
        p.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
        p.add_run(complain_fifth_paragraph_text)

    def add_sixth_paragraph(self, personal_info, chat_id):
        p = self.document.add_paragraph('')
        p.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
        p.add_run("шестой абзац")
    def add_footer(self):
        section = self.document.sections[0]
        footer = section.footer
        paragraph = footer.paragraphs[0]
        paragraph.alignment = WD_ALIGN_PARAGRAPH.LEFT
        self.document.styles['Footer'].font.size = Pt(10)
        paragraph_format = self.document.styles['Footer'].paragraph_format
        paragraph_format.space_before = Pt(0)
        paragraph_format.space_after = Pt(0)
        paragraph_format.line_spacing_rule = WD_LINE_SPACING.SINGLE
        paragraph_format.first_line_indent = Cm(0)
        paragraph.add_run("______________________________________\n")
        for i in range(len(footer_text)):
            super_text = paragraph.add_run(str(i + 1))
            super_text.font.superscript = True
            paragraph.add_run(footer_text[i])

complain_third_paragraph_text = "Международный пакт о гражданских и политических правах (далее – МПГПП), " \
                                "ратифицированный Республикой Беларусь, запрещает подвергать людей " \
                                "жестокому, бесчеловечному или унижающему достоинство обращению или " \
                                "наказанию. Аналогичный запрет содержится в части 3 статьи 25 " \
                                "Конституции Республики Беларусь (далее – Конституция). " \
                                "Кроме того, МПГПП закрепляет, что все лица, лишенные " \
                                "свободы, имеют право на гуманное обращение и уважение достоинства, " \
                                "присущего человеческой личности."

complain_fourth_paragraph_text = "Как минимум, условия содержания людей, лишенных свободы, должны " \
                                 "соответствовать Минимальным стандартным правилам Организации " \
                                 "Объединенных Наций в отношении обращения с заключенными (далее – " \
                                 "Правила Нельсона Манделы). Правила Нельсона Манделы не указывают " \
                                 "на образцовые условия содержания. Они предназначены для того, чтобы " \
                                 "на основе общепризнанных достижений современной мысли изложить то, " \
                                 "что обычно считается правильным с принципиальной и практической точек зрения в " \
                                 "области обращения с заключенными. Исходя из Предварительного замечания 3 " \
                                 "Правил Нельсона Манделы, в понятие заключенных включаются и административно " \
                                 "арестованные лица."

complain_fifth_paragraph_text = "В соответствии с частью 4 статьи 2.4 Процессуально-исполнительного кодекса " \
                                "Республики Беларусь об административных правонарушениях (далее – ПИКоАП), " \
                                "содержание физического лица, задержанного за административное правонарушение " \
                                "или административно арестованного, должно осуществляться в условиях, исключающих " \
                                "угрозу для его жизни и здоровья."

'''
