#
# A library that allows to create an inline calendar keyboard.
# grcanosa https://github.com/grcanosa
#
"""
Base methods for calendar keyboard creation and processing.
"""

import datetime
import calendar
import enum

import config
if config.IS_REAL_BOT:
    from telebot.types import InlineKeyboardButton, InlineKeyboardMarkup


class CalendarActions(enum.Enum):
    C_DAY = "DAY"  #
    C_IGNORE = "IGNORE"
    C_PREV_MONTH = "PREV-MONTH"
    C_NEXT_MONTH = "NEXT-MONTH"


month_list = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь",
              "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"]

weekdays_list = ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"]


def get_new_calendar(action, year, month):
    current_date = datetime.datetime(year, month, 1)
    if action == CalendarActions.C_PREV_MONTH.value:
        new_date = current_date - datetime.timedelta(days=1)
    else:
        new_date = current_date + datetime.timedelta(days=31)
    return create_calendar(int(new_date.year), int(new_date.month))


def create_callback_data(action, year, month, day):
    """ Create the callback data associated to each button"""
    return ";".join([action, str(year), str(month), str(day)])


def separate_callback_data(data):
    """ Separate the callback data"""
    return data.split(";")


def create_calendar(year=None, month=None):
    if not config.IS_REAL_BOT:
        return None

    keyboard = InlineKeyboardMarkup()

    now = datetime.datetime.now()
    if year is None:
        year = now.year
    if month is None:
        month = now.month

    data_ignore = create_callback_data(CalendarActions.C_IGNORE.value, year, month, 0)

    # print(month)
    button = InlineKeyboardButton(month_list[(month - 1) % 12] + " " + str(year), callback_data=data_ignore)
    keyboard.add(button)

    row = []
    for day in weekdays_list:
        button = InlineKeyboardButton(day, callback_data=data_ignore)
        row.append(button)
    keyboard.row(*row)

    my_calendar = calendar.monthcalendar(year, month)
    for week in my_calendar:
        row = []
        for day in week:
            if day == 0:
                row.append(InlineKeyboardButton(" ", callback_data=data_ignore))
            else:
                callback_data = create_callback_data(CalendarActions.C_DAY.value, year, month, day)
                row.append(InlineKeyboardButton(str(day), callback_data=callback_data))
        keyboard.row(*row)
    # Last row - Buttons
    # здесь день может быть любым, потому что он не используется потом. Ставим 1
    day_default = 1
    row = [
        InlineKeyboardButton("<",
                             callback_data=create_callback_data(
                                 CalendarActions.C_PREV_MONTH.value, year, month, day_default)),
        InlineKeyboardButton(" ", callback_data=data_ignore),
        InlineKeyboardButton(">",
                             callback_data=create_callback_data(
                                 CalendarActions.C_NEXT_MONTH.value, year, month, day_default))]
    keyboard.row(*row)

    return keyboard
