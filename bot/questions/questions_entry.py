from config import PersonalInfoKeys, ComplainPlaceType, PlaceKeys
import enum
import config


if config.IS_LOCAL:
    import choose_places_to_complain
    import question_groups.abstract_group as abstract_group
    import question_groups.virus1 as virus
    import question_groups.food2 as food
    import question_groups.walk3 as walk
    import question_groups.med_help4 as med_help
    import question_groups.shower5 as shower
    import question_groups.cant_complain6 as cant_complain
    import question_groups.world7 as world
    import question_groups.bed8 as bed
    import question_groups.cell9 as cell
    import question_groups.transport_food as transport_food
    import question_groups.transport as transport
else:
    import states.choose_places_to_complain as choose_places_to_complain
    import questions.question_groups.abstract_group as abstract_group
    import questions.question_groups.virus1 as virus
    import questions.question_groups.food2 as food
    import questions.question_groups.walk3 as walk
    import questions.question_groups.med_help4 as med_help
    import questions.question_groups.shower5 as shower
    import questions.question_groups.cant_complain6 as cant_complain
    import questions.question_groups.world7 as world
    import questions.question_groups.bed8 as bed
    import questions.question_groups.cell9 as cell
    import questions.question_groups.transport_food as transport_food
    import questions.question_groups.transport as transport


class QuestionGroupsName(enum.Enum):
    VIRUS = 1
    FOOD = 2
    WALK = 3
    MED = 4
    SHOWER = 5
    CANNOT = 6
    WORLD = 7
    BED = 8
    CELL = 9
    TRANSPORT_FOOD = 10
    TRANSPORT = 11


def make_question_group(question_group_number, asking_type,
                        place_name="", transfer_current_name="", transfer_next_name="", transfer_date="",
                        prison_days=0):
    if question_group_number == QuestionGroupsName.VIRUS.value:
        return virus.VirusQuestionGroup(asking_type, place_name)
    if question_group_number == QuestionGroupsName.FOOD.value:
        return food.FoodQuestionGroup(asking_type, place_name)
    if question_group_number == QuestionGroupsName.WALK.value:
        return walk.WalkQuestionGroup(asking_type, place_name)
    if question_group_number == QuestionGroupsName.MED.value:
        return med_help.MedQuestionGroup(asking_type, place_name)
    if question_group_number == QuestionGroupsName.SHOWER.value:
        return shower.ShowerQuestionGroup(asking_type=asking_type, prison_days=prison_days, place_name=place_name)
    if question_group_number == QuestionGroupsName.CANNOT.value:
        return cant_complain.CantQuestionGroup(asking_type, place_name)
    if question_group_number == QuestionGroupsName.WORLD.value:
        return world.WorldQuestionGroup(asking_type, place_name)
    if question_group_number == QuestionGroupsName.BED.value:
        return bed.BedQuestionGroup(asking_type, place_name)
    if question_group_number == QuestionGroupsName.CELL.value:
        return cell.CellQuestionGroup(asking_type, place_name)
    if question_group_number == QuestionGroupsName.TRANSPORT_FOOD.value:
        return transport_food.TransportFoodQuestionGroup(
            asking_type, transfer_current_name, transfer_next_name, transfer_date)
    if question_group_number == QuestionGroupsName.TRANSPORT.value:
        return transport.TransportQuestionGroup(asking_type)
    return abstract_group.QuestionGroup(asking_type)


def get_prison_days_count(personal_info, chat_id):
    # TODO костыль, потому что нужно знать только, что количество дней больше 7
    TEN_DAYS = 10
    prison_days_count = personal_info.get_parameter(chat_id, PersonalInfoKeys.KeyPrisonDaysCount)
    if prison_days_count:
        return prison_days_count

    places = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyPlacesList)
    # assert places and len(places) > 0

    if not places:
        return 0

    places_count = len(places)

    first_after_court_place_index = places_count
    for i in range(places_count):
        is_after_court = places[i].place_get_parameter(config.PlaceKeys.KeyPlaceIsAfterCourt)
        if is_after_court:
            first_after_court_place_index = i
            break

    start_date = places[first_after_court_place_index].place_get_parameter(config.PlaceKeys.KeyPlaceStartDate)
    release_date = personal_info.get_parameter(chat_id, config.PersonalInfoKeys.KeyReleaseDate)
    assert start_date and release_date

    start_year, start_month, start_day = start_date
    release_year, release_month, release_day = release_date

    if release_year < start_year or release_year == start_year and release_month < start_month or \
            release_year == start_year and release_month == start_month and release_day <= start_day:
        return 0

    if release_year == start_year:
        if release_month == start_month:
            return release_day - start_day
        if release_month > start_month + 1:
            return TEN_DAYS
        if release_day < start_day:
            # TODO здесь можно что-то умнее придумать
            return 31 - start_day + release_day
        return TEN_DAYS

    if not (release_month == 1 and start_month == 12):
        return TEN_DAYS

    if release_day < start_day:
        # а здесь декабрь
        return 31 - start_day + release_day
    return TEN_DAYS


class QuestionsDB(object):

    def __init__(self, personal_info, chat_id, place_info, selected_groups):
        self.personal_info = personal_info
        self.chat_id = chat_id
        self.selected_groups = selected_groups
        self.selected_group_number = 0
        self.questions_db = {}
        self.selected_place_type = place_info.place_type

        self.questions_name = ""
        self.asking_type = None
        if self.selected_place_type == config.ComplainPlaceType.SPlacePrison:
            # TODO убрать костыль с asking_type
            if place_info.is_after_court:
                self.asking_type = abstract_group.AskingType.AfterCourt
            else:
                self.asking_type = abstract_group.AskingType.BeforeCourt
            if place_info.is_after_court == 2:
                self.asking_type = abstract_group.AskingType.BothSideOfCourt
            self.current_name = place_info.current_name
            self.questions_name = "Условия содержания в {}".format(self.current_name)

        elif self.selected_place_type == config.ComplainPlaceType.SPlaceTransport:
            self.current_name = place_info.current_name
            self.next_name = place_info.next_name
            self.transfer_date = place_info.transfer_date
            self.questions_name = "Условия поездки из {} в {}".format(self.current_name, self.next_name)
            self.questions_name = choose_places_to_complain.fix_court_message(self.questions_name)

        elif self.selected_place_type == config.ComplainPlaceType.SPlaceTransportFood:
            self.current_name = place_info.current_name
            self.next_name = place_info.next_name
            self.transfer_date = place_info.transfer_date
            self.questions_name = "Питание в день поездки из {} в {}".format(self.current_name, self.next_name)
            self.questions_name = choose_places_to_complain.fix_court_message(self.questions_name)

        elif self.selected_place_type == config.ComplainPlaceType.SPlaceTransportFoodCourt:
            self.current_name = place_info.current_name
            self.next_name = place_info.next_name
            self.next_next_name = place_info.next_next_name
            self.transfer_date = place_info.transfer_date

            if self.current_name == self.next_next_name:
                self.questions_name = "Питание в день поездки из {} в {} и обратно".format(
                    self.current_name, self.next_name)
            else:  # self.transfer_current_name != self.transfer_next_name:
                self.questions_name = "Питание в день поездки из {} в {} и из {} в {}".format(
                    self.current_name, self.next_name, self.next_name, self.next_next_name)

            self.questions_name = choose_places_to_complain.fix_court_message(self.questions_name)
            # print(self.questions_name)

    @property
    def get_title(self):
        message = "_{}\n".format(self.questions_name)

        group_number = self.selected_group_number
        question_group = self.questions_db[group_number]

        # TODO возможно, тут будет еще про камеру
        if self.selected_place_type == ComplainPlaceType.SPlacePrison:
            groups_count = len(self.selected_groups)
            group_name = question_group.get_short_name()
            message += "Раздел: {} ({} из {})\n".format(group_name, group_number + 1, groups_count)

        # questions_count = question_group.get_questions_count()
        # question_number = question_group.get_question_number()
        # message += "Вопрос " + str(question_number) + " из " + str(questions_count) + "\n"
        return message + "_"

    def get_current_question(self):
        if not self.questions_db.get(self.selected_group_number):
            question_group_number = self.selected_groups[self.selected_group_number]

            if question_group_number == QuestionGroupsName.TRANSPORT_FOOD.value:
                question_group = make_question_group(self.selected_groups[self.selected_group_number],
                                                     self.asking_type, transfer_current_name=self.current_name,
                                                     transfer_next_name=self.next_name,
                                                     transfer_date=self.transfer_date)

            elif question_group_number == QuestionGroupsName.SHOWER.value:
                prison_days_count = get_prison_days_count(self.personal_info, self.chat_id)
                if prison_days_count < 0:
                    prison_days_count = 0
                self.personal_info.set_parameter(self.chat_id, PersonalInfoKeys.KeyPrisonDaysCount, prison_days_count)
                # print(prison_days_count)
                question_group = make_question_group(
                    self.selected_groups[self.selected_group_number],
                    self.asking_type, place_name=self.current_name, prison_days=prison_days_count)
            else:
                question_group = make_question_group(self.selected_groups[self.selected_group_number],
                                                     self.asking_type, place_name=self.current_name)
            self.questions_db[self.selected_group_number] = question_group
            question = question_group.get_question()
            return question

        return self.questions_db[self.selected_group_number].get_question()

    def step_forward(self):
        if self.questions_db[self.selected_group_number].forward():
            return True

        # TODO тут можно вставить какое-нибудь условие для группы
        groups_count = len(self.selected_groups)
        if self.selected_group_number < groups_count - 1:
            self.selected_group_number += 1
            question_group = make_question_group(self.selected_groups[self.selected_group_number],
                                                 self.asking_type, self.current_name)
            self.questions_db[self.selected_group_number] = question_group
            return True
        return False

    def topic_forward(self):
        groups_count = len(self.selected_groups)
        if self.selected_group_number < groups_count - 1:
            self.selected_group_number += 1
            question_group = make_question_group(self.selected_groups[self.selected_group_number],
                                                 self.asking_type, self.current_name)
            self.questions_db[self.selected_group_number] = question_group
            return True
        return False

    def step_backward(self):
        if self.questions_db[self.selected_group_number].backward():
            return True
        # TODO тут можно вставить какое-нибудь условие для группы
        if self.selected_group_number > 0:
            self.selected_group_number -= 1
            return True
        return False

    def process(self):
        complains_list = []
        for group in self.questions_db.values():
            group_process = group.process()
            if group_process:
                complains_list.append(group_process)
        return complains_list
