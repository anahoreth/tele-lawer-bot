import config
import enum

if config.IS_LOCAL:
    import question_groups.abstract_group as abstract_group
    import question_groups.questions_types as questions_types
else:
    import questions.question_groups.abstract_group as abstract_group
    import questions.question_groups.questions_types as questions_types


class TransportQuestionsNames(enum.Enum):
    KeyZero = questions_types.ZERO_QUESTION
    KeyFirstOne = 1  # Было ли вам тесно во время транспортировки?
    KeyFirstOneOne = 1.11  # Какая примерно была длина камеры, в которой вас перевозили (в метрах)?
    KeyFirstOneTwo = 1.12  # Какая примерно была ширина камеры, в которой вас перевозили (в метрах)?
    KeyFirstOneThree = 1.13  # Сколько людей было с вами в этой камере (назовите максимальное количество, включая вас)?
    KeyFirstOneFour = 1.14  # Сковывали ли вас наручниками с другими задержанными во время транспортировки?
    KeyFirstOneFourOneOne = 1.1411  # Сколько человек было в связке с вами, не включая вас?
    KeyFirstOneFourTwoOne = 1.1421  # Примерно как долго вы были скованы (например, «1 час 45 минут»)?

    KeyFirstTwoOne = 1.21  # Сковывали ли вас наручниками с другими задержанными во время транспортировки?
    KeyFirstTwoOneOneOne = 1.2111  # Сколько человек было в связке с вами?
    KeyFirstTwoOneOneTwo = 1.2112  # Примерно как долго вы были скованы (например, «1 час 45 минут»)?

    KeySecond = 2  # Было ли вам холодно во время нахождения в машине?
    KeySecondOneOne = 2.11  # Примерно как долго вас держали в машине в холоде (например, «1 час 45 минут»)?
    KeySecondTwoOne = 2.21  # Было ли вам жарко во время нахождения в машине?
    KeySecondTwoOneOneOne = 2.2111  # Примерно как долго вас держали в жаре (например, «1 час 45 минут»)?

    KeyThree = 3  # Хватало ли вам свежего воздуха при транспортировке?
    KeyLast = questions_types.LAST_QUESTION
    KeyCount = 3


class TransportQuestionGroup(abstract_group.QuestionGroup):

    def __init__(self, asking_type):
        super().__init__(asking_type)
        self.question_name = TransportQuestionsNames.KeyFirstOne

    def get_short_name(self):
        return "Транспортировка"

    def get_questions_count(self):
        return TransportQuestionsNames.KeyCount.value

    def make_new_question(self):
        if self.question_name == TransportQuestionsNames.KeyFirstOne:
            return TransportQuestionFirstOne(self.asking_type)
        if self.question_name == TransportQuestionsNames.KeyFirstOneOne:
            return TransportQuestionFirstOneOne(self.asking_type)
        if self.question_name == TransportQuestionsNames.KeyFirstOneTwo:
            return TransportQuestionFirstOneTwo(self.asking_type)
        if self.question_name == TransportQuestionsNames.KeyFirstOneThree:
            return TransportQuestionFirstOneThree(self.asking_type)
        if self.question_name == TransportQuestionsNames.KeyFirstOneFour:
            return TransportQuestionFirstOneFour(self.asking_type)
        if self.question_name == TransportQuestionsNames.KeyFirstOneFourOneOne:
            return TransportQuestionFirstOneFourOneOne(self.asking_type)
        if self.question_name == TransportQuestionsNames.KeyFirstOneFourTwoOne:
            return TransportQuestionFirstOneFourTwoOne(self.asking_type)
        if self.question_name == TransportQuestionsNames.KeyFirstTwoOne:
            return TransportQuestionFirstTwoOne(self.asking_type)
        if self.question_name == TransportQuestionsNames.KeyFirstTwoOneOneOne:
            return TransportQuestionFirstTwoOneOneOne(self.asking_type)
        if self.question_name == TransportQuestionsNames.KeyFirstTwoOneOneTwo:
            return TransportQuestionFirstTwoOneOneTwo(self.asking_type)
        if self.question_name == TransportQuestionsNames.KeySecond:
            return TransportQuestionSecond(self.asking_type)
        if self.question_name == TransportQuestionsNames.KeySecondOneOne:
            return TransportQuestionSecondOneOne(self.asking_type)
        if self.question_name == TransportQuestionsNames.KeySecondTwoOne:
            return TransportQuestionSecondTwoOne(self.asking_type)
        if self.question_name == TransportQuestionsNames.KeySecondTwoOneOneOne:
            return TransportQuestionSecondTwoOneOneOne(self.asking_type)
        if self.question_name == TransportQuestionsNames.KeyThree:
            return TransportQuestionThree(self.asking_type)
        return None

    def process(self):
        complains_list = []

        # First
        choice_first = self.get_choice(TransportQuestionsNames.KeyFirstOne)
        if choice_first == 0:
            length = self.get_choice(TransportQuestionsNames.KeyFirstOneOne)
            wide = self.get_choice(TransportQuestionsNames.KeyFirstOneTwo)
            people_count = self.get_choice(TransportQuestionsNames.KeyFirstOneThree)
            choice_first_one_four = self.get_choice(TransportQuestionsNames.KeyFirstOneFour)
            if choice_first_one_four == 0:
                people_count_bunch = self.get_choice(TransportQuestionsNames.KeyFirstOneFourOneOne)
                time = self.get_choice(TransportQuestionsNames.KeyFirstOneFourTwoOne)
                complains_list.append(process_first_one_one(length, wide, people_count, people_count_bunch, time))
            elif choice_first_one_four == 1:
                complains_list.append(process_first_one_two(length, wide, people_count))

        elif choice_first == 1:
            if self.get_choice(TransportQuestionsNames.KeyFirstTwoOne) == 0:
                people_count = self.get_choice(TransportQuestionsNames.KeyFirstTwoOneOneOne)
                time = self.get_choice(TransportQuestionsNames.KeyFirstTwoOneOneTwo)
                complains_list.append(process_first_two(people_count, time))

        # Second
        choice_second = self.get_choice(TransportQuestionsNames.KeySecond)
        if  choice_second == 0:
            time = self.get_choice(TransportQuestionsNames.KeySecondOneOne)
            complains_list.append(process_second_one(time))
        elif choice_second == 1:
            if self.get_choice(TransportQuestionsNames.KeySecondTwoOne) == 0:
                time = self.get_choice(TransportQuestionsNames.KeySecondTwoOneOneOne)
                complains_list.append(process_second_two(time))

        # Third
        choice_third = self.get_choice(TransportQuestionsNames.KeyThree)
        if choice_third == 1:
            complains_list.append(process_third())

        return complains_list


def process_first_one_one(length, wide, people_count, people_count_bunch, time):

    try:
        people_count_bunch = int(people_count_bunch)
        if people_count_bunch in [11, 12, 13, 14]:
            people_count_str = 'было {} человек'.format(people_count_bunch)
        elif (people_count_bunch % 10) in [2, 3, 4]:
            people_count_str = 'было {} человека'.format(people_count_bunch)
        elif people_count_bunch % 10 == 1:
            people_count_str = 'был {} человек'.format(people_count_bunch)
        else:
            people_count_str = 'было {} человек'.format(people_count_bunch)

    except ValueError:
        people_count_str = "__"

    text = "Кроме того, я и другие задержанные были все время скованы в одну цепь. В связке со мной {}. " \
           "Мы были скованы примерно {}. Из-за этого " \
           "у меня отсутствовало личное пространство и возможность двигать руками по " \
           "своему усмотрению.".format(people_count_str, time)

    return process_first_one_two(length, wide, people_count) + text


def process_first_one_two(length, wide, people_count):
    try:
        sq = str(round(float(length) * float(wide)))
    except ValueError:
        sq = "__"

    try:
        per_person = str(round(float(length) * float(wide) / int(people_count), 2))

    except ValueError:
        per_person = "__"
    try:
        people_count = int(people_count)
        if people_count in [11, 12, 13, 14]:
            people_count_str = 'находилось {} человек'.format(people_count)
        elif (people_count % 10) in [2, 3, 4]:
            people_count_str = 'находилось {} человека'.format(people_count)
        elif people_count % 10 == 1:
            people_count_str = 'находился {} человек'.format(people_count)
        else:
            people_count_str = 'находилось {} человек'.format(people_count)

    except ValueError:
        people_count_str = "__"

    return "Во время транспортировки мне было очень тесно. Площадь отдела автомобиля, в котором меня перевозили, " \
    "составляла примерно {} кв.м. При этом там {}, включая меня. Т.е. " \
           "на одного человека приходилось {} кв.м. ".format(sq, people_count_str, per_person)


def process_first_two(people_count, time):
    try:
        people_count = int(people_count)
        if people_count in [11, 12, 13, 14]:
            people_count_str = 'было {} человек'.format(people_count)
        elif (people_count % 10) in [2, 3, 4]:
            people_count_str = 'было {} человека'.format(people_count)
        elif people_count % 10 == 1:
            people_count_str = 'был {} человек'.format(people_count)
        else:
            people_count_str = 'было {} человек'.format(people_count)

    except ValueError:
        people_count_str = "__"

    if time is None:
        time = "___"

    return 'Во время транспортировки меня сковали в одну цепь с другими задержанными. ' \
           'В связке со мной {}. Мы были скованы примерно {}. ' \
           'Из-за этого у меня отсутствовало личное пространство и возможность двигать руками по ' \
           'своему усмотрению.'.format(people_count_str, time)


def process_second_one(time):
    if time is None:
        time = "___"
    return 'При транспортировке мне было холодно. В холодной машине меня держали примерно {}. ' \
           'При этом сотрудники, осуществлявшие конвоирование, не могли не осознавать неадекватный температурный' \
           ' режим и то, что подобное обращение является жестоким и бесчеловечным.'.format(time)


def process_second_two(time):
    if time is None:
        time = "___"
    return 'При транспортировке мне было жарко. В жаркой душной машине меня держали примерно {}. ' \
           'При этом сотрудники, осуществлявшие конвоирование, не могли не осознавать неадекватный температурный ' \
           'режим и то, что подобное обращение является жестоким и бесчеловечным.'.format(time)


def process_third():
    return 'Мне не хватало свежего воздуха. Это вызывало значительный дискомфорт. У меня не было возможности ' \
           'элементарно вдохнуть воздух полной грудью. Такие условия транспортировки являются негуманными.'


class TransportQuestionFirstOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Было ли вам тесно во время транспортировки?"
        answers = ['1. Да', "2. Нет"]
        super().__init__(text, answers, asking_type)

    def process(self):
        return ""

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 0:
            return TransportQuestionsNames.KeyFirstOneOne
        return TransportQuestionsNames.KeyFirstTwoOne

    def get_previous_question_name(self):
        return TransportQuestionsNames.KeyZero


class TransportQuestionFirstOneOne(questions_types.GetNumberQuestion):

    def __init__(self, asking_type):
        text = "Какая примерно была длина камеры, в которой вас перевозили (в метрах)?\n\n" \
               '(дробные числа вводите через точку, например "2.5")'
        super().__init__(text, asking_type)

    def process(self):
        return self.get_choice()

    def get_next_question_name(self):
        return TransportQuestionsNames.KeyFirstOneTwo

    def get_previous_question_name(self):
        return TransportQuestionsNames.KeyFirstOne


class TransportQuestionFirstOneTwo(questions_types.GetNumberQuestion):

    def __init__(self, asking_type):
        text = "Какая примерно была ширина камеры, в которой вас перевозили (в метрах)?\n\n" \
                '(дробные числа вводите через точку, например "1.9")'
        super().__init__(text, asking_type)

    def process(self):
        return self.get_choice()

    def get_next_question_name(self):
        return TransportQuestionsNames.KeyFirstOneThree

    def get_previous_question_name(self):
        return TransportQuestionsNames.KeyFirstOneOne


class TransportQuestionFirstOneThree(questions_types.GetNumberQuestion):

    def __init__(self, asking_type):
        text = "Сколько людей, включая вас, в этой камере " \
               "(назовите максимальное количество)?"
        super().__init__(text, asking_type)

    def process(self):
        return self.get_choice()

    def get_next_question_name(self):
        return TransportQuestionsNames.KeyFirstOneFour

    def get_previous_question_name(self):
        return TransportQuestionsNames.KeyFirstOneTwo


class TransportQuestionFirstOneFour(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Сковывали ли вас наручниками с другими задержанными во время транспортировки?"
        answers = ['1. Да', "2. Нет"]
        super().__init__(text, answers, asking_type)

    def process(self):
        return ""

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 0:
            return TransportQuestionsNames.KeyFirstOneFourOneOne
        return TransportQuestionsNames.KeySecond

    def get_previous_question_name(self):
        return TransportQuestionsNames.KeyFirstOneThree


class TransportQuestionFirstOneFourOneOne(questions_types.GetNumberQuestion):

    def __init__(self, asking_type):
        text = "Сколько человек было в связке с вами, не включая вас?"
        super().__init__(text, asking_type)

    def process(self):
        return self.get_choice()

    def get_next_question_name(self):
        return TransportQuestionsNames.KeyFirstOneFourTwoOne

    def get_previous_question_name(self):
        return TransportQuestionsNames.KeyFirstOneFour


class TransportQuestionFirstOneFourTwoOne(questions_types.GetNumberQuestion):

    def __init__(self, asking_type):
        text = 'Примерно как долго вы были скованы (например, "2 часа 12 минут")?'
        super().__init__(text, asking_type)

    def process(self):
        return self.get_choice()

    def get_next_question_name(self):
        return TransportQuestionsNames.KeySecond

    def get_previous_question_name(self):
        return TransportQuestionsNames.KeyFirstOneFourOneOne


class TransportQuestionFirstTwoOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Сковывали ли вас наручниками с другими задержанными во время транспортировки?"
        answers = ['1. Да', "2. Нет"]
        super().__init__(text, answers, asking_type)

    def process(self):
        return ""

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 0:
            return TransportQuestionsNames.KeyFirstTwoOneOneOne
        return TransportQuestionsNames.KeySecond

    def get_previous_question_name(self):
        return TransportQuestionsNames.KeyFirstOne


class TransportQuestionFirstTwoOneOneOne(questions_types.GetNumberQuestion):

    def __init__(self, asking_type):
        text = "Сколько человек было в связке с Вами?"
        super().__init__(text, asking_type)

    def process(self):
        return ""

    def get_next_question_name(self):
        return TransportQuestionsNames.KeyFirstTwoOneOneTwo

    def get_previous_question_name(self):
        return TransportQuestionsNames.KeyFirstTwoOne


class TransportQuestionFirstTwoOneOneTwo(questions_types.GetNumberQuestion):

    def __init__(self, asking_type):
        text = "Примерно как долго вы были скованы (например, «1 час 45 минут»)?"
        super().__init__(text, asking_type)

    def process(self):
        return ""

    def get_next_question_name(self):
        return TransportQuestionsNames.KeySecond

    def get_previous_question_name(self):
        return TransportQuestionsNames.KeyFirstTwoOneOneOne


class TransportQuestionSecond(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Было ли вам холодно во время нахождения в машине?"
        answers = ['1. Да', "2. Нет"]
        super().__init__(text, answers, asking_type)

    def process(self):
        return ""

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 0:
            return TransportQuestionsNames.KeySecondOneOne
        return TransportQuestionsNames.KeySecondTwoOne

    def get_previous_question_name(self):
        return TransportQuestionsNames.KeyFirst


class TransportQuestionSecondOneOne(questions_types.GetNumberQuestion):

    def __init__(self, asking_type):
        text = 'Примерно как долго вас держали в машине в холоде (например, "4 часа 19 минут")?'
        super().__init__(text, asking_type)

    def process(self):

        return 'При транспортировке мне было холодно. В холодной машине меня держали примерно ' + self.get_choice() + \
               '. При этом сотрудники, осуществлявшие конвоирование, не могли не осознавать неадекватный ' \
               'температурный режим и то, что подобное обращение является жестоким и бесчеловечным.'

    def get_next_question_name(self):
        return TransportQuestionsNames.KeyThree

    def get_previous_question_name(self):
        return TransportQuestionsNames.KeySecond


class TransportQuestionSecondTwoOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Было ли вам жарко во время нахождения в машине?"
        answers = ['1. Да', "2. Нет"]
        super().__init__(text, answers, asking_type)

    def process(self):
        return ""

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 0:
            return TransportQuestionsNames.KeySecondTwoOneOneOne
        return TransportQuestionsNames.KeyThree

    def get_previous_question_name(self):
        return TransportQuestionsNames.KeySecond


class TransportQuestionSecondTwoOneOneOne(questions_types.GetNumberQuestion):

    def __init__(self, asking_type):
        text = "Примерно как долго вас держали в машине в жаре (например, «3 часа 7 минут»)?"
        super().__init__(text, asking_type)

    def process(self):
        return 'При транспортировке мне было жарко. В жаркой душной машине меня держали примерно ' + self.get_choice() + \
               '. При этом сотрудники, осуществлявшие конвоирование, не могли не осознавать неадекватный ' \
               'температурный режим и то, что подобное обращение является жестоким и бесчеловечным.'

    def get_next_question_name(self):
        return TransportQuestionsNames.KeyThree

    def get_previous_question_name(self):
        return TransportQuestionsNames.KeySecond


class TransportQuestionThree(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Хватало ли вам свежего воздуха при транспортировке?"
        answers = ['1. Да', "2. Нет"]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 1:
            return
        return "Мне не хватало свежего воздуха. Это вызывало значительный дискомфорт. У меня не было возможности " \
               "элементарно вдохнуть воздух полной грудью. Такие условия транспортировки являются негуманными."

    def get_next_question_name(self):
        return TransportQuestionsNames.KeyLast

    def get_previous_question_name(self):
        return TransportQuestionsNames.KeySecond
