import config
import enum
from collections import deque

if config.IS_LOCAL:
    import question_groups.abstract_group as abstract_group
    import question_groups.questions_types as questions_types
   # import cell_questions as cell_questions
else:
    import questions.question_groups.abstract_group as abstract_group
    import questions.question_groups.questions_types as questions_types
  #  import questions.question_groups.cell_questions as cell_questions


class CellQuestionsNames(enum.Enum):
    KeyZero = questions_types.ZERO_QUESTION
    KeyFirst = 1  # В скольких камерах, включая карцер, вас держали
    KeySecond = 2  # На условия в каких камерах вы хотите пожаловаться?
    KeyThird = 3  # Примерно сколько дней вы находились в ... камере?
    KeyThirdOne = 3.1  # 1.9.2.2.2.	Когда вы были в этой камере?

    KeyFourth = 4  # Я хочу пожаловаться на

    # первые три вопроса общие для до/после суда
    KeyCellFirstOneOne = 5.1  # Какая примерно была длина камеры (в метрах)?
    KeyCellFirstOneTwo = 5.2  # Какая примерно была ширина камеры (в метрах)?
    KeyCellFirstOneThree = 5.3  # Сколько задержанных было (до суда) в камере?
    KeyCellFirstOneFour = 5.4  # Сколько задержанных было (после суда) в камере?

    KeyCellSecondOne = 6.1  # Была ли в камере грязь или плесень?
    KeyCellSecondTwo = 6.2  # Были ли в камере насекомые или грызуны?
    KeyCellSecondThree = 6.3  # Содержали ли вас вместе с людьми с заразными болезнями
    KeyCellSecondThreeOne = 6.31  # Я зафиксировала ваш ответ, но

    KeyCellThirdOne = 7.1  # В камере было холодно?
    KeyCellThirdSecond = 7.2  # В камере было жарко?

    KeyCellFourthOne = 8.1  # Было ли в камере достаточно свежего воздуха?
    KeyCellFourthOneOne = 8.11  # Был ли неприятный запах в камере?
    KeyCellFourthOneOneOne = 8.111  # Я зафиксировала ваш ответ, но
    KeyCellFourthOneTwo = 8.12  # Был ли неприятный запах в камере?
    KeyCellFourthOneTwoOne = 8.121  # Я зафиксировала ваш ответ, но

    KeyCellFifthOne = 9.1  # Естественного освещения было достаточно
    KeyCellFifthTwo = 9.2  # Искусственного освещения было достаточно
    KeyCellFifthThree = 9.3  # Мешал ли свет в камере спать ночью?

    KeyCellSixthOne = 10.1  # Была ли камера оборудована туалетом?
    KeyCellSixthOneOne = 10.11  # Он работал?
    KeyCellSixthTwo = 10.2  # Могли ли другие сокамерники видеть вас во время посещения туалета?
    KeyCellSixthThree = 10.3  # Ваша кровать располагалась слишком близко к туалету?
    KeyCellSixthThreeOne = 10.31  # Сколько примерно сантиметров было до туалета?

    KeyCellSevenOneOne = 11.11  # Укажите, чего не было в камере (Для ЦИП)
    KeyCellSevenOneTwo = 11.12  # Выдавали ли вам хотя бы один комплект шашек
    KeyCellSevenTwoOne = 11.21  # Укажите, чего не было в камере (ДО СУДА)
    KeyCellSevenTwoTwo = 11.22  # Выдавали ли вам хотя бы один комплект шашек,
    KeyCellSevenThreeOne = 11.31  # Укажите, чего не было в камере (ДО и ПОСЛЕ СУДА)
    KeyCellSevenThreeTwo = 11.32  # Выдавали ли вам хотя бы один комплект шашек,

    KeyLast = questions_types.LAST_QUESTION
    KeyCount = 11  # TODO


class CellQuestionGroup(abstract_group.QuestionGroup):

    def get_people_name(self, count):
        if count == 1:
            return "был размещен один человек"
        if 2 <= count <= 4:
            return "было размещено {} человека".format(count)
        return "было размещено {} человек".format(count)

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        super().__init__(asking_type)
        self.question_name = CellQuestionsNames.KeyFirst
        self.complains_list = []

    def get_short_name(self):
        return "Состояние и обстановка камеры"

    def get_questions_count(self):
        return CellQuestionsNames.KeyCount.value

    def process_first_both(self):
        try:
            length = float(self.get_choice(CellQuestionsNames.KeyCellFirstOneOne))
            wide = float(self.get_choice(CellQuestionsNames.KeyCellFirstOneTwo))
            space = round(length * wide, 2)
            people_count_before = int(self.get_choice(CellQuestionsNames.KeyCellFirstOneThree))
            people_count_after = int(self.get_choice(CellQuestionsNames.KeyCellFirstOneFour))
        except:
            space = None
            people_count_before = None
            people_count_after = None
        if not (space and people_count_before and people_count_after):
            return ""

        is_enough_space_before = (space / people_count_before > 2.5)
        is_enough_space_after = (space / people_count_after > 4)

        if not (is_enough_space_after and is_enough_space_before):
            return "До суда камера была переполнена. Площадь камеры составляла примерно {} " \
                   "кв.м., при этом в ней {}." \
                   " Таким образом, на одного человека приходилось менее 2,5 квадратных метров, " \
                   "что нарушает часть 4 статьи 13 Закона Республики Беларусь от 16 июня 2003 г. " \
                   "№ 215-З «О порядке и условиях содержания лиц под стражей». После суда камера также была" \
                   " переполнена. В ней {}." \
                   " Таким образом, на одного человека приходилось менее 4 квадратных метров, что нарушает часть" \
                   " 1 статьи 18.7 ПИКоАП.".format(space, self.get_people_name(people_count_before),
                                                   self.get_people_name(people_count_after))

        if not is_enough_space_before:
            return "До суда камера была переполнена. Площадь камеры составляла примерно {} " \
                   "кв.м., при этом в ней {}." \
                   " Таким образом, на одного человека приходилось менее 2,5 квадратных метров, " \
                   "что нарушает часть 4 статьи 13 Закона Республики Беларусь от 16 июня 2003 г. " \
                   "№ 215-З «О порядке и условиях содержания лиц под " \
                   "стражей».".format(space, self.get_people_name(people_count_before))

        if not is_enough_space_after:
            return "После суда камера была переполнена. Площадь камеры составляла примерно " \
                   "{} кв.м., при этом в ней {}. " \
                   "Таким образом, на одного человека приходилось менее 4 квадратных метров, " \
                   "что нарушает часть 1 статьи 18.7 ПИКоАП.".format(space,
                                                                     self.get_people_name(people_count_after))


    def process_first(self):
        if self.asking_type.value == abstract_group.AskingType.BothSideOfCourt.value:
            return self.process_first_both()

        try:
            length = float(self.get_choice(CellQuestionsNames.KeyCellFirstOneOne))
            wide = float(self.get_choice(CellQuestionsNames.KeyCellFirstOneTwo))
            space = round(length * wide, 2)
            people_count = int(self.get_choice(CellQuestionsNames.KeyCellFirstOneThree))
        except:
            space = None
            people_count = None
        if not (space and people_count):
            return ""

        is_enough_space = True
        if self.asking_type.value == abstract_group.AskingType.BeforeCourt.value:
            is_enough_space = (space / people_count > 2.5)
        elif self.asking_type.value == abstract_group.AskingType.AfterCourt.value:
            is_enough_space = (space / people_count > 4)
        if is_enough_space:
            return ""

        if self.asking_type.value == abstract_group.AskingType.BeforeCourt.value:
            return 'Камера была переполнена. Площадь камеры составляла примерно {} кв.м., ' \
                   'при этом в ней {}. ' \
                   'Таким образом, на одного человека приходилось менее 2,5 квадратных метров, что нарушает ' \
                   'часть 4 статьи 13 Закона Республики Беларусь «О порядке и условиях содержания лиц под стражей» ' \
                   'от 16 июня 2003 г. № 215-З.'.format(space, self.get_people_name(people_count))
        elif self.asking_type.value == abstract_group.AskingType.AfterCourt.value:
            return "Камера была переполнена. Площадь камеры составляла примерно {} кв.м., " \
                   "при этом в ней {}. Таким образом, " \
                   "на одного человека приходилось менее 4 квадратных метров, что нарушает " \
                   "часть 1 статьи 18.7 ПИКоАП.".format(space, self.get_people_name(people_count))
        return ""

    def process_cell(self):
        complains_list = []
        for question_name in self.questions_array:
            if question_name in [CellQuestionsNames.KeyFirst,
                                 CellQuestionsNames.KeySecond,
                                 CellQuestionsNames.KeyFourth]:
                continue
            question = self.questions_array.get(question_name)
            if question is None:
                continue
            complain = question.process()
            if complain:
                complains_list.append(complain)
        process_first_result = self.process_first()
        if process_first_result:
            complains_list.append(process_first_result)
        return complains_list

    def process(self):
        question_second = self.questions_array.get(CellQuestionsNames.KeySecond)
        if question_second:
            question_second_process = question_second.process()
            if question_second_process:
                self.complains_list.insert(0, question_second_process)
        question_first_process = self.questions_array.get(CellQuestionsNames.KeyFirst).process()
        if question_first_process:
            self.complains_list.insert(0, question_first_process)
        process_cell_result = self.process_cell()
        if process_cell_result:
            self.complains_list += process_cell_result
        return self.complains_list

    def get_question(self):
        if self.question_name == CellQuestionsNames.KeyThird:
            process_cell_result = self.process_cell()
            if process_cell_result:
                self.complains_list += process_cell_result
            self.delete_cell_questions()

        question = self.questions_array.get(self.question_name)
        if question is not None:
            return question

        new_question = self.make_new_question()
        if new_question is None:
            return None
        self.questions_array[self.question_name] = new_question
        return new_question

    def delete_cell_questions(self):
        keys = [k for k in self.questions_array.keys()]
        for question_name in keys:
            if question_name in [CellQuestionsNames.KeyFirst,
                                 CellQuestionsNames.KeySecond]:
                continue
            self.questions_array.pop(question_name)

    def make_new_question(self):
        if self.question_name == CellQuestionsNames.KeyFirst:
            return QuestionFirst(self.asking_type, self.place_name)
        if self.question_name == CellQuestionsNames.KeySecond:
            return QuestionSecond(self.asking_type, self.place_name)
        if self.question_name == CellQuestionsNames.KeyThird:
            return QuestionThird(self.asking_type, self.place_name)
        if self.question_name == CellQuestionsNames.KeyThirdOne:
            return QuestionThirdOne(self.asking_type, self.place_name)
        if self.question_name == CellQuestionsNames.KeyFourth:
            return QuestionFourth(self.asking_type, self.place_name)

        global current_cell_asking_type
        asking_type = current_cell_asking_type if current_cell_asking_type else self.asking_type

        if CellQuestionsNames(self.question_name.value) == CellQuestionsNames.KeyCellFirstOneOne:
            return CellQuestionFirstOneOne(asking_type)
        if CellQuestionsNames(self.question_name.value) == CellQuestionsNames.KeyCellFirstOneTwo:
            return CellQuestionFirstOneTwo(asking_type)
        if CellQuestionsNames(self.question_name.value) == CellQuestionsNames.KeyCellFirstOneThree:
            return CellQuestionFirstOneThree(asking_type)
        if CellQuestionsNames(self.question_name.value) == CellQuestionsNames.KeyCellFirstOneFour:
            return CellQuestionFirstOneFour(asking_type)
        if CellQuestionsNames(self.question_name.value) == CellQuestionsNames.KeyCellSecondOne:
            return CellQuestionSecondOne(asking_type)
        if CellQuestionsNames(self.question_name.value) == CellQuestionsNames.KeyCellSecondTwo:
            return CellQuestionSecondTwo(asking_type)
        if CellQuestionsNames(self.question_name.value) == CellQuestionsNames.KeyCellSecondThree:
            return CellQuestionSecondThree(asking_type)
        if CellQuestionsNames(self.question_name.value) == CellQuestionsNames.KeyCellSecondThreeOne:
            return CellQuestionSecondThreeOne(asking_type)
        if CellQuestionsNames(self.question_name.value) == CellQuestionsNames.KeyCellThirdOne:
            return CellQuestionThirdOne(asking_type)
        if CellQuestionsNames(self.question_name.value) == CellQuestionsNames.KeyCellThirdSecond:
            return CellQuestionThirdSecond(asking_type)

        if CellQuestionsNames(self.question_name.value) == CellQuestionsNames.KeyCellFourthOne:
            return CellQuestionFourthOne(asking_type)
        if CellQuestionsNames(self.question_name.value) == CellQuestionsNames.KeyCellFourthOneOne:
            return CellQuestionFourthOneOne(asking_type)
        if CellQuestionsNames(self.question_name.value) == CellQuestionsNames.KeyCellFourthOneOneOne:
            return CellQuestionFourthOneOneOne(asking_type)
        if CellQuestionsNames(self.question_name.value) == CellQuestionsNames.KeyCellFourthOneTwo:
            return CellQuestionFourthOneTwo(asking_type)
        if CellQuestionsNames(self.question_name.value) == CellQuestionsNames.KeyCellFourthOneTwoOne:
            return CellQuestionFourthOneTwoOne(asking_type)
        if CellQuestionsNames(self.question_name.value) == CellQuestionsNames.KeyCellFifthOne:
            return CellQuestionFifthOne(asking_type)
        if CellQuestionsNames(self.question_name.value) == CellQuestionsNames.KeyCellFifthTwo:
            return CellQuestionFifthTwo(asking_type)
        if CellQuestionsNames(self.question_name.value) == CellQuestionsNames.KeyCellFifthThree:
            return CellQuestionFifthThree(asking_type)

        if CellQuestionsNames(self.question_name.value) == CellQuestionsNames.KeyCellSixthOne:
            return CellQuestionSixthOne(asking_type)
        if CellQuestionsNames(self.question_name.value) == CellQuestionsNames.KeyCellSixthOneOne:
            return CellQuestionSixthOneOne(asking_type)
        if CellQuestionsNames(self.question_name.value) == CellQuestionsNames.KeyCellSixthTwo:
            return CellQuestionSixthTwo(asking_type)
        if CellQuestionsNames(self.question_name.value) == CellQuestionsNames.KeyCellSixthThree:
            return CellQuestionSixthThree(asking_type)
        if CellQuestionsNames(self.question_name.value) == CellQuestionsNames.KeyCellSixthThreeOne:
            return CellQuestionSixthThreeOne(asking_type)

        if CellQuestionsNames(self.question_name.value) == CellQuestionsNames.KeyCellSevenOneOne:
            return CellQuestionSevenOneOne(asking_type)
        if CellQuestionsNames(self.question_name.value) == CellQuestionsNames.KeyCellSevenOneTwo:
            return CellQuestionSevenOneTwo(asking_type)
        if CellQuestionsNames(self.question_name.value) == CellQuestionsNames.KeyCellSevenTwoOne:
            return CellQuestionSevenTwoOne(asking_type)
        if CellQuestionsNames(self.question_name.value) == CellQuestionsNames.KeyCellSevenTwoTwo:
            return CellQuestionSevenTwoTwo(asking_type)
        if CellQuestionsNames(self.question_name.value) == CellQuestionsNames.KeyCellSevenThreeOne:
            return CellQuestionSevenThreeOne(asking_type)
        if CellQuestionsNames(self.question_name.value) == CellQuestionsNames.KeyCellSevenThreeTwo:
            return CellQuestionSevenThreeTwo(asking_type)

        return None


# TODO сделать возможность больше, чем восемь камер
def get_cell_number_name(i):
    if i == 1:
        return "Первой"
    if i == 2:
        return "Второй"
    if i == 3:
        return "Третьей"
    if i == 4:
        return "Четвертой"
    if i == 5:
        return "Пятой"
    if i == 6:
        return "Шестой"
    if i == 7:
        return "Седьмой"
    if i == 8:
        return "Восьмой"
    if i == 9:
        return "Девятой"
    if i == 10:
        return "Десятой"
    if i == 11:
        return "Одиннадцатой"
    if i == 12:
        return "Двенадцатой"
    if i == 13:
        return "Тринадцатой"
    if i == 14:
        return "Четырнадцатой"
    if i == 15:
        return "Пятнадцатой"
    if i == 16:
        return "Шестнадцатой"
    return ""


def get_order_answers():
    answers = []
    if cells_count == 0:
        return answers
    for i in range(cells_count):
        answer = "{}. {}".format(i + 1, get_cell_number_name(i + 1))
        answers.append(answer)
    return answers


cats = [
    "Тесноту в камере",
    "Загрязненность камеры и угрозу заражения болезнями",
    "Температуру в камере",
    "Отсутствие свежего воздуха",
    "Освещение в камере",
    "Оборудование туалета",
    "Оборудование камеры",
]

# TODO рзобраться, чтобы это была реально глобальная переменная

selected_cells_queue = deque()
current_cell_number = 0
current_cell_asking_type = None
selected_cells_cats = deque()


def cell_get_next_question_name():
    questions_dict = {
        0: CellQuestionsNames.KeyCellFirstOneOne,
        1: CellQuestionsNames.KeyCellSecondOne,
        2: CellQuestionsNames.KeyCellThirdOne,
        3: CellQuestionsNames.KeyCellFourthOne,
        4: CellQuestionsNames.KeyCellFifthOne,
        5: CellQuestionsNames.KeyCellSixthOne,
        61: CellQuestionsNames.KeyCellSevenOneOne,
        62: CellQuestionsNames.KeyCellSevenTwoOne,
        63: CellQuestionsNames.KeyCellSevenThreeOne,
    }

    global selected_cells_cats
    if len(selected_cells_cats) > 0:
        number = selected_cells_cats.popleft()
        return questions_dict[number]

    if len(selected_cells_queue) > 0:
        global current_cell_number
        cell_number, asking_type = selected_cells_queue.popleft()
        global current_cell_asking_type
        current_cell_asking_type = asking_type
        current_cell_number = cell_number
        return CellQuestionsNames.KeyThird

    return CellQuestionsNames.KeyLast


class QuestionFirst(questions_types.GetNumberQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "В скольких камерах, включая карцер, вас держали в {}?".format(self.place_name)
        super().__init__(text, asking_type)

    def get_next_question_name(self):
        global cells_count
        cells_count = int(self.get_choice())
        if cells_count == 1:
            global current_cell_number
            current_cell_number = 1
            return CellQuestionsNames.KeyThird
        return CellQuestionsNames.KeySecond

    def process(self):
        choice = self.get_choice()
        if int(choice) > 1:
            return "Во время нахождения в {} меня держали в {} камерах.".format(self.place_name, cells_count)
        return ""

    def get_previous_question_name(self):
        return CellQuestionsNames.KeyZero


class QuestionSecond(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "На условия в каких камерах вы хотите пожаловаться?"
        if cells_count > 16:
            text += "\n\n(после шестнадцатой камеры будут указываться только номера)"
        answers = get_order_answers()
        super().__init__(text, answers, asking_type, questions_types.QuestionAnswerType.ANSWER_QUERY_MANY)

    def get_next_question_name(self):
        global selected_cells_queue
        for a in self.get_choice():
            selected_cells_queue.append((a + 1, self.asking_type))

        cell_number, asking_type = selected_cells_queue.popleft()
        global current_cell_number, current_cell_asking_type
        current_cell_number = cell_number
        current_cell_asking_type = asking_type

        return CellQuestionsNames.KeyThird

    def get_previous_question_name(self):
        return CellQuestionsNames.KeyFirst


class QuestionThird(questions_types.GetNumberQuestion):

    def get_days_name(self, days_count):
        if days_count in [11, 12, 13, 14]:
            return "{} дней".format(days_count)
        if days_count % 10 == 1:
            return "{} день".format(days_count)
        if days_count % 10 in [2, 3, 4]:
            return "{} дня".format(days_count)
        return "{} дней".format(days_count)

    def __init__(self, asking_type, place_name):
        self.place_name = place_name

        self.cell_number_name = get_cell_number_name(current_cell_number).lower()
        if self.cell_number_name == "второй":
            self.cell_number_name = "во второй"
        else:
            self.cell_number_name = "в " + self.cell_number_name

        text = "Примерно сколько дней вы находились {} камере?".format(self.cell_number_name)
        super().__init__(text, asking_type)

    def process(self):
        days_count = self.get_choice()
        days_name = self.get_days_name(int(days_count))
        if days_count == -1:
            return ""

        if cells_count == 1:
            return "Во время нахождения в {} меня держали в одной камере. Условия в этой камере были бесчеловечными," \
                   " унижали мое человеческое достоинство и представляли угрозу, как минимум, моему " \
                   "здоровью. В этой камере меня держали {}.".format(self.place_name, days_name)
        return "Условия {} камере, в которой меня держали в {}, были бесчеловечными, " \
               "унижали мое человеческое достоинство и представляли угрозу, как минимум, моему " \
               "здоровью. В этой камере меня держали {}.".format(self.cell_number_name, self.place_name, days_name)

    def get_next_question_name(self):
        if self.asking_type.value == abstract_group.AskingType.BothSideOfCourt.value:
            return CellQuestionsNames.KeyThirdOne
        return CellQuestionsNames.KeyFourth

    def get_previous_question_name(self):
        return CellQuestionsNames.KeySecond


class QuestionThirdOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Когда вы были в этой камере?"
        answers = ["1. До суда",
                   "2. После суда",
                   "3. И до, и после суда",
                   ]

        super().__init__(text, answers, asking_type, questions_types.QuestionAnswerType.ANSWER_QUERY_ONLY)

    def get_next_question_name(self):
        global current_cell_asking_type

        choice = self.get_choice()

        if choice == 0:
            current_cell_asking_type = abstract_group.AskingType.BeforeCourt
        elif choice == 1:
            current_cell_asking_type = abstract_group.AskingType.AfterCourt
        elif choice == 2:
            current_cell_asking_type = abstract_group.AskingType.BothSideOfCourt
        return CellQuestionsNames.KeyFourth

    def get_previous_question_name(self):
        return CellQuestionsNames.KeyThird


class QuestionFourth(questions_types.GetChoiceQuestion):

    def get_cats_answers(self):
        answers = []
        for i in range(len(cats)):
            answer = "{}. {}".format(i + 1, cats[i])
            answers.append(answer)
        return answers

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Я хочу пожаловаться на"
        answers = self.get_cats_answers()
        super().__init__(text, answers, asking_type, questions_types.QuestionAnswerType.ANSWER_QUERY_MANY)

    def get_next_question_name(self):
        global selected_cells_cats
        selected_cells_cats = deque()
        for a in self.get_choice():
            if a == 6:
                if self.asking_type.value == abstract_group.AskingType.AfterCourt.value:
                    selected_cells_cats.append(61)
                elif self.asking_type.value == abstract_group.AskingType.BeforeCourt.value:
                    selected_cells_cats.append(62)
                elif self.asking_type.value == abstract_group.AskingType.BothSideOfCourt.value:
                    selected_cells_cats.append(63)
            else:
                selected_cells_cats.append(a)

        return cell_get_next_question_name()

    def get_previous_question_name(self):
        return CellQuestionsNames.KeyThird


class CellQuestionFirstOneOne(questions_types.GetNumberQuestion):

    def __init__(self, asking_type):
        text = "Какая примерно была длина камеры (в метрах)?\n\n" \
               '(дробные числа вводите через точку, например "3.2")'

        super().__init__(text, asking_type)

    def get_next_question_name(self):
        return CellQuestionsNames.KeyCellFirstOneTwo

    def get_previous_question_name(self):
        return CellQuestionsNames.KeyFourth


class CellQuestionFirstOneTwo(questions_types.GetNumberQuestion):

    def __init__(self, asking_type):
        text = "Какая примерно была ширина камеры (в метрах)\n\n" \
               '(дробные числа вводите через точку, например "1.5")'
        super().__init__(text, asking_type)

    def get_next_question_name(self):
        return CellQuestionsNames.KeyCellFirstOneThree

    def get_previous_question_name(self):
        return CellQuestionsNames.KeyCellFirstOneOne


class CellQuestionFirstOneThree(questions_types.GetNumberQuestion):

    def __init__(self, asking_type):
        if asking_type.value == abstract_group.AskingType.BothSideOfCourt.value:
            text = "Сколько задержанных, включая вас, было в камере *до суда* (назовите максимальное количество)?"
        else:
            text = "Сколько задержанных, включая вас, было в камере (назовите максимальное количество)?"
        super().__init__(text, asking_type)

    def get_next_question_name(self):
        if self.asking_type.value == abstract_group.AskingType.BothSideOfCourt.value:
            return CellQuestionsNames.KeyCellFirstOneFour
        return cell_get_next_question_name()

    def get_previous_question_name(self):
        return CellQuestionsNames.KeyCellFirstOneTwo


class CellQuestionFirstOneFour(questions_types.GetNumberQuestion):

    def __init__(self, asking_type):
        text = "Сколько задержанных, включая вас, было в камере *после суда* (назовите максимальное количество)?"
        super().__init__(text, asking_type)

    def get_next_question_name(self):
        return cell_get_next_question_name()

    def get_previous_question_name(self):
        return CellQuestionsNames.KeyCellFirstOneThree


class CellQuestionSecondOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Была ли в камере грязь или плесень?"
        answers = [
            "1. Были грязь и плесень",
            "2. В камере было грязно",
            "3. В камере была плесень",
            "4. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 0:
            if self.asking_type.value == abstract_group.AskingType.BeforeCourt.value:
                return "В камере было грязно, что нарушает пункт 99 Правил № 996. Также это не соответствует " \
                       "правилу 17 Правил Нельсона Манделы, согласно которому все помещения, которыми задержанные " \
                       "пользуются регулярно, должны всегда содержаться в должном порядке и самой строгой чистоте. " \
                       "Также в камере была плесень, что свидетельствует о том, что в камере было слишком влажно, " \
                       "что не соответствует Правилу 13 Правил Нельсона Манделы. Наличие грязи и плесени способствует" \
                       " распространению заболеваний."

            elif self.asking_type.value == abstract_group.AskingType.AfterCourt.value:
                return "В камере было грязно, что не соответствует правилу 17 Правил Нельсона Манделы, согласно " \
                       "которому все помещения, которыми административно арестованные пользуются регулярно, должны " \
                       "всегда содержаться в должном порядке и самой строгой чистоте. Также в камере была плесень, " \
                       "что свидетельствует о том, что в камере было слишком влажно, что не соответствует правилу " \
                       "13 Правил Нельсона Манделы. Наличие грязи и плесени способствует распространению заболеваний."
            elif self.asking_type.value == abstract_group.AskingType.BothSideOfCourt.value:
                return "В камере было грязно, что нарушает пункт 99 Правил № 996. Также это не соответствует " \
                       "правилу 17 Правил Нельсона Манделы, согласно которому все помещения, " \
                       "которыми задержанные пользуются регулярно, должны всегда содержаться в " \
                       "должном порядке и самой строгой чистоте. Также в камере была плесень, что свидетельствует " \
                       "о том, что в камере было слишком влажно, что не соответствует правилу 13 Правил Нельсона " \
                       "Манделы. Наличие грязи и плесени способствует распространению заболеваний."
        if choice == 1:
            if self.asking_type.value == abstract_group.AskingType.BeforeCourt.value:
                return "В камере было грязно, что нарушает пункт 99 Правил № 996. Это также не соответствует " \
                       "правилу 17 Правил Нельсона Манделы, согласно которому все помещения, которыми задержанные " \
                       "пользуются регулярно, должны всегда содержаться в должном порядке и самой строгой чистоте. " \
                       "Наличие грязи способствует распространению заболеваний, появлению насекомых, грызунов."

            elif self.asking_type.value == abstract_group.AskingType.AfterCourt.value:
                return "В камере было грязно, что не соответствует правилу 17 Правил Нельсона Манделы, согласно " \
                       "которому все помещения, которыми административно арестованные пользуются регулярно, " \
                       "должны всегда содержаться в должном порядке и самой строгой чистоте. Наличие грязи " \
                       "способствует распространению заболеваний, появлению насекомых, грызунов."
            elif self.asking_type.value == abstract_group.AskingType.BothSideOfCourt.value:
                return "В камере было грязно, что нарушает пункт 99 Правил № 996. Это также не соответствует " \
                       "правилу 17 Правил Нельсона Манделы, согласно которому все помещения, которыми задержанные" \
                       " пользуются регулярно, должны всегда содержаться в должном порядке и самой строгой чистоте." \
                       " Наличие грязи способствует распространению заболеваний, появлению насекомых, грызунов."

        if choice == 2:
            return "В камере была плесень, что свидетельствует о том, что в камере было слишком влажно. Это не " \
                   "соответствует правилу 13 Правил Нельсона Манделы. Наличие плесени способствует распространению " \
                   "заболеваний."

        return ""

    def get_next_question_name(self):
        return CellQuestionsNames.KeyCellSecondTwo

    def get_previous_question_name(self):
        return CellQuestionsNames.KeyCellFirstOneOne


class CellQuestionSecondTwo(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Были ли в камере насекомые или грызуны?"
        answers = [
            "1. Да, насекомые",
            "2. Да, грызуны",
            "3. Да, и насекомые, и грызуны",
            "4. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        complain = ""
        if choice == 0:
            complain = "В камере были насекомые. "
        elif choice == 1:
            complain = "В камере были грызуны. "
        elif choice == 2:
            complain = "В камере были насекомые и грызуны. "
        if complain:
            if self.asking_type.value == abstract_group.AskingType.BeforeCourt.value:
                new_complain = "Согласно части первой пункта 100 Правил № 996 в целях предотвращения " \
                               "возникновения, распространения инфекционных заболеваний, чесотки, педикулеза, а " \
                               "также их локализации и ликвидации в установленном законодательством порядке " \
                               "проводятся дезинфекционные, дезинсекционные и дератизационные мероприятия. " \
                               "Очевидно, что в данном случае эти мероприятия не были проведены надлежащим образом."
            elif self.asking_type.value == abstract_group.AskingType.AfterCourt.value:
                new_complain = "Согласно части первой пункта 106 Правил № 313 в целях предотвращения возникновения, " \
                               "распространения инфекционных заболеваний, чесотки, педикулеза, а также их локализации " \
                               "и ликвидации в установленном законодательством порядке проводятся дезинфекционные, " \
                               "дезинсекционные и дератизационные мероприятия. Очевидно, что в данном случае эти " \
                               "мероприятия не были проведены надлежащим образом."
            else:  # asking_type == abstract_group.AskingType.Both:
                new_complain = "Согласно части первой пункта 100 Правил № 996, части " \
                               "первой пункта 106 Правил № 313 в целях предотвращения возникновения, распространения" \
                               " инфекционных заболеваний, чесотки, педикулеза, а также их локализации и ликвидации" \
                               " в установленном законодательством порядке проводятся дезинфекционные, " \
                               "дезинсекционные и дератизационные мероприятия. Очевидно, что в данном случае эти" \
                               " мероприятия не были проведены надлежащим образом."
            complain += new_complain
        return complain

    def get_next_question_name(self):
        return CellQuestionsNames.KeyCellSecondThree

    def get_previous_question_name(self):
        return CellQuestionsNames.KeyCellSecondOne


class CellQuestionSecondThree(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Содержали ли вас вместе с людьми с заразными болезнями  (ОРВИ, педикулез, пневмония, чесотка и т.д.)?"
        answers = [
            "1. Да",
            "2. Нет / не знаю",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 0:
            if self.asking_type.value == abstract_group.AskingType.BeforeCourt.value:
                return "Меня содержали в камере с людьми с заразными болезнями. Этим меня подвергли угрозе " \
                               "заражения. Это нарушает пункт 88 Правил № 996, согласно которому лицо с инфекционным " \
                               "заболеванием, педикулезом, чесоткой должно содержаться в отдельной камере."
            elif self.asking_type.value == abstract_group.AskingType.AfterCourt.value:
                return "Меня содержали в камере с людьми с заразными болезнями. Этим меня подвергли угрозе " \
                               "заражения. Это нарушает пункт 94 Правил № 313, согласно которому лицо с инфекционным " \
                               "заболеванием, педикулезом, чесоткой должно содержаться в отдельной камере."
            elif self.asking_type.value == abstract_group.AskingType.BothSideOfCourt.value:
                return "Меня содержали в камере с людьми с заразными болезнями. Этим меня подвергли угрозе заражения." \
                       " Это нарушает пункт 88 Правил № 996 и пункт 94 Правил № 313, согласно которым лицо с " \
                       "инфекционным заболеванием, педикулезом, чесоткой должно содержаться в отдельной камере."
        return ""

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 0:
            return CellQuestionsNames.KeyCellSecondThreeOne
        return cell_get_next_question_name()

    def get_previous_question_name(self):
        return CellQuestionsNames.KeyCellSecondTwo


class CellQuestionSecondThreeOne(questions_types.JustInfoQuestion):

    def __init__(self, asking_type):
        text = "_Я зафиксировала ваш ответ, но рекомендую самостоятельно уточнить в готовой жалобе, чем были " \
               "заражены ваши сокамерники и как это угрожало вашему здоровью_"
        super().__init__(text, asking_type)

    def get_next_question_name(self):
        return cell_get_next_question_name()

    def get_previous_question_name(self):
        return CellQuestionsNames.KeyCellSecondThree


class CellQuestionThirdOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "В камере было холодно?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 0:
            return "В камере было холодно. Такое плохое отопление нарушает правило 13 Правил Нельсона Манделы, " \
                   "согласно которому при оборудовании камеры должное внимание следует на климатические условия, " \
                   "в том числе на отопление."
        return ""

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 1:
            return CellQuestionsNames.KeyCellThirdSecond
        return cell_get_next_question_name()

    def get_previous_question_name(self):
        return CellQuestionsNames.KeyCellSecondOne


class CellQuestionThirdSecond(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "В камере было жарко?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 0:
            return "В камере было жарко. Такое неадекватное отопление нарушает правило 13 Правил Нельсона Манделы, " \
                   "согласно которому при оборудовании камеры должное внимание следует на климатические условия, " \
                   "в том числе на отопление."
        return ""

    def get_next_question_name(self):
        return cell_get_next_question_name()

    def get_previous_question_name(self):
        return CellQuestionsNames.KeyCellThirdOne


class CellQuestionFourthOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Было ли в камере достаточно свежего воздуха?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 0:
            return CellQuestionsNames.KeyCellFourthOneOne
        # TODO возможно здесь нужно как-то сделать так, чтобы правильно переходил,
        # когда пользователь не ответил на вопрос
        return CellQuestionsNames.KeyCellFourthOneTwo

    def get_previous_question_name(self):
        return CellQuestionsNames.KeyCellThirdOne


class CellQuestionFourthOneOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Был ли неприятный запах в камере?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 0:
            return "В камере был неприятный запах."
        return ""

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 0:
            return CellQuestionsNames.KeyCellFourthOneOneOne
        return cell_get_next_question_name()

    def get_previous_question_name(self):
        return CellQuestionsNames.CellQuestionFourthOne


class CellQuestionFourthOneOneOne(questions_types.JustInfoQuestion):

    def __init__(self, asking_type):
        text = "_Я зафиксировала ваш ответ, но рекомендую уточнить этот момент в готовой жалобе_"
        super().__init__(text, asking_type)

    def get_next_question_name(self):
        return cell_get_next_question_name()

    def get_previous_question_name(self):
        return CellQuestionsNames.KeyCellFourthOneOne


class CellQuestionFourthOneTwo(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Был ли неприятный запах в камере?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 0:
            return "В камере не хватало свежего воздуха, было душно, стоял неприятный запах. Это значит, что через " \
                   "окна не поступало достаточно свежего воздуха, а вентиляционная система не справлялась со своими " \
                   "функциями. Такие условия содержания нарушают пункт «а» правила 14 Правил Нельсона Манделы."
        elif choice == 1:
            return "В камере не хватало свежего воздуха, и было душно. В камере не хватало свежего воздуха, " \
                   "было душно, стоял неприятный запах. Это значит, что через окна не поступало достаточно свежего " \
                   "воздуха, а вентиляционная система не справлялась со своими функциями. Такие условия содержания " \
                   "нарушают пункт «а» правила 14 Правил Нельсона Манделы."
        return ""

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 0:
            return CellQuestionsNames.KeyCellFourthOneTwoOne
        return cell_get_next_question_name()

    def get_previous_question_name(self):
        return CellQuestionsNames.CellQuestionFourthOne


class CellQuestionFourthOneTwoOne(questions_types.JustInfoQuestion):

    def __init__(self, asking_type):
        text = "_Я зафиксировала ваш ответ, но рекомендую уточнить этот момент в готовой жалобе_"
        super().__init__(text, asking_type)

    def get_next_question_name(self):
        return cell_get_next_question_name()

    def get_previous_question_name(self):
        return CellQuestionsNames.KeyCellFourthOneTwo


class CellQuestionFifthOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Естественного освещения было достаточно, чтобы комфортно читать и писать?"
        answers = [
            "1. Да",
            "2. Нет",
            "3. Естественное освещение отсутствовало",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 1:
            return "В камере не хватало естественного света. Из-за этого у меня не было возможности комфортно читать " \
                   "и писать при дневном свете. Это нарушает пункт «a» правила 14 Правил Нельсона Манделы, " \
                   "согласно которому окна в камерах должны иметь достаточные размеры для того, чтобы там можно было " \
                   "читать и писать при дневном свете."
        elif choice == 2:
            return "В камере не было естественного света. Это грубейшим образом нарушает пункт «а» правила 14 Правил " \
                   "Нельсона Манделы, согласно которому в камерах должны быть окна, пропускающие достаточное " \
                   "количество дневного света. "
        return ""

    def get_next_question_name(self):
        return CellQuestionsNames.KeyCellFifthTwo

    def get_previous_question_name(self):
        return CellQuestionsNames.CellQuestionFourthOne


class CellQuestionFifthTwo(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Искусственного освещения было достаточно, чтобы комфортно читать и писать?"
        answers = [
            "1. Да",
            "2. Нет",
            "3. Естественное освещение отсутствовало",
            "4. Днём искусственное освещение было слишком ярким",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 1:
            return 'Искусственное освещение в камере было слишком тусклым. Из-за этого у меня не было ' \
                   'возможности читать и писать комфортно, без опасности для зрения. Это нарушает пункт «b» ' \
                   'правила 14 Правил Нельсона Манделы, согласно которому искусственное освещение в камерах ' \
                   'должно быть достаточным для того, чтобы там можно было читать или писать без опасности д' \
                   'ля зрения.'
        elif choice == 2:
            return "В камере отсутствовало искусственное освещение. Это грубейшим образом нарушает пункт «b» " \
                   "правила 14 Правил Нельсона Манделы, согласно которому в камерах должно быть искусственное " \
                   "освещение, достаточное для того, чтобы там можно было читать или писать без опасности для зрения."
        elif choice == 3:
            return "Искусственное освещение в камере было слишком ярким. Из-за этого у меня не было возможности " \
                   "находиться в камере без опасности для зрения. Это грубейшим образом нарушает пункт «b» правила " \
                   "14 Правил Нельсона Манделы, согласно которому искусственное освещение в камерах должно быть " \
                   "достаточным для того, чтобы находиться там без опасности для зрения."
        return ""

    def get_next_question_name(self):
        return CellQuestionsNames.KeyCellFifthThree

    def get_previous_question_name(self):
        return CellQuestionsNames.KeyCellFifthOne


class CellQuestionFifthThree(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Мешал ли свет в камере спать ночью?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 0:
            return 'По ночам в камере горел яркий свет. Это мешало мне спать и, соответственно, не позволяло ' \
                   'восстанавливать силы.'

        return ""

    def get_next_question_name(self):
        return cell_get_next_question_name()

    def get_previous_question_name(self):
        return CellQuestionsNames.KeyCellFifthTwo


class CellQuestionSixthOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Была ли камера оборудована туалетом?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 1:
            if self.asking_type.value == abstract_group.AskingType.BeforeCourt.value:
                return "Камера не была оборудована туалетом. Это нарушает пункт 47 Правил № 996."
            elif self.asking_type.value == abstract_group.AskingType.AfterCourt.value:
                return "Камера не была оборудована туалетом. Это нарушает пункт 37 Правил № 313."
            elif self.asking_type.value == abstract_group.AskingType.BothSideOfCourt.value:
                return "Камера не была оборудована туалетом. Это нарушает пункт 47 Правил № 996 " \
                       "и пункт 37 Правил № 313."

        return ""

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 0:
            return CellQuestionsNames.KeyCellSixthOneOne
        return cell_get_next_question_name()

    def get_previous_question_name(self):
        return CellQuestionsNames.KeyCellFifthOne


class CellQuestionSixthOneOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Он работал?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 1:
            return "Туалет в камере был в нерабочем состоянии. Следовательно, им невозможно было пользоваться. " \
                   "Отсутствие возможности свободно пользоваться туалетом является бесчеловечным обращением."
        return ""

    def get_next_question_name(self):
        return CellQuestionsNames.KeyCellSixthTwo

    def get_previous_question_name(self):
        return CellQuestionsNames.KeyCellSixthOne


class CellQuestionSixthTwo(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Могли ли другие сокамерники видеть вас во время посещения туалета?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 0:
            return "В камере не было обеспечено уединение при использовании туалета. Из-за этого каждое использование " \
                   "туалета провоцировало ощущение дискомфорта. Такое обращение нарушает правило 15 Правил Нельсона " \
                   "Манделы, согласно которому у арестованных должна быть возможность удовлетворять свои естественные " \
                   "потребности в условия пристойности."
        return ""

    def get_next_question_name(self):
        return CellQuestionsNames.KeyCellSixthThree

    def get_previous_question_name(self):
        return CellQuestionsNames.KeyCellSixthOne


class CellQuestionSixthThree(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Ваша кровать располагалась слишком близко к туалету?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 0:
            return CellQuestionsNames.KeyCellSixthThreeOne
        return cell_get_next_question_name()

    def get_previous_question_name(self):
        return CellQuestionsNames.KeyCellSixthTwo


class CellQuestionSixthThreeOne(questions_types.GetNumberQuestion):

    def __init__(self, asking_type):
        text = "Сколько примерно сантиметров было до туалета?"
        super().__init__(text, asking_type)

    def process(self):
        choice = self.get_choice()
        return "Моя кровать находилась слишком близко к туалету: в {} см. Такие условия были антисанитарными " \
               "и порождали постоянное ощущение брезгливости.".format(choice)

    def get_next_question_name(self):
        return cell_get_next_question_name()

    def get_previous_question_name(self):
        return CellQuestionsNames.KeyCellSixthThree


class CellQuestionSevenOneOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Укажите, чего не было в камере"
        answers = [
            "1. Отдельного спального места",
            "2. Стола",
            "3. Скамеек с посадочными местами по количеству мест в камере",
            "4. Тумбочки для туалетных принадлежностей",
            "5. Радио",
            "6. Все перечисленное было",
        ]
        super().__init__(text, answers, asking_type, questions_types.QuestionAnswerType.ANSWER_QUERY_MANY)

    def process(self):
        choice = self.get_choice()
        answer = ""
        if 0 in choice:
            answer += "В камере у меня не было отдельного спального места, что нарушает пункт 38 Правил № 313. "
        if 1 in choice:
            answer += "В камере не было стола. "
        if 2 in choice:
            answer += "Камера не была оборудована скамейками с достаточным количеством посадочных мест. "
        if 3 in choice:
            answer += "У меня не было возможности пользоваться тумбочкой для туалетных принадлежностей. "
        if 4 in choice:
            answer += "В камере отсутствовал радиоприемник. "
        if answer:
            answer += "Это нарушает пункт 37 Правил № 313."
        return answer

    def get_next_question_name(self):
        return CellQuestionsNames.KeyCellSevenOneTwo

    def get_previous_question_name(self):
        return CellQuestionsNames.KeyCellSixthOne


class CellQuestionSevenOneTwo(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Выдавали ли вам хотя бы один комплект шашек, шахмат или домино на камеру?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 1:
            return "Для общего пользования в камеру не был выдан комплект настольных игр, что нарушает абзац " \
                   "третий пункта 36 Правил № 313."

    def get_next_question_name(self):
        return cell_get_next_question_name()

    def get_previous_question_name(self):
        return CellQuestionsNames.KeyCellSevenOneOne


class CellQuestionSevenTwoOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Укажите, чего не было в камере"
        answers = [
            "1. Отдельного спального места",
            "2. Стола",
            "3. Скамеек с посадочными местами по количеству мест в камере",
            "4. Тумбочки для туалетных принадлежностей",
            "5. Урны для мусора",
            "6. Радио или репродуктора",
            "7. Все перечисленное было",
        ]
        super().__init__(text, answers, asking_type, questions_types.QuestionAnswerType.ANSWER_QUERY_MANY)

    def process(self):
        choice = self.get_choice()
        answer = ""
        if 0 in choice:
            answer += "У меня не было отдельного спального места, что нарушает пункт 48 Правил № 996. "
        if 1 in choice:
            answer += "В камере не было стола. "
        if 2 in choice:
            answer += "Камера не была оборудована скамейками с достаточным количеством посадочных мест. "
        if 3 in choice:
            answer += "Мне не было предоставлено место для хранения средств личной гигиены. "
        if 4 in choice:
            answer += "В камере не было урны для мусора. "
        if 5 in choice:
            answer += "Камера не была оборудована радиоприемником или абонентским громкоговорителем. "
        if answer:
            answer += "Это нарушает пункт 47 Правил № 996."
        return answer

    def get_next_question_name(self):
        return CellQuestionsNames.KeyCellSevenTwoTwo

    def get_previous_question_name(self):
        return CellQuestionsNames.KeyCellSixthOne


class CellQuestionSevenTwoTwo(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Выдавали ли вам хотя бы один комплект шашек, шахмат или домино на камеру?"
        answers = [
            "1. Да",
            "2. Нет",
            "3. Не просили",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 1:
            return "Для общего пользования в камеру не был выдан комплект настольных игр, что не соответствует " \
                   "части третьей пункта 51 Правил № 996."

    def get_next_question_name(self):
        return cell_get_next_question_name()

    def get_previous_question_name(self):
        return CellQuestionsNames.KeyCellSevenTwoOne


class CellQuestionSevenThreeOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Укажите, чего не было в камере"
        answers = [
            "1. Отдельного спального места",
            "2. Стола",
            "3. Скамеек с посадочными местами по количеству мест в камере",
            "4. Тумбочки для туалетных принадлежностей",
            "5. Радио или репродуктора",
            "6. Все перечисленное было",
        ]
        super().__init__(text, answers, asking_type, questions_types.QuestionAnswerType.ANSWER_QUERY_MANY)

    def process(self):
        choice = self.get_choice()
        answer = ""
        if 5 in choice:
            return answer

        if 0 in choice:
            answer += "У меня не было отдельного спального места, что нарушает пункт 48 Правил № 996. "
        if 1 in choice:
            answer += "В камере не было стола. "
        if 2 in choice:
            answer += "Камера не была оборудована скамейками с достаточным количеством посадочных мест. "
        if 3 in choice:
            answer += "Мне не было предоставлено место для хранения средств личной гигиены. "
        if 4 in choice:
            answer += "Камера не была оборудована радиоприемником или абонентским громкоговорителем. "
        if answer:
            answer += "Это нарушает пункт 37 Правил № 313 и пункт 47 Правил № 996."
        return answer

    def get_next_question_name(self):
        return CellQuestionsNames.KeyCellSevenThreeTwo

    def get_previous_question_name(self):
        return CellQuestionsNames.KeyCellSixthOne


class CellQuestionSevenThreeTwo(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Выдавали ли вам хотя бы один комплект шашек, шахмат или домино на камеру?"
        answers = [
            "1. Да",
            "2. Нет",
            "3. Не просили",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 1:
            return "Для общего пользования в камеру не был выдан комплект настольных игр, что не соответствует " \
                   "части третьей пункта 51 Правил № 996."

    def get_next_question_name(self):
        return cell_get_next_question_name()

    def get_previous_question_name(self):
        return CellQuestionsNames.KeyCellSevenThreeOne
