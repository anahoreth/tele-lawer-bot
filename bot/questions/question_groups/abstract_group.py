
import config
import enum

if config.IS_LOCAL:
    import question_groups.questions_types as questions_types
else:
    import questions.question_groups.questions_types as questions_types


class AskingType(enum.Enum):
    BeforeCourt = 1
    AfterCourt = 2
    BothSideOfCourt = 3


class QuestionGroup(object):

    def __init__(self, asking_type):
        self.questions_array = {}
        self.asking_type = asking_type
        self.question_name = None

    def get_short_name(self):
        return ""

    def get_questions_count(self):
        return 0

    def get_question_number(self):
        if self.question_name is None:
            return 0
        return self.question_name.value

    def process(self):
        complains_list = []
        for question in self.questions_array.values():
            complain = question.process()
            if complain != "":
                complains_list.append(complain)
        return complains_list

    def backward(self):
        question = self.questions_array.get(self.question_name)
        if question is None:
            return False
        prev_question_name = question.get_previous_question_name()
        # print(prev_question_name, self.question_name)
        if prev_question_name.value != questions_types.ZERO_QUESTION:
            self.question_name = prev_question_name
        return prev_question_name.value != questions_types.ZERO_QUESTION

    def forward(self):
        question = self.questions_array.get(self.question_name)
        if question is None:
            return False

        next_question_name = question.get_next_question_name()
        self.question_name = next_question_name
        return self.question_name.value != questions_types.LAST_QUESTION

    def make_new_question(self):
        return None

    def get_question(self):
        question = self.questions_array.get(self.question_name)
        if question is not None:
            return question

        new_question = self.make_new_question()
        if new_question is None:
            return None
        self.questions_array[self.question_name] = new_question
        return new_question

    def get_choice(self, question_name):
        question = self.questions_array.get(question_name)
        if question is None:
            return None
        return question.get_choice()
