
import config
import enum


if config.IS_LOCAL:
    import question_groups.abstract_group as abstract_group
    import question_groups.questions_types as questions_types
else:
    import questions.question_groups.abstract_group as abstract_group
    import questions.question_groups.questions_types as questions_types


class VirusQuestionsNames(enum.Enum):
    KeyZero = questions_types.ZERO_QUESTION
    KeyFirst = 1  # Предоставлялись ли средства индивидуальной защиты (маски, перчатки, и т.д.)?
    KeySecond = 2  # Была ли возможность находиться на расстоянии 1,5 метра от людей, содержащихся в вашей камере?
    KeyThree = 3  # Предоставлялись ли средства для дезинфекции (антисептики, дезинфицирующие средства для уборки)?
    KeyFour = 4  # Использовали ли сотрудники средства индивидуальной защиты?
    KeyLast = questions_types.LAST_QUESTION
    KeyCount = 4


class VirusQuestionGroup(abstract_group.QuestionGroup):

    def __init__(self, asking_type, place_name):
        super().__init__(asking_type)
        self.place_name = place_name
        self.question_name = VirusQuestionsNames.KeyFirst

    def get_short_name(self):
        return "Коронавирус"

    def get_questions_count(self):
        return VirusQuestionsNames.KeyCount.value

    def make_new_question(self):
        if self.question_name == VirusQuestionsNames.KeyFirst:
            return VirusQuestionFirst(self.asking_type, self.place_name)
        if self.question_name == VirusQuestionsNames.KeySecond:
            return VirusQuestionSecond(self.asking_type, self.place_name)
        if self.question_name == VirusQuestionsNames.KeyThree:
            return VirusQuestionThird(self.asking_type, self.place_name)
        if self.question_name == VirusQuestionsNames.KeyFour:
            return VirusQuestionFour(self.asking_type, self.place_name)
        return None

    def process(self):
        complains_list = []
        for question in self.questions_array.values():
            complain = question.process()
            if complain != "":
                complains_list.append(complain)

        answer_second = self.get_choice(VirusQuestionsNames.KeySecond)
        added_text = ""
        if answer_second == 0:
            added_text = "При этом люди могут бессимптомно переносить и распространять данный вирус. "

        answer_four = self.get_choice(VirusQuestionsNames.KeyFour)
        answer_text = ""
        if answer_four == 1:
            answer_text = "Сотрудники {} не всегда использовали средства индивидуальной защиты. {}" \
                   "Я не могу знать наверняка, что меня не заразили сотрудники {}.".format(
                    self.place_name, added_text, self.place_name)
        elif answer_four == 2:
            answer_text = "Сотрудники {} не использовали средства индивидуальной защиты. {}Я не могу знать " \
                           "наверняка, что" \
                          " меня не заразили сотрудники {}.".format(self.place_name, added_text, self.place_name)

        if answer_text:
            complains_list.append(answer_text)
        if len(complains_list) > 0:
            virus_before_complain_text = ["Во время содержания в {} меня подвергали опасности заразиться " \
                                          "COVID-19. Отсутствовала профилактика коронавируса.".format(self.place_name)]

            complains_list = virus_before_complain_text + complains_list
            complains_list += virus_after_complains_text
        return complains_list


virus_after_complains_text = [
    "Согласно данным Всемирной организации здравоохранения, несмотря на активное принятие мер по сдерживанию "
    "распространения COVID-19, кумулятивное число заболеваний и летальных исходов не снижает темпа роста. "
    "Кроме того, согласно последним научным исследованиям COVID-19 нередко протекает в лёгкой или средней форме, "
    "но в некоторых случаях вызывает сильные воспалительные процессы (цитокиновый шторм), которые могут привести "
    "к смертельной пневмонии и острому респираторному дистресс-синдрому. COVID-19 также вызывает иные осложнения, "
    "в числе которых острая сердечная недостаточность, острая почечная недостаточность, кардиомиопатии, "
    "септический шок и другие.",
    "Перечисленные выше обстоятельства нарушают мое конституционное право на охрану здоровья, а также "
    "требования части 4 статьи 2.4 ПИКоАП, в соответствии с которыми содержание лица, задержанного за "
    "административное правонарушение или административно арестованного, должно осуществляться в условиях, "
    "исключающих угрозу для его жизни и здоровья."]


class VirusQuestionFirst(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Предоставлялись ли средства индивидуальной защиты (маски, перчатки, и т.д.)?"
        answers = [
            '1. Да, всегда и в достаточном количестве',
            "2. Да, но недостаточно",
            "3. Не всегда",
            "4. Нет"
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        answer_number = self.get_choice()
        if answer_number == 1:
            return "Средства индивидуальной защиты предоставлялись в недостаточном количестве."
        elif answer_number == 2:
            return "Средства индивидуальной защиты предоставлялись не всегда"
        elif answer_number == 3:
            return "Мне не предоставлялись никакие средства индивидуальной защиты: ни маски" \
                       ", ни перчатки, ни какие-либо другие средства."
        return ""

    def get_next_question_name(self):
        return VirusQuestionsNames.KeySecond

    def get_previous_question_name(self):
        return VirusQuestionsNames.KeyZero


class VirusQuestionSecond(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Была ли возможность находиться на расстоянии 1,5 метра от людей, содержащихся в вашей камере?"
        answers = [
            "1. Да",
            "2. Не всегда",
            "3. Нет"
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        answer_number = self.get_choice()
        if answer_number == 1:
            return "У меня не всегда была возможность выдерживать физическую дистанцию в 1,5 метра от " \
                       "других людей, необходимую для уменьшения риска заражения. Вместе с тем, люди могут " \
                       "бессимптомно переносить и распространять данный вирус. Я не могу знать наверняка, " \
                       "что меня не заразили другие люди, которых содержали в одной камере со мной."

        if answer_number == 2:
            return "У меня не было возможности выдерживать физическую дистанцию в 1,5 метра от других " \
                       "людей. При этом люди могут бессимптомно переносить и распространять данный вирус. " \
                       "Я не могу знать наверняка, что меня не заразили другие люди, которых содержали в одной " \
                       "камере со мной."
        return ""

    def get_next_question_name(self):
        return VirusQuestionsNames.KeyThree

    def get_previous_question_name(self):
        return VirusQuestionsNames.KeyFirst


class VirusQuestionThird(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Предоставлялись ли средства для дезинфекции (антисептики, дезинфицирующие средства для уборки)?"
        answers = [
            "1. Да, всегда и в достаточном количестве",
            "2. Да, но недостаточно",
            "3. Не всегда",
            "4. Нет"
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        answer_number = self.get_choice()
        if answer_number == 1:
            return "Средства для дезинфекции (антисептики, " \
                       "дезинфицирующие средства для уборки) не предоставлялись в достаточном количестве."
        if answer_number == 2:
            return "Средства для дезинфекции (антисептики, дезинфицирующие средства для уборки) " \
                       "предоставлялись не всегда."
        if answer_number == 3:
            return "Средства для дезинфекции (антисептики, дезинфицирующие средства для уборки) " \
                       "не предоставлялись."
        return ""

    def get_next_question_name(self):
        return VirusQuestionsNames.KeyFour

    def get_previous_question_name(self):
        return VirusQuestionsNames.KeySecond


class VirusQuestionFour(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Использовали ли сотрудники средства индивидуальной защиты?"
        answers = [
            "1. Да",
            "2. Не всегда",
            "3. Нет"
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        return ""

    def get_next_question_name(self):
        return VirusQuestionsNames.KeyLast

    def get_previous_question_name(self):
        return VirusQuestionsNames.KeyThree
