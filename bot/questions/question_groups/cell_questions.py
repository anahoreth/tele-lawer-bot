
import config

if config.IS_LOCAL:
    import question_groups.questions_types as questions_types
    import question_groups.abstract_group as abstract_group
    import question_groups.cell9 as cell
else:
    import questions.question_groups.questions_types as questions_types
    import questions.question_groups.abstract_group as abstract_group
    import questions.question_groups.cell9 as cell


class CellQuestionFirstOneOne(questions_types.GetNumberQuestion):

    def __init__(self, asking_type):
        text = "Какая примерно была длина камеры (в метрах)?\n\n" \
               '(дробные числа вводите через точку, например "1.4")'
        super().__init__(text, asking_type)

    def get_next_question_name(self):
        return cell.CellQuestionsNames.KeyCellFirstOneTwo

    def get_previous_question_name(self):
        return cell.CellQuestionsNames.KeyFourth


class CellQuestionFirstOneTwo(questions_types.GetNumberQuestion):

    def __init__(self, asking_type):
        text = "Какая примерно была ширина камеры (в метрах)?\n\n" \
               '(дробные числа вводите через точку, например "8.8")'
        super().__init__(text, asking_type)

    def get_next_question_name(self):
        return cell.CellQuestionsNames.KeyCellFirstOneThree

    def get_previous_question_name(self):
        return cell.CellQuestionsNames.KeyCellFirstOneOne


class CellQuestionFirstOneThree(questions_types.GetNumberQuestion):

    def __init__(self, asking_type):
        text = "Сколько задержанных, включая вас, было в камере (назовите максимальное количество)?"
        super().__init__(text, asking_type)

    def get_next_question_name(self):
        return cell.cell_get_next_question_name()

    def get_previous_question_name(self):
        return cell.CellQuestionsNames.KeyCellFirstOneTwo


class CellQuestionSecondOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Была ли в камере грязь или плесень?"
        answers = [
            "1. Были грязь и плесень",
            "2. В камере было грязно",
            "3. В камере была плесень",
            "4. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 0:
            if self.asking_type.value == abstract_group.AskingType.BeforeCourt.value:
                return "В камере было грязно, что нарушает пункт 99 Правил № 996. Также это не соответствует " \
                       "правилу 17 Правил Нельсона Манделы, согласно которому все помещения, которыми задержанные " \
                       "пользуются регулярно, должны всегда содержаться в должном порядке и самой строгой чистоте. " \
                       "Также в камере была плесень, что свидетельствует о том, что в камере было слишком влажно, " \
                       "что не соответствует правилу 13 Правил Нельсона Манделы. Наличие грязи и плесени способствует" \
                       " распространению заболеваний."

            elif self.asking_type.value == abstract_group.AskingType.AfterCourt.value:
                return "В камере было грязно, что не соответствует правилу 17 Правил Нельсона Манделы, согласно " \
                       "которому все помещения, которыми административно арестованные пользуются регулярно, должны " \
                       "всегда содержаться в должном порядке и самой строгой чистоте. Также в камере была плесень, " \
                       "что свидетельствует о том, что в камере было слишком влажно, что не соответствует правилу " \
                       "13 Правил Нельсона Манделы. Наличие грязи и плесени способствует распространению заболеваний."
        if choice == 1:
            if self.asking_type.value == abstract_group.AskingType.BeforeCourt.value:
                return "В камере было грязно, что нарушает пункт 99 Правил № 996. Это также не соответствует " \
                       "правилу 17 Правил Нельсона Манделы, согласно которому все помещения, которыми задержанные " \
                       "пользуются регулярно, должны всегда содержаться в должном порядке и самой строгой чистоте. " \
                       "Наличие грязи способствует распространению заболеваний, появлению насекомых, грызунов."

            elif self.asking_type.value == abstract_group.AskingType.AfterCourt.value:
                return "В камере было грязно, что не соответствует правилу 17 Правил Нельсона Манделы, согласно " \
                       "которому все помещения, которыми административно арестованные пользуются регулярно, " \
                       "должны всегда содержаться в должном порядке и самой строгой чистоте. Наличие грязи " \
                       "способствует распространению заболеваний, появлению насекомых, грызунов."

        if choice == 2:
            return "В камере была плесень, что свидетельствует о том, что в камере было слишком влажно. Это не " \
                   "соответствует правилу 13 Правил Нельсона Манделы. Наличие плесени способствует распространению " \
                   "заболеваний."

        return ""

    def get_next_question_name(self):
        return cell.CellQuestionsNames.KeyCellSecondTwo

    def get_previous_question_name(self):
        return cell.CellQuestionsNames.KeyCellFirstOneOne


class CellQuestionSecondTwo(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Были ли в камере насекомые или грызуны?"
        answers = [
            "1. Да, насекомые",
            "2. Да, грызуны",
            "3. Да, и насекомые, и грызуны",
            "4. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        complain = ""
        if choice == 0:
            complain = "В камере были насекомые. "
        elif choice == 1:
            complain = "В камере были грызуны. "
        elif choice == 2:
            complain = "В камере были насекомые и грызуны. "
        if complain:
            if self.asking_type.value == abstract_group.AskingType.BeforeCourt.value:
                new_complain = "Согласно части первой пункта 100 Правил № 996 в целях предотвращения " \
                               "возникновения, распространения инфекционных заболеваний, чесотки, педикулеза, а " \
                               "также их локализации и ликвидации в установленном законодательством порядке " \
                               "проводятся дезинфекционные, дезинсекционные и дератизационные мероприятия. " \
                               "Очевидно, что в данном случае эти мероприятия не были проведены надлежащим образом."
            else:  # asking_type == abstract_group.AskingType.AfterCourt:
                new_complain = "Согласно части первой пункта 106 Правил № 313 в целях предотвращения возникновения, " \
                               "распространения инфекционных заболеваний, чесотки, педикулеза, а также их локализации " \
                               "и ликвидации в установленном законодательством порядке проводятся дезинфекционные, " \
                               "дезинсекционные и дератизационные мероприятия. Очевидно, что в данном случае эти " \
                               "мероприятия не были проведены надлежащим образом."
            complain += new_complain
        return complain

    def get_next_question_name(self):
        return cell.CellQuestionsNames.KeyCellSecondThree

    def get_previous_question_name(self):
        return cell.CellQuestionsNames.KeyCellSecondOne


class CellQuestionSecondThree(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Содержали ли вас вместе с людьми с заразными болезнями  (ОРВИ, педикулез, пневмония, чесотка и т.д.)?"
        answers = [
            "1. Да",
            "2. Нет / не знаю",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 0:
            if self.asking_type.value == abstract_group.AskingType.BeforeCourt.value:
                return "Меня содержали в камере с людьми с заразными болезнями. Этим меня подвергли угрозе " \
                               "заражения. Это нарушает пункт 88 Правил № 996, согласно которому лицо с инфекционным " \
                               "заболеванием, педикулезом, чесоткой должно содержаться в отдельной камере."
            elif self.asking_type.value == abstract_group.AskingType.AfterCourt.value:
                return "Меня содержали в камере с людьми с заразными болезнями. Этим меня подвергли угрозе " \
                               "заражения. Это нарушает пункт 94 Правил № 313, согласно которому лицо с инфекционным " \
                               "заболеванием, педикулезом, чесоткой должно содержаться в отдельной камере."
        return ""

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 0:
            return cell.CellQuestionsNames.KeyCellSecondThreeOne
        return cell.cell_get_next_question_name()

    def get_previous_question_name(self):
        return cell.CellQuestionsNames.KeyCellSecondTwo


class CellQuestionSecondThreeOne(questions_types.JustInfoQuestion):

    def __init__(self, asking_type):
        text = "_Я зафиксировала ваш ответ, но рекомендую самостоятельно уточнить в готовой жалобе, чем были " \
               "заражены ваши сокамерники и как это угрожало вашему здоровью_"
        super().__init__(text, asking_type)

    def get_next_question_name(self):
        return cell.cell_get_next_question_name()

    def get_previous_question_name(self):
        return cell.CellQuestionsNames.KeyCellSecondThree


class CellQuestionThirdOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "В камере было холодно?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 0:
            return "В камере было холодно. Такое плохое отопление нарушает правило 13 Правил Нельсона Манделы, " \
                   "согласно которому при оборудовании камеры должное внимание следует на климатические условия, " \
                   "в том числе на отопление."
        return ""

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 1:
            return cell.CellQuestionsNames.KeyCellThirdSecond
        return cell.cell_get_next_question_name()

    def get_previous_question_name(self):
        return cell.CellQuestionsNames.KeyCellSecondOne


class CellQuestionThirdSecond(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "В камере было жарко?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 0:
            return "В камере было жарко. Такое неадекватное отопление нарушает правило 13 Правил Нельсона Манделы, " \
                   "согласно которому при оборудовании камеры должное внимание следует на климатические условия, " \
                   "в том числе на отопление."
        return ""

    def get_next_question_name(self):
        return cell.cell_get_next_question_name()

    def get_previous_question_name(self):
        return cell.CellQuestionsNames.KeyCellThirdOne


class CellQuestionFourthOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Было ли в камере достаточно свежего воздуха?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 0:
            return cell.CellQuestionsNames.KeyCellFourthOneOne
        # TODO возможно здесь нужно как-то сделать так, чтобы правильно переходил,
        # когда пользователь не ответил на вопрос
        return cell.CellQuestionsNames.KeyCellFourthOneTwo

    def get_previous_question_name(self):
        return cell.CellQuestionsNames.KeyCellThirdOne


class CellQuestionFourthOneOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Был ли неприятный запах в камере?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 0:
            return "В камере был неприятный запах."
        return ""

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 0:
            return cell.CellQuestionsNames.KeyCellFourthOneOneOne
        return cell.cell_get_next_question_name()

    def get_previous_question_name(self):
        return cell.CellQuestionsNames.CellQuestionFourthOne


class CellQuestionFourthOneOneOne(questions_types.JustInfoQuestion):

    def __init__(self, asking_type):
        text = "_Я зафиксировала ваш ответ, но рекомендую уточнить этот момент в готовой жалобе_"
        super().__init__(text, asking_type)

    def get_next_question_name(self):
        return cell.cell_get_next_question_name()

    def get_previous_question_name(self):
        return cell.CellQuestionsNames.KeyCellFourthOneOne


class CellQuestionFourthOneTwo(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Был ли неприятный запах в камере?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 0:
            return "В камере не хватало свежего воздуха, было душно, стоял неприятный запах. Это значит, что через " \
                   "окна не поступало достаточно свежего воздуха, а вентиляционная система не справлялась со своими " \
                   "функциями. Такие условия содержания нарушают пункт «а» правила 14 Правил Нельсона Манделы."
        elif choice == 1:
            return "В камере не хватало свежего воздуха, и было душно. В камере не хватало свежего воздуха, " \
                   "было душно, стоял неприятный запах. Это значит, что через окна не поступало достаточно свежего " \
                   "воздуха, а вентиляционная система не справлялась со своими функциями. Такие условия содержания " \
                   "нарушают пункт «а» правила 14 Правил Нельсона Манделы."
        return ""

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 0:
            return cell.CellQuestionsNames.KeyCellFourthOneTwoOne
        return cell.cell_get_next_question_name()

    def get_previous_question_name(self):
        return cell.CellQuestionsNames.CellQuestionFourthOne


class CellQuestionFourthOneTwoOne(questions_types.JustInfoQuestion):

    def __init__(self, asking_type):
        text = "_Я зафиксировала ваш ответ, но рекомендую уточнить этот момент в готовой жалобе_"
        super().__init__(text, asking_type)

    def get_next_question_name(self):
        return cell.cell_get_next_question_name()

    def get_previous_question_name(self):
        return cell.CellQuestionsNames.KeyCellFourthOneTwo


class CellQuestionFifthOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Естественного освещения было достаточно, чтобы комфортно читать и писать?"
        answers = [
            "1. Да",
            "2. Нет",
            "3. Естественное освещение отсутствовало",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 1:
            return "В камере не хватало естественного света. Из-за этого у меня не было возможности комфортно читать " \
                   "и писать при дневном свете. Это нарушает пункт «a» правила 14 Правил Нельсона Манделы, " \
                   "согласно которому окна в камерах должны иметь достаточные размеры для того, чтобы там можно было " \
                   "читать и писать при дневном свете."
        elif choice == 2:
            return "В камере не было естественного света. Это грубейшим образом нарушает пункт «а» правила 14 Правил " \
                   "Нельсона Манделы, согласно которому в камерах должны быть окна, пропускающие достаточное " \
                   "количество дневного света. "
        return ""

    def get_next_question_name(self):
        return cell.CellQuestionsNames.KeyCellFifthTwo

    def get_previous_question_name(self):
        return cell.CellQuestionsNames.CellQuestionFourthOne


class CellQuestionFifthTwo(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Искусственного освещения было достаточно, чтобы комфортно читать и писать?"
        answers = [
            "1. Да",
            "2. Нет",
            "3. Искусственное освещение отсутствовало",
            "4. Днём искусственное освещение было слишком ярким",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 1:
            return 'Искусственное освещение в камере было слишком тусклым. Из-за этого у меня не было ' \
                   'возможности читать и писать комфортно, без опасности для зрения. Это нарушает пункт «b» ' \
                   'правила 14 Правил Нельсона Манделы, согласно которому искусственное освещение в камерах ' \
                   'должно быть достаточным для того, чтобы там можно было читать или писать без опасности д' \
                   'ля зрения.'
        elif choice == 2:
            return "В камере отсутствовало искусственное освещение. Это грубейшим образом нарушает пункт «b» " \
                   "правила 14 Правил Нельсона Манделы, согласно которому в камерах должно быть искусственное " \
                   "освещение, достаточное для того, чтобы там можно было читать или писать без опасности для зрения."
        elif choice == 3:
            return "Искусственное освещение в камере было слишком ярким. Из-за этого у меня не было возможности " \
                   "находиться в камере без опасности для зрения. Это грубейшим образом нарушает пункт «b» правила " \
                   "14 Правил Нельсона Манделы, согласно которому искусственное освещение в камерах должно быть " \
                   "достаточным для того, чтобы находиться там без опасности для зрения."
        return ""

    def get_next_question_name(self):
        return cell.CellQuestionsNames.KeyCellFifthThree

    def get_previous_question_name(self):
        return cell.CellQuestionsNames.KeyCellFifthOne


class CellQuestionFifthThree(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Мешал ли свет в камере спать ночью?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 0:
            return 'По ночам в камере горел яркий свет. Это мешало мне спать и, соответственно, не позволяло ' \
                   'восстанавливать силы.'

        return ""

    def get_next_question_name(self):
        return cell.cell_get_next_question_name()

    def get_previous_question_name(self):
        return cell.CellQuestionsNames.KeyCellFifthTwo


class CellQuestionSixthOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Была ли камера оборудована туалетом?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 1:
            if self.asking_type.value == abstract_group.AskingType.BeforeCourt.value:
                return "Камера не была оборудована туалетом. Это нарушает пункт 47 Правил № 996."
            elif self.asking_type.value == abstract_group.AskingType.AfterCourt.value:
                return "Камера не была оборудована туалетом. Это нарушает пункт 37 Правил № 313."

        return ""

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 0:
            return cell.CellQuestionsNames.KeyCellSixthOneOne
        return cell.CellQuestionsNames.CellQuestionSixthTwo

    def get_previous_question_name(self):
        return cell.CellQuestionsNames.KeyCellFifthOne


class CellQuestionSixthOneOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Он работал?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 1:
            return "Туалет в камере был в нерабочем состоянии. Следовательно, им невозможно было пользоваться. " \
                   "Отсутствие возможности свободно пользоваться туалетом является бесчеловечным обращением."
        return ""

    def get_next_question_name(self):
        return cell.CellQuestionsNames.CellQuestionSixthTwo

    def get_previous_question_name(self):
        return cell.CellQuestionsNames.KeyCellSixthOne


class CellQuestionSixthTwo(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Могли ли другие сокамерники видеть вас во время посещения туалета?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 1:
            return "В камере не было обеспечено уединение при использовании туалета. Из-за этого каждое использование " \
                   "туалета провоцировало ощущение дискомфорта. Такое обращение нарушает правило 15 Правил Нельсона " \
                   "Манделы, согласно которому у арестованных должна быть возможность удовлетворять свои естественные " \
                   "потребности в условия пристойности. "
        return ""

    def get_next_question_name(self):
        return cell.CellQuestionsNames.KeyCellSixthThree

    def get_previous_question_name(self):
        return cell.CellQuestionsNames.KeyCellSixthOne


class CellQuestionSixthThree(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Ваша кровать располагалась слишком близко к туалету?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 0:
            return cell.CellQuestionsNames.KeyCellSixthThreeOne
        return cell.cell_get_next_question_name()

    def get_previous_question_name(self):
        return cell.CellQuestionsNames.KeyCellSixthTwo


class CellQuestionSixthThreeOne(questions_types.GetNumberQuestion):

    def __init__(self, asking_type):
        text = "Сколько примерно сантиметров было до туалета?"
        super().__init__(text, asking_type)

    def process(self):
        choice = self.get_choice()
        return "Моя кровать находилась слишком близко к туалету: в {} см. Такие условия были антисанитарными " \
               "и порождали постоянное ощущение брезгливости.".format(choice)

    def get_next_question_name(self):
        return cell.cell_get_next_question_name()

    def get_previous_question_name(self):
        return cell.CellQuestionsNames.KeyCellSixthThree


class CellQuestionSevenOneOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Укажите, чего не было в камере"
        answers = [
            "1. Отдельного спального места",
            "2. Стола",
            "3. Скамеек с посадочными местами по количеству мест в камере",
            "4. Тумбочки для туалетных принадлежностей",
            "5. Радио",
            "6. Все перечисленное было",
        ]
        super().__init__(text, answers, asking_type, questions_types.QuestionAnswerType.ANSWER_QUERY_MANY)

    def process(self):
        choice = self.get_choice()
        answer = ""
        if 0 not in choice:
            answer += "В камере у меня не было отдельного спального места, что нарушает пункт 38 Правил № 313. "
        if 1 not in choice:
            answer += "В камере не было стола. "
        if 2 not in choice:
            answer += "Камера не была оборудована скамейками с достаточным количеством посадочных мест. "
        if 3 not in choice:
            answer += "У меня не было возможности пользоваться тумбочкой для туалетных принадлежностей. "
        if 4 not in choice:
            answer += "В камере отсутствовал радиоприемник. "
        if answer:
            answer += "Это нарушает пункт 37 Правил № 313."
        return answer

    def get_next_question_name(self):
        return cell.CellQuestionsNames.KeyCellSevenOneTwo

    def get_previous_question_name(self):
        return cell.CellQuestionsNames.KeyCellSixthOne


class CellQuestionSevenOneTwo(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Выдавали ли вам хотя бы один комплект шашек, шахмат или домино на камеру?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 1:
            return "Для общего пользования в камеру не был выдан комплект настольных игр, что нарушает абзац " \
                   "третий пункта 36 Правил № 313."

    def get_next_question_name(self):
        return cell.cell_get_next_question_name()

    def get_previous_question_name(self):
        return cell.CellQuestionsNames.KeyCellSevenOneOne


class CellQuestionSevenTwoOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Укажите, чего не было в камере"
        answers = [
            "1. Отдельного спального места",
            "2. Стола",
            "3. Скамеек с посадочными местами по количеству мест в камере",
            "4. Тумбочки для туалетных принадлежностей",
            "5. Урны для мусора",
            "6. Радио или репродуктора",
            "7. Все перечисленное было",
        ]
        super().__init__(text, answers, asking_type, questions_types.QuestionAnswerType.ANSWER_QUERY_MANY)

    def process(self):
        choice = self.get_choice()
        answer = ""
        if 0 not in choice:
            answer += "У меня не было отдельного спального места, что нарушает пункт 48 Правил № 996. "
        if 1 not in choice:
            answer += "В камере не было стола. "
        if 2 not in choice:
            answer += "Камера не была оборудована скамейками с достаточным количеством посадочных мест. "
        if 3 not in choice:
            answer += "Мне не было предоставлено место для хранения средств личной гигиены. "
        if 4 not in choice:
            answer += "В камере не было урны для мусора. "
        if 5 not in choice:
            answer += "Камера не была оборудована радиоприемником или абонентским громкоговорителем. "
        if answer:
            answer += "Это нарушает пункт 47 Правил № 996."
        return answer

    def get_next_question_name(self):
        return cell.CellQuestionsNames.KeyCellSevenTwoTwo

    def get_previous_question_name(self):
        return cell.CellQuestionsNames.KeyCellSixthOne


class CellQuestionSevenTwoTwo(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Выдавали ли вам хотя бы один комплект шашек, шахмат или домино на камеру?"
        answers = [
            "1. Да",
            "2. Нет",
            "3. Не просили",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 1:
            return "Для общего пользования в камеру не был выдан комплект настольных игр, что не соответствует " \
                   "части третьей пункта 51 Правил № 996."

    def get_next_question_name(self):
        return cell.cell_get_next_question_name()

    def get_previous_question_name(self):
        return cell.CellQuestionsNames.KeyCellSevenTwoOne
