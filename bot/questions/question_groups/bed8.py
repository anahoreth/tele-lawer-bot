import config
import enum


if config.IS_LOCAL:
    import question_groups.abstract_group as abstract_group
    import question_groups.questions_types as questions_types
else:
    import questions.question_groups.abstract_group as abstract_group
    import questions.question_groups.questions_types as questions_types


class BedQuestionsNames(enum.Enum):
    KeyZero = questions_types.ZERO_QUESTION
    KeyFirst = 1  # Отметьте, что вам не выдавали
    KeyFirstTwoOne = 1.21  # Сколько ночей вы спали без одеяла?
    KeyFirstThreeOne = 1.31  # Сколько ночей вы спали без матраца?
    KeyFirstThreeTwo = 1.32  # Отбирали ли у вас днем матрац?
    KeyFirstFourOne = 1.41  # Сколько ночей вы спали без наматрасника?
    KeyFirstFourTwo = 1.42  # Отбирали ли у вас днем наматрасник?
    KeyFirstFiveOne = 1.51  # Сколько ночей вы спали без подушки?
    KeyFirstSixOne = 1.61  # Сколько ночей вы спали без наволочки?
    KeyFirstSevenOne = 1.71  # Сколько ночей вы спали без наволочки?
    KeyFirstEightOne = 1.81  # Сколько ночей вы спали без простыни?
    KeyFirstNineOne = 1.91  # Сколько дней вы провели без полотенца?
    KeyFirstTen = 1.10  # Сколько дней вы провели без полотенца?
    KeySecond = 2  # Постельные принадлежности и белье были чистые?
    KeyLast = questions_types.LAST_QUESTION
    KeyCount = 2


class BedQuestionGroup(abstract_group.QuestionGroup):
    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        super().__init__(asking_type)
        self.question_name = BedQuestionsNames.KeyFirst

    def get_short_name(self):
        return "Постельные принадлежности"

    def get_questions_count(self):
        return BedQuestionsNames.KeyCount.value

    def process(self):
        complains_list = []
        for question in self.questions_array.values():
            complain = question.process()
            if complain == "":
                continue
            if complain[0] != " ":
                complains_list.append(complain)
            else:
                complains_list[len(complains_list) - 1] += complain
        return complains_list

    def make_new_question(self):
        if self.question_name == BedQuestionsNames.KeyFirst:
            return BedQuestionFirst(self.asking_type, self.place_name)
        if self.question_name == BedQuestionsNames.KeyFirstTwoOne:
            return BedQuestionFirstTwoOne(self.asking_type)
        if self.question_name == BedQuestionsNames.KeyFirstThreeOne:
            return BedQuestionFirstThreeOne(self.asking_type)
        if self.question_name == BedQuestionsNames.KeyFirstThreeTwo:
            return BedQuestionFirstThreeTwo(self.asking_type)
        if self.question_name == BedQuestionsNames.KeyFirstFourOne:
            return BedQuestionFirstFourOne(self.asking_type)
        if self.question_name == BedQuestionsNames.KeyFirstFourTwo:
            return BedQuestionFirstFourTwo(self.asking_type)
        if self.question_name == BedQuestionsNames.KeyFirstFiveOne:
            return BedQuestionFirstFiveOne(self.asking_type)
        if self.question_name == BedQuestionsNames.KeyFirstSixOne:
            return BedQuestionFirstSixOne(self.asking_type)
        if self.question_name == BedQuestionsNames.KeyFirstSevenOne:
            return BedQuestionFirstSevenOne(self.asking_type)
        if self.question_name == BedQuestionsNames.KeyFirstEightOne:
            return BedQuestionFirstEightOne(self.asking_type)
        if self.question_name == BedQuestionsNames.KeyFirstNineOne:
            return BedQuestionFirstNineOne(self.asking_type)
        if self.question_name == BedQuestionsNames.KeyFirstTen:
            return BedQuestionFirstTen(self.asking_type)
        if self.question_name == BedQuestionsNames.KeySecond:
            return BedQuestionSecond(self.asking_type)
        return None


# TODO потом можно убрать этот костыль
from collections import deque
selected_bed_things = deque()


def get_bed_next_question_name():
    questions_dict = {
        0: BedQuestionsNames.KeyFirstTwoOne,
        1: BedQuestionsNames.KeyFirstThreeOne,
        2: BedQuestionsNames.KeyFirstFourOne,
        3: BedQuestionsNames.KeyFirstFiveOne,
        4: BedQuestionsNames.KeyFirstSixOne,
        5: BedQuestionsNames.KeyFirstSevenOne,
        6: BedQuestionsNames.KeyFirstEightOne,
        7: BedQuestionsNames.KeyFirstNineOne,
        8: BedQuestionsNames.KeyFirstTen,
    }
    if len(selected_bed_things) > 0:
        number = selected_bed_things.popleft()
        return questions_dict[number]
    return BedQuestionsNames.KeySecond


class BedQuestionFirst(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Отметьте, что вам *НЕ* выдавали"
        answers = [
            "1. Одеяло полушерстяное",
            "2. Матрац ватный (отметьте также, если его отбирали днем)",
            "3. Наволочка тюфячная (наматрасник)",
            "4. Подушка ватная",
            "5. Наволочка подушечная нижняя",
            "6. Наволочка подушечная верхняя",
            "7. Простынь",
            "8. Полотенце",
            "9. Полотенце гигиеническое (выдается лицам женского пола) ",
            "10. Все выдавали",
        ]
        super().__init__(text, answers, asking_type, questions_types.QuestionAnswerType.ANSWER_QUERY_MANY)

    def process(self):
        choice = self.get_choice()
        if 9 in choice:
            return ""

        if self.asking_type.value == abstract_group.AskingType.BeforeCourt.value:
            return "Во время нахождения в {} меня не обеспечили постельными принадлежностями и постельным " \
                   "бельем по Норме обеспечения задержанных лиц постельными принадлежностями и постельным " \
                   "бельем (Приложение 3 к Правилам № 996).".format(self.place_name)
        if self.asking_type.value == abstract_group.AskingType.BothSideOfCourt.value:
            return "Во время нахождения в {} меня не обеспечили постельными принадлежностями и " \
                   "постельным бельем по Нормам обеспечения (Приложение 3 к Правилам № 996 и " \
                   "Приложение 3 к Правилам № 313).".format(self.place_name)

        # abstract_group.AskingType.AfterCourt
        return "Во время нахождения в {} меня не обеспечили постельными принадлежностями и " \
               "постельным бельем по Норме обеспечения административно арестованных постельными " \
               "принадлежностями и постельным бельем (Приложение 3 к Правилам № 313).".format(self.place_name)

    def get_next_question_name(self):
        choice = self.get_choice()
        if 9 in choice:
            return BedQuestionsNames.KeySecond
        choice_list = list(choice)
        choice_list.sort()
        for a in choice_list:
            selected_bed_things.append(a)
        return get_bed_next_question_name()

    def get_previous_question_name(self):
        return BedQuestionsNames.KeyZero


def get_text_begin_days(choice):
    try:
        choice_int = int(choice)
    except:
        choice_int = -1

    if choice_int == -1:
        return "__ дней"

    if choice_int in [11, 12, 13, 14]:
        return "{} дней".format(choice_int)
    if choice_int % 10 == 1:
        return "{} день".format(choice_int)
    if choice_int % 10 in [2, 3, 4]:
        return "{} дня".format(choice_int)
    return "{} дней".format(choice_int)


def get_text_begin(choice):
    try:
        choice_int = int(choice)
    except:
        choice_int = -1
    if choice_int == -1:
        return "__ ночей"
    if choice_int in [11, 12, 13, 14]:
        return "{} ночей".format(choice)
    if choice_int % 10 == 1:
        return "{} ночь".format(choice)
    if choice_int % 10 in [2, 3, 4]:
        return "{} ночи".format(choice)
    return "{} ночей".format(choice)


class BedQuestionFirstTwoOne(questions_types.GetNumberQuestion):

    def __init__(self, asking_type):
        text = "Сколько ночей вы спали без одеяла?"
        super().__init__(text, asking_type)

    def process(self):
        choice = self.get_choice()

        text_begin = get_text_begin(choice)
        return text_begin + " мне пришлось спать без одеяла, поскольку его не выдавали."

    def get_next_question_name(self):
        return get_bed_next_question_name()

    def get_previous_question_name(self):
        return BedQuestionsNames.KeyFirst


class BedQuestionFirstThreeOne(questions_types.GetNumberQuestion):

    def __init__(self, asking_type):
        text = "Сколько ночей вы спали без матраца?"
        super().__init__(text, asking_type)

    def process(self):
        choice = self.get_choice()

        text_begin = get_text_begin(choice)
        return text_begin + " мне пришлось спать без ватного матраца, поскольку его не выдавали."

    def get_next_question_name(self):
        return BedQuestionsNames.KeyFirstThreeTwo

    def get_previous_question_name(self):
        return BedQuestionsNames.KeyFirst


class BedQuestionFirstThreeTwo(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Отбирали ли у вас днем матрац?"
        answers = ['1. Да', "2. Нет"]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 0:
            return " Кроме того, днем у меня отбирали матрац. Из-за этого днем у меня не было возможности " \
                   "удобно сидеть на кровати."
        return ""

    def get_next_question_name(self):
        return get_bed_next_question_name()

    def get_previous_question_name(self):
        return BedQuestionsNames.KeyFirst


class BedQuestionFirstFourOne(questions_types.GetNumberQuestion):

    def __init__(self, asking_type):
        text = "Сколько ночей вы спали без наматрасника?"
        super().__init__(text, asking_type)

    def process(self):
        choice = self.get_choice()

        text_begin = get_text_begin(choice)
        return text_begin + " мне пришлось спать без тюфячной наволочки, поскольку её не выдавали."

    def get_next_question_name(self):
        return BedQuestionsNames.KeyFirstFourTwo

    def get_previous_question_name(self):
        return BedQuestionsNames.KeyFirst


class BedQuestionFirstFourTwo(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Отбирали ли у вас днем наматрасник?"
        answers = ['1. Да', "2. Нет"]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 0:
            return " Кроме того, днем её у меня отбирали. Из-за этого днем у меня не было возможности " \
                   "удобно и гигиенично сидеть на кровати."
        return ""

    def get_next_question_name(self):
        return get_bed_next_question_name()

    def get_previous_question_name(self):
        return BedQuestionsNames.KeyFirst


class BedQuestionFirstFiveOne(questions_types.GetNumberQuestion):

    def __init__(self, asking_type):
        text = "Сколько ночей вы спали без подушки?"
        super().__init__(text, asking_type)

    def process(self):
        choice = self.get_choice()

        text_begin = get_text_begin(choice)
        return text_begin + " мне пришлось спать без подушки, поскольку её не выдавали."

    def get_next_question_name(self):
        return get_bed_next_question_name()

    def get_previous_question_name(self):
        return BedQuestionsNames.KeyFirst


class BedQuestionFirstSixOne(questions_types.GetNumberQuestion):

    def __init__(self, asking_type):
        text = "Сколько ночей вы спали без нижней наволочки?"
        super().__init__(text, asking_type)

    def process(self):
        choice = self.get_choice()
        text_begin = get_text_begin(choice)
        return text_begin + " мне пришлось спать без наволочки подушечной нижней, поскольку её не выдавали."

    def get_next_question_name(self):
        return get_bed_next_question_name()

    def get_previous_question_name(self):
        return BedQuestionsNames.KeyFirst


class BedQuestionFirstSevenOne(questions_types.GetNumberQuestion):

    def __init__(self, asking_type):
        text = "Сколько ночей вы спали без верхней наволочки?"
        super().__init__(text, asking_type)

    def process(self):
        choice = self.get_choice()

        text_begin = get_text_begin(choice)
        return text_begin + " мне пришлось спать без наволочки подушечной верхней, поскольку её не выдавали."

    def get_next_question_name(self):
        return get_bed_next_question_name()

    def get_previous_question_name(self):
        return BedQuestionsNames.KeyFirst


class BedQuestionFirstEightOne(questions_types.GetNumberQuestion):

    def __init__(self, asking_type):
        text = "Сколько ночей вы спали без простыни?"
        super().__init__(text, asking_type)

    def process(self):
        choice = self.get_choice()

        text_begin = get_text_begin(choice)
        return text_begin + " мне пришлось спать без простыни, поскольку её не выдавали."

    def get_next_question_name(self):
        return get_bed_next_question_name()

    def get_previous_question_name(self):
        return BedQuestionsNames.KeyFirst


class BedQuestionFirstNineOne(questions_types.GetNumberQuestion):

    def __init__(self, asking_type):
        text = "Сколько дней вы провели без полотенца?"
        super().__init__(text, asking_type)

    def process(self):
        choice = self.get_choice()

        text_begin = get_text_begin_days(choice)
        # print(text_begin, choice)
        return text_begin + " мне не выдавали полотенце, вследствие чего отсутствовала возможность полноценно " \
                            "проводить необходимые гигиенические процедуры."

    def get_next_question_name(self):
        return get_bed_next_question_name()

    def get_previous_question_name(self):
        return BedQuestionsNames.KeyFirst


class BedQuestionFirstTen(questions_types.GetNumberQuestion):

    def __init__(self, asking_type):
        text = "Сколько дней вы провели без гигиенического полотенца?"
        super().__init__(text, asking_type)

    def process(self):
        choice = self.get_choice()

        text_begin = get_text_begin_days(choice)
        return text_begin + " мне не выдавали гигиеническое полотенце."

    def get_next_question_name(self):
        return get_bed_next_question_name()

    def get_previous_question_name(self):
        return BedQuestionsNames.KeyFirst


class BedQuestionSecond(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Постельные принадлежности и белье были чистые?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 1:
            return "Выданные постельные принадлежности и белье были грязными. " \
                   "Из-за этого пользоваться ими было негигиенично и небезопасно для здоровья."
        return ""

    def get_next_question_name(self):
        return BedQuestionsNames.KeyLast

    def get_previous_question_name(self):
        return BedQuestionsNames.KeyFirst
