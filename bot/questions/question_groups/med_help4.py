import config
import enum

if config.IS_LOCAL:
    import question_groups.abstract_group as abstract_group
    import question_groups.questions_types as questions_types
else:
    import questions.question_groups.abstract_group as abstract_group
    import questions.question_groups.questions_types as questions_types


class MedQuestionsNames(enum.Enum):
    KeyZero = questions_types.ZERO_QUESTION
    KeyFirst = 1  # Этот раздел может включать большое...
    KeySecond = 2  # Были ли у вас телесные повреждения во время заключения?
    KeySecondOne = 2.1  # Проводился ли медицинский осмотр телесных повреждений?
    KeySecondOneOne = 2.11  # Осмотр провели в течение суток с выявления повреждений или вашей просьбы?
    KeySecondOneOneOne = 2.111  # Через какое время после выявления повреждений или вашей просьбы провели осмотр?
    KeySecondOneTwo = 2.12  # Осмотр проводился сотрудником одного с вами пола?
    KeySecondOneThree = 2.13  # Результаты осмотра письменно зафиксировали и сообщили вам?
    KeyThird = 3  # Были ли вам необходимы лекарства?
    KeyThirdOne = 3.1  # Был ли у вас доступ к нужным лекарствам в необходимом количестве?
    KeyThirdOneOne = 3.11  # Я зафиксировала ваш ответ, но рекомендую в тексте жалобы
    KeyFour = 4  # Болели ли вы во время заключения?
    KeyFourOne = 4.1  # Вы начали болеть до заключения или после?
    KeyFourOneOne = 4.11  # Обеспечивалось ли вам необходимое лечение?
    KeyFourOneOneOne = 4.111  # Я зафиксировала ваш ответ,
    KeyFourOneTwo = 4.12  # Обеспечивалось ли вам необходимое лечение?
    KeyFourOneTwoOne = 4.121  # Я зафиксировала ваш ответ,
    KeyFive = 5  # Вызывали ли скорую медицинскую помощь при необходимости?
    KeyFiveOne = 5.1  # просто для того, чтобы сообщение вывести
    KeyLast = questions_types.LAST_QUESTION
    KeyCount = 5


class MedQuestionGroup(abstract_group.QuestionGroup):
    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        super().__init__(asking_type)
        self.question_name = MedQuestionsNames.KeyFirst

    def get_short_name(self):
        return "Отсутствие медицинской помощи"

    def get_questions_count(self):
        return MedQuestionsNames.KeyCount.value

    def make_new_question(self):
        if self.question_name == MedQuestionsNames.KeyFirst:
            return MedQuestionFirst(self.asking_type)
        if self.question_name == MedQuestionsNames.KeySecond:
            return MedQuestionSecond(self.asking_type, self.place_name)
        if self.question_name == MedQuestionsNames.KeySecondOne:
            return MedQuestionSecondOne(self.asking_type)
        if self.question_name == MedQuestionsNames.KeySecondOneOne:
            return MedQuestionSecondOneOne(self.asking_type)
        if self.question_name == MedQuestionsNames.KeySecondOneOneOne:
            return MedQuestionSecondOneOneOne(self.asking_type)
        if self.question_name == MedQuestionsNames.KeySecondOneTwo:
            return MedQuestionSecondOneTwo(self.asking_type)
        if self.question_name == MedQuestionsNames.KeySecondOneThree:
            return MedQuestionSecondOneThree(self.asking_type)
        if self.question_name == MedQuestionsNames.KeyThird:
            return MedQuestionThird(self.asking_type)
        if self.question_name == MedQuestionsNames.KeyThirdOne:
            return MedQuestionThirdOne(self.asking_type)
        if self.question_name == MedQuestionsNames.KeyThirdOneOne:
            return MedQuestionThirdOneOne(self.asking_type)
        if self.question_name == MedQuestionsNames.KeyFour:
            return MedQuestionFour(self.asking_type)
        if self.question_name == MedQuestionsNames.KeyFourOne:
            return MedQuestionFourOne(self.asking_type)
        if self.question_name == MedQuestionsNames.KeyFourOneOne:
            return MedQuestionFourOneOne(self.asking_type, self.place_name)
        if self.question_name == MedQuestionsNames.KeyFourOneOneOne:
            return MedQuestionFourOneOneOne(self.asking_type)
        if self.question_name == MedQuestionsNames.KeyFourOneTwo:
            return MedQuestionFourOneTwo(self.asking_type, self.place_name)
        if self.question_name == MedQuestionsNames.KeyFourOneTwoOne:
            return MedQuestionFourOneTwoOne(self.asking_type)
        if self.question_name == MedQuestionsNames.KeyFive:
            return MedQuestionFive(self.asking_type, self.place_name)
        if self.question_name == MedQuestionsNames.KeyFiveOne:
            return MedQuestionFiveOne(self.asking_type)
        return None

    def process(self):
        complains_list = []
        for question in self.questions_array.values():
            complain = question.process()
            if complain != "":
                complains_list.append(complain)

        all_complains_count = len(complains_list)

        if all_complains_count == 0:
            return complains_list

        count = 0
        answer_third_one = self.get_choice(MedQuestionsNames.KeyThirdOne)
        if answer_third_one == 1:
            count += 1
        answer_four_one_one = self.get_choice(MedQuestionsNames.KeyFourOneOne)
        if answer_four_one_one == 1:
            count += 1
        answer_four_one_two = self.get_choice(MedQuestionsNames.KeyFourOneTwo)
        if answer_four_one_two == 1:
            count += 1
        answer_five = self.get_choice(MedQuestionsNames.KeyFive)
        if answer_five == 2:
            count += 1

        if count > 0:
            new_complain = "Согласно статье 45 Конституции гражданам Республики Беларусь гарантируется право на " \
                           "охрану здоровья. Кроме того, правило 24 Правил Нельсона Манделы устанавливает " \
                           "необходимость обеспечивать те же стандарты медико-санитарного обслуживания, " \
                           "которые существуют в обществе, а также бесплатный доступ к необходимым медико-санитарным " \
                           "услугам без дискриминации. Медицинское обслуживание следует организовывать таким образом, " \
                           "чтобы обеспечить непрерывность лечения и ухода."
            complains_list.insert(all_complains_count - count, new_complain)
        return complains_list


class MedQuestionFirst(questions_types.JustInfoQuestion):

    def __init__(self, asking_type):
        text = "_Этот раздел может включать большое количество нарушений. Я помогу " \
               "пожаловаться на самые типичные из них, но рекомендую самостоятельно " \
               "дописать свои замечания в готовом тексте жалобы._"
        super().__init__(text, asking_type)

    def get_next_question_name(self):
        return MedQuestionsNames.KeySecond

    def get_previous_question_name(self):
        return MedQuestionsNames.KeyZero


class MedQuestionSecond(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Были ли у вас телесные повреждения во время заключения?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 0:
            return "Во время нахождения в {} у меня были телесные повреждения.".format(self.place_name)
        return ""

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 0:
            return MedQuestionsNames.KeySecondOne
        return MedQuestionsNames.KeyThird

    def get_previous_question_name(self):
        return MedQuestionsNames.KeyFirst


class MedQuestionSecondOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Проводился ли медицинский осмотр телесных повреждений?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 1:
            if self.asking_type.value == abstract_group.AskingType.BeforeCourt.value:
                return "Исходя из пункта 89 Правил № 996 в случае наличия телесных повреждений в течение одних " \
                       "суток с момента их выявления либо моей просьбы должно было быть произведено " \
                       "освидетельствование. Однако освидетельствование не было произведено, что является незаконным."
            elif self.asking_type.value == abstract_group.AskingType.BothSideOfCourt.value:
                return "Исходя из пункта 89 Правил № 996, пункта 95 Правил № 313, в случае наличия телесных " \
                       "повреждений в течение одних суток с момента их выявления либо моей просьбы должно было быть" \
                       " произведено освидетельствование. Однако освидетельствование не было произведено," \
                       " что является незаконным."
            else:  # abstract_group.AskingType.AfterCourt
                return "Исходя из пункта 95 Правил № 313, в случае наличия телесных повреждений в течение одних " \
                       "суток с момента их выявления либо моей просьбы должно было быть произведено " \
                       "освидетельствование. Однако освидетельствование не было произведено, что является незаконным."
        return ""

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 0:
            return MedQuestionsNames.KeySecondOneOne
        return MedQuestionsNames.KeyThird

    def get_previous_question_name(self):
        return MedQuestionsNames.KeySecond


class MedQuestionSecondOneOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Осмотр провели в течение суток с выявления повреждений или вашей просьбы?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 1:
            return MedQuestionsNames.KeySecondOneOneOne
        return MedQuestionsNames.KeySecondOneTwo

    def get_previous_question_name(self):
        return MedQuestionsNames.KeySecondOne


class MedQuestionSecondOneOneOne(questions_types.GetNumberQuestion):

    def __init__(self, asking_type):
        text = "Через какое время после выявления повреждений или вашей просьбы провели осмотр?\n" \
               '_Введите ответ с маленькой буквы в формате: "через 2 дня" или "спустя 50 часов"_'

        super().__init__(text, asking_type)

    def process(self):
        asking_type = self.asking_type
        if asking_type.value == abstract_group.AskingType.BeforeCourt.value:
            return "В нарушение части первой пункта 89 Правил № 996 освидетельствование было " \
                   "проведено только {}.".format(self.choice)
        elif asking_type.value == abstract_group.AskingType.BothSideOfCourt.value:
            return "В нарушение части первой пункта 89 Правил № 996 и части первой пункта 95 Правил № 313 " \
                   "освидетельствование было проведено только {}.".format(self.choice)
        else:  # abstract_group.AskingType.AfterCourt
            return "В нарушение части первой пункта 95 Правил № 313 освидетельствование " \
                   "было проведено только {}.".format(self.choice)


    def get_next_question_name(self):
        return MedQuestionsNames.KeySecondOneTwo

    def get_previous_question_name(self):
        return MedQuestionsNames.KeySecondOneOne


class MedQuestionSecondOneTwo(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Осмотр проводился сотрудником одного с вами пола?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 1:
            if self.asking_type.value == abstract_group.AskingType.BeforeCourt.value:
                return "Освидетельствование проводилось сотрудником не одного со мной пола, что нарушает " \
                       "часть вторую пункта 89 Правил № 996."
            elif self.asking_type.value == abstract_group.AskingType.BothSideOfCourt.value:
                return "Освидетельствование проводилось сотрудником не одного со мной пола, что нарушает " \
                       "часть вторую пункта 89 Правил № 996, часть вторую пункта 95 Правил № 313."
            else:  # abstract_group.AskingType.AfterCourt
                return "Освидетельствование проводилось сотрудником не одного со мной пола, " \
                       "что нарушает часть вторую пункта 95 Правил № 313."
        return ""

    def get_next_question_name(self):
        return MedQuestionsNames.KeySecondOneThree

    def get_previous_question_name(self):
        return MedQuestionsNames.KeySecondOneOne


class MedQuestionSecondOneThree(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Результаты осмотра письменно зафиксировали и сообщили Вам?"
        answers = [
            "1. Да",
            "2. Не зафиксировали",
            "3. Не сообщили мне",
            "4. Не зафиксировали и не сообщили ",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if self.asking_type.value == abstract_group.AskingType.BeforeCourt.value:
            if choice == 1:
                return "Результаты медицинского освидетельствования телесных повреждений, которые у меня были, не были " \
                    "зафиксированы в установленном порядке. Это нарушает часть три пункта 89 Правил № 996."
            if choice == 2:
                return "В нарушение части три пункта 89 Правил № 996, мне не сообщили результаты медицинского " \
                       "освидетельствования телесных повреждений и не передали копию акта медицинского " \
                       "освидетельствования."
            if choice == 3:
                return "Результаты медицинского освидетельствования телесных повреждений, которые у меня были, " \
                       "не были зафиксированы в установленном порядке. Кроме того, результаты мне не сообщили. " \
                       "Это нарушает часть три пункта 89 Правил № 996."
        elif self.asking_type.value == abstract_group.AskingType.BothSideOfCourt.value:
            if choice == 1:
                return "Результаты медицинского освидетельствования телесных повреждений, которые у меня были, не " \
                       "были зафиксированы в установленном порядке. Это нарушает часть третью пункта 89 Правил " \
                       "№ 996, частью третью пункта 95 Правил № 313."
            if choice == 2:
                return "В нарушение части третьей пункта 95 Правил № 313, части третьей пункта № 89 Правил № 996 " \
                       "мне не сообщили результаты медицинского освидетельствования телесных повреждений."
            if choice == 3:
                return "Результаты медицинского освидетельствования телесных повреждений, которые у меня были, не " \
                       "были зафиксированы в установленном порядке. Кроме того, результаты мне не сообщили. " \
                       "Это нарушает часть третью пункта 89 Правил № 996, часть третью пункта 95 Правил № 313."
        else:  # abstract_group.AskingType.AfterCourt
            if choice == 1:
                return "Результаты медицинского освидетельствования телесных повреждений, которые у меня были, " \
                       "не были зафиксированы в журнале регистрации случаев оказания первичной медицинской помощи. " \
                       "Это нарушает часть три пункта 95 Правил № 313."
            if choice == 2:
                return "В нарушение части три пункта 95 Правил № 313, мне не сообщили результаты медицинского " \
                       "освидетельствования телесных повреждений."
            if choice == 3:
                return "Результаты медицинского освидетельствования телесных повреждений, которые у меня были, не " \
                       "были зафиксированы в журнале регистрации случаев оказания первичной медицинской помощи. " \
                       "Кроме того, результаты мне не сообщили. Это нарушает часть третью пункта 95 Правил № 313."
        return ""

    def get_next_question_name(self):
        return MedQuestionsNames.KeyThird

    def get_previous_question_name(self):
        return MedQuestionsNames.KeySecondOneTwo


class MedQuestionThird(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Были ли Вам необходимы лекарства?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 0:
            return MedQuestionsNames.KeyThirdOne
        return MedQuestionsNames.KeyFour

    def get_previous_question_name(self):
        return MedQuestionsNames.KeySecond


class MedQuestionThirdOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Был ли у вас доступ к нужным лекарствам в необходимом количестве?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 1:
            return "Тем не менее, мне не был предоставлен доступ к необходимым мне лекарствам."
        return ""

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 1:
            return MedQuestionsNames.KeyThirdOneOne
        return MedQuestionsNames.KeyFour

    def get_previous_question_name(self):
        return MedQuestionsNames.KeyThird


class MedQuestionThirdOneOne(questions_types.JustInfoQuestion):

    def __init__(self, asking_type):
        text = "_Я зафиксировала ваш ответ, но рекомендую в тексте жалобы самостоятельно уточнить подробности_"
        super().__init__(text, asking_type)

    def get_next_question_name(self):
        return MedQuestionsNames.KeyFour

    def get_previous_question_name(self):
        return MedQuestionsNames.KeyThirdOne


class MedQuestionFour(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Болели ли вы во время заключения?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 0:
            return MedQuestionsNames.KeyFourOne
        return MedQuestionsNames.KeyFive

    def get_previous_question_name(self):
        return MedQuestionsNames.KeyThird


class MedQuestionFourOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Вы начали болеть до заключения или после?"
        answers = [
            "1. До",
            "2. После",
        ]
        super().__init__(text, answers, asking_type)

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 0:
            return MedQuestionsNames.KeyFourOneOne
        return MedQuestionsNames.KeyFourOneTwo

    def get_previous_question_name(self):
        return MedQuestionsNames.KeyFour


class MedQuestionFourOneOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Обеспечивалось ли вам необходимое лечение?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 1:
            return "При поступлении в {} у меня имелись проблемы со здоровьем, " \
                   "однако мне не было обеспечено необходимое лечение.".format(self.place_name)
        return ""

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 1:
            return MedQuestionsNames.KeyFourOneOneOne
        return MedQuestionsNames.KeyFive

    def get_previous_question_name(self):
        return MedQuestionsNames.KeyFourOne


class MedQuestionFourOneOneOne(questions_types.JustInfoQuestion):

    def __init__(self, asking_type):
        text = "_Я зафиксировала ваш ответ, но рекомендую в тексте жалобы самостоятельно уточнить подробности_"
        super().__init__(text, asking_type)

    def get_next_question_name(self):
        return MedQuestionsNames.KeyFive

    def get_previous_question_name(self):
        return MedQuestionsNames.KeyFourOneOne


class MedQuestionFourOneTwo(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Обеспечивалось ли вам необходимое лечение?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 1:
            return "Во время содержания в {} у меня появились проблемы со здоровьем, " \
                   "однако мне не было обеспечено необходимое лечение.".format(self.place_name)
        return ""

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 1:
            return MedQuestionsNames.KeyFourOneTwoOne
        return MedQuestionsNames.KeyFive

    def get_previous_question_name(self):
        return MedQuestionsNames.KeyFourOne


class MedQuestionFourOneTwoOne(questions_types.JustInfoQuestion):

    def __init__(self, asking_type):
        text = "_Я зафиксировала ваш ответ, но рекомендую в тексте жалобы " \
               "самостоятельно уточнить подробности с указанием причин заболевания_"
        super().__init__(text, asking_type)

    def get_next_question_name(self):
        return MedQuestionsNames.KeyFive

    def get_previous_question_name(self):
        return MedQuestionsNames.KeyFourOneOne


class MedQuestionFive(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Вызывали ли скорую медицинскую помощь при необходимости?"
        answers = [
            "1. Не было необходимости",
            "2. Да",
            "3. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 2:
            if self.asking_type.value == abstract_group.AskingType.BeforeCourt.value:
                return "Во время содержания в {} у меня обострились проблемы со здоровьем, и появилась потребность " \
                       "в оказании скорой медицинской помощи. Тем не менее, в нарушение пункта 83 Правил № 996, " \
                       "скорую помощь мне не вызвали.".format(self.place_name)
            elif self.asking_type.value == abstract_group.AskingType.BothSideOfCourt.value:
                return "Во время содержания в {} у меня обострились проблемы со здоровьем, и появилась " \
                       "потребность в оказании скорой медицинской помощи. Тем не менее, в нарушение пункта " \
                       "83 Правил № 996, пункта 90 Правил № 313 скорую помощь мне не вызвали.".format(self.place_name)
            else:  # abstract_group.AskingType.AfterCourt
                return "Во время содержания в {} у меня обострились проблемы со " \
                       "здоровьем, и появилась потребность в оказании скорой медицинской " \
                       "помощи. Тем не менее, в нарушение пункта 90 Правил № 313, " \
                       "скорую помощь мне не вызвали.".format(self.place_name)
        return ""

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 2:
            return MedQuestionsNames.KeyFiveOne
        return MedQuestionsNames.KeyLast

    def get_previous_question_name(self):
        return MedQuestionsNames.KeyThird


class MedQuestionFiveOne(questions_types.JustInfoQuestion):

    def __init__(self, asking_type):
        text = "_Я зафиксировала ваш ответ, но рекомендую самостоятельно уточнить, " \
               "при каких обстоятельствах вам нужна была скорая_"
        super().__init__(text, asking_type)

    def get_next_question_name(self):
        return MedQuestionsNames.KeyLast

    def get_previous_question_name(self):
        return MedQuestionsNames.KeyFive
