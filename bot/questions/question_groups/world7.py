import config
import enum

if config.IS_LOCAL:
    import question_groups.abstract_group as abstract_group
    import question_groups.questions_types as questions_types
else:
    import questions.question_groups.abstract_group as abstract_group
    import questions.question_groups.questions_types as questions_types


class WorldQuestionsNames(enum.Enum):
    KeyZero = questions_types.ZERO_QUESTION
    KeyFirst = 1  # Получили ли вы все отправленные вам письма?
    KeySecond = 2  # Отправляли ли все ваши письма?
    KeyThree = 3  # Допускали ли к вам адвоката?
    KeyThreeTwoOne = 3.21  # Я зафиксировала ваш ответ...
    KeyFour = 4  # Разрешали ли вам разговоры по таксофону?
    KeyFourTwoOne = 4.21  # У вас была телефонная карточка или возможность ее купить?
    KeyLast = questions_types.LAST_QUESTION
    KeyCount = 4


class WorldQuestionGroup(abstract_group.QuestionGroup):
    def __init__(self, asking_type, place_name):
        super().__init__(asking_type)
        self.place_name = place_name
        self.question_name = WorldQuestionsNames.KeyFirst

    def get_short_name(self):
        return "Отсутствие контактов с внешним миром и доступа к адвокату"

    def get_questions_count(self):
        return WorldQuestionsNames.KeyCount.value

    def make_new_question(self):
        if self.question_name == WorldQuestionsNames.KeyFirst:
            return WorldQuestionFirst(self.asking_type, self.place_name)
        if self.question_name == WorldQuestionsNames.KeySecond:
            return WorldQuestionSecond(self.asking_type, self.place_name)
        if self.question_name == WorldQuestionsNames.KeyThree:
            return WorldQuestionThree(self.asking_type, self.place_name)
        if self.question_name == WorldQuestionsNames.KeyThreeTwoOne:
            return WorldQuestionThreeTwoOne(self.asking_type)
        if self.question_name == WorldQuestionsNames.KeyFour:
            return WorldQuestionFour(self.asking_type, self.place_name)
        if self.question_name == WorldQuestionsNames.KeyFourTwoOne:
            return WorldQuestionFourTwoOne(self.asking_type, self.place_name)
        return None


class WorldQuestionFirst(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Получили ли вы все отправленные вам письма?"
        answers = [
            "1. Да",
            "2. Нет",
            "3. Не знаю",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        if self.get_choice() != 1:
            return ""
        asking_type_value = self.asking_type.value
        if asking_type_value == abstract_group.AskingType.BeforeCourt.value:
            return "Мне достоверно известно, что администрация {} не вручила мне некоторые письма. Это " \
                   "нарушает правило 58 Правил Нельсона Манделы, согласно которому администрация должна " \
                   "давать возможность общаться через регулярные промежутки времени и под должным надзором " \
                   "с семьями или друзьями посредством письменной переписки и с использованием, если есть " \
                   "такая возможность, телекоммуникационных, электронных, цифровых и иных средств, а также в " \
                   "ходе свиданий. ".format(self.place_name)
        if asking_type_value == abstract_group.AskingType.BothSideOfCourt.value:
            return "Мне достоверно известно, что администрация {} не вручила мне некоторые письма. Это нарушает" \
                   " пункт 62 Правил № 313, согласно которому вручение писем и телеграмм, поступающих на имя " \
                   "административно арестованного, производится не позднее чем в трехдневный срок со дня поступления" \
                   " в место отбывания административного ареста письма, телеграммы, посылки, бандероли," \
                   " мелкого пакета, за исключением праздничных и выходных дней. Кроме того, это нарушает" \
                   " правило 58 Правил Нельсона Манделы, согласно которому администрация должна давать возможность" \
                   " общаться через регулярные промежутки времени и под должным надзором с семьями или друзьями " \
                   "посредством письменной переписки и с использованием, если есть такая возможность, " \
                   "телекоммуникационных, электронных, цифровых и иных средств, а также в " \
                   "ходе свиданий. ".format(self.place_name)
        # abstract_group.AskingType.AfterCourt
        return "Мне достоверно известно, что администрация {} не вручила мне некоторые письма. Это " \
                   "нарушает пункт 62 Правил № 313, согласно которому вручение писем и телеграмм, поступающих " \
                   "на имя административно арестованного, производится не позднее чем в трехдневный срок со дня " \
                   "поступления в место отбывания административного ареста письма, телеграммы, посылки, бандероли," \
                   " мелкого пакета, за исключением праздничных и выходных дней.".format(self.place_name)

    def get_next_question_name(self):
        return WorldQuestionsNames.KeySecond

    def get_previous_question_name(self):
        return WorldQuestionsNames.KeyZero


class WorldQuestionSecond(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Отправляли ли все ваши письма?"
        answers = [
            "1. Да",
            "2. Нет",
            "3. Не знаю",
            "4. Я не отправлял(а) письма",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice != 1:
            return ""
        asking_type_value = self.asking_type.value
        if asking_type_value == abstract_group.AskingType.BeforeCourt.value:
            return "Администрация {} отправила не все мои письма. Это нарушает правило 58 Правил Нельсона " \
                   "Манделы, согласно которому администрация должна давать возможность общаться через регулярные " \
                   "промежутки времени и под должным надзором с семьями или друзьями посредством письменной " \
                   "переписки и с использованием, если есть такая возможность, телекоммуникационных, электронных," \
                   " цифровых и иных средств, а также в ходе свиданий. ".format(self.place_name)
        if asking_type_value == abstract_group.AskingType.BothSideOfCourt.value:
            return "Администрация {} отправила не все мои письма. Это нарушает пункт 62 Правил № 313, " \
                   "согласно которому отправление писем, телеграмм, посылок, бандеролей или мелких пакетов " \
                   "производится не позднее чем в трехдневный срок со дня поступления в место отбывания " \
                   "административного ареста письма, телеграммы, посылки, бандероли, мелкого пакета, за " \
                   "исключением праздничных и выходных дней. Кроме того, это нарушает правило 58 Правил " \
                   "Нельсона Манделы, согласно которому администрация должна давать возможность общаться через" \
                   " регулярные промежутки времени и под должным надзором с семьями или друзьями посредством " \
                   "письменной переписки и с использованием, если есть такая возможность, телекоммуникационных," \
                   " электронных, цифровых и иных средств, а также в ходе свиданий. ".format(self.place_name)
        # abstract_group.AskingType.AfterCourt

        return "Администрация {} отправила не все мои письма. Это нарушает пункт 62 Правил № 313, согласно" \
                   " которому отправление писем, телеграмм, посылок, бандеролей или мелких пакетов производится " \
                   "не позднее чем в трехдневный срок со дня поступления в место отбывания административного " \
                   "ареста письма, телеграммы, посылки, бандероли, мелкого пакета, за исключением праздничных " \
                   "и выходных дней.".format(self.place_name)

    def get_next_question_name(self):
        return WorldQuestionsNames.KeyThree

    def get_previous_question_name(self):
        return WorldQuestionsNames.KeyFirst


class WorldQuestionThree(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Допускали ли к вам адвоката?"
        answers = [
            "1. Да",
            "2. Нет",
            "3. У меня не было адвоката",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 1:
            return "Во время нахождения в {} мне понадобилась юридическая помощь, для чего необходимо было " \
                   "встретиться с адвокатом. Согласно статье 62 Конституции, каждый человек имеет право " \
                   "на юридическую помощь. Противодействие оказанию правовой помощи в Республике " \
                   "Беларусь запрещается. Тем не менее, ко мне незаконно не пустили адвоката.".format(self.place_name)
        return ""

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 1:
            return WorldQuestionsNames.KeyThreeTwoOne
        if self.asking_type.value == abstract_group.AskingType.BeforeCourt.value:
            return WorldQuestionsNames.KeyLast
        return WorldQuestionsNames.KeyFour

    def get_previous_question_name(self):
        return WorldQuestionsNames.KeySecond


class WorldQuestionThreeTwoOne(questions_types.JustInfoQuestion):

    def __init__(self, asking_type):
        text = "_Я зафиксировала ваш ответ, но рекомендую самостоятельно уточнить, при каких обстоятельствах и как " \
               "сотрудники препятствовали встрече с адвокатом_"
        super().__init__(text, asking_type)

    def get_next_question_name(self):
        if self.asking_type.value == abstract_group.AskingType.BeforeCourt.value:
            return WorldQuestionsNames.KeyLast
        return WorldQuestionsNames.KeyFour

    def get_previous_question_name(self):
        return WorldQuestionsNames.KeyThree


#TODO подчеркивание
class WorldQuestionFour(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        if asking_type.value == abstract_group.AskingType.BothSideOfCourt.value:
            text = "Разрешали ли вам разговоры по таксофону *после суда*?"

        elif asking_type.value == abstract_group.AskingType.AfterCourt.value:
            text = "Разрешали ли вам разговоры по таксофону?"

        assert text
        answers = [
            "1. Да",
            "2. Нет",
            "3. Я не просил(а) сделать звонок",
            "4. Таксофона не было",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice != 3:
            return ""
        if self.asking_type.value == abstract_group.AskingType.BothSideOfCourt.value:
            return "Во время отбывания административного ареста в {} меня фактически лишили права на " \
                   "телефонные звонки по таксофону. В соответствии с пунктом 66 Правил № 313 на территории " \
                   "места отбывания административного ареста для ведения телефонных разговоров устанавливаются " \
                   "таксофоны. Вместе с тем, в {} таксофон отсутствовал. Таким образом, мое право на " \
                   "телефонные звонки, предусмотренное пунктом 65 Правил № 313, " \
                   "было нарушено.".format(self.place_name, self.place_name)
        # МПС
        return "Во время нахождения в {} меня фактически лишили права на телефонные звонки по таксофону. " \
               "В соответствии с пунктом 66 Правил № 313 на территории места отбывания административного ареста " \
               "для ведения телефонных разговоров устанавливаются таксофоны. Вместе с тем, в {} таксофон " \
               "отсутствовал. Таким образом, мое право на телефонные звонки, предусмотренное пунктом 65 Правил " \
               "№ 313, было нарушено.".format(self.place_name, self.place_name)


    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 1:
            return WorldQuestionsNames.KeyFourTwoOne
        return WorldQuestionsNames.KeyLast

    def get_previous_question_name(self):
        return WorldQuestionsNames.KeyThree


class WorldQuestionFourTwoOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "У вас была телефонная карточка или возможность ее купить?"
        answers = [
            "1. Была карточка",
            "2. Не было карточки, но была возможность ее купить",
            "3. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 0:
            if self.asking_type.value == abstract_group.AskingType.BothSideOfCourt.value:
                return "Во время отбывания административного ареста в {} мне необоснованно отказали в " \
                       "телефонных разговорах по таксофону. Согласно пунктам 65, 69 Правил № 313 начальник места" \
                       " отбывания административного ареста может разрешить административно арестованным телефонные" \
                       " разговоры. Административно арестованный, которому разрешен телефонный разговор, для оплаты" \
                       " телефонного разговора может использовать принадлежащую ему телефонную карточку. У меня была " \
                       "телефонная карточка, следовательно, объективные основания для отказа мне в телефонном " \
                       "разговоре отсутствовали.".format(self.place_name)
            return "Во время нахождения в {} мне необоснованно отказали в телефонных разговорах по таксофону. " \
                   "Согласно пунктам 65, 69 Правил № 313 начальник места отбывания административного ареста может " \
                   "разрешить административно арестованным телефонные разговоры. Административно арестованный, " \
                   "которому разрешен телефонный разговор, для оплаты телефонного разговора может использовать " \
                   "принадлежащую ему телефонную карточку. У меня была телефонная карточка, следовательно, " \
                   "объективные основания для отказа мне в телефонном разговоре отсутствовали.".format(self.place_name)
        if choice == 1:
            if self.asking_type.value == abstract_group.AskingType.BothSideOfCourt.value:
                return "Во время отбывания административного ареста в {} мне необоснованно отказали в " \
                       "телефонных разговорах по таксофону. Согласно пунктам 65, 70 Правил № 313 начальник места" \
                       " отбывания административного ареста может разрешить административно арестованным телефонные" \
                       " разговоры. При наличии у административно арестованного личных денежных средств телефонная" \
                       " карточка может быть приобретена сотрудником места отбывания административного ареста. У" \
                       " меня была необходимая сумма денежных средств для покупки телефонной карточки, следовательно," \
                       " объективные основания для отказа мне в телефонном " \
                       "разговоре отсутствовали.".format(self.place_name)
            return "Во время нахождения в {} мне необоснованно отказали в телефонных разговорах по таксофону. " \
                   "Согласно пунктам 65, 70 Правил № 313 начальник места отбывания административного ареста может " \
                   "разрешить административно арестованным телефонные разговоры. При наличии у административно " \
                   "арестованного личных денежных средств телефонная карточка может быть приобретена сотрудником " \
                   "места отбывания административного ареста. У меня была необходимая сумма денежных средств для " \
                   "покупки телефонной карточки, следовательно, объективные основания для отказа мне в телефонном " \
                   "разговоре отсутствовали. ".format(self.place_name)
        return ""

    def get_next_question_name(self):
        return WorldQuestionsNames.KeyLast

    def get_previous_question_name(self):
        return WorldQuestionsNames.KeyFour
