import config
import enum

if config.IS_LOCAL:
    import question_groups.abstract_group as abstract_group
    import question_groups.questions_types as questions_types
else:
    import questions.question_groups.abstract_group as abstract_group
    import questions.question_groups.questions_types as questions_types


class FoodQuestionsNames(enum.Enum):
    KeyZero = questions_types.ZERO_QUESTION
    KeyFirst = 1
    KeySecond = 2
    KeyThird = 3
    KeyThirdOne = 3.1
    KeyThirdOneOne = 3.11
    KeyFour = 4
    KeyFourOne = 4.1
    KeyFourTwo = 4.2
    KeyFive = 5
    KeySix = 6
    KeySeven = 7
    KeyEight = 8
    KeyNine = 9
    KeyLast = questions_types.LAST_QUESTION
    KeyCount = 9


class FoodQuestionGroup(abstract_group.QuestionGroup):
    def __init__(self, asking_type, place_name):
        super().__init__(asking_type)
        self.question_name = FoodQuestionsNames.KeyFirst
        self.place_name = place_name

    def get_short_name(self):
        return "Еда"

    def get_questions_count(self):
        return FoodQuestionsNames.KeyCount.value

    def make_new_question(self):
        if self.question_name == FoodQuestionsNames.KeyFirst:
            return FoodQuestionFirst(self.asking_type, self.place_name)
        if self.question_name == FoodQuestionsNames.KeySecond:
            return FoodQuestionSecond(self.asking_type)
        if self.question_name == FoodQuestionsNames.KeyThird:
            return FoodQuestionThird(self.asking_type)
        if self.question_name == FoodQuestionsNames.KeyThirdOne:
            return FoodQuestionThirdOne(self.asking_type)
        if self.question_name == FoodQuestionsNames.KeyThirdOneOne:
            return FoodQuestionThirdOneOne(self.asking_type)
        if self.question_name == FoodQuestionsNames.KeyFour:
            return FoodQuestionFour(self.asking_type)
        if self.question_name == FoodQuestionsNames.KeyFourOne:
            return FoodQuestionFourOne(self.asking_type)
        if self.question_name == FoodQuestionsNames.KeyFourTwo:
            return FoodQuestionFourTwo(self.asking_type)
        if self.question_name == FoodQuestionsNames.KeyFive:
            return FoodQuestionFive(self.asking_type)
        if self.question_name == FoodQuestionsNames.KeySix:
            return FoodQuestionSix(self.asking_type)
        if self.question_name == FoodQuestionsNames.KeySeven:
            return FoodQuestionSeven(self.asking_type)
        if self.question_name == FoodQuestionsNames.KeyEight:
            return FoodQuestionEight(self.asking_type)
        if self.question_name == FoodQuestionsNames.KeyNine:
            return FoodQuestionNine(self.asking_type)
        return None

    def process(self):
        complains_list = []
        for question in self.questions_array.values():
            complain = question.process()
            if complain != "":
                complains_list.append(complain)

        # сколько жалоб по поводу воды
        counter_complains_water = 0
        answer_eight = self.get_choice(FoodQuestionsNames.KeyEight)
        if answer_eight and answer_eight != 0:
            counter_complains_water += 1
        answer_nine = self.get_choice(FoodQuestionsNames.KeyNine)
        if answer_nine and answer_nine != 3:
            counter_complains_water += 1

        # количество жалоб по последним двум вопросам (то есть, по воде) больше,
        # чем количество жалоб в принципе, значит были жалобы на еду, и надо добавлять еще абзац
        if len(complains_list) > counter_complains_water:
            new_complain = "Правила Нельсона Манделы содержат положения, которые касаются питания. В частности, " \
                           "согласно пункту 1 правила 22 мне должны были предоставить пищу, которая должна была " \
                           "быть достаточно питательной для поддержания здоровья и сил, имеющей достаточно хорошее " \
                           "качество, хорошо приготовленной и поданной. Указанные выше условия нарушают данное Правило."
            complains_list.insert(len(complains_list) - counter_complains_water, new_complain)

        answer_first = self.get_choice(FoodQuestionsNames.KeyFirst)
        if len(complains_list) > 0:
            if answer_first == 1:
                new_complain = "Кроме того, на протяжении всего времени содержания в {} нарушались и " \
                               "другие мои права, касающиеся питания.".format(self.place_name)
                complains_list.insert(1, new_complain)
            else:
                new_complain = "На протяжении всего времени содержания в {} нарушались мои права, " \
                               "касающиеся питания.".format(self.place_name)
                complains_list.insert(0, new_complain)

        return complains_list


class FoodQuestionFirst(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Кормили ли вас в день поступления в ЦИП / ИВС?"
        answers = [
            "1. Да",
            "2. Нет",
            "3. Я отказался(-ась) от приема пищи"
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 1:
            return "Во время моего содержания в {} были нарушены мои права, " \
                   "касающиеся питания. В день поступления мне не предоставили пищу. " \
                   "Голод угнетал меня и отбирал силы.".format(self.place_name)
        return ""

    def get_next_question_name(self):
        return FoodQuestionsNames.KeySecond

    def get_previous_question_name(self):
        return FoodQuestionsNames.KeyZero


class FoodQuestionSecond(questions_types.GetChoiceQuestion):
    def __init__(self, asking_type):
        text = "Устраивало ли вас качество пищи?"
        answers = [
            "1. Да",
            "2. Нет (Вы можете сами уточнить причины в готовом тексте жалобы)",
            "3. Я отказался(-ась) от приема пищи"
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 1:
            return "Качество пищи, которой меня кормили, было низким. Из-за этого мне было " \
                   "неприятно ее есть, и мое здоровье было поставлено под угрозу. "
        return ""

    def get_next_question_name(self):
        if self.get_choice() == 2:
            return FoodQuestionsNames.KeyEight
        return FoodQuestionsNames.KeyThird

    def get_previous_question_name(self):
        return FoodQuestionsNames.KeyFirst


class FoodQuestionThird(questions_types.GetChoiceQuestion):
    def __init__(self, asking_type):
        text = "Были ли учтены индивидуальные особенности питания " \
               "(предоставлялась ли вегетарианская, постная, диабетическая или другая пища)?"
        answers = [
            "1. Да",
            "2. У меня нет особенностей питания",
            "3. Нет"
        ]
        super().__init__(text, answers, asking_type)

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 2:
            return FoodQuestionsNames.KeyThirdOne

        return FoodQuestionsNames.KeyFour

    def get_previous_question_name(self):
        return FoodQuestionsNames.KeySecond


class FoodQuestionThirdOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        # TODO пока что можно выбрать только один вариант, но надо ли заморачиваться, чтобы
        # TODO добавить "другая пища"
        text = "Какая именно пища вам не предоставлялась?\n"

        self.food_types = ["Вегетарианская",
                           "Веганская",
                           "Халяльная",
                           "Кошерная",
                           "Постная",
                           "Диабетическая",
                           ]

        answers = [str(i + 1) + ". " + self.food_types[i] + " пища" for i in range(len(self.food_types))]
        answers.append("7. Иной вариант")

        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        # 7. Иной вариант
        if choice == len(self.food_types):
            return ""

        food_type = self.food_types[choice].lower()
        return "При предоставлении еды не были учтены особенности моего питания. Так, мне не " \
               "предоставлялась {} пища. Вследствие этого у меня не было возможности " \
               "полноценно питаться, получать необходимое количество калорий и необходимых организму " \
               "витаминов и микроэлементов.".format(food_type)

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == len(self.food_types):
            return FoodQuestionsNames.KeyThirdOneOne
        return FoodQuestionsNames.KeyFour

    def get_previous_question_name(self):
        return FoodQuestionsNames.KeySecond


class FoodQuestionThirdOneOne(questions_types.GetNumberQuestion):

    def __init__(self, asking_type):
        # TODO пока что можно выбрать только один вариант, но надо ли заморачиваться, чтобы
        # TODO добавить "другая пища"
        text = "Какая именно пища вам не предоставлялась? \n" \
               "(введите прилагательное в именительном падеже с маленькой буквы через пробел)\n"

        super().__init__(text, asking_type)

    def process(self):
        choice = self.get_choice()
        return "При предоставлении еды не были учтены особенности моего питания. Так, мне не " \
               "предоставлялась " + str(choice) + \
               " пища. Вследствие этого у меня не было возможности " \
               "полноценно питаться, получать необходимое количество калорий и необходимых организму " \
               "витаминов и микроэлементов."

    def get_next_question_name(self):
        return FoodQuestionsNames.KeyFour

    def get_previous_question_name(self):
        return FoodQuestionsNames.KeyThirdOne


class FoodQuestionFour(questions_types.GetChoiceQuestion):
    def __init__(self, asking_type):
        text = "Сколько приемов пищи __*обычно*__ было в течение дня?"
        answers = [
            "1. Меньше трех",
            "2. Три или больше"
        ]
        super().__init__(text, answers, asking_type)

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 0:
            return FoodQuestionsNames.KeyFourOne
        else:
            return FoodQuestionsNames.KeyFourTwo

    def get_previous_question_name(self):
        return FoodQuestionsNames.KeyThird


class FoodQuestionFourOne(questions_types.GetChoiceQuestion):
    def __init__(self, asking_type):
        text = "Было ли вам __*обычно*__ достаточно пищи, предоставленной в течение дня?"
        answers = [
            "1. Да",
            "2. Нет"
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        asking_type = self.asking_type
        if choice == 0:
            if asking_type == abstract_group.AskingType.BeforeCourt:
                return "Обычно меня кормили менее трех раз в день. Такое обращение нарушает пункт 114 Правил № 996."
            elif asking_type == abstract_group.AskingType.AfterCourt:
                return "Обычно меня кормили менее трех раз в день, что не было достаточным для меня " \
                       "и что в среднем является недостаточным для человека."
            else: # abstract_group.AskingType.BothSides:
                return "Обычно меня кормили менее трех раз в день, что не было достаточным для меня и что в " \
                       "среднем является недостаточным для человека. Такое обращение нарушает пункт 114 Правил" \
                       " № 996, из которого следует, что должно быть три приема пищи в день."
        else:  # choice == 1
            if asking_type == abstract_group.AskingType.BeforeCourt:
                return "Обычно меня кормили менее трех раз в день. Такое обращение нарушает пункт 114 Правил № 996. " \
                       "Мне не хватало пищи, предоставленной в течение дня. Такое питание не позволяло мне " \
                       "полноценно поддерживать свое здоровье и силы."
            elif asking_type == abstract_group.AskingType.AfterCourt:
                return "Обычно меня кормили менее трех раз в день. Мне не хватало пищи, предоставленной в течение " \
                       "дня. Такое питание не позволяло мне полноценно поддерживать свое здоровье и силы."
            else:  # abstract_group.AskingType.BothSides
                return "Обычно меня кормили менее трех раз в день. Такое обращение нарушает пункт 114 Правил № 996," \
                       " из которого следует, что должно быть три приема пищи в день. Мне не хватало пищи, " \
                       "предоставленной в течение дня. Такое питание не позволяло мне полноценно поддерживать " \
                       "свое здоровье и силы."

    def get_next_question_name(self):
        return FoodQuestionsNames.KeyFive

    def get_previous_question_name(self):
        # TODO тут можно что-то умнее придумать
        return FoodQuestionsNames.KeyFour


class FoodQuestionFourTwo(questions_types.GetChoiceQuestion):
    def __init__(self, asking_type):
        text = "Было ли вам __*обычно*__ достаточно пищи, предоставленной в течение дня?"
        answers = [
            "1. Да",
            "2. Нет"
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 1:
            return "Обычно меня кормили три раза в день, но мне не хватало пищи, предоставленной в течение дня. " \
                   "Такое питание не позволяло мне полноценно поддерживать свое здоровье и силы."
        return ""

    def get_next_question_name(self):
        return FoodQuestionsNames.KeyFive

    def get_previous_question_name(self):
        # TODO тут можно что-то умнее придумать
        return FoodQuestionsNames.KeyThird


class FoodQuestionFive(questions_types.GetChoiceQuestion):
    def __init__(self, asking_type):
        text = "Было ли достаточно времени, отведенного для приема пищи?"
        answers = [
            "1. Да",
            "2. Не всегда",
            "3. Нет"
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 1:
            return "Мне не всегда хватало отведенного времени, чтобы закончить прием пищи. Из-за этого не " \
                   "всегда получалось доесть порцию, что, в свою очередь, вызывало голод."
        if choice == 2:
            return "Мне постоянно не хватало отведенного времени, чтобы закончить прием пищи. Из-за этого не всегда " \
                   "получалось доесть порцию, что, в свою очередь, постоянно вызывало голод."
        return ""

    def get_next_question_name(self):
        return FoodQuestionsNames.KeySix

    def get_previous_question_name(self):
        return FoodQuestionsNames.KeyFour


class FoodQuestionSix(questions_types.GetChoiceQuestion):
    def __init__(self, asking_type):
        text = "Были ли в наличии столовые приборы и посуда?"
        answers = [
            "1. Да",
            "2. Приборов не хватало",
            "3. Посуды не хватало",
            "4. Не хватало и приборов, и посуды"
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 1:
            return "Столовых приборов не хватало. Такие условия приема пищи являются антисанитарными."
        if choice == 2:
            return "Для потребления пищи не хватало посуды. Такие условия приема пищи являются антисанитарными."
        if choice == 3:
            return "Для потребления пищи не хватало посуды и приборов. " \
                   "Такие условия приема пищи являются антисанитарными."
        return ""

    def get_next_question_name(self):
        return FoodQuestionsNames.KeySeven

    def get_previous_question_name(self):
        return FoodQuestionsNames.KeyFive


class FoodQuestionSeven(questions_types.GetChoiceQuestion):
    def __init__(self, asking_type):
        text = "Были ли чистыми посуда и столовые приборы?"
        answers = [
            "1. Да, всегда",
            "2. Не всегда",
            "3. Нет"
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 1:
            return "Посуда и столовые приборы не всегда были чистыми. Это значит, что нарушались санитарные нормы. " \
                   "Более того, грязная посуда и приборы вызывают чувство отвращения и нежелание есть в таких условиях."
        if choice == 2:
            return "Посуда и столовые приборы всегда были грязными. Это значит, что нарушались санитарные нормы. " \
                   "Более того, грязная посуда и приборы вызывают чувство отвращения и нежелание есть " \
                   "в таких условиях."
        return ""

    def get_next_question_name(self):
        return FoodQuestionsNames.KeyEight

    def get_previous_question_name(self):
        return FoodQuestionsNames.KeySix


# TODO необходимо добавить фразу из документа про правила Н. Монделлы


class FoodQuestionEight(questions_types.GetChoiceQuestion):
    def __init__(self, asking_type):
        text = "Был ли у вас доступ к водопроводной или питьевой воде?"
        answers = [
            "1. Да",
            "2. Да, но в ограниченном количестве",
            "3. Не всегда",
            "4. Нет"
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        asking_type = self.asking_type
        if choice == 1:
            if asking_type == abstract_group.AskingType.BeforeCourt:
                return "У меня был доступ к питьевой воде лишь в ограниченном количестве. Это нарушает пункты 47 и " \
                       "49 Правил № 996. Жажда доставляла значительный дискомфорт, мысли часто занимала " \
                       "невозможность попить. Кроме того, достаточное количество питьевой воды является необходимым " \
                       "для полноценного функционирования организма. Недостаток воды может негативно сказываться на " \
                       "здоровье. Согласно пункту 2 правила 22 Правил Нельсона Манделы должен быть обеспечен доступ " \
                       "к питьевой воде, когда возникает такая потребность."
            elif asking_type == abstract_group.AskingType.BothSideOfCourt:
                return "У меня был доступ к питьевой воде лишь в ограниченном количестве. Это нарушает пункты 47 " \
                       "и 49 Правил № 996, а также пункт 37 Правил № 313. Жажда доставляла значительный " \
                       "дискомфорт, мысли часто занимала невозможность попить. Кроме того, достаточное количество" \
                       " питьевой воды является необходимым для полноценного функционирования организма. Недостаток" \
                       " воды может негативно сказываться на здоровье. Согласно пункту 2 правила 22 Правил Нельсона" \
                       " Манделы должен быть обеспечен доступ к питьевой воде, когда возникает такая потребность."
            else:  # asking_type == abstract_group.AskingType.AfterCourt
                return "У меня не всегда был доступ к питьевой воде. Это нарушает пункт 37 Правил № 313. " \
                       "Жажда доставляла значительный дискомфорт, мысли часто занимала невозможность попить. " \
                       "Кроме того, достаточное количество питьевой воды является необходимым для полноценного " \
                       "функционирования организма. Недостаток воды может негативно сказываться на здоровье. " \
                       "Согласно пункту 2 правила 22 Правил Нельсона Манделы должен быть обеспечен доступ к " \
                       "питьевой воде, когда возникает такая потребность."

        if choice == 2:
            if asking_type == abstract_group.AskingType.BeforeCourt:
                return "У меня не всегда был доступ к питьевой воде. Это нарушает пункты 47 и 49 Правил № 996. " \
                       "Жажда доставляла значительный дискомфорт, мысли часто занимала невозможность попить. " \
                       "Кроме того, достаточное количество питьевой воды является необходимым для полноценного " \
                       "функционирования организма. Недостаток воды может негативно сказываться на здоровье. " \
                       "Согласно пункту 2 правила 22 Правил Нельсона Манделы должен быть обеспечен доступ к  " \
                       "питьевой воде, когда возникает такая потребность."
            elif asking_type == abstract_group.AskingType.BothSideOfCourt:
                return "У меня не всегда был доступ к питьевой воде. Это нарушает пункты 47 и 49 Правил № 996, " \
                       "а также пункт 37 Правил № 313. Жажда доставляла значительный дискомфорт, мысли часто" \
                       " занимала невозможность попить. Кроме того, достаточное количество питьевой воды является" \
                       " необходимым для полноценного функционирования организма. Недостаток воды может негативно" \
                       " сказываться на здоровье. Согласно пункту 2 правила 22 Правил Нельсона Манделы должен быть" \
                       " обеспечен доступ к питьевой воде, когда возникает такая потребность."
            else:  # asking_type == abstract_group.AskingType.AfterCourt
                return "У меня не всегда был доступ к питьевой воде. Это нарушает пункт 37 Правил № 313. Жажда " \
                       "доставляла значительный дискомфорт, мысли часто занимала невозможность попить. Кроме того, " \
                       "достаточное количество питьевой воды является необходимым для полноценного функционирования " \
                       "организма. Недостаток воды может негативно сказываться на здоровье. Согласно пункту 2 правила " \
                       "22 Правил Нельсона Манделы должен быть обеспечен доступ к питьевой воде, когда возникает " \
                       "такая потребность."

        if choice == 3:
            if asking_type == abstract_group.AskingType.BeforeCourt:
                return "У меня не было доступа к питьевой воде. Это нарушает пункты 47 и 49 Правил № 996. " \
                       "Жажда доставляла значительный дискомфорт, мысли часто занимала невозможность попить. " \
                       "Кроме того, достаточное количество питьевой воды является необходимым для полноценного " \
                       "функционирования организма. Недостаток воды может негативно сказываться на здоровье. " \
                       "Согласно пункту 2 правила 22 Правил Нельсона Манделы должен быть обеспечен доступ к " \
                       "питьевой воде, когда возникает такая потребность."
            elif asking_type == abstract_group.AskingType.BothSideOfCourt:
                return "У меня не было доступа к питьевой воде. Это нарушает пункты пункты 47 и 49 Правил № 996, " \
                       "а также пункт 37 Правил № 313. Жажда доставляла значительный дискомфорт, мысли часто занимала" \
                       " невозможность попить. Кроме того, достаточное количество питьевой воды является необходимым" \
                       " для полноценного функционирования организма. Согласно пункту 2 правила 22 Правил Нельсона" \
                       " Манделы должен быть обеспечен доступ к питьевой воде, когда возникает такая потребность."
            else:  # asking_type == abstract_group.AskingType.AfterCourt
                return "У меня не было доступа к питьевой воде. Это нарушает пункт 37 Правил № 313. Жажда " \
                       "доставляла значительный дискомфорт, мысли часто занимала невозможность попить. Кроме того, " \
                       "достаточное количество питьевой воды является необходимым для полноценного функционирования " \
                       "организма. Недостаток воды может негативно сказываться на здоровье. Согласно пункту 2 правила " \
                       "22 Правил Нельсона Манделы должен быть обеспечен доступ к питьевой воде, когда возникает " \
                       "такая потребность."
        return ""

    def get_next_question_name(self):
        return FoodQuestionsNames.KeyNine

    def get_previous_question_name(self):
        return FoodQuestionsNames.KeySeven


class FoodQuestionNine(questions_types.GetChoiceQuestion):
    def __init__(self, asking_type):
        text = "Привлекали ли вас к раздаче пищи или мытью посуды?"
        answers = [
            "1. Привлекали к раздаче пищи",
            "2. Привлекали к мытью посуды",
            "3. Привлекали к раздаче пищи и мытью посуды",
            "4. Нет"
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        asking_type = self.asking_type
        if choice == 0:
            if asking_type == abstract_group.AskingType.BeforeCourt:
                return "Меня привлекали к раздаче пищи. Это является принудительным трудом, что запрещено " \
                       "статьей 41 Конституции."
            elif asking_type == abstract_group.AskingType.BothSideOfCourt:
                return "Меня привлекали к раздаче пищи. Это является принудительным трудом, что запрещено " \
                       "статьей 41 Конституции. Кроме того, это нарушает пункт 107 Правил № 313."
            else:  # asking_type == abstract_group.AskingType.AfterCourt
                return "Меня привлекали к раздаче пищи, что прямо нарушает пункт 107 Правил № 313. Кроме того, " \
                       "это является принудительным трудом, что запрещено статьей 41 Конституции."
        if choice == 1:
            if asking_type == abstract_group.AskingType.BeforeCourt:
                return "Меня привлекали к мытью столовой посуды. Это является принудительным трудом, что запрещено " \
                       "статьей 41 Конституции."
            elif asking_type == abstract_group.AskingType.BothSideOfCourt:
                return "Меня привлекали к мытью столовой посуды. Это является принудительным трудом, что " \
                       "запрещено статьей 41 Конституции. Кроме того, это нарушает пункт 107 Правил № 313."
            else:  # asking_type == abstract_group.AskingType.AfterCourt
                return "Меня привлекали к мытью столовой посуды, что прямо нарушает пункт 107 Правил № 313. " \
                       "Кроме того, это является принудительным трудом, что запрещено статьей 41 Конституции."
        if choice == 2:
            if asking_type == abstract_group.AskingType.BeforeCourt:
                return "Меня привлекали к раздаче пищи и мытью столовой посуды. Это является принудительным трудом, " \
                       "что запрещено статьей 41 Конституции."
            elif asking_type == abstract_group.AskingType.BothSideOfCourt:
                return "Меня привлекали к раздаче пищи и мытью столовой посуды. Это является принудительным трудом, " \
                       "что запрещено статьей 41 Конституции. Кроме того, это нарушает пункт 107 Правил № 313."
            else:  # asking_type == abstract_group.AskingType.AfterCourt
                return "Меня привлекали к раздаче пищи и мытью столовой посуды, что прямо нарушает пункт 107 " \
                       "Правил № 313. Кроме того, это является принудительным трудом, что запрещено статьей 41 " \
                       "Конституции."

        return ""

    def get_next_question_name(self):
        return FoodQuestionsNames.KeyLast

    def get_previous_question_name(self):
        return FoodQuestionsNames.KeyEight
