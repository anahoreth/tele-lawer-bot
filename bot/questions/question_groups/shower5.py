import config
import enum

if config.IS_LOCAL:
    import question_groups.abstract_group as abstract_group
    import question_groups.questions_types as questions_types
else:
    import questions.question_groups.abstract_group as abstract_group
    import questions.question_groups.questions_types as questions_types


class ShowerQuestionsNames(enum.Enum):
    KeyZero = questions_types.ZERO_QUESTION
    KeyFirst = 1  # 1. Была ли возможность принять душ при приеме?
    KeySecond = 2  # 2. Была ли возможность принять душ раз в неделю?
    KeySecondOneOne = 2.11  # 2.11 Обычно душ длился не менее 15 минут?
    KeySecondTwoOne = 2.21  # 2.21 Как часто вам давали пользоваться душем?
    KeySecondTwoTwo = 2.22  # 2.22 Обычно душ длился не менее 15 минут?
    KeyThree = 3  # 3. Выдавались ли вам на каждые три дня
    KeyLast = questions_types.LAST_QUESTION
    KeyCount = 3


class ShowerQuestionGroup(abstract_group.QuestionGroup):
    def __init__(self, asking_type, place_name, prison_days):

        self.place_name = place_name
        super().__init__(asking_type)
        self.prison_days = prison_days

        self.question_name = None

        if self.asking_type.value == abstract_group.AskingType.BeforeCourt.value:
            self.question_name = ShowerQuestionsNames.KeyThree
        else: #After, Both
            self.question_name = ShowerQuestionsNames.KeyFirst

    def get_short_name(self):
        return "Отсутствие доступа к душу или средствам личной гигиены"

    def get_questions_count(self):
        return ShowerQuestionsNames.KeyCount.value

    def make_new_question(self):
        if self.question_name == ShowerQuestionsNames.KeyFirst:
            return ShowerQuestionFirst(self.asking_type, self.prison_days, self.place_name)
        if self.question_name == ShowerQuestionsNames.KeySecond:
            return ShowerQuestionSecond(self.asking_type)
        if self.question_name == ShowerQuestionsNames.KeySecondOneOne:
            return ShowerQuestionSecondOneOne(self.asking_type, self.place_name)
        if self.question_name == ShowerQuestionsNames.KeySecondTwoOne:
            return ShowerQuestionSecondTwoOne(self.asking_type, self.place_name)
        if self.question_name == ShowerQuestionsNames.KeySecondTwoTwo:
            return ShowerQuestionSecondTwoTwo(self.asking_type)
        if self.question_name == ShowerQuestionsNames.KeyThree:
            return ShowerQuestionThree(self.asking_type, self.place_name)
        return None

    def process(self):
        complains_list = []
        for question in self.questions_array.values():
            complain = question.process()
            if complain == "":
                continue
            if complain[0] != " ":
                complains_list.append(complain)
            else:
                complains_list[len(complains_list) - 1] += complain

        return complains_list


class ShowerQuestionFirst(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type, prison_days, place_name):
        self.place_name = place_name
        text = "Была ли возможность принять душ при приеме в ЦИП / ИВС?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)
        self.prison_days = prison_days

    def process(self):
        choice = self.get_choice()
        if choice != 1:
            return ""
        if self.asking_type.value == abstract_group.AskingType.AfterCourt.value:
            return "При доставлении в {} мне не была предоставлена возможность помыться в душевой. " \
                   "Это нарушает пункт 104 Правил № 313.".format(self.place_name)
        # self.asking_type.value == abstract_group.AskingType.Both.value
        return "При доставлении в {} для отбывания административного ареста мне не была предоставлена возможность" \
               " помыться в душевой. Это нарушает пункт 104 Правил № 313.".format(self.place_name)

    def get_next_question_name(self):
        if self.prison_days >= 7:
            return ShowerQuestionsNames.KeySecond
        return ShowerQuestionsNames.KeyThree

    def get_previous_question_name(self):
        return MedQuestionsNames.KeyZero


class ShowerQuestionSecond(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Была ли возможность принять душ раз в неделю?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def get_next_question_name(self):
        if self.choice == 0:
            return ShowerQuestionsNames.KeySecondOneOne
        elif self.choice == 1:
            return ShowerQuestionsNames.KeySecondTwoOne
        return ShowerQuestionsNames.KeyLast

    def get_previous_question_name(self):
        return MedQuestionsNames.KeyFirst


class ShowerQuestionSecondOneOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Обычно душ длился не менее 15 минут?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        if self.choice != 1:
            return ""
        if self.asking_type.value == abstract_group.AskingType.AfterCourt.value:
            return "Во время содержания в {} возможность помыться в душевой предоставлялась мне раз в неделю, " \
                   "но продолжительность помывки обычно была меньше 15 минут. " \
                   "Это нарушает пункт 44 Правил № 313.".format(self.place_name)
        # self.asking_type.value == abstract_group.AskingType.Both.value
        return "Во время отбывания административного ареста в {} возможность помыться в душевой предоставлялась" \
               " мне раз в неделю, но продолжительность помывки обычно была меньше 15 минут. " \
               "Это нарушает пункт 44 Правил № 313.".format(self.place_name)

    def get_next_question_name(self):
        return ShowerQuestionsNames.KeyThree

    def get_previous_question_name(self):
        return MedQuestionsNames.KeyFirst


class ShowerQuestionSecondTwoOne(questions_types.GetNumberQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Как часто вам давали пользоваться душем?\n(введите ответ с маленькой буквы в формате: " \
               "«раз в девять дней»  или «ни разу»)"
        super().__init__(text, asking_type)

    def process(self):
        if self.asking_type.value == abstract_group.AskingType.AfterCourt.value:
            return 'Во время содержания в {} возможность принять душ была предоставлена мне не раз в неделю, ' \
                   'как того требует законодательство, а {}, что нарушает пункт 44 ' \
                   'Правил № 313. '.format(self.place_name, self.choice)
        # self.asking_type.value == abstract_group.AskingType.Both.value
        return 'Во время отбывания административного ареста в {} возможность принять душ была предоставлена' \
               ' мне не раз в неделю, как того требует законодательство, а {}, ' \
               'что нарушает пункт 44 Правил № 313. '.format(self.place_name, self.choice)

    def get_next_question_name(self):
        return ShowerQuestionsNames.KeySecondTwoTwo

    def get_previous_question_name(self):
        return ShowerQuestionsNames.KeySecond


class ShowerQuestionSecondTwoTwo(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Обычно душ длился не менее 15 минут?"
        answers = [
            "1. Да",
            "2. Нет",
            "3. Меня не водили в душ",
        ]

        super().__init__(text, answers, asking_type)

    def process(self):
        # здесь не зависит от того, "после суда" или "до и после суда"
        last_par = ' Кроме того, согласно правилу 16 Правил Нельсона Манделы необходимо обеспечивать такие ' \
               'условия, в которых каждый может купаться или принимать душ хотя бы раз в неделю в умеренном ' \
               'климате.'
        if self.choice == 0:
            return last_par
        elif self.choice == 1:
            new_complain = "Также продолжительность помывки была меньше 15 минут, что нарушает пункт 44 Правил № 313."
            return last_par + new_complain
        return ""

    def get_next_question_name(self):
        return ShowerQuestionsNames.KeyThree

    def get_previous_question_name(self):
        return ShowerQuestionsNames.KeySecond


class ShowerQuestionThree(questions_types.GetChoiceQuestion):

    names_long = [
        "20 грамм хозяйственного мыла",
        "2,5 метра туалетной бумаги",
        "5 грамм туалетного мыла – для мужчин, 10 грамм – для женщин",
        "Средства личной гигиены для женщин – 6 штук тампонов или прокладок",
        "Всё выдавалось",
    ]

    names_short = [
        "хозяйственного мыла",
        "туалетной бумаги",
        "туалетного мыла",
        "средств личной гигиены для женщин"
    ]

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Отметьте, что из этого *НЕ* выдавалось вам на каждые три дня:"
        answers = [str(i + 1) + ". " + self.names_long[i] for i in range(len(self.names_long))]

        super().__init__(text, answers, asking_type,
                         questions_types.QuestionAnswerType.ANSWER_QUERY_MANY)

    def process(self):
        choice = self.get_choice()
        if not choice:
            return ""
        if 4 in choice:
            return ""
        choice_str = [self.names_short[i] for i in choice]
        list_of_complains = ", ".join(choice_str)

        if self.asking_type.value == abstract_group.AskingType.BeforeCourt.value:
            return "Во время содержания в {} в нарушение Норм обеспечения задержанных лиц средствами " \
                       "личной гигиены (Приложение 4 к Правилам № 996) мне не выдавалось " \
                   "необходимого количества {}.".format(self.place_name, list_of_complains)
        if self.asking_type.value == abstract_group.AskingType.AfterCourt.value:
            return "Во время содержания в {} в нарушение Норм обеспечения административно арестованных " \
                   "средствами личной гигиены  (Приложение 4 к Правилам № 313) мне не выдавалось необходимого " \
                   "количества {}.".format(self.place_name, list_of_complains)
        # if self.asking_type.value == abstract_group.AskingType.AfterCourt.value:
        return "Во время содержания в {} в нарушение Норм обеспечения задержанных лиц средствами личной " \
               "гигиены (Приложение 4 к Правилам № 996 и Приложение 4 к Правилам № 313) мне не " \
               "выдавалось необходимого количества {}.".format(self.place_name, list_of_complains)

    def get_next_question_name(self):
        return ShowerQuestionsNames.KeyLast

    def get_previous_question_name(self):
        return ShowerQuestionsNames.KeySecond
