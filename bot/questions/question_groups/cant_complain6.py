import config
import enum

if config.IS_LOCAL:
    import question_groups.abstract_group as abstract_group
    import question_groups.questions_types as questions_types
else:
    import questions.question_groups.abstract_group as abstract_group
    import questions.question_groups.questions_types as questions_types


def get_text_before_global(place_name):
    return "Согласно части первой статьи 60 Конституции каждому гарантируется защита его прав и свобод " \
                   "компетентным, независимым и беспристрастным судом в определенные законом сроки. " \
           "Однако мое право " \
                   "на защиту суда было нарушено. Так, вследствие действий сотрудников {} у меня не получилось " \
                   "подать жалобу на постановление об административном нарушении.".format(place_name)


class CantQuestionsNames(enum.Enum):
    KeyZero = questions_types.ZERO_QUESTION
    KeyFirst = 1  # Разъяснялись ли вам права, обязанности и режим содержания?
    KeySecond = 2  # Была ли у вас возможность подать жалобу на решение суда об аресте?
    KeySecondThreeOne = 2.31  # Что вам помешало подать жалобу? (можно выбрать несколько вариантов ответа)
    KeyThree = 3  # Принимали ли сотрудники ваши жалобы на их действия или на условия содержания?
    KeyThreeOne = 3.1  # Я зафиксировала ваш ответ...
    KeyFour = 4  # Выдавали ли по вашей просьбе бумагу и письменные принадлежности
    KeyFive = 51  # Отправляли ли ваши жалобы в место назначения
    KeyLast = questions_types.LAST_QUESTION
    KeyCount = 5


class CantQuestionGroup(abstract_group.QuestionGroup):
    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        super().__init__(asking_type)
        self.question_name = CantQuestionsNames.KeyFirst

    def process(self):
        lst = [a for a in CantQuestionsNames]

        question_first_process = ""
        question_second_three_one_process = []
        text_before = []
        complains_list = []
        for question_name in lst:
            if question_name == CantQuestionsNames.KeyFirst:
                question = self.questions_array.get(question_name)
                if question:
                    question_first_process = self.questions_array.get(CantQuestionsNames.KeyFirst).process()
                continue
            if question_name == CantQuestionsNames.KeySecond:
                question = self.questions_array.get(question_name)
                if question:
                    process_result = self.questions_array.get(CantQuestionsNames.KeySecond).process()
                    if process_result:
                        text_before = [process_result]
                continue
            if question_name == CantQuestionsNames.KeySecondThreeOne:
                question = self.questions_array.get(question_name)
                if question:
                    question_second_three_one_process = \
                        self.questions_array.get(CantQuestionsNames.KeySecondThreeOne).process()
                continue

            question = self.questions_array.get(question_name)

            if question:
                complain = question.process()
                if complain:
                    complains_list.append(complain)

        if question_second_three_one_process:
            complains_list = text_before + question_second_three_one_process + complains_list

        if question_first_process:
            return [question_first_process] + complains_list

        return complains_list

    def get_short_name(self):
        return "Неразъяснение прав или отсутствие возможности жаловаться "

    def get_questions_count(self):
        return CantQuestionsNames.KeyCount.value

    def make_new_question(self):
        if self.question_name == CantQuestionsNames.KeyFirst:
            return CantQuestionFirst(self.asking_type, self.place_name)
        if self.question_name == CantQuestionsNames.KeySecond:
            return CantQuestionSecond(self.asking_type, self.place_name)
        if self.question_name == CantQuestionsNames.KeySecondThreeOne:
            return CantQuestionSecondThreeOne(self.asking_type)
        if self.question_name == CantQuestionsNames.KeyThree:
            return CantQuestionThree(self.asking_type, self.place_name)
        if self.question_name == CantQuestionsNames.KeyThreeOne:
            return CantQuestionThreeOne(self.asking_type)
        if self.question_name == CantQuestionsNames.KeyFour:
            return CantQuestionFour(self.asking_type)
        if self.question_name == CantQuestionsNames.KeyFive:
            return CantQuestionFive(self.asking_type, self.place_name)
        return None




class CantQuestionFirst(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Разъяснялись ли вам права, обязанности и режим содержания?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice != 1:
            return
        asking_type_value = self.asking_type.value
        if asking_type_value == abstract_group.AskingType.BeforeCourt.value:
            return "При приеме в {} мне не разъяснили права и обязанности, а также режим содержания. " \
                   "Это нарушает пункт 9 Правил № 996, согласно которому задержанных должны ознакомить с их " \
                   "правами и обязанностями, режимом содержания, порядком подачи ходатайств, предложений, " \
                   "заявлений и жалоб.".format(self.place_name)
        if asking_type_value == abstract_group.AskingType.BothSideOfCourt.value:
            return "При приеме в {} как во время задержания, так и при отбывании ареста мне не разъяснили " \
                   "права и обязанности, а также режим содержания. Это нарушает пункт 9 Правил № 996 и пункт 6 " \
                   "Правил № 313, согласно которым сотрудники {} обязаны ознакомить с правами и " \
                   "обязанностями, режимом содержания, порядком подачи ходатайств, предложений, " \
                   "заявлений и жалоб.".format(self.place_name, self.place_name)
        # abstract_group.AskingType.AfterCourt
        return "При приеме в {} мне не разъяснили права и обязанности, а также режим содержания. Это " \
                   "нарушает пункт 6 Правил № 313, согласно которому сотрудники {} должны ознакомить " \
                   "арестованных с их правами и обязанностями, режимом содержания, порядком подачи ходатайств, " \
                   "предложений, заявлений и жалоб.".format(self.place_name, self.place_name)
        return ""

    def get_next_question_name(self):
        if self.asking_type == abstract_group.AskingType.BeforeCourt:
            return CantQuestionsNames.KeyThree
        return CantQuestionsNames.KeySecond

    def get_previous_question_name(self):
        return CantQuestionsNames.KeyZero


class CantQuestionSecond(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Была ли у вас возможность подать жалобу на решение суда об аресте?"
        answers = [
            "1. Я не хотел(а) подавать жалобу",
            "2. Да",
            "3. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 2:
            return get_text_before_global(self.place_name)
        return ""

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == 2:
            return CantQuestionsNames.KeySecondThreeOne
        return CantQuestionsNames.KeyThree

    def get_previous_question_name(self):
        return CantQuestionsNames.KeyFirst


class CantQuestionSecondThreeOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Что вам помешало подать жалобу? (можно выбрать несколько вариантов ответа)"
        answers = [
            "1. Ко мне не пустили адвоката",
            "2. У меня не было бумаги и письменных принадлежностей, а администрация отказалась их предоставить",
            "3. Администрация отказалась отправить жалобу",
            "4. Иное (укажите самостоятельно в готовом тексте жалобы)",
        ]
        super().__init__(text, answers, asking_type, questions_types.QuestionAnswerType.ANSWER_QUERY_MANY)

    def process(self):
        choice = self.get_choice()
        result_process = []
        if 0 in choice:
            result_process.append("Для подготовки жалобы мне была необходима помощь адвоката. Исходя из статьи 62 Конституции, " \
                   "каждый имеет право на юридическую помощь. Противодействие оказанию правовой помощи в " \
                   "Республике Беларусь запрещается. Тем не менее, ко мне незаконно не пустили адвоката. Из-за " \
                   "этого у меня не получилось подготовить качественную жалобу.")
        if 1 in choice:
            result_process.append("У меня не было бумаги и письменных принадлежностей. Согласно пункту 43 Правил № 313 для написания" \
                   " ходатайств, предложений, заявлений и жалоб административно арестованным по их просьбе выдаются " \
                   "письменные принадлежности и бумага. Однако мне отказали в выдаче данных предметов. Из-за этого " \
                   "у меня не получилось написать жалобу.")
        if 2 in choice:
            result_process.append("Администрация отказалась отправить мою жалобу в суд. Согласно пункту 74 Правил № 313 жалобы на " \
                   "постановления по делу об административном правонарушении направляются в порядке, предусмотренном " \
                   "статьей 12.2 ПИКоАП. Таким образом, администрация обязана была направить мою жалобу в " \
                   "соответствующий суд. Тем не менее, жалоба не была направлена.")
        return result_process

    def get_next_question_name(self):
        return CantQuestionsNames.KeyThree

    def get_previous_question_name(self):
        return CantQuestionsNames.KeySecond


class CantQuestionThree(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Принимали ли сотрудники ваши жалобы на их действия или на условия содержания?"

        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        if self.get_choice() != 1:
            return ""

        asking_type = self.asking_type
        if asking_type.value == abstract_group.AskingType.BeforeCourt.value:
            return "Во время содержания в {} сотрудники не принимали мои жалобы. Исходя из пунктов 65 и 66 " \
                   "Правил № 996, мои жалобы, в том числе на условия содержания, должны были приниматься как в " \
                   "письменной, так и в устной форме.".format(self.place_name)
        if asking_type.value == abstract_group.AskingType.BothSideOfCourt.value:
            return "Во время содержания в {} сотрудники не принимали мои жалобы. Исходя из пунктов 65 и " \
                   "66 Правил № 996, а также пунктов 71 и 72 Правил № 313 мои жалобы, в том числе на условия " \
                   "содержания, должны были приниматься как в письменной," \
                   " так и в устной форме.".format(self.place_name)
        # abstract_group.AskingType.AfterCourt
        return "Во время содержания в {} сотрудники не принимали мои жалобы. Исходя из пунктов 71 и 72 " \
                   "Правил № 313, мои жалобы, в том числе на условия содержания, должны были приниматься " \
                   "сотрудниками {} как в письменной, " \
                   "так и в устной форме.".format(self.place_name, self.place_name)

    def get_next_question_name(self):
        if self.get_choice() == 1:
            return CantQuestionsNames.KeyThreeOne
        return CantQuestionsNames.KeyFour

    def get_previous_question_name(self):
        return CantQuestionsNames.KeySecond


class CantQuestionThreeOne(questions_types.JustInfoQuestion):

    def __init__(self, asking_type):
        text = "_Я зафиксировала ваш ответ, но рекомендую самостоятельно уточнить, " \
               "как именно сотрудники реагировали на ваши жалобы_"
        super().__init__(text, asking_type)

    def get_next_question_name(self):
        return CantQuestionsNames.KeyFour

    def get_previous_question_name(self):
        return CantQuestionsNames.KeyThree


class CantQuestionFour(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Выдавали ли по вашей просьбе бумагу и письменные принадлежности для написания заявлений или жалоб " \
               "(кроме жалобы на арест)?"
        answers = [
            "1. Да",
            "2. Нет",
            "3. Не просил(а)",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice != 1:
            return ""
        if self.asking_type.value == abstract_group.AskingType.BeforeCourt.value:
            return "У меня не было бумаги и письменных принадлежностей для подготовки заявлений и жалоб. Это " \
                   "нарушает пункт 52 Правил № 996, согласно которому для написания ходатайств, предложений, " \
                   "заявлений и жалоб задержанным лицам по их просьбе выдаются письменные принадлежности и " \
                   "бумага. Однако мне было отказано в выдаче данных предметов. Из-за этого у меня не получилось " \
                   "жаловаться в письменном виде."
        if self.asking_type.value == abstract_group.AskingType.BothSideOfCourt.value:
            return "У меня не было бумаги и письменных принадлежностей для подготовки заявлений и жалоб. Это " \
                   "нарушает пункт 52 Правил № 996 и пункт 43 Правил № 313, согласно которым для написания " \
                   "ходатайств, предложений, заявлений и жалоб задержанным лицам и административно арестованным" \
                   " по просьбе выдаются письменные принадлежности и бумага. Однако мне было отказано в выдаче" \
                   " данных предметов. Из-за этого у меня не получилось жаловаться в письменном виде."
        # abstract_group.AskingType.AfterCourt
        return "У меня не было бумаги и письменных принадлежностей для подготовки заявлений и жалоб. Это " \
                   "нарушает пункт 43 Правил № 313, согласно которому для написания ходатайств, предложений, " \
                   "заявлений и жалоб административно арестованным по их просьбе выдаются письменные " \
                   "принадлежности и бумага. Однако мне было отказано в выдаче данных предметов. Из-за этого у " \
                   "меня не получилось жаловаться в письменном виде."

        return ""

    def get_next_question_name(self):
        return CantQuestionsNames.KeyFive

    def get_previous_question_name(self):
        return CantQuestionsNames.KeyThree


class CantQuestionFive(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Отправляли ли ваши жалобы в место назначения (кроме жалобы на постановление суда)?"
        answers = [
            "1. Да",
            "2. Нет",
            "3. Не знаю",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice != 1:
            return ""
        if self.asking_type.value == abstract_group.AskingType.BeforeCourt.value:
            return "Во время содержания в {} администрация не отправила адресату мои жалобы. Это нарушает " \
                   "пункт 3 правила 56 Правил Нельсона Манделы, согласно которому каждый должен иметь возможность" \
                   " обращаться к центральным органам тюремного управления и судебным или иным компетентным " \
                   "органам, включая те, что уполномочены пересматривать дело или принимать меры по исправлению " \
                   "положения, с заявлениями или жалобами по поводу обращения с ним или с ней, которые не должны " \
                   "подвергаться цензуре.".format(self.place_name)
        if self.asking_type.value == abstract_group.AskingType.BothSideOfCourt.value:
            return "Во время содержания в {} администрация не отправила адресату мои жалобы. Исходя из " \
                   "пункта 72 Правил № 313, жалобы должны быть направлены начальником {} соответствующему " \
                   "адресату. Согласно пункту 79 Правил № 313 о направлении ходатайства, предложения, заявления" \
                   " или жалобы объявляется административно арестованному, подавшему их. Кроме того, это нарушает" \
                   " пункт 3 правила 56 Правил Нельсона Манделы, согласно которому каждый должен иметь возможность" \
                   " обращаться к центральным органам тюремного управления и судебным или иным компетентным " \
                   "органам, включая те, что уполномочены пересматривать дело или принимать меры по исправлению" \
                   " положения, с заявлениями или жалобами по поводу обращения с ним или с ней, которые не" \
                   " должны подвергаться цензуре.".format(self.place_name, self.place_name)
        # abstract_group.AskingType.AfterCourt
        return "Во время содержания в {} администрация не отправила адресату мои жалобы и, соответственно," \
                   " меня не оповещали об их отправлении. Исходя из пункта 72 Правил № 313, жалобы должны быть " \
                   "направлены начальником {} соответствующему адресату. Согласно пункту 79 Правил № 313 о " \
                   "направлении ходатайства, предложения, заявления или жалобы объявляется административно " \
                   "арестованному, подавшему их. ".format(self.place_name, self.place_name)


    def get_next_question_name(self):
        return CantQuestionsNames.KeyLast

    def get_previous_question_name(self):
        return CantQuestionsNames.KeyFour
