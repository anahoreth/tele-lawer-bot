import config
import enum

if config.IS_LOCAL:
    import question_groups.abstract_group as abstract_group
    import question_groups.questions_types as questions_types
else:
    import questions.question_groups.abstract_group as abstract_group
    import questions.question_groups.questions_types as questions_types



class WalkQuestionsNames(enum.Enum):
    KeyZero = questions_types.ZERO_QUESTION
    KeyFirstOne = 1.1  # Были ли дни, когда вас не выводили на прогулки
    KeyFirstOneOneOne = 1.111  # Сколько примерно таких дней было?
    KeyFirstTwo = 1.2  # Когда вас выводили на прогулку, она всегда длилась не менее часа?
    KeyFirstTwoTwoOne = 1.221  # Сколько раз примерно вас выводили на прогулку?
    KeyFirstTwoTwoTwo = 1.222  # Сколько раз примерно прогулка длилась менее часа
    KeySecondOne = 2.1  # Были ли дни, когда вас не выводили на прогулки
    KeySecondOneOneOne = 2.111  # Сколько примерно таких дней было?
    KeySecondTwo = 2.2  # Когда вас выводили, она всегда длилась не менее 2 часов
    KeySecondTwoTwoOne = 2.221  # Сколько раз примерно вас выводили на прогулку?
    KeySecondTwoTwoTwo = 2.222  # Сколько раз примерно прогулка длилась менее 2 часов

    KeyThirdOne = 3.1  # Были ли дни, когда вас не выводили на прогулки
    KeyThirdOneOneOne = 3.111  # Сколько примерно таких дней было?
    KeyThirdTwo = 3.2  # Когда вас выводили до суда, она всегда длилась не менее 2 часов
    KeyThirdTwoTwoOne = 3.221  # Сколько раз примерно вас выводили на прогулку до суда?
    KeyThirdTwoTwoTwo = 3.222  # Сколько раз примерно прогулка длилась менее 2 часов до суда?
    KeyThirdThree = 3.3  # 1.3.3.	Когда вас выводили на прогулку после суда, она всегда длилась не менее часа?
    KeyThirdThreeTwoOne = 3.321  #1.3.3.2.1.Сколько раз примерно после суда вас выводили на прогулку?
    KeyThirdThreeTwoTwo = 3.322  # 1.3.3.2.2.Сколько раз примерно прогулка длилась менее часа?

    KeyLast = questions_types.LAST_QUESTION
    KeyCount = 2


class WalkQuestionGroup(abstract_group.QuestionGroup):
    def __init__(self, asking_type, place_name):
        super().__init__(asking_type)
        self.place_name = place_name
        self.question_name = None
        if self.asking_type.value == abstract_group.AskingType.AfterCourt.value:
            self.question_name = WalkQuestionsNames.KeyFirstOne
        elif self.asking_type.value == abstract_group.AskingType.BeforeCourt.value:
            self.question_name = WalkQuestionsNames.KeySecondOne
        elif self.asking_type.value == abstract_group.AskingType.BothSideOfCourt.value:
            self.question_name = WalkQuestionsNames.KeyThirdOne
        assert self.question_name

    def get_short_name(self):
        return "Отсутствие прогулок или их порядок"

    def get_questions_count(self):
        return WalkQuestionsNames.KeyCount.value

    def make_new_question(self):
        if self.question_name == WalkQuestionsNames.KeyFirstOne:
            return WalkQuestionFirstOne(self.asking_type, self.place_name)
        if self.question_name == WalkQuestionsNames.KeyFirstOneOneOne:
            return WalkQuestionFirstOneOneOne(self.asking_type, self.place_name)
        if self.question_name == WalkQuestionsNames.KeyFirstTwo:
            return WalkQuestionFirstTwo(self.asking_type)
        if self.question_name == WalkQuestionsNames.KeyFirstTwoTwoOne:
            return WalkQuestionFirstTwoTwoOne(self.asking_type, self.place_name)
        if self.question_name == WalkQuestionsNames.KeyFirstTwoTwoTwo:
            return WalkQuestionFirstTwoTwoTwo(self.asking_type)
        if self.question_name == WalkQuestionsNames.KeySecondOne:
            return WalkQuestionSecondOne(self.asking_type, self.place_name)
        if self.question_name == WalkQuestionsNames.KeySecondOneOneOne:
            return WalkQuestionSecondOneOneOne(self.asking_type, self.place_name)
        if self.question_name == WalkQuestionsNames.KeySecondTwo:
            return WalkQuestionSecondTwo(self.asking_type)
        if self.question_name == WalkQuestionsNames.KeySecondTwoTwoOne:
            return WalkQuestionSecondTwoTwoOne(self.asking_type, self.place_name)
        if self.question_name == WalkQuestionsNames.KeySecondTwoTwoTwo:
            return WalkQuestionSecondTwoTwoTwo(self.asking_type)

        if self.question_name == WalkQuestionsNames.KeyThirdOne:
            return WalkQuestionThirdOne(self.asking_type, self.place_name)
        if self.question_name == WalkQuestionsNames.KeyThirdOneOneOne:
            return WalkQuestionThirdOneOneOne(self.asking_type, self.place_name)
        if self.question_name == WalkQuestionsNames.KeyThirdTwo:
            return WalkQuestionThirdTwo(self.asking_type)
        if self.question_name == WalkQuestionsNames.KeyThirdTwoTwoOne:
            return WalkQuestionThirdTwoTwoOne(self.asking_type, self.place_name)
        if self.question_name == WalkQuestionsNames.KeyThirdTwoTwoTwo:
            return WalkQuestionThirdTwoTwoTwo(self.asking_type)
        if self.question_name == WalkQuestionsNames.KeyThirdThree:
            return WalkQuestionThirdThree(self.asking_type)
        if self.question_name == WalkQuestionsNames.KeyThirdThreeTwoOne:
            return WalkQuestionThirdThreeTwoOne(self.asking_type, self.place_name)
        if self.question_name == WalkQuestionsNames.KeyThirdThreeTwoTwo:
            return WalkQuestionThirdThreeTwoTwo(self.asking_type)
        return None

    def process(self):
        complains_list = []
        for question in self.questions_array.values():
            complain = question.process()
            if complain == "":
                continue
            if complain[0] != " ":
                complains_list.append(complain)
            else:
                complains_list[len(complains_list) - 1] += complain

        answer_first_one = self.get_choice(WalkQuestionsNames.KeyFirstOne)
        answer_first_two = self.get_choice(WalkQuestionsNames.KeyFirstTwo)
        if answer_first_one == 0 or answer_first_two == 1:
            new_complain = "Согласно пункту 1 правила 23 Правил Нельсона Манделы каждый имеет ежедневно право по " \
                           "крайней мере на час подходящих физических упражнений во дворе, если это позволяет погода."
            complains_list.append(new_complain)
        return complains_list


class WalkQuestionFirstOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Были ли дни, когда вас не выводили на прогулки (кроме дней, когда возили в суд)?"
        answers = [
            "1. Да",
            "2. Нет",
            "3. Меня ни разу не вывели на прогулку",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 2:
            return "Во время содержания в {} меня ни разу не вывели на прогулку. Исходя из самого названия " \
                   "главы 13 Правил № 313, прогулки должны быть ежедневными.".format(self.place_name)
        return ""

    def get_next_question_name(self):
        if self.get_choice() == 0:
            return WalkQuestionsNames.KeyFirstOneOneOne
        elif self.get_choice() == 1:
            return WalkQuestionsNames.KeyFirstTwo
        return WalkQuestionsNames.KeyLast

    def get_previous_question_name(self):
        return WalkQuestionsNames.KeyZero


class WalkQuestionFirstOneOneOne(questions_types.GetNumberQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Сколько примерно таких дней было?"
        super().__init__(text, asking_type)

    def get_text_begin(self):
        # TODO проверка правильности введенного числа, костыль с GetNumberQuestion
        choice_int = get_int_choice(self.get_choice())
        text = "Во время содержания в {}".format(self.place_name)
        next_text = "когда меня не вывели на прогулку."
        if choice_int in [11, 12, 13, 14]:
            return "{} было примерно {} дней, {}".format(text, choice_int, next_text)
        if choice_int % 10 == 1:
            return "{} был примерно {} день, {}".format(text, choice_int, next_text)
        if choice_int % 10 in [2, 3, 4]:
            return "{} было примерно {} дня, {}".format(text, choice_int, next_text)
        return "{} было примерно {} дней, {}".format(text, choice_int, next_text)

    def process(self):
        text_begin = self.get_text_begin()
        return text_begin + " Исходя из самого названия главы 13 Правил № 313, прогулки должны быть ежедневными."

    def get_next_question_name(self):
        return WalkQuestionsNames.KeyFirstTwo

    def get_previous_question_name(self):
        return WalkQuestionsNames.KeyFirstOne


class WalkQuestionFirstTwo(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Когда вас выводили на прогулку, она всегда длилась не менее часа?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        return ""

    def get_next_question_name(self):
        if self.get_choice() == 1:
            return WalkQuestionsNames.KeyFirstTwoTwoOne
        return WalkQuestionsNames.KeyLast

    def get_previous_question_name(self):
        return WalkQuestionsNames.KeyFirstOne


class WalkQuestionFirstTwoTwoOne(questions_types.GetNumberQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Сколько раз примерно вас выводили на прогулку?"
        super().__init__(text, asking_type)

    def process(self):
        # TODO проверка правильности введенного числа, костыль с GetNumberQuestion
        choice_int = get_int_choice(self.get_choice())

        if choice_int == 1:
            return "Во время содержания в {} меня вывели на прогулку один раз. " \
                   "Эта прогулка длилась менее часа.".format(self.place_name)
        text = "Во время содержания в {} меня вывели на прогулку примерно".format(self.place_name)
        if choice_int in [11, 12, 13, 14]:
            return "{} {} раз.".format(text, choice_int)
        if choice_int % 10 in [2, 3, 4]:
            return "{} {} раза.".format(text, choice_int)
        return "{} {} раз.".format(text, choice_int)

    def get_next_question_name(self):
        if get_int_choice(self.get_choice()) == 1:
            return WalkQuestionsNames.KeyLast
        return WalkQuestionsNames.KeyFirstTwoTwoTwo

    def get_previous_question_name(self):
        return WalkQuestionsNames.KeyFirstTwo


class WalkQuestionFirstTwoTwoTwo(questions_types.GetNumberQuestion):
    def __init__(self, asking_type):
        text = "Сколько раз примерно прогулка длилась менее часа?"
        super().__init__(text, asking_type)

    def process(self):
        choice_int = get_int_choice(self.get_choice())
        return " Из всех прогулок продолжительность {} была менее одного часа.".format(choice_int)

    def get_next_question_name(self):
        return WalkQuestionsNames.KeyLast

    def get_previous_question_name(self):
        return WalkQuestionsNames.KeyFirstTwoTwoOne


class WalkQuestionSecondOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Были ли дни, когда вас не выводили на прогулки (кроме дней, когда возили в суд)?"
        answers = [
            "1. Да",
            "2. Нет",
            "3. Меня ни разу не вывели на прогулку",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 2:
            return "Во время содержания в {} меня ни разу не вывели на прогулку. " \
                   "Это нарушает пункт 101 Правил № 996.".format(self.place_name)
        return ""

    def get_next_question_name(self):
        if self.get_choice() == 0:
            return WalkQuestionsNames.KeySecondOneOneOne
        elif self.get_choice() == 1:
            return WalkQuestionsNames.KeySecondTwo
        return WalkQuestionsNames.KeyLast

    def get_previous_question_name(self):
        return WalkQuestionsNames.KeyZero


class WalkQuestionSecondOneOneOne(questions_types.GetNumberQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Сколько примерно таких дней было?"
        super().__init__(text, asking_type)

    def process(self):
        choice = self.get_choice()
        choice_int = get_int_choice(choice)
        text = "Во время содержания в {}".format(self.place_name)
        next_text = "когда меня не вывели на прогулку. Это нарушает пункт 101 Правил № 996."

        if choice_int in [11, 12, 13, 14]:
            return "{} было примерно {} дней, {}".format(text, choice, next_text)
        if choice_int % 10 == 1:
            return "{} был примерно {} день, {}".format(text, choice, next_text)
        if choice_int % 10 in [2, 3, 4]:
            return "{} было примерно {} дня, {}".format(text, choice, next_text)
        return "{} было примерно {} дней, {}".format(text, choice, next_text)

    def get_next_question_name(self):
        return WalkQuestionsNames.KeySecondTwo

    def get_previous_question_name(self):
        return WalkQuestionsNames.KeySecondOne


class WalkQuestionSecondTwo(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Когда вас выводили на прогулку, она всегда длилась не менее 2 часов (кроме дней нахождения в карцере)?"
        answers = [
            "1. Да",
            "2. Нет",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        return ""

    def get_next_question_name(self):
        if self.get_choice() == 1:
            return WalkQuestionsNames.KeySecondTwoTwoOne
        return WalkQuestionsNames.KeyLast

    def get_previous_question_name(self):
        return WalkQuestionsNames.KeySecondOne


class WalkQuestionSecondTwoTwoOne(questions_types.GetNumberQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Сколько раз примерно вас выводили на прогулку?"
        super().__init__(text, asking_type)

    def process(self):
        choice = self.get_choice()
        choice_int = get_int_choice(choice)
        if choice_int == -1:
            return ""
        if choice_int == 1:
            return "Во время содержания в {} меня вывели на прогулку один раз. Эта прогулка длилась менее двух " \
                   "часов, что нарушает пункт 101 Правил № 996. ".format(self.place_name)

        text = "Во время содержания в {} меня вывели на прогулку примерно".format(self.place_name)

        if choice_int in [11, 12, 13, 14]:
            return "{} {} раз.".format(text, choice)
        if choice_int % 10 in [2, 3, 4]:
            return "{} {} раза.".format(text, choice)
        return "{} {} раз.".format(text, choice)

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == "1":
            return WalkQuestionsNames.KeyLast
        return WalkQuestionsNames.KeySecondTwoTwoTwo

    def get_previous_question_name(self):
        return WalkQuestionsNames.KeySecondTwo


class WalkQuestionSecondTwoTwoTwo(questions_types.GetNumberQuestion):
    def __init__(self, asking_type):
        text = "Сколько раз примерно прогулка длилась менее 2 часов?"
        super().__init__(text, asking_type)

    def process(self):
        return " Из всех прогулок продолжительность {} была менее двух часов, " \
               "что нарушает пункт 101 Правил № 996.".format(self.get_choice())

    def get_next_question_name(self):
        return WalkQuestionsNames.KeyLast

    def get_previous_question_name(self):
        return WalkQuestionsNames.KeySecondTwoTwoOne


def get_int_choice(choice):
    # TODO проверка правильности введенного числа
    return int(choice)


# до и после суда


class WalkQuestionThirdOne(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Были ли дни, когда вас не выводили на прогулки (кроме дней, когда возили в суд)?"
        answers = [
            "1. Да",
            "2. Нет",
            "3. Меня ни разу не вывели на прогулку",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        choice = self.get_choice()
        if choice == 2:
            return "Во время содержания в {} меня ни разу не вывели на прогулку. " \
                   "Исходя из самого названия главы 13 Правил № 313 и пункта 101 Правил № 996, " \
                   "прогулки должны быть ежедневными.".format(self.place_name)
        return ""

    def get_next_question_name(self):
        if self.get_choice() == 0:
            return WalkQuestionsNames.KeyThirdOneOneOne
        elif self.get_choice() == 1:
            return WalkQuestionsNames.KeyThirdTwo
        return WalkQuestionsNames.KeyLast

    def get_previous_question_name(self):
        return WalkQuestionsNames.KeyZero


class WalkQuestionThirdOneOneOne(questions_types.GetNumberQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Сколько примерно таких дней было?"
        super().__init__(text, asking_type)

    def process(self):
        choice = self.get_choice()
        choice_int = get_int_choice(choice)
        text = "Во время содержания в {}".format(self.place_name)
        next_text = "когда меня не вывели на прогулку. Исходя из самого названия главы 13 Правил № 313 и пункта" \
                    " 101 Правил № 996, прогулки должны быть ежедневными."

        if choice_int in [11, 12, 13, 14]:
            return "{} было примерно {} дней, {}".format(text, choice, next_text)
        if choice_int % 10 == 1:
            return "{} был примерно {} день, {}".format(text, choice, next_text)
        if choice_int % 10 in [2, 3, 4]:
            return "{} было примерно {} дня, {}".format(text, choice, next_text)
        return "{} было примерно {} дней, {}".format(text, choice, next_text)

    def get_next_question_name(self):
        return WalkQuestionsNames.KeyThirdTwo

    def get_previous_question_name(self):
        return WalkQuestionsNames.KeyThirdOne



class WalkQuestionThirdTwo(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Когда вас выводили на прогулку *до суда*, она всегда длилась не менее 2 часов " \
               "(кроме дней нахождения в карцере)?"
        answers = [
            "1. Да",
            "2. Нет",
            "3. Меня не выводили на прогулку до суда",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        return ""

    def get_next_question_name(self):
        if self.get_choice() == 1:
            return WalkQuestionsNames.KeyThirdTwoTwoOne
        return WalkQuestionsNames.KeyThirdThree

    def get_previous_question_name(self):
        return WalkQuestionsNames.KeyThirdOne


class WalkQuestionThirdTwoTwoOne(questions_types.GetNumberQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Сколько раз примерно вас выводили на прогулку до суда?"
        super().__init__(text, asking_type)

    def process(self):
        choice = self.get_choice()
        choice_int = get_int_choice(choice)
        if choice_int == -1:
            return ""
        if choice_int == 1:
            return "Во время содержания в {} до суда меня вывели на прогулку один раз. Эта прогулка длилась менее двух " \
                   "часов, что нарушает пункт 101 Правил № 996. ".format(self.place_name)

        text = "Во время содержания в {} до суда меня вывели на прогулку примерно".format(self.place_name)

        if choice_int in [11, 12, 13, 14]:
            return "{} {} раз.".format(text, choice)
        if choice_int % 10 in [2, 3, 4]:
            return "{} {} раза.".format(text, choice)
        return "{} {} раз.".format(text, choice)

    def get_next_question_name(self):
        choice = self.get_choice()
        if choice == "1":
            return WalkQuestionsNames.KeyThirdThree
        return WalkQuestionsNames.KeyThirdTwoTwoTwo

    def get_previous_question_name(self):
        return WalkQuestionsNames.KeyThirdTwo


class WalkQuestionThirdTwoTwoTwo(questions_types.GetNumberQuestion):
    def __init__(self, asking_type):
        text = "Сколько раз примерно прогулка длилась менее 2 часов?"
        super().__init__(text, asking_type)

    def process(self):
        return " Из всех прогулок продолжительность {} была менее двух часов, " \
               "что нарушает пункт 101 Правил № 996.".format(self.get_choice())

    def get_next_question_name(self):
        return WalkQuestionsNames.KeyThirdThree

    def get_previous_question_name(self):
        return WalkQuestionsNames.KeyThirdTwoTwoOne


class WalkQuestionThirdThree(questions_types.GetChoiceQuestion):

    def __init__(self, asking_type):
        text = "Когда вас выводили на прогулку *после суда*, она всегда длилась не менее часа?"
        answers = [
            "1. Да",
            "2. Нет",
            "3. Меня не выводили на прогулку после суда",
        ]
        super().__init__(text, answers, asking_type)

    def process(self):
        return ""

    def get_next_question_name(self):
        if self.get_choice() == 1:
            return WalkQuestionsNames.KeyThirdThreeTwoOne
        return WalkQuestionsNames.KeyLast

    def get_previous_question_name(self):
        return WalkQuestionsNames.KeyThirdTwo


class WalkQuestionThirdThreeTwoOne(questions_types.GetNumberQuestion):

    def __init__(self, asking_type, place_name):
        self.place_name = place_name
        text = "Сколько раз примерно после суда вас выводили на прогулку?"
        super().__init__(text, asking_type)

    def process(self):
        # TODO проверка правильности введенного числа, костыль с GetNumberQuestion
        choice_int = get_int_choice(self.get_choice())

        if choice_int == 1:
            return "Во время содержания в {} после суда меня вывели на прогулку один раз. " \
                   "Эта прогулка длилась менее часа.".format(self.place_name)
        text = "Во время содержания в {} после суда меня вывели на прогулку примерно".format(self.place_name)
        if choice_int in [11, 12, 13, 14]:
            return "{} {} раз.".format(text, choice_int)
        if choice_int % 10 in [2, 3, 4]:
            return "{} {} раза.".format(text, choice_int)
        return "{} {} раз.".format(text, choice_int)

    def get_next_question_name(self):
        if get_int_choice(self.get_choice()) == 1:
            return WalkQuestionsNames.KeyLast
        return WalkQuestionsNames.KeyThirdThreeTwoTwo

    def get_previous_question_name(self):
        return WalkQuestionsNames.KeyThirdThree


class WalkQuestionThirdThreeTwoTwo(questions_types.GetNumberQuestion):
    def __init__(self, asking_type):
        text = "Сколько раз примерно прогулка длилась менее часа?"
        super().__init__(text, asking_type)

    def process(self):
        choice_int = get_int_choice(self.get_choice())
        return " Из всех прогулок продолжительность {} была менее одного часа.".format(choice_int)

    def get_next_question_name(self):
        return WalkQuestionsNames.KeyLast

    def get_previous_question_name(self):
        return WalkQuestionsNames.KeyThirdThreeTwoOne

