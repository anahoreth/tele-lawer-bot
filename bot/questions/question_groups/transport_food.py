import config
import enum


if config.IS_LOCAL:
    import choose_places_to_complain
    import question_groups.abstract_group as abstract_group
    import question_groups.questions_types as questions_types
else:
    import states.choose_places_to_complain as choose_places_to_complain
    import questions.question_groups.abstract_group as abstract_group
    import questions.question_groups.questions_types as questions_types


class TransportFoodQuestionsNames(enum.Enum):
    KeyZero = questions_types.ZERO_QUESTION
    KeyFirst = 1  # Кормили ли вас завтраком?
    KeySecond = 2  # Кормили ли вас обедом?
    KeyThird = 3  # Кормили ли вас ужином?
    KeyFour = 4  # Позволили ли вам есть свою еду?
    KeyLast = questions_types.LAST_QUESTION
    KeyCount = 4


class TransportFoodQuestionGroup(abstract_group.QuestionGroup):
    def __init__(self, asking_type, transfer_current_name, transfer_next_name, transfer_date):
        super().__init__(asking_type)
        self.transfer_date = transfer_date
        self.transfer_current_name = transfer_current_name
        self.transfer_next_name = transfer_next_name
        self.question_name = TransportFoodQuestionsNames.KeyFirst

    def get_short_name(self):
        return "Питание в день поездки"

    def get_questions_count(self):
        return TransportFoodQuestionsNames.KeyCount.value

    def make_new_question(self):
        if self.question_name == TransportFoodQuestionsNames.KeyFirst:
            return TransportFoodQuestionFirst(self.asking_type)
        if self.question_name == TransportFoodQuestionsNames.KeySecond:
            return TransportFoodQuestionSecond(self.asking_type)
        if self.question_name == TransportFoodQuestionsNames.KeyThird:
            return TransportFoodQuestionThree(self.asking_type)
        if self.question_name == TransportFoodQuestionsNames.KeyFour:
            return TransportFoodQuestionFour(self.asking_type)
        return None

    def process(self):

        before_complain = "{:02d}.{:02d}.{:04d}, когда меня возили из {} в {}, со " \
                           "мной обращались бесчеловечным образом.".format(self.transfer_date[2],
                self.transfer_date[1], self.transfer_date[0], self.transfer_current_name, self.transfer_next_name)

        before_complain = choose_places_to_complain.fix_court_message(before_complain)

        complains_list = [before_complain]
        # First
        choice_first = self.get_choice(TransportFoodQuestionsNames.KeyFirst)
        choice_second = self.get_choice(TransportFoodQuestionsNames.KeySecond)
        choice_third = self.get_choice(TransportFoodQuestionsNames.KeyThird)

        meal_text = ""
        if choice_first == 1 and choice_second == 1 and choice_third == 1:
            meal_text = "ни разу не покормили: ни завтраком, ни обедом, ни ужином"
        elif choice_first == 1 and choice_second == 1 and choice_third == 2:
            meal_text = "не кормили ни завтраком, ни обедом"
        else:
            meal_list = []
            if choice_first == 1:
                meal_list.append("завтраком")
            if choice_second == 1:
                meal_list.append("обедом")
            if choice_third == 1:
                meal_list.append("ужином")

            if len(meal_list) == 1:
                meal_text = "не покормили {}".format(meal_list[0])
            elif len(meal_list) == 2:
                meal_text = "не покормили {} и {}".format(meal_list[0], meal_list[1])
        if meal_text:
            text_meal = "В этот день меня {}. На основании пункта 1 правила 22 Правил " \
                   "Нельсона Манделы мне должна была быть обеспечена пища, достаточно питательная для " \
                   "поддержания здоровья и сил, имеющая достаточно хорошее качество, хорошо приготовленная " \
                   "и поданная. Тем не менее, этого сделано не было.".format(meal_text)
            complains_list.append(text_meal)


        #своя еда
        choice_four = self.get_choice(TransportFoodQuestionsNames.KeyFour)
        if choice_four == 3:
            text_my_food = "В этот день мне не разрешили есть свою еду. Сотрудники, осуществлявшие конвоирование, не " \
                   "реагировали на мои жалобы на голод и отклонили мои просьбы выдать мне мою еду. Считаю подобные " \
                   "действия незаконным запретом распоряжаться собственным имуществом, а также осознанным жестоким " \
                   "обращением."
            complains_list.append(text_my_food)

        # если суд
        is_court = (self.transfer_next_name.find('суд') != -1 or self.transfer_next_name.find('Суд') != -1)
        if meal_text != "" and is_court:
            text = "Таким образом, в день рассмотрения моего дела в суде из-за отсутствия нормального питания, " \
                   "меня доставили в суд в состоянии голода и стресса. В день рассмотрения административного дела " \
                   "в суде такое обращение является особенно непозволительным, поскольку препятствует возможности " \
                   "полноценно защищать себя в суде, ведь это требует спокойствия, рассудительности, возможности " \
                   "вспомнить все факты и надлежаще изложить их суду."
            complains_list.append(text)

        return complains_list


class TransportFoodQuestionFirst(questions_types.GetChoiceQuestion):
    def __init__(self, asking_type):
        text = "Кормили ли вас завтраком?"
        answers = ['1. Да', "2. Нет"]
        super().__init__(text, answers, asking_type)

    def process(self):
        return ""

    def get_next_question_name(self):
        return TransportFoodQuestionsNames.KeySecond
    def get_previous_question_name(self):
        return TransportFoodQuestionsNames.KeyZero


class TransportFoodQuestionSecond(questions_types.GetChoiceQuestion):
    def __init__(self, asking_type):
        text = "Кормили ли вас обедом?"
        answers = ['1. Да', "2. Нет", "3. Меня отпустили до обеда"]
        super().__init__(text, answers, asking_type)

    def process(self):
        return ""

    def get_next_question_name(self):
        return TransportFoodQuestionsNames.KeyThird

    def get_previous_question_name(self):
        return TransportFoodQuestionsNames.KeyFirst


class TransportFoodQuestionThree(questions_types.GetChoiceQuestion):
    def __init__(self, asking_type):
        text = "Кормили ли вас ужином?"
        answers = ['1. Да', "2. Нет", "3. Меня отпустили до ужина"]
        super().__init__(text, answers, asking_type)

    def process(self):
        return ""

    def get_next_question_name(self):
        return TransportFoodQuestionsNames.KeyFour

    def get_previous_question_name(self):
        return TransportFoodQuestionsNames.KeySecond


class TransportFoodQuestionFour(questions_types.GetChoiceQuestion):
    def __init__(self, asking_type):
        text = "Позволили ли вам есть свою еду?"
        answers = ['1. У меня не было своей еды',
                   "2. Я не просил(а) выдать мою еду",
                   "3. Да",
                   "4. Нет",
                   ]
        super().__init__(text, answers, asking_type)

    def process(self):
        return ""

    def get_next_question_name(self):
        return TransportFoodQuestionsNames.KeyLast

    def get_previous_question_name(self):
        return TransportFoodQuestionsNames.KeyThird
