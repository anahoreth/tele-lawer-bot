
import enum
import keyboard
import config


ZERO_QUESTION = 'zero_question'
LAST_QUESTION = 'last_question'
DEFAULT_CHOICE = -1


FRACTION_MESSAGE = ""


class QuestionAnswerType(enum.Enum):
    ANSWER_DEFAULT = 0
    ANSWER_MESSAGE = 1
    ANSWER_QUERY_ONLY = 2
    ANSWER_QUERY_MANY = 3


class Question(object):
    def __init__(self, question_text, asking_type):
        self.question_text = question_text
        self.choice = DEFAULT_CHOICE
        self.asking_type = asking_type

    def get_type(self):
        return QuestionAnswerType.ANSWER_DEFAULT

    def get_message(self):
        return self.question_text

    def process(self):
        return ""

    def set_choice(self, answer_number):
        self.choice = answer_number

    def get_choice(self):
        return self.choice

    def get_keyboard(self):
        return None


class GetChoiceQuestion(Question):
    def __init__(self, question_text, answers, asking_type, question_type=QuestionAnswerType.ANSWER_QUERY_ONLY):

        self.question_type = question_type
        super().__init__(question_text, asking_type)
        if self.question_type == QuestionAnswerType.ANSWER_QUERY_MANY:
            self.choice = set()
        self.answers = answers

    def get_type(self):
        return self.question_type

    def get_message(self):
        message = self.question_text
        answers = ""
        for i in range(self.get_answers_count()):
            if self.question_type == QuestionAnswerType.ANSWER_QUERY_ONLY and i == self.get_choice() or \
                    self.question_type == QuestionAnswerType.ANSWER_QUERY_MANY and i in self.get_choice():
                answers += "*"
            answers += self.answers[i]
            if self.question_type == QuestionAnswerType.ANSWER_QUERY_ONLY and i == self.get_choice() or \
                    self.question_type == QuestionAnswerType.ANSWER_QUERY_MANY and i in self.get_choice():
                answers += "*"
            answers += "\n"
        message += "\n\n" + answers
        # message += "\n\n(Выбранный вариант будет отмечен *жирным шрифтом*)"
        return message

    def get_keyboard(self):
        is_ok_needed = self.question_type == QuestionAnswerType.ANSWER_QUERY_MANY
        return keyboard.get_question_keyboard(self.get_answers_count(), is_ok_needed=is_ok_needed)

    def get_answers_count(self):
        return len(self.answers)


class GetNumberQuestion(Question):
    def __init__(self, question_text, asking_type):
        super().__init__(question_text, asking_type)

    def get_message(self):
        message = self.question_text
        if self.choice != DEFAULT_CHOICE:
            message += "\n\nРанее Вы ввели значение: " + str(self.choice) + "\n"
        return message

    def get_type(self):
        return QuestionAnswerType.ANSWER_MESSAGE

    # TODO убрать этот костыль
    def get_choice(self):
        return self.choice


class JustInfoQuestion(Question):
    def __init__(self, question_text, asking_type):
        super().__init__(question_text, asking_type)

    def get_keyboard(self):
        is_ok_needed = True
        return keyboard.get_question_keyboard(0, is_ok_needed)


