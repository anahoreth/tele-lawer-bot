import config
import enum
import names

if config.IS_REAL_BOT:
    from telebot import types
    from telebot.types import InlineKeyboardButton, InlineKeyboardMarkup


# декоратор, который проверяет IS_REAL_BOT: если True, то возвращать None
def none_if_not_real_bot_decorator(func):
    def wrapper(*args, **kwargs):
        if not config.IS_REAL_BOT:
            return None
        res = func(*args, **kwargs)
        return res
    return wrapper


@none_if_not_real_bot_decorator
def get_reply_keyboard_from_list(button_text_list):
    keyboard = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True, one_time_keyboard=True)
    for button_text in button_text_list:
        button = types.KeyboardButton(text=button_text)
        keyboard.add(button)
    return keyboard


@none_if_not_real_bot_decorator
def get_inline_keyboard_from_list(buttons_texts, callback_data):
    keyboard = InlineKeyboardMarkup()
    for buttons_texts, callback_datum in zip(buttons_texts, callback_data):
        # TODO тут по одной кнопке добавляется, может можно не через row это делать
        row = []
        button = InlineKeyboardButton(buttons_texts, callback_data=callback_datum)
        row.append(button)
        keyboard.row(*row)
    return keyboard


@none_if_not_real_bot_decorator
def get_answers_keyboard(answers_list):
    keyboard = InlineKeyboardMarkup()
    for answers_row in answers_list:

        if isinstance(answers_row, str):
            keyboard.row(InlineKeyboardButton(answers_row, callback_data=answers_row))
            continue
        row = []
        for answer in answers_row:
            button = InlineKeyboardButton(answer, callback_data=answer)
            row.append(button)
        keyboard.row(*row)
    return keyboard


def get_city_keyboard(is_court=False):
    collection = names.CourtCities if is_court else names.Cities
    cities_list = [city.value for city in collection]
    return get_reply_keyboard_from_list(cities_list)


def get_place_keyboard(city, is_court=False):
    places_list = names.COURT_DICT.get(city) if is_court else names.PLACE_DICT.get(city)
    return get_reply_keyboard_from_list(places_list)


class YesNoButtons(enum.Enum):
    Yes = 'Да'
    No = 'Нет'

YES_STRING = YesNoButtons.Yes.value

YesNoButtonsList = [[YesNoButtons.Yes.value, YesNoButtons.No.value]]


# TODO на такой клавиатуре если не "Да", то автоматически "нет". МОжно добавить другие проверки
def is_yes(string):
    return string == YesNoButtons.Yes.value


def get_yes_no_keyboard():
    return get_answers_keyboard(YesNoButtonsList)


# TODO тут надо добавить, куда
class AfterCourtButtons(enum.Enum):
    TheSame = 'Вернули в '
    Another = 'В другое место'


def get_after_court_keyboard(previous_place_name):
    # TODO здесь обрезаем, чтобы влезло в кнопку (33 символа)
    previous_place_button_name = AfterCourtButtons.TheSame.value + previous_place_name
    previous_place_button_name = previous_place_button_name[:33]
    after_court_buttons_list = [previous_place_button_name,
                                AfterCourtButtons.Another.value]
    return get_reply_keyboard_from_list(after_court_buttons_list)


@none_if_not_real_bot_decorator
def get_keyboard_digits(counter):
    keyboard = InlineKeyboardMarkup()
    row = []
    for number in range(1, counter + 1):
        str_num = str(number)
        button = InlineKeyboardButton(str_num, callback_data=str_num)
        row.append(button)
        if len(row) == 8:
            keyboard.row(*row)
            row = []
    keyboard.row(*row)
    row = []
    button = InlineKeyboardButton("Готово", callback_data='0')
    row.append(button)
    keyboard.row(*row)

    return keyboard


@none_if_not_real_bot_decorator
def get_categories_keyboard(categories_count):
    keyboard = InlineKeyboardMarkup()

    row = []
    for number in range(categories_count):
        str_num = str(number + 1)
        button = InlineKeyboardButton(str_num, callback_data=str_num)
        row.append(button)
        if (number + 1) % 3 == 0:
            keyboard.row(*row)
            row = []

    row = []
    button = InlineKeyboardButton("Готово", callback_data='0')
    row.append(button)
    keyboard.row(*row)

    return keyboard


@none_if_not_real_bot_decorator
def get_question_keyboard(counter, is_ok_needed=False):
    keyboard = InlineKeyboardMarkup()

    if counter == 10:
        for i in range(2):
            row = []
            for number in range(1, 6):
                str_num = str(i * 5 + number)
                button = InlineKeyboardButton(str_num, callback_data=str_num)
                row.append(button)
            keyboard.row(*row)
    else:
        row = []
        for number in range(1, counter + 1):
            str_num = str(number)
            button = InlineKeyboardButton(str_num, callback_data=str_num)
            row.append(button)
            if len(row) == 8:
                keyboard.row(*row)
                row = []
        keyboard.row(*row)

    row = []
    button = InlineKeyboardButton("<", callback_data='PrevQ')
    row.append(button)

    if is_ok_needed:
        button2 = InlineKeyboardButton("OK", callback_data='NextQ')
    else:
        button2 = InlineKeyboardButton(">", callback_data='NextQ')
    row.append(button2)
    keyboard.row(*row)

    row = []
    button = InlineKeyboardButton("След. раздел", callback_data='NextTopic')
    row.append(button)
    keyboard.row(*row)

    row = []
    button = InlineKeyboardButton("Завершить", callback_data='Ready')
    row.append(button)
    keyboard.row(*row)
    return keyboard
