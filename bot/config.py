from enum import Enum

# признак того, что бот запускается локально
IS_LOCAL = False
IS_DEMO = False
IS_REAL_BOT = True


if IS_LOCAL:
    # для тестирования
    TOKEN = "1153823747:AAEVJ0HhKhREAXv-fy5vEtpaKthG0c9wDQk"
    ADMIN_ID = "111"
    # @make_yourself_grievance_test_bot
    # TOKEN = "732492566:AAE4WwzWRksKR9vXlfnWWaecjkl3jHBiRZs"
else:
    from run_server import TOKEN_MASTER, ADMIN_ID_MASTER
    ADMIN_ID = ADMIN_ID_MASTER
    TOKEN = TOKEN_MASTER


class PlaceKeys(Enum):
    KeyPlaceCity = 'place_city'
    KeyPlaceName = 'place_name'
    KeyPlaceStartDate = "place_start_date"
    KeyPlaceIsCourt = 'place_is_court'
    KeyPlaceIsLast = 'place_is_last'
    KeyPlaceIsFirst = 'place_is_first'
    KeyPlaceIsAfterCourt = 'place_is_after_court'
    KeyPlaceIsReleasedInCourt = 'place_is_released_in_court'
    KeyPlaceCourtCity = 'court_city'
    KeyPlaceCourtPlace = 'court_name'
    KeyPlaceCourtDate = 'court_date'
    KeyPlaceArrestEndDate = 'arrest_end_date'
    KeyPlaceIsVideoCourt = 'is_video_court'


class PersonalInfoKeys(Enum):
    KeyState = "state"
    KeyAddress = "address"
    KeyEmail = "email"
    KeyPersonName = "person_name"
    KeyReleaseDate = 'release_date'
    KeyPrisonDaysCount = 'prison_days'
    KeyIsArrestCourtDecision = "is_court_decision"
    KeyPlacesList = 'places_list'
    KeyCourtDate = "court_date"
    KeyDate = "date"

    KeyComplainSelectedPlaceNumbers = 'complain_s_places'
    KeyComplainSelectedPlaceNames = 'complain_s_places_names'
    KeyComplainSelectedPlaceCurrentNumber = 'complain_s_places_current_number'

    KeyComplainQuestions = "questions"
    KeyQuestionsSelectedCategories = "question_selected_categories"
    KeyComplainCurrentQuestionNumber = 'qu_number'

    # TODO DELETE?
    KeyPlacesOK = "places_ok"


class ComplainPlaceType(Enum):
    SPlacePrison = 0
    SPlaceTransport = 1
    SPlaceTransportFood = 2
    SPlaceTransportFoodCourt = 3

















