import config
import dbworker

import states.initial as initial
import states.admin_password as admin_password
import states.choose_questions_category as choose_questions_category
import states.ask_question as ask_question
import states.choose_places_to_complain as choose_places_to_complain


if config.IS_REAL_BOT:
    import admin_table


class BotInfo(object):

    def get_bot(self):
        return self.bot

    @property
    def personal_info(self):
        return self._personal_info

    def __init__(self, my_bot):
        self.bot = my_bot
        self._personal_info = dbworker.PersonalInfo()
        if config.IS_REAL_BOT:
            self.admin_table = admin_table.AdminTable()

    def get_state(self, chat_id):
        return self.personal_info.get_current_state(chat_id)

    def set_state(self, chat_id, state):
        self.personal_info.set_state(chat_id, state)

    def bot_send_message(self, chat_id, message, keyboard, parse_mode="Markdown"):
        if not message:
            print("empty message in bot_send_message")
            return None

        if keyboard:
            return self.bot.send_message(chat_id, text=message, reply_markup=keyboard, parse_mode=parse_mode)
        return self.bot.send_message(chat_id, text=message, parse_mode=parse_mode)

    def bot_edit_message(self, chat_id, message, message_id, keyboard, parse_mode="Markdown"):
        self.bot.edit_message_text(chat_id=chat_id, text=message, message_id=message_id,
                                   reply_markup=keyboard, parse_mode=parse_mode)

    def handle_command(self, message):
        chat_id = message.chat.id
        command_name = message.text
        state = self.get_state(chat_id)
        if command_name == "/start":
            self.personal_info.init_chat(chat_id)
            next_state = initial.GetEmailState(self, chat_id) if config.IS_DEMO else initial.GetNameState(self, chat_id)
            self.set_state(chat_id, next_state)
        elif command_name == "/admin":
            next_state = admin_password.GetAdminPasswordState(self, chat_id)
            self.set_state(chat_id, next_state)
        elif command_name == "/ta" or command_name == "/tb":
            is_after_court = command_name == "/ta"
            self._personal_info = get_test_personal_info_place(chat_id, is_after_court)
            next_state = choose_questions_category.StateGetCategories(self, chat_id)
            self.set_state(chat_id, next_state)
        elif command_name == "/tt":
            self._personal_info = get_test_personal_info_transport(chat_id)
            next_state = ask_question.StateComplainQuestions(self, chat_id)
            self.set_state(chat_id, next_state)
        else:
            state.handle_command(message)
            next_state = state.get_next_state()
            self.set_state(chat_id, next_state)

    def handle_message(self, message):
        chat_id = message.chat.id
        state = self.get_state(chat_id)
        if not state:
            print("State is None in handle_message")
            return
        state.handle_message(message)
        next_state = state.get_next_state()
        if next_state:
            self.set_state(chat_id, next_state)

    def handle_query(self, call):
        chat_id = call.message.chat.id
        state = self.get_state(chat_id)
        if not state:
            print("State is None in handle_query")
            return
        state.handle_query(call)
        next_state = state.get_next_state()
        if next_state:
            self.set_state(chat_id, next_state)


def get_test_personal_info_place(chat_id, is_after_court):
    personal_info = dbworker.PersonalInfo()
    personal_info.init_chat(chat_id)

    place_name = "ИВС Бешенковичского РОВД"
    personal_info.add_place(chat_id)
    personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceName, place_name)
    personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceCity, 'Витебская область')
    personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceStartDate, (2020, 2, 3))
    personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsCourt, False)
    personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsLast, True)
    personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsFirst, True)
    personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsAfterCourt, True)
    personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceCourtPlace, "Суд Бешенковичского района")
    personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceCourtDate, (2020, 2, 3))
    personal_info.set_parameter(chat_id, config.PersonalInfoKeys.KeyReleaseDate, (2020, 5, 6))

    personal_info.set_parameter(chat_id, config.PersonalInfoKeys.KeyQuestionsSelectedCategories, [])
    personal_info.set_parameter(chat_id, config.PersonalInfoKeys.KeyComplainSelectedPlaceCurrentNumber, 0)
    places = [choose_places_to_complain.ComplainPlaceInfo(place_type=config.ComplainPlaceType.SPlacePrison,
                                                          current_name=place_name, is_after_court=is_after_court)]

    personal_info.set_parameter(chat_id, config.PersonalInfoKeys.KeyComplainSelectedPlaceNames, places)
    return personal_info


def get_test_personal_info_transport(chat_id):
    personal_info = dbworker.PersonalInfo()
    personal_info.init_chat(chat_id)

    place1 = "ИВС Бешенковичского РОВД"
    place2 = "ЦИП Новополоцкого ГОВД"
    court1 = "Суд Бешенковичского района"
    test_city1 = 'Витебская область'
    test_date0 = (2020, 2, 3)
    test_date2 = (2020, 4, 5)
    test_date3 = (2020, 5, 6)

    personal_info.add_place(chat_id)
    personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceName, place1)
    personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceCity, test_city1)
    personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceStartDate, test_date0)
    personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsCourt, False)
    personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsLast, False)
    personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsFirst, True)
    personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsAfterCourt, True)
    personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceCourtPlace, court1)
    personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceCourtDate, test_date0)

    personal_info.add_place(chat_id)
    personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceName, place2)
    personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceCity, test_city1)
    personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceStartDate, test_date2)
    personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsCourt, False)
    personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsLast, True)
    personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsFirst, False)
    personal_info.set_place_parameter(chat_id, config.PlaceKeys.KeyPlaceIsAfterCourt, True)

    personal_info.set_parameter(chat_id, config.PersonalInfoKeys.KeyReleaseDate, test_date3)
    personal_info.set_parameter(chat_id, config.PersonalInfoKeys.KeyQuestionsSelectedCategories, [])
    personal_info.set_parameter(chat_id, config.PersonalInfoKeys.KeyComplainSelectedPlaceCurrentNumber, 0)

    places = [choose_places_to_complain.ComplainPlaceInfo(place_type=config.ComplainPlaceType.SPlaceTransport,
                                                          current_name=place1, next_name=place2,
                                                          transfer_date=test_date2),
              choose_places_to_complain.ComplainPlaceInfo(place_type=config.ComplainPlaceType.SPlaceTransportFood,
                                                          current_name=place1, next_name=place2,
                                                          transfer_date=test_date2),
              ]

    personal_info.set_parameter(chat_id, config.PersonalInfoKeys.KeyComplainSelectedPlaceNames, places)
    return personal_info
